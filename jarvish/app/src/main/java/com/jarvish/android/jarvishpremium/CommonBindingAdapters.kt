package com.jarvish.android.jarvishpremium

import android.content.Context
import android.util.Log
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("hideKeyboardOnInputDone")
fun hideKeyboardOnInputDone(view: EditText, enabled: Boolean) {
    Log.d("[Keyboard]", "hideKeyboardOnInputDone")
    if (!enabled) return
    val listener = TextView.OnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            Log.d("[Keyboard]", "IME_ACTION_DONE")
            view.clearFocus()
            val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE)
                    as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        Log.d("[Keyboard]", "XXXXX")
        false
    }
    view.setOnEditorActionListener(listener)
}

@BindingAdapter("loseFocusWhen")
fun loseFocusWhen(view: EditText, condition: Boolean) {
    if (!view.hasFocus()) {
        Log.d("[Keyboard]", "123")
        view.clearFocus()
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE)
                as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    } else {
        Log.d("[Keyboard]", "321")
    }
}