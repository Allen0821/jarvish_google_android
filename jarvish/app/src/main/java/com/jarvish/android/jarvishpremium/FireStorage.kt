package com.jarvish.android.jarvishpremium

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.jarvish.android.jarvishpremium.firestore.Profile

import java.io.File

private const val TAG = "FireStorage"

class FireStorage {
    var storage = Firebase.storage

    fun uploadUserAvatar(uid: String, uri: Uri, completed: (downloadUrl: Uri?) -> Unit) {
        val storageRef = storage.reference
        val path = "user/$uid/avatar.jpg"
        val ref = storageRef.child(path)
        Log.d(TAG, "uri: $uri")
        val uploadTask = ref.putFile(uri)

        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            ref.downloadUrl
        }.addOnCompleteListener {
            Log.d(TAG, "continueWith: ${it.result}")
            completed(it.result)
        }
    }


    companion object {

    }
}