package com.jarvish.android.jarvishpremium;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;

public class JLocation {
    private static final String TAG = "JLocation";

    public static void getLastLocation(Context context, Call call) {
        try {
            FusedLocationProviderClient fusedLocationClient =
                    LocationServices.getFusedLocationProviderClient(context);
            fusedLocationClient.getLastLocation()
                    .addOnCompleteListener((Task<Location> task) -> {
                        if (task.isSuccessful() && task.getResult() != null) {
                            call.onCompleted(task.getResult());
                        } else {
                            Log.e(TAG, "Failed to get location.");
                            call.onError();
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission: " + unlikely);
            call.onError();
        }
    }


    public interface Call {
        void onCompleted(Location location);
        void onError();
    }
}
