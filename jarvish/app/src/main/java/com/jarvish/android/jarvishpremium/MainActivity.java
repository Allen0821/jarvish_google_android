package com.jarvish.android.jarvishpremium;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;


import com.google.android.libraries.places.api.Places;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.jarvish.android.jarvishpremium.ble.HelmetBleManager;
import com.jarvish.android.jarvishpremium.databinding.ActivityMainBinding;
import com.jarvish.android.jarvishpremium.gmap.GoogleMapFragment;
import com.jarvish.android.jarvishpremium.helmet.HelmetFragment;
import com.jarvish.android.jarvishpremium.icatch.ICatch;
import com.jarvish.android.jarvishpremium.login.model.AuthErrorCode;
import com.jarvish.android.jarvishpremium.login.model.AuthMethod;
import com.jarvish.android.jarvishpremium.personal.PersonalFragment;
import com.jarvish.android.jarvishpremium.track.TracksFragment;
import com.jarvish.android.jarvishpremium.weather.core.Weather;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;


public class MainActivity extends AppCompatActivity implements
        /*PersonalFragment.OnPersonalFragmentListener,*/
        GoogleMapFragment.OnGoogleMapFragmentListener {
    private static final String TAG = "MainActivity";

    public static final int RC_SPEECH_TO_TEXT = 601;
    //private final static int REQUEST_GALLERY_CODE = 1303;

    private Fragment mCurrentFragment;
    private TracksFragment mTracksFragment;
    private PersonalFragment mPersonalFragment;
    private GoogleMapFragment mGoogleMapFragment;

    private HelmetFragment mHelmetFragment;


    private HelmetBleManager mHelmetBleManager;

    private int selectedTab = 0;

    private ActivityMainBinding mBinding;

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            selectedTab = item.getItemId();
            Log.d(TAG, "onNavigationItemSelected tab: " + selectedTab);
            switch (item.getItemId()) {
                case R.id.navigation_track:
                    Log.d(TAG, "selected track");
                    if (mTracksFragment == null) {
                        mTracksFragment = TracksFragment.newInstance();
                    }
                    switchFragment(mTracksFragment);
                    return true;
                case R.id.navigation_navigation:
                    Log.d(TAG, "selected navigation");
                    /*if (mNavigationFragment == null) {
                        mNavigationFragment = new NavigationFragment();
                    }
                    switchFragment(mNavigationFragment);*/
                    if (mGoogleMapFragment == null) {
                        mGoogleMapFragment = new GoogleMapFragment();
                    }
                    switchFragment(mGoogleMapFragment);
                    return true;
                case R.id.navigation_helmet:
                    Log.d(TAG, "selected helmet");
                    if (mHelmetFragment == null) {
                        mHelmetFragment = HelmetFragment.newInstance("", "");
                    }
                    switchFragment(mHelmetFragment);
                    return true;
                case R.id.navigation_personal:
                    Log.d(TAG, "selected personal");
                    if (mPersonalFragment == null) {
                        mPersonalFragment = new PersonalFragment();
                    }
                    switchFragment(mPersonalFragment);
                    return true;
            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        MainContext mainContext = MainContext.INSTANCE;
        mainContext.initialize(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        //resetBottomIconSize();
        mBinding.navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mBinding.navigation.setSelectedItemId(R.id.navigation_navigation);
        //initHelmetReceiver();
        mHelmetBleManager = HelmetBleManager.getInstance(this);
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        Weather.getInstance().initialize(this, getString(R.string.weather_api_key));
    }



    /*private void resetBottomIconSize() {
        for (int i = 0; i < navigation.getChildCount(); i++) {
            final View iconView = navigation.getChildAt(i);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            // set your height here
            layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
            // set your width here
            layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
            iconView.setLayoutParams(layoutParams);
        }
    }*/


    /*private void initHelmetReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, intentFilter);
    }*/


    @Override
    protected void onResume() {
        super.onResume();
        //setHelmetIcon(mHelmet);
        Log.d(TAG, "onResume: " + selectedTab);
        
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: " + selectedTab);
    }

    /*private HelmetBleManagerListener.HelmetBleStatusListener mHelmetBleStatusListener =
            new HelmetBleManagerListener.HelmetBleStatusListener() {
                @Override
                public void helmetConnected() {
                    Log.d(TAG, "helmetConnected");
                    //mHelmetFragment.setView(true, "");
                }


                @Override
                public void helmetDisconnected() {
                    Log.d(TAG, "helmetDisconnected");
                    setHelmetIcon(false);
                    //mHelmetFragment.setView(false, "");
                }


                @Override
                public void helmetConnectFailed() {
                    Log.d(TAG, "helmetConnectFailed");
                    setHelmetIcon(false);
                    //mHelmetFragment.setView(false, "");
                }

                @Override
                public void helmetIdentified(String number) {
                    Log.d(TAG, "helmetIdentified");
                    setHelmetIcon(true);
                    //mHelmetFragment.fetchHelmetSettings();
                    //mHelmetBleManager.startBatteryWatch();
                }
            };*/


    /*private void setHelmetIcon(boolean helmet) {
        if (helmet) {
            ivHelmet.setImageDrawable(getResources().getDrawable(R.drawable.ic_helmet_black_32dp, null));
            tvBattery.setVisibility(View.VISIBLE);
        } else {
            ivHelmet.setImageDrawable(getResources().getDrawable(R.drawable.ic_no_helmet_black_32dp, null));
            tvBattery.setText(String.valueOf(0));
            tvBattery.setVisibility(View.GONE);
        }
    }*/


    /*@OnClick(R.id.ivHelmet)
    protected void onClickHelmet() {
        if (!mHelmet) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, ScanBleActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, HelmetSettingActivity.class);
            startActivity(intent);
        }
    }*/

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        // Unbind from the service
        //unregisterReceiver(mReceiver);

        mHelmetBleManager.destroyHelmetBleManager();
        ICatch.getInstance().destroyICatch();
    }

    public void switchFragment(Fragment fragment) {
        if (mCurrentFragment != null) {
            if (!fragment.isAdded()) {
                Log.d(TAG, "add fragment");
                getSupportFragmentManager().beginTransaction()
                        .hide(mCurrentFragment)
                        .add(R.id.flContent, fragment)
                        .commit();
            } else {
                Log.d(TAG, "show fragment");
                getSupportFragmentManager().beginTransaction()
                        .hide(mCurrentFragment)
                        .show(fragment)
                        .commit();
            }
        } else {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.flContent,fragment).commit();
        }

        if (fragment != mHelmetFragment && mCurrentFragment == mHelmetFragment) {
            Intent intent = new Intent();
            intent.setAction("SWITCH0");
            sendBroadcast(intent);
        }

        if (fragment == mHelmetFragment && mCurrentFragment != mHelmetFragment) {
            Intent intent = new Intent();
            intent.setAction("SWITCH1");
            sendBroadcast(intent);
        }

        mCurrentFragment = fragment;

    }


    /*@Override
    public void onOpenTrackDetail(TrackLog trackLog) {
        Intent intent = new Intent();
        intent.putExtra(getString(R.string.key_track_log_index), 0);
        String tracklog = new Gson().toJson(trackLog);
        intent.putExtra(getString(R.string.key_track_log), tracklog);
        intent.setClass(MainActivity.this, TrackDetailActivity.class);
        startActivity(intent);
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@@@@: " + requestCode);
        switch (requestCode) {
            case RC_SPEECH_TO_TEXT:
                /*if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Log.d(TAG, text.get(0));
                    mNavigationFragment.searchPlaceMic(text.get(0));
                }*/
                break;
            default:
                break;
        }
    }

    /*@Override
    public void onSignOut() {

    }*/

    /*private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null) {
                BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String action = intent.getAction();
                switch (action) {
                    case BluetoothDevice.ACTION_ACL_CONNECTED:
                        Log.d(TAG, "ACTION_ACL_CONNECTED");
                        //setBluetoothMic(true);
                        break;
                    case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                        Log.d(TAG, "ACTION_ACL_DISCONNECTED");
                        //setBluetoothMic(false);
                        break;
                    default:
                }
            }
        }
    };*/


    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onMap() {

    }
}
