package com.jarvish.android.jarvishpremium

import android.content.Context

enum class MainContext {
    INSTANCE;

    lateinit var mainActivity: MainActivity

    val context: Context
        get() = mainActivity.applicationContext

    fun initialize(activity: MainActivity) {
        mainActivity = activity
    }
}