/**
 * Created by CupLidSheep on 22,October,2020
 */
package com.jarvish.android.jarvishpremium;

import com.jarvish.android.jarvishpremium.model.UserManual;

import java.util.ArrayList;
import java.util.Locale;

public class ManualSetting {

    private static int getManualLocaleIndex() {
        Locale locale = Locale.getDefault();
        if (locale.getCountry().equals("TW")) {
            return 0;
        } else {
            return 1;
        }
    }

    public static ArrayList<UserManual> getUserGuideLink() {
        ArrayList<UserManual> userManuals = new ArrayList<>();
        int index = getManualLocaleIndex();
        if (index == 0) {
            userManuals.add(new UserManual("JARVISH X/ANGELO X 安全帽快速指南",
                    "https://drive.google.com/uc?export=view&id=1F7MkaQrEjiIZrz_Lt-numlry30NgLnsu"));
            userManuals.add(new UserManual("FLASH F系列 安全帽快速指南",
                    "https://drive.google.com/uc?export=view&id=1BZRYc58-WYO5iNN29F9IldOc-Dlso5tp"));
            userManuals.add(new UserManual("R1/X1 安全帽快速指南",
                    "https://drive.google.com/uc?export=view&id=1Sw1CeX8UtzhESuFCYyKZ1hnKhcUYX7AA"));
            userManuals.add(new UserManual("MES系列 安全帽快速指南",
                    "https://drive.google.com/uc?export=view&id=1QVtFzatTJi8qvctpj_h46cFKd2rE9jgA"));
            userManuals.add(new UserManual("Bluetooth 與 Wi-Fi 連線說明",
                    "https://drive.google.com/uc?export=view&id=1vAunveBTnpYDvOIgzUvyR5O5uLLI4CTD"));
            userManuals.add(new UserManual("前後對講連線說明",
                    "https://drive.google.com/uc?export=view&id=1phclsHfK8jx6J24lArVnNNnXfRN8xmg9"));
            userManuals.add(new UserManual("APP 使用說明",
                    "https://drive.google.com/uc?export=view&id=1dcGevnEKQELLJn8FT7AfoHlNMt0akEHq"));

        } else {
            userManuals.add(new UserManual("JARVISH X/ANGELO X Quick Start Guide ",
                    "https://drive.google.com/uc?export=view&id=1czvT2r6y8VHVbKrZ-TSwbwQGIdPsy5cE"));
            userManuals.add(new UserManual("JARVISH X/ANGELO X Connection Guide",
                    "https://drive.google.com/uc?export=view&id=1Es-LeA5gUHxfwXbjFnwh1CC08TACx0oN"));
            userManuals.add(new UserManual("APP User Guide",
                    "https://drive.google.com/uc?export=view&id=1C2cDodsPPOHOo_Y_E5t3CAersb3t2PEh"));
        }
        return userManuals;
    }

    public static String getPrivacyPolicyLink() {
        int index = getManualLocaleIndex();
        if (index == 0) {
            return "https://sites.google.com/jarvish.com/jarvishprivacypolicytw/%E9%A6%96%E9%A0%81";
        } else {
            return "https://sites.google.com/jarvish.com/jarvishprivacypolicyen/%E9%A6%96%E9%A0%81";
        }
    }
}
