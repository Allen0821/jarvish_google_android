package com.jarvish.android.jarvishpremium;

import java.util.Locale;

public class MapInfoUtil {

    public static String estimatedTime(int seconds) {
        String estimated;

        int hour = seconds / 3600;
        int min = (seconds % 3600) / 60;
        if (min <= 0) {
            min = 1;
        }

        if (hour > 0) {
            estimated = String.format(Locale.getDefault(), "%d hr %d min", hour, min);
        } else {
            estimated = String.format(Locale.getDefault(), "%d min", min);
        }

        return estimated;
    }



    public static String estimatedDistance(int metre) {
        String estimated;

        double  km = metre / 1000;
        if (km > 0) {
            estimated = String.format(Locale.getDefault(), "%.1f km", km);
        } else {
            int m = metre % 1000;
            estimated = String.format(Locale.getDefault(), "%3d m", m);
        }

        return estimated;
    }
}
