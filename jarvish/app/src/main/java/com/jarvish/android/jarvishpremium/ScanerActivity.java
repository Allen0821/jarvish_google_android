package com.jarvish.android.jarvishpremium;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import com.jarvish.android.jarvishpremium.databinding.ActivityScanerBinding;
import com.journeyapps.barcodescanner.CaptureManager;

public class ScanerActivity extends AppCompatActivity {
    //private static final String TAG = "ScanerActivity";
    private ActivityScanerBinding mBinding;

    private CaptureManager capture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_scaner);

        //dbvView = initializeContent();

        capture = new CaptureManager(this, mBinding.dbvView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.setShowMissingCameraPermissionDialog(false);
        capture.decode();

        mBinding.btCancel.setOnClickListener(v -> finish());
    }


    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }


    /**
     * Check if the device's camera has a Flashlight.
     * @return true if there is Flashlight, otherwise false.
     */
    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void switchFlashlight(View view) {
        /*if (getString(R.string.turn_on_flashlight).equals(switchFlashlightButton.getText())) {
            dbvView.setTorchOn();
        } else {
            dbvView.setTorchOff();
        }*/
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
