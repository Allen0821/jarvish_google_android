package com.jarvish.android.jarvishpremium;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


import java.util.Date;

public class SharedPreferencesUtil {
    private static final String TAG = "SharedPreferencesUtil";


    public static boolean getGuide(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_first), Context.MODE_PRIVATE)
                .getBoolean(context.getString(R.string.preference_guide), true);
    }


    public static void setGuide(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_first), Context.MODE_PRIVATE);
        sharedPref.edit()
                .putBoolean(context.getString(R.string.preference_guide), false)
                .apply();
    }


    public static void storeLoginData(Context context, String session, String uid) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE);
        sharedPref.edit()
                .putString(context.getString(R.string.preference_jarvish_session), session)
                .putString(context.getString(R.string.preference_jarvish_uid), uid)
                .putLong(context.getString(R.string.preference_jarvish_login_date), new Date().getTime())
                .apply();
    }


    public static String getLoginSession(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE)
                .getString(context.getString(R.string.preference_jarvish_session), "NO");
    }


    public static String getLoginUid(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE)
                .getString(context.getString(R.string.preference_jarvish_uid), "NO");
    }


    public static Long getLoginDate(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE)
                .getLong(context.getString(R.string.preference_jarvish_login_date), new Date().getTime());
    }


    public static void removeLoginData(Context context) {
        Log.d(TAG, "remove Login Data");
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE);
        sharedPref.edit()
                .clear()
                .apply();
    }


    public static void storeUserProfileAvatar(Context context, String avatar) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE);
        sharedPref.edit()
                .putString(context.getString(R.string.preference_jarvish_profile_avatar), avatar)
                .apply();
    }


    public static void storeUserProfileName(Context context, String name) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE);
        sharedPref.edit()
                .putString(context.getString(R.string.preference_jarvish_profile_name), name)
                .apply();
    }


    public static String getUserProfileAvatar(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE)
                .getString(context.getString(R.string.preference_jarvish_profile_avatar), "");
    }


    public static String getUserProfileName(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE)
                .getString(context.getString(R.string.preference_jarvish_profile_name), "");
    }


    public static void storeHelmetModel(Context context, String model) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE);
        sharedPref.edit()
                .putString(context.getString(R.string.preference_jarvish_helmet_model), model)
                .apply();
    }


    public static String getHelmetModel(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE)
                .getString(context.getString(R.string.preference_jarvish_helmet_model), "");
    }


    public static void storeHelmetType(Context context, String type) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE);
        sharedPref.edit()
                .putString(context.getString(R.string.key_helmet_model_number), type)
                .apply();
    }


    public static String getHelmetType(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE)
                .getString(context.getString(R.string.key_helmet_model_number), "");
    }




    public static void setAutoTracking(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putBoolean(context.getString(R.string.preference_setting_display_gostations), value)
                .apply();
    }


    public static boolean getAutoTracking(Context context) {
        return  PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(context.getString(R.string.preference_setting_display_gostations),
                        false);
    }



    /*public static void setDisplayGoStationSetting(Context context, boolean value) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE);
        sharedPref.edit()
                .putBoolean(context.getString(R.string.preference_setting_display_gostations), value)
                .apply();
    }


    public static boolean getDisplayGoStationSetting(Context context) {
        return  context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE)
                .getBoolean(context.getString(R.string.preference_setting_display_gostations), false);
    }*/


    public static void setAppUnits(Context context, int type) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE);
        sharedPref.edit()
                .putInt(context.getString(R.string.preference_app_units), type)
                .apply();
    }


    public static int getAppUnits(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_data), Context.MODE_PRIVATE)
                .getInt(context.getString(R.string.preference_app_units), 0);
    }


    public static String getYourHelmetPhoto(Context context) {
        return context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_first), Context.MODE_PRIVATE)
                .getString("YOUR_HELMET", "");
    }


    public static void setYourHelmetPhoto(Context context, String helmet) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.
                getString(R.string.preference_jarvish_first), Context.MODE_PRIVATE);
        sharedPref.edit()
                .putString("YOUR_HELMET", helmet)
                .apply();
    }
}
