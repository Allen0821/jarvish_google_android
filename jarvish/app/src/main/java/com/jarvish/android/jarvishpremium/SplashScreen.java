package com.jarvish.android.jarvishpremium;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.jarvish.android.jarvishpremium.databinding.ActivitySplashScreenBinding;
import com.jarvish.android.jarvishpremium.login.model.AuthErrorCode;
import com.jarvish.android.jarvishpremium.login.model.AuthMethod;
import com.jarvish.android.jarvishpremium.login.ui.AuthenticationActivity;


public class SplashScreen extends AppCompatActivity {

    private ActivitySplashScreenBinding mBinding;
    String emailAddress;
    String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);
        //setContentView(R.layout.activity_splash_screen);
        Intent intent = new Intent();
        Uri data = getIntent().getData();
        if (data != null) {
            link = data.toString();
            Log.d("Auth", "auth link: " + link);
            if (AuthMethod.Companion.emailVerificationRedirection(link)) {
                emailAddress = AuthMethod.Companion.getAuthEmailAddress(this);
                intent.setClass(SplashScreen.this, AuthenticationActivity.class);
                intent.putExtra(getString(R.string.auth_link), link);
                intent.putExtra(getString(R.string.auth_email_address), emailAddress);
                intent.putExtra(getString(R.string.auth_open_app), true);
            } else {
                intent.setClass(SplashScreen.this, MainActivity.class);
            }
        } else {
            intent.setClass(SplashScreen.this, MainActivity.class);
        }
        new Handler().postDelayed(() -> {
            startActivity(intent);
            finish();
        }, 800);

        /*FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // User is signed in
            Log.d("Auth", "User is signed in: " + user.getUid());
            Intent main = new Intent(SplashScreen.this, MainActivity.class);
            new Handler().postDelayed(() -> {
                startActivity(main);
                finish();
            }, 800);
        } else {
            Log.d("Auth", "No user is signed in");
            Intent auth = new Intent(SplashScreen.this, AuthenticationActivity.class);
            Uri data = getIntent().getData();
            if (data != null) {
                link = data.toString();
                Log.d("Auth", "auth link: " + link);
                if (AuthMethod.Companion.emailVerificationRedirection(link)) {
                    emailAddress = AuthMethod.Companion.getAuthEmailAddress(this);
                    auth.putExtra(getString(R.string.auth_link), link);
                    auth.putExtra(getString(R.string.auth_email_address), emailAddress);
                }
            }
            auth.putExtra(getString(R.string.auth_open_app), true);
            new Handler().postDelayed(() -> {
                startActivity(auth);
                finish();
            }, 1200);
        }*/
    }

}
