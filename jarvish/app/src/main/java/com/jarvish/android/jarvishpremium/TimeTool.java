package com.jarvish.android.jarvishpremium;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeTool {

    public static String calculateDuration(long d) {
        if (d <= 0) {
            return "00:00:00";
        }
        long seconds = d % 60;
        long minutes = d / (60) % 60;
        long hours = d / (60 * 60);
        return String.format(Locale.getDefault(), "%1$02d:%2$02d:%3$02d", hours, minutes, seconds);
    }

    public static String secondsToMinuteOrHours(int remainTime) {
        String time = "";
        if (remainTime < 0) {
            return "--:--:--";
        }
        Integer h = remainTime / 3600;
        Integer m = (remainTime % 3600) / 60;
        Integer s = remainTime % 60;

        if (h > 0) {
            if (h < 10) {
                time = "0" + h.toString();
            } else {
                time = h.toString();
            }
            time = time + ":";
        }

        if (m < 10) {
            time = time + "0" + m.toString();
        } else {
            time = time + m.toString();
        }
        time = time + ":";
        if (s < 10) {
            time = time + "0" + s.toString();
        } else {
            time = time + s.toString();
        }
        return time;
    }


    public static String timestampConvertFormatString(long timestamp, String format) {
        try{
            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());
            return formatter.format(new Date(timestamp));
        }catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
