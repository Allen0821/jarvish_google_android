package com.jarvish.android.jarvishpremium;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.text.style.AbsoluteSizeSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.NonNull;

public class Util {

    public static int getViewPixel(Context context, int i) {
        return Math.round(TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, i, context.getResources().getDisplayMetrics()));
    }

    public static int dpToPx(Context context, int dps) {
        return Math.round(context.getResources().getDisplayMetrics().density * dps);
    }


    public static String estimatedTime(int seconds) {
        String estimated;

        int hour = seconds / 3600;
        int min = (seconds % 3600) / 60;
        if (min <= 0) {
            min = 1;
        }

        if (hour > 0) {
            estimated = String.format(Locale.getDefault(), "%d hr %d min", hour, min);
        } else {
            estimated = String.format(Locale.getDefault(), "%d min", min);
        }

        return estimated;
    }



    public static String estimatedDistance(int metre) {
        String estimated;

        double  km = metre / 1000;
        if (km > 0) {
            estimated = String.format(Locale.getDefault(), "%.1f km", km);
        } else {
            int m = metre % 1000;
            estimated = String.format(Locale.getDefault(), "%3d m", m);
        }

        return estimated;
    }


    public static double roundingDoubleToDouble(double d, int scale) {
        try {
            BigDecimal bigDecimal = new BigDecimal(Double.toString(d));
            //Log.d(TAG, "rounding double bigDecimal: " + bigDecimal);
            return bigDecimal.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
        } catch (Exception e) {
            //Log.e(TAG, "rounding double exception: " + e.toString());
            return 0;
        }
    }


    public static String getTrackPointTimeFormat(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format("HH:mm", cal).toString();
    }


    public static Timestamp getTimestamp() {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        return new Timestamp(calendar.getTimeInMillis());
    }


    public static String calculateDistanceKm(double m) {
        double meter = m % 1000;
        double km = m / 1000;
        if (km >= 1) {
            return String.format(Locale.getDefault(), "%.2f", km) + "km";
        } else {
            return String.format(Locale.getDefault(), "%.0f", meter) + "m";
        }
    }


    public static String calculateDistanceMi(double m) {
        double ft = m * 3.28;
        double mi;
        if (ft < 1000) {
            return String.format(Locale.getDefault(), "%.0f", ft) + "ft";
        } else {
            mi = m / 1609.344;
            return String.format(Locale.getDefault(), "%.2f", mi) + "mi";
        }
    }


    public static String calculateAverageSpeedKm(double m) {
        double km = m * 60 * 60 / 1000;
        return String.format(Locale.getDefault(), "%.2f", km) + "km/hr";
    }


    public static String calculateAverageSpeedMi(double m) {
        double mi = m * 60 * 60 / 1609.344;
        return String.format(Locale.getDefault(), "%.2f", mi) + "mi/hr";
    }


    public static SpannableString calculateSpannableStringDistanceKm(double input) {
        double meter = input % 1000;
        double km = input / 1000;
        meter = roundingDoubleToDouble(meter, 2);
        String k;
        String m;
        String ku = "km";
        String mu = "m";
        String res;
        SpannableString distance;
        if (km >= 1) {
            k = String.format(Locale.getDefault(), "%.2f", km);
            res = k + ku;
            distance = new SpannableString(res);
            distance.setSpan(new AbsoluteSizeSpan(36), k.length(), res.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            m = String.format(Locale.getDefault(), "%.0f", meter);
            res = m + mu;
            distance = new SpannableString(res);
            distance.setSpan(new AbsoluteSizeSpan(36), m.length(), res.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return distance;
    }


    public static SpannableString calculateSpannableStringDistanceMi(double meter) {
        double ft = meter * 3.28;
        double mi;
        String m;
        String f;
        String mu = "mi";
        String fu = "ft";
        String res;
        SpannableString distance;
        if (ft > 1000) {
            mi = meter / 1609.344;
            m = String.format(Locale.getDefault(), "%.2f", mi);
            res = m + mu;
            distance = new SpannableString(res);
            distance.setSpan(new AbsoluteSizeSpan(36), m.length(), res.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            f = String.format(Locale.getDefault(), "%.0f", ft);
            res = f + fu;
            distance = new SpannableString(res);
            distance.setSpan(new AbsoluteSizeSpan(36), f.length(), res.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return distance;
    }


    public static SpannableString calculateSpannableStringAverageSpeedKm(double m) {
        double km = m * 60 * 60 / 1000;
        String d = String.format(Locale.getDefault(), "%.2f", km);
        String u = "km/hr";
        String res = d + u;
        SpannableString speed = new SpannableString(res);
        speed.setSpan(new AbsoluteSizeSpan(36), d.length(), res.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return speed;
    }


    public static SpannableString calculateSpannableStringAverageSpeedMi(double m) {
        double mi =  m * 60 * 60 / 1609.344;
        String d = String.format(Locale.getDefault(), "%.2f", mi);
        String u = "mi/hr";
        String res = d + u;
        SpannableString speed = new SpannableString(res);
        speed.setSpan(new AbsoluteSizeSpan(36), d.length(), res.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return speed;
    }


    public static String getHelmetVideoDate(String strDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.getDefault());
        try {
            Date date = format.parse(strDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "unknown";
        }
    }


    public static String getDateCurrentTimeZone(long timestamp) {
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
            return formatter.format(new Date(timestamp));
        }catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String getDataSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }


    public static String getDefaultTrackLogTitle(Context context) {
        Calendar calendar = Calendar.getInstance();
        int w = calendar.get(Calendar.DAY_OF_WEEK);
        boolean isFirstSunday = (calendar.getFirstDayOfWeek() == Calendar.SUNDAY);
        if(isFirstSunday){
            w = w - 1;
            if(w == 0){
                w = 7;
            }
        }
        String week;
        switch (w) {
            case 1:
                week = context.getString(R.string.track_title_monday);
                break;
            case 2:
                week = context.getString(R.string.track_title_tuesday);
                break;
            case 3:
                week = context.getString(R.string.track_title_wednesday);
                break;
            case 4:
                week = context.getString(R.string.track_title_thursday);
                break;
            case 5:
                week = context.getString(R.string.track_title_friday);
                break;
            case 6:
                week = context.getString(R.string.track_title_saturday);
                break;
            case 7:
                week = context.getString(R.string.track_title_sunday);
                break;
            default:
                week = context.getString(R.string.track_title_monday);
                break;
        }
        int h = calendar.get(Calendar.HOUR_OF_DAY);
        String hour;
        if (h >= 3 && h < 6) {
            hour = context.getString(R.string.track_title_3_6);
        } else if (h >= 6 && h < 8) {
            hour = context.getString(R.string.track_title_6_8);
        } else if (h >= 8 && h < 11) {
            hour = context.getString(R.string.track_title_8_11);
        } else if (h >= 11 && h < 13) {
            hour = context.getString(R.string.track_title_11_13);
        } else if (h >= 13 && h < 17) {
            hour = context.getString(R.string.track_title_13_17);
        } else if (h >= 17 && h < 19) {
            hour = context.getString(R.string.track_title_17_19);
        } else if (h >= 19 && h < 23) {
            hour = context.getString(R.string.track_title_19_23);
        } else {
            hour = context.getString(R.string.track_title_23_3);
        }
        return  week + " " + hour;
    }


    public static void getStatusBarSize(Activity activity) {
        Rect rectangle = new Rect();
        Window window = activity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop =
                window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight= contentViewTop - statusBarHeight;

        Log.i("XXX", "StatusBar Height= " + statusBarHeight + " , TitleBar Height = " + titleBarHeight);
    }


    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        Log.i("XXX", "StatusBar Height= " + resourceId);
        Log.i("XXX", "StatusBar Height= " + result);
        return result;
    }


    public static String getDateTimeFormat() {
        Locale locale = Locale.getDefault();
        if (locale.getLanguage().equals("zh")) {
            if (locale.getCountry().equals("TW")) {
                return "EE HH:mm, yyyy年M月d日";
            }
        }
        return "EE HH:mm, MMMM d, yyyy";
    }


    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() == null) {
            return;
        }
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }


    public static boolean verifySerialNumber(final String number) {
        if (number == null || number.length() != 13) {
            return false;
        }
        return number.trim().matches("^[0-9]*$");
    }

    public static boolean xmasBefore() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        try {
            Date strDate = sdf.parse("01/01/2021");
            return !new Date().after(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
