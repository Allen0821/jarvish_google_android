package com.jarvish.android.jarvishpremium

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.getColor
import com.google.android.material.snackbar.Snackbar


class ViewUtil {
    companion object {
        fun hideKeyboard(context: Context, view: View) {
            val iMm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            iMm.hideSoftInputFromWindow(view.windowToken, 0)
            view.clearFocus()
        }

        fun disableInteraction(activity: Activity) {
            activity.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }

        fun enableInteraction(activity: Activity) {
            activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }

        fun alertView(context: Context, @StringRes nameResource: Int) {
            val dialog = AlertDialog.Builder(context, R.style.AppAlertDialog)
                    .setMessage(nameResource)
                    .setNegativeButton(R.string.action_confirm, null)
                    .show()
            val textView: TextView? = dialog.findViewById(android.R.id.message)
            if (textView != null) {
                textView.textSize = 18F
            }
        }

        fun snackbarView(view: View, context: Context, @StringRes nameResource: Int) {
            Snackbar.make(view, nameResource, Snackbar.LENGTH_SHORT)
                    .setTextColor(getColor(context, R.color.white))
                    .setBackgroundTint(getColor(context, R.color.color_primary_01))
                    .setActionTextColor(getColor(context, R.color.white))
                    .show()
        }
    }
}