/**
 * Created by CupLidSheep on 29,December,2020
 */
package com.jarvish.android.jarvishpremium.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.BleScanResultItemBinding;
import com.polidea.rxandroidble2.scan.ScanResult;

import java.util.ArrayList;

public class BleScanResultAdapter extends RecyclerView.Adapter<BleScanResultAdapter.BleScanResultViewHolder> {
    private static final String TAG ="BleScanResultAdapter";
    private final ArrayList<ScanResult> mScanResults;
    private final OnBleScanResultAdapterListener mListener;

    public BleScanResultAdapter(ArrayList<ScanResult> scanResults, OnBleScanResultAdapterListener listener) {
        this.mScanResults = scanResults;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public BleScanResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder");
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        BleScanResultItemBinding binding =
                BleScanResultItemBinding.inflate(layoutInflater, parent, false);
        return new BleScanResultViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BleScanResultViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder");
        holder.bind(mScanResults.get(position), mListener);

    }

    @Override
    public int getItemCount() {
        return mScanResults.size();
    }

    static class BleScanResultViewHolder extends RecyclerView.ViewHolder {
        private final BleScanResultItemBinding mBinding;

        public BleScanResultViewHolder(@NonNull BleScanResultItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(ScanResult scanResult, OnBleScanResultAdapterListener listener) {
            /*if (scanResult.getBleDevice().getName() != null &&
                    !scanResult.getBleDevice().getName().isEmpty()) {
                mBinding.tvBleName.setText(scanResult.getBleDevice().getName());
            } else {
                mBinding.tvBleName.setText(scanResult.getBleDevice().getMacAddress());
            }

            mBinding.ivHelmet.setImageDrawable(ContextCompat.getDrawable(itemView.getContext(),
                    R.drawable.ic_helmet_36dp));

            mBinding.getRoot().setOnClickListener(v -> listener.onSelected(scanResult));*/
            Log.d(TAG, "bind: " + scanResult.getBleDevice().getMacAddress());
            mBinding.setResult(scanResult);
            mBinding.getRoot().setOnClickListener(v -> listener.onSelected(scanResult));
            mBinding.executePendingBindings();
        }
    }

    public interface OnBleScanResultAdapterListener {
        void onSelected(ScanResult scanResult);
    }
}
