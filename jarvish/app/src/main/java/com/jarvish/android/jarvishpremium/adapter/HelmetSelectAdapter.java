/*
 * Created by CupLidSheep on 29,December,2020
 */
package com.jarvish.android.jarvishpremium.adapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.HelmetSelectItemBinding;
import com.jarvish.android.jarvishpremium.model.HelmetStyle;

import java.util.ArrayList;

public class HelmetSelectAdapter extends RecyclerView.Adapter<HelmetSelectAdapter.HelmetSelectViewHolder> {
    private final ArrayList<HelmetStyle> helmetStyles;
    private final OnHelmetSelectAdapterListener mListener;

    public HelmetSelectAdapter(ArrayList<HelmetStyle> helmetStyles, OnHelmetSelectAdapterListener listener) {
        this.helmetStyles = helmetStyles;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public HelmetSelectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        HelmetSelectItemBinding binding =
                HelmetSelectItemBinding.inflate(layoutInflater, parent, false);
        return new HelmetSelectViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull HelmetSelectViewHolder holder, int position) {
        holder.bind(helmetStyles.get(position), mListener);
    }

    @Override
    public int getItemCount() {
        return this.helmetStyles.size();
    }

    static class HelmetSelectViewHolder extends RecyclerView.ViewHolder {
        private final HelmetSelectItemBinding binding;

        public HelmetSelectViewHolder(HelmetSelectItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(HelmetStyle helmetStyle, OnHelmetSelectAdapterListener listener) {
            Drawable drawable = ResourcesCompat.getDrawable(
                    itemView.getContext().getResources(),
                    itemView.getContext().getResources()
                            .getIdentifier(helmetStyle.getImage(), "drawable",
                                    itemView.getContext().getPackageName()),
                    null);
            binding.ivItem.setImageDrawable(drawable);
            binding.tvItem.setText(helmetStyle.getName());
            binding.getRoot().setOnClickListener(view -> listener.onSelected(helmetStyle));
        }
    }

    public interface OnHelmetSelectAdapterListener {
        void onSelected(HelmetStyle helmetStyle);
    }
}
