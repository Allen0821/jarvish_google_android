package com.jarvish.android.jarvishpremium.adapter;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.databinding.PersonalFunctionViewBinding;
import com.jarvish.android.jarvishpremium.model.PzzaFuc;

import java.util.ArrayList;

public class PersonalFunctionAdapter extends
        RecyclerView.Adapter<PersonalFunctionAdapter.PersonalFunctionViewHolder> {
    private static final String TAG = "PersonalFunctionAdapter";
    private final ArrayList<PzzaFuc> mFunctionList;
    private final PersonalFunctionAdapter.OnPersonalFunctionListener mOnPersonalFunctionListener;

    public PersonalFunctionAdapter(ArrayList<PzzaFuc> list, PersonalFunctionAdapter.OnPersonalFunctionListener listener) {
        this.mFunctionList = list;
        this.mOnPersonalFunctionListener = listener;
    }


    @NonNull
    @Override
    public PersonalFunctionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        PersonalFunctionViewBinding binding =
                PersonalFunctionViewBinding.inflate(layoutInflater, parent, false);
        return new PersonalFunctionAdapter.PersonalFunctionViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull PersonalFunctionViewHolder holder, int position) {
        holder.bind(mFunctionList.get(position), mOnPersonalFunctionListener);
    }


    @Override
    public int getItemCount() {
        return mFunctionList.size();
    }

    static class PersonalFunctionViewHolder extends RecyclerView.ViewHolder {
        private final PersonalFunctionViewBinding mBinding;

        PersonalFunctionViewHolder(@NonNull PersonalFunctionViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(PzzaFuc data, PersonalFunctionAdapter.OnPersonalFunctionListener listener) {
            Log.d(TAG, "GroupView title: " + data.getTitle());
            this.mBinding.tvItem.setText(data.getTitle());
            int size = Util.dpToPx(itemView.getContext(), 32);
            data.getIcon().setBounds(0,0,size,size);
            if (data.getType() == 0) {
                this.mBinding.tvItem.setCompoundDrawables(data.getIcon(), null, null, null);
            } else {
                Drawable right = ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_arrow_right);
                if (right != null) {
                    int s = Util.dpToPx(itemView.getContext(), 24);
                    right.setBounds(0,0,s,s);
                }
                this.mBinding.tvItem.setCompoundDrawables(data.getIcon(), null, right, null);
            }


            itemView.setOnClickListener((View v) -> listener.onSelectedItem(data.getTitle()));
            mBinding.executePendingBindings();
        }
    }

    public interface OnPersonalFunctionListener {
        void onSelectedItem(String title);
    }
}
