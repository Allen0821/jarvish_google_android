package com.jarvish.android.jarvishpremium.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jarvish.android.jarvishpremium.databinding.RegisterItemViewBinding
import com.jarvish.android.jarvishpremium.firestore.Register

class RegisterAdapter :
        ListAdapter<Register, RegisterAdapter.ViewHolder>(RegisterDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d("StoreRegister", "position: $position")
        holder.bind(getItem(position))
    }

    class ViewHolder private constructor(val binding: RegisterItemViewBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(register: Register) {
            binding.register = register
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = RegisterItemViewBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class RegisterDiffCallback: DiffUtil.ItemCallback<Register>() {
    override fun areItemsTheSame(oldItem: Register, newItem: Register): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Register, newItem: Register): Boolean {
        return oldItem == newItem
    }

}