/**
 * Created by CupLidSheep on 11,November,2020
 */
package com.jarvish.android.jarvishpremium.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.ShareTrackItemViewBinding;
import com.jarvish.android.jarvishpremium.track.ShareTrackItem;

import java.util.ArrayList;

public class ShareTrackAdapter extends RecyclerView.Adapter<ShareTrackAdapter.ShareTrackViewHolder> {

    private final ArrayList<ShareTrackItem> mShareTrackItems;
    private final OnShareTrackListener mOnShareTrackListener;

    public ShareTrackAdapter(ArrayList<ShareTrackItem> shareTrackItems, OnShareTrackListener listener) {
        this.mShareTrackItems = shareTrackItems;
        this.mOnShareTrackListener = listener;
    }

    @NonNull
    @Override
    public ShareTrackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ShareTrackItemViewBinding binding =
                ShareTrackItemViewBinding.inflate(layoutInflater, parent, false);
        return new ShareTrackAdapter.ShareTrackViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ShareTrackViewHolder holder, int position) {
        holder.bind(mShareTrackItems.get(position), mOnShareTrackListener);
    }

    @Override
    public int getItemCount() {
        return mShareTrackItems.size();
    }

    static class ShareTrackViewHolder extends RecyclerView.ViewHolder {
        private final ShareTrackItemViewBinding mBinding;

        public ShareTrackViewHolder(@NonNull ShareTrackItemViewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(ShareTrackItem item, @NonNull OnShareTrackListener listener) {
            mBinding.ivIcon.setImageDrawable(item.getIcon());
            mBinding.tvItem.setText(item.getName());
            itemView.setOnClickListener(view -> listener.onSelectedItem(item.getIndex()));
        }
    }

    public interface OnShareTrackListener {
        void onSelectedItem(int index);
    }
}
