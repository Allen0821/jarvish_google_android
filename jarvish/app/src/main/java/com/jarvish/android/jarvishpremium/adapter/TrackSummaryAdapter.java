package com.jarvish.android.jarvishpremium.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.SharedPreferencesUtil;
import com.jarvish.android.jarvishpremium.TimeTool;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.databinding.TrackLogItemBinding;
import com.jarvish.android.jarvishpremium.db.TrackLog;

import java.util.ArrayList;


public class TrackSummaryAdapter extends RecyclerView.Adapter<TrackSummaryAdapter.TrackSummaryViewHolder> {
    private static final String TAG = "TrackSummaryAdapter";
    private final ArrayList<TrackLog> mTrackLogs;
    private final TrackSummaryAdapter.OnTrackSummaryListener mOnTrackSummaryListener;


    public TrackSummaryAdapter(ArrayList<TrackLog> trackLogs, TrackSummaryAdapter.OnTrackSummaryListener listener) {
        this.mTrackLogs = trackLogs;
        this.mOnTrackSummaryListener = listener;
    }


    @NonNull
    @Override
    public TrackSummaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        TrackLogItemBinding binding =
                TrackLogItemBinding.inflate(layoutInflater, parent, false);
        return new TrackSummaryAdapter.TrackSummaryViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull TrackSummaryViewHolder holder, int position) {
        holder.bind(mTrackLogs.get(position), mOnTrackSummaryListener);
    }


    @Override
    public int getItemCount() {
        return this.mTrackLogs.size();
    }


    static class TrackSummaryViewHolder extends RecyclerView.ViewHolder {
        private final TrackLogItemBinding mBinding;

        TrackSummaryViewHolder(@NonNull TrackLogItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(TrackLog trackLog, TrackSummaryAdapter.OnTrackSummaryListener listener) {

            Log.d(TAG, "average: " + trackLog.getAverageSpeed());
            Log.d(TAG, "distance: " + trackLog.getDistance());
            mBinding.tvTrackTitle.setText(trackLog.getTitle());
            mBinding.tvTrackCreate.setText(TimeTool.timestampConvertFormatString(trackLog.getTimestamp(), "yyyy-M-d"));

            mBinding.tvDuration.setText(TimeTool.calculateDuration(trackLog.getDuration()));

            String distance = getDistanceString(trackLog.getDistance());
            mBinding.tvDistance.setText(distance);

            String average = getSpeedString(trackLog.getAverageSpeed());
            mBinding.tvSpeed.setText(average);

            itemView.setOnClickListener(v -> listener.onSelectedTrackLog(trackLog));
            mBinding.ivDelete.setOnClickListener(v -> listener.onDeleteTrackLog(trackLog));
        }


        private String getDistanceString(double m) {
            if (SharedPreferencesUtil.getAppUnits(itemView.getContext()) == 0) {
                return Util.calculateDistanceKm(m);
            } else {
                return Util.calculateDistanceMi(m);
            }
        }


        private String getSpeedString(double m) {
            if (SharedPreferencesUtil.getAppUnits(itemView.getContext()) == 0) {
                return Util.calculateAverageSpeedKm(m);
            } else {
                return Util.calculateAverageSpeedMi(m);
            }
        }
    }


    public interface OnTrackSummaryListener {
        void onSelectedTrackLog(TrackLog trackLog);
        void onDeleteTrackLog(TrackLog trackLog);
    }
}
