package com.jarvish.android.jarvishpremium.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jarvish.android.jarvishpremium.databinding.UserInfoItemBinding
import com.jarvish.android.jarvishpremium.user.model.UserInfo

class UserInfoAdapter(private val clickListener: (UserInfo) -> Unit): ListAdapter<UserInfo, UserInfoAdapter.ViewHolder>(UserInfoDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener)
    }

    class ViewHolder private constructor(val binding: UserInfoItemBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(userInfo: UserInfo, clickListener: (UserInfo) -> Unit) {
            binding.userInfo = userInfo
            binding.root.setOnClickListener {
                clickListener(userInfo)
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = UserInfoItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

}

class UserInfoDiffCallback: DiffUtil.ItemCallback<UserInfo>() {
    override fun areItemsTheSame(oldItem: UserInfo, newItem: UserInfo): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: UserInfo, newItem: UserInfo): Boolean {
        return oldItem == newItem
    }

}