package com.jarvish.android.jarvishpremium.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.UserManualItemBinding;
import com.jarvish.android.jarvishpremium.model.UserManual;

import java.util.ArrayList;

public class UserManualAdapter extends RecyclerView.Adapter<UserManualAdapter.UserManualViewHolder> {
    //private static final String TAG = "UserManualAdapter";
    private final ArrayList<UserManual> mUserManualList;
    private final UserManualAdapter.OnUserManualListener mOnUserManualListener;

    public UserManualAdapter(ArrayList<UserManual> list, UserManualAdapter.OnUserManualListener listener) {
        this.mUserManualList = list;
        this.mOnUserManualListener = listener;
    }


    @NonNull
    @Override
    public UserManualViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        UserManualItemBinding binding =
                UserManualItemBinding.inflate(layoutInflater, parent, false);
        return new UserManualAdapter.UserManualViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull UserManualViewHolder holder, int position) {
        holder.bind(mUserManualList.get(position), mOnUserManualListener);
    }


    @Override
    public int getItemCount() {
        return mUserManualList.size();
    }

    static class UserManualViewHolder extends RecyclerView.ViewHolder {
        private final UserManualItemBinding mBinding;

        UserManualViewHolder(@NonNull UserManualItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(UserManual userManual, UserManualAdapter.OnUserManualListener listener) {
            mBinding.tvManual.setText(userManual.getName());
            itemView.setOnClickListener((View v) -> listener.onSelectedManual(userManual));
        }
    }


    public interface OnUserManualListener {
        void onSelectedManual(UserManual manual);
    }
}
