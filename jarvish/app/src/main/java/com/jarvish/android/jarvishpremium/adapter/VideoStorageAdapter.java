/*
 * Created by CupLidSheep on 12,March,2021
 */
package com.jarvish.android.jarvishpremium.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.VideoStorageViewBinding;
import com.jarvish.android.jarvishpremium.helmet.viewmodels.VideoStorage;

import java.util.ArrayList;

public class VideoStorageAdapter extends RecyclerView.Adapter<VideoStorageAdapter.VideoStorageViewHolder> {
    private final ArrayList<VideoStorage> videoStorages;


    public VideoStorageAdapter(ArrayList<VideoStorage> videoStorages) {
        this.videoStorages = videoStorages;
    }

    @NonNull
    @Override
    public VideoStorageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        VideoStorageViewBinding binding =
                VideoStorageViewBinding.inflate(layoutInflater, parent, false);
        return new VideoStorageAdapter.VideoStorageViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoStorageViewHolder holder, int position) {
        holder.bind(videoStorages.get(position));
    }

    @Override
    public int getItemCount() {
        return videoStorages.size();
    }

    static class VideoStorageViewHolder extends RecyclerView.ViewHolder {
        private final VideoStorageViewBinding binding;

        public VideoStorageViewHolder(@NonNull VideoStorageViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(VideoStorage videoStorage) {
            binding.tvTitle.setText(videoStorage.getTitle());
            binding.tvValue.setText(videoStorage.getValue());
            int color;
            if (videoStorage.getIndex() == 0) {
                color = itemView.getContext().getColor(R.color.color_primary_01);
            } else if (videoStorage.getIndex() == 1) {
                color = itemView.getContext().getColor(R.color.color_primary_02);
            } else {
                color = itemView.getContext().getColor(R.color.color_gray_01);
            }
            binding.cardView.setCardBackgroundColor(color);
            //itemView.setBackgroundColor(itemView.getContext().getColor(android.R.color.holo_red_dark));
        }
    }
}
