package com.jarvish.android.jarvishpremium.ble;

public class DirectionDefine {
    public static final int FORWARD = 0x01;
    public static final int HEAVY_LEFT = 0x02;
    public static final int TURN_LEFT = 0x03;
    public static final int TURN_RIGHT = 0x04;
    public static final int HEAVY_RIGHT = 0x05;
    public static final int KEEP_LEFT = 0x06;
    public static final int KEEP_RIGHT = 0x07;
    public static final int LIGHT_LEFT = 0x08;
    public static final int LIGHT_RIGHT = 0x09;
    public static final int RETURN = 0x0a;
    public static final int ROUNDABOUT = 0x0b;
    public static final int UP_MORE = 0x0c;
    public static final int NONE = 0x0d;

}
