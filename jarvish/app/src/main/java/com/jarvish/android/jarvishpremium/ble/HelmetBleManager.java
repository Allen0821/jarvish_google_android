package com.jarvish.android.jarvishpremium.ble;

import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.model.Helmet;
import com.jarvish.android.jarvishpremium.model.Product;
import com.polidea.rxandroidble2.scan.ScanResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HelmetBleManager {
    private static final String TAG = "HelmetBleManager";
    private static volatile HelmetBleManager mHelmetBleManager;
    private final Context mContext;

    private final ArrayList<HelmetBleManagerListener.HelmetBleStatusListener> mHelmetBleStatusListener;
    private final ArrayList<HelmetBleManagerListener.HelmetBleMessageListener> mHelmetBleMessageListener;
    //private final HelmetBleManagerListener.HelmetBleSpeechTriggerListener mHelmetBleSpeechTriggerListener;

    private Disposable mBatteryWatchDisposable;

    private final Helmet mHelmet;


    private HelmetBleManager(Context context){
        if (mHelmetBleManager != null){
            throw new RuntimeException("Use getInstance() method to get HelmetBleManager instance.");
        }
        mContext = context;
        mHelmetBleStatusListener = new ArrayList<>();
        mHelmetBleMessageListener = new ArrayList<>();
        mHelmet = new Helmet();
        Intent intent = new Intent(mContext, HelmetBleService.class);
        mContext.bindService(intent, mHelmetConnection, Context.BIND_AUTO_CREATE);
    }


    public static HelmetBleManager getInstance(Context context) {
        if (mHelmetBleManager == null) {
            synchronized (HelmetBleManager.class) {
                if (mHelmetBleManager == null) {
                    mHelmetBleManager = new HelmetBleManager(context.getApplicationContext());
                }
            }
        }
        return mHelmetBleManager;
    }


    protected HelmetBleManager readResolve(Context context) {
        return getInstance(context);
    }


    public void addHelmetBleStatusListener(HelmetBleManagerListener.HelmetBleStatusListener listener) {
        if (mHelmetBleStatusListener != null) {
            mHelmetBleStatusListener.add(listener);
        }
    }


    public void removeHelmetBleStatusListener(HelmetBleManagerListener.HelmetBleStatusListener listener) {
        if (mHelmetBleStatusListener != null) {
            mHelmetBleStatusListener.remove(listener);
        }
    }


    public void addHelmetBleMessageListener(HelmetBleManagerListener.HelmetBleMessageListener listener) {
        if (mHelmetBleMessageListener != null) {
            mHelmetBleMessageListener.add(listener);
        }
    }


    public void removeHelmetBleMessageListener(HelmetBleManagerListener.HelmetBleMessageListener listener) {
        if (mHelmetBleMessageListener != null) {
            mHelmetBleMessageListener.remove(listener);
        }
    }


    /*public void addHelmetBleSpeechTriggerListener(HelmetBleManagerListener.HelmetBleSpeechTriggerListener listener) {
        if (mHelmetBleSpeechTriggerListener != null) {
            mHelmetBleSpeechTriggerListener.add(listener);
        }
    }


    public void removeHelmetBleSpeechTriggerListener(HelmetBleManagerListener.HelmetBleSpeechTriggerListener listener) {
        if (mHelmetBleSpeechTriggerListener != null) {
            mHelmetBleSpeechTriggerListener.remove(listener);
        }
    }*/


    /** Defines callbacks for service binding, passed to bindService() */
    private HelmetBleService mHelmetBleService;
    private boolean mBound = false;
    private final ServiceConnection mHelmetConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            Log.d(TAG, "onServiceConnected");
            HelmetBleService.HelmetBleBinder binder = (HelmetBleService.HelmetBleBinder) service;
            mHelmetBleService = binder.getService();
            mBound = true;
            mHelmetBleService.addHelmetBleServiceListener(mHelmetBleServiceListener);
            //mHelmetBleService.connectHelmet(mHelmetMacAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.d(TAG, "onServiceDisconnected");
            mBound = false;
        }
    };


    public void startBleScan() {
        if (isBluetoothEnable()) {
            mHelmetBleService.scanHelmet();
        }
    }

    public void stopBleScan() {
        if (isBluetoothEnable()) {
            mHelmetBleService.stopScan();
        }
    }


    public void connectDevice(@NonNull String deviceMac) {
        if (mBound) {
            Log.d(TAG, "connect device");
            if (isBluetoothEnable()) {
                mHelmetBleService.connectHelmet(deviceMac);
            }
        } else {
            Log.d(TAG, "try again");
        }
    }


    public void disconnectHelmet() {
        if (mBound) {
            Log.d(TAG, "disconnect helmet");
            stopBatteryWatch();
            mHelmetBleService.triggerDisconnect();
        }
    }


    public void destroyHelmetBleManager() {
        if (mBound) {
            Log.d(TAG, "unbind helmet ble service");
            stopBatteryWatch();
            mContext.unbindService(mHelmetConnection);
        }
        if (mDisposableHelmetSettings != null) {
            mDisposableHelmetSettings.dispose();
            mDisposableHelmetSettings = null;
        }
        mHelmetBleStatusListener.clear();
        mHelmetBleMessageListener.clear();
        //mHelmetBleSpeechTriggerListener.clear();
        mBound = false;
        mHelmetBleManager = null;
    }


    private Disposable mDisposableHelmetSettings;
    public void fetchHelmetSettings() {
        Log.d(TAG, "fetchHelmetSettings");
        if (mDisposableHelmetSettings != null) {
            mDisposableHelmetSettings.dispose();
            mDisposableHelmetSettings = null;
        }
        mDisposableHelmetSettings = Observable.interval(2500, 1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    Log.d(TAG, "fetch step: " + aLong.intValue());
                    switch (aLong.intValue()) {
                        case 0:
                            mHelmetBleManager.getModelNumber();
                            break;
                        case 1:
                            mHelmetBleManager.getModelNumber();
                            break;
                        case 2:
                            mHelmetBleManager.getFirmwareVersion();
                            break;
                        case 3:
                            mHelmetBleManager.getTailLight();
                            break;
                        case 4:
                            mHelmetBleManager.getBatteryEfficiency();
                            break;
                        case 5:
                            mHelmetBleManager.getVideoSettings();
                            break;
                        case 6:
                            Log.d(TAG, "Helmet: " + new Gson().toJson(mHelmet));
                            if (mHelmetBleStatusListener != null) {
                                for(HelmetBleManagerListener.HelmetBleStatusListener listener : mHelmetBleStatusListener) {
                                    listener.onFetchHelmet(mHelmet);
                                }
                            }
                            mDisposableHelmetSettings.dispose();
                            mDisposableHelmetSettings = null;
                            startBatteryWatch();
                            break;
                    }
                }, Throwable::printStackTrace);
    }


    public void startBatteryWatch() {
        stopBatteryWatch();
        mBatteryWatchDisposable = Observable.interval(1, 30, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::batteryWatch);
    }

    public void stopBatteryWatch() {
        if (mBatteryWatchDisposable != null) {
            mBatteryWatchDisposable.dispose();
            mBatteryWatchDisposable = null;
        }
    }

    private void batteryWatch(Long aLong) {
        //Log.d(TAG, "battery watch:" + aLong);
        if (mBound) {
            mHelmetBleService.sendBleCommand(JBleProtocol
                    .getBatteryLevel());
        }
    }


    public void getModelNumber() {
        if (mBound) {
            Log.d(TAG, "getModelNumber");
            mHelmetBleService.sendBleCommand(JBleProtocol
                    .getSerialNumber());
        }
    }


    public void getFirmwareVersion() {
        if (mBound) {
            Log.d(TAG, "getFirmwareVersion");
            mHelmetBleService.sendBleCommand(JBleProtocol.getFirmwareVersion());
        }
    }


    /*public void getModelName() {
        if (mBound) {
            Log.d(TAG, "getModelName");
            mHelmetBleService.sendBleCommand(HelmetBleMessage
                    .convertIntTo2Bytes(HelmetBleMessage.MSG_ID_HELMET_NAME));
        }
    }*/

    public void getTailLight() {
        if (mBound) {
            Log.d(TAG, "getTailLight");
            mHelmetBleService.sendBleCommand(JBleProtocol.getTailLight());
        }
    }

    public void setTailLight(boolean value) {
        if (mBound) {
            Log.d(TAG, "setTailLight");
            mHelmetBleService.sendBleCommand(JBleProtocol.setTailLight(value));
        }
    }

    public void setBatteryEfficiency(int value) {
        if (mBound) {
            Log.d(TAG, "setBatteryEfficiency");
            mHelmetBleService.sendBleCommand(JBleProtocol.setBatteryEfficiency(value));
        }
    }

    public void getBatteryEfficiency() {
        if (mBound) {
            Log.d(TAG, "getBatteryEfficiency");
            mHelmetBleService.sendBleCommand(JBleProtocol.getBatteryEfficiency());
        }
    }


    public void setVideoTimestamp(boolean value) {
        if (mBound) {
            Log.d(TAG, "setVideoTimestamp");
            mHelmetBleService.sendBleCommand(JBleProtocol.setVideoTimestamp(value));
        }
    }

    public void setVideoRecordingTime(int value) {
        if (mBound) {
            Log.d(TAG, "setVideoRecordingTime");
            mHelmetBleService.sendBleCommand(JBleProtocol.setVideoRecordingTime(value));
        }
    }

    public void setVideoResolution(int value) {
        if (mBound) {
            Log.d(TAG, "setVideoResolution");
            mHelmetBleService.sendBleCommand(JBleProtocol.setVideoResolution(value));
        }
    }


    public void getVideoSettings() {
        if (mBound) {
            Log.d(TAG, "getVideoSettings");
            mHelmetBleService.sendBleCommand(JBleProtocol.getVideoSetting());
        }
    }

    public void setSystemTime() {
        if (mBound) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            mHelmetBleService.sendBleCommand(JBleProtocol
                    .setSystemTime(
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH) + 1,
                            calendar.get(Calendar.DAY_OF_MONTH),
                            calendar.get(Calendar.HOUR_OF_DAY),
                            calendar.get(Calendar.MINUTE),
                            calendar.get(Calendar.SECOND)));
        }
    }


    /*public void setSensorData(boolean value) {
        if (mBound) {
            Log.d(TAG, "setSensorData");
            mHelmetBleService.sendBleCommand(HelmetBleMessage.setSensorFusionData(value, 10));
        }
    }*/


    /*public void setDirectionStep(int direction) {
        if (mBound) {
            Log.d(TAG, "set direction step: " + direction);
            mHelmetBleService.sendBleCommand(HelmetBleMessage.setDirectionStep(direction));
        }
    }*/


    public void resetDefault() {
        if (mBound) {
            Log.d(TAG, "reset default");
            mHelmetBleService.sendBleCommand(JBleProtocol
                    .resetDefault());
        }
        /*Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onResetDefaultCompleted(true);
                    }
                }
            }
        }, 5000);*/
    }


    private void readHelmetAck(byte[] ack) {
        int index = JBleProtocol.getAckIndex(ack);
        boolean value = JBleProtocol.getAckValue(ack);
        Log.d(TAG, "read Ack= " + index);
        switch (index) {
            case JBleProtocol.MSG_ID_TAIL_LIGHT:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onTailLightAck(value);
                    }
                }
                break;
            case JBleProtocol.MSG_ID_BATTERY_EFFICIENCY:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onBatteryEfficiencyAck(value);
                    }
                }
                break;
            case JBleProtocol.MSG_ID_VIDEO_TIMESTAMP:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onVideoTimestampAck(value);
                    }
                }
                break;
            case JBleProtocol.MSG_ID_VIDEO_RESOLUTION:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onVideoResolutionAck(value);
                    }
                }
                break;
            case JBleProtocol.MSG_ID_VIDEO_RECORDING_TIME:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onVideoRecordingTimeAck(value);
                    }
                }
                break;
            case JBleProtocol.MSG_ID_SYS_DATE_TIME:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onSyncSystemTimeAck(value);
                    }
                }
                break;
            case JBleProtocol.MSG_ID_RESET_DEFAULT:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onResetDefaultAck(value);
                    }
                }
                break;
        }
    }


    private void parseHelmetBleMessage(byte[] msg) {
        int header = JBleProtocol.getHeaderId(msg);
        switch (header) {
            case JBleProtocol.MSG_ID_MODEL_NUMBER:
                Log.d(TAG, "MSG_ID_MODEL_NUMBER: " + JBleProtocol.getBleStringDate(msg));
                String serial = JBleProtocol.getBleStringDate(msg);
                //serial = "8601534hjklff";
                mHelmet.setSerialNumber(serial);
                mHelmet.setLanguage(mHelmet.paresLanguage(mHelmet.getSerialNumber()));
                Product product = new Product(serial);
                mHelmet.setProduct(product);
                if (!mHelmet.getProduct().getProductType().isEmpty()) {
                    mHelmet.setStyle(mHelmet.getProduct().getProductType());
                    mHelmet.setName(JCandy.getHelmetName(mHelmet.getProduct().getProductType()));
                    mHelmet.setImage(JCandy.getYourHelmet(mHelmet.getProduct().getProductType(), ""));
                }
                break;
            case JBleProtocol.MSG_ID_FIRMWARE_VERSION:
                Log.d(TAG, "MSG_ID_FIRMWARE_VERSION: " + JBleProtocol.getBleStringDate(msg));
                mHelmet.setFirmwareVersion(JBleProtocol.getBleStringDate(msg));
                break;
            case JBleProtocol.MSG_ID_HELMET_NAME:
                mHelmet.setName(JBleProtocol.getBleStringDate(msg));
                break;
            case JBleProtocol.MSG_ID_BATTERY_LEVEL:
                mHelmet.setBatteryLevel(JBleProtocol.parseBatteryLevel(msg));
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onBatteryLevel(mHelmet.getBatteryLevel());
                    }
                }
                break;
            case JBleProtocol.MSG_ID_TAIL_LIGHT:
                Log.d(TAG, "MSG_ID_TAIL_LIGHT: " +JBleProtocol.parseTailLight(msg));
                mHelmet.setTailLight(JBleProtocol.parseTailLight(msg));
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onTailLight(mHelmet.isTailLight());
                    }
                }
                break;
            case JBleProtocol.MSG_ID_BATTERY_EFFICIENCY:
                mHelmet.setBatteryEfficiency(JBleProtocol.paresGetParameter(msg));
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onBatteryEfficiency(mHelmet.getBatteryEfficiency());
                    }
                }
                break;
            case JBleProtocol.MSG_ID_GET_VIDEO_SETTINGS:
                Log.d(TAG, "MSG_ID_GET_VIDEO_SETTINGS");
                mHelmet.setVideoResolution(JBleProtocol.parseVideoResolution(msg));
                mHelmet.setVideoTimestamp(JBleProtocol.parseVideoTimestamp(msg));
                mHelmet.setVideoRecordingTime(JBleProtocol.parseVideoRecordingTime(msg));
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onVideoResolution(mHelmet.getVideoResolution());
                        listener.onVideoRecordingTime(mHelmet.getVideoRecordingTime());
                        listener.onVideoTimestamp(mHelmet.isVideoTimestamp());
                    }
                }
                break;
            case JBleProtocol.MSG_ID_ALEXA_WAKE_ON_EVENT:
                Log.d(TAG, "MSG_ID_ALEXA_WAKE_ON_EVENT");
                /*if (mHelmetBleSpeechTriggerListener != null) {
                    for(HelmetBleManagerListener.HelmetBleSpeechTriggerListener listener : mHelmetBleSpeechTriggerListener) {
                        listener.onSearchPlace();
                    }
                }*/
                break;
            case JBleProtocol.MSG_ID_GYROSCOPE:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.helmetGyroscope(JBleProtocol.parseSensorFusionData(msg));
                    }
                }
                break;
            case JBleProtocol.MSG_ID_ACCELEROMETER:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.helmetAccelerometer(JBleProtocol.parseSensorFusionData(msg));
                    }
                }
                break;
            case JBleProtocol.MSG_ID_MAGNETOMETER:
                if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.helmetMagnetometer(JBleProtocol.parseSensorFusionData(msg));
                    }
                }
                break;
            case JBleProtocol.MSG_ID_RESET_DEFAULT:
                /*if (mHelmetBleMessageListener != null) {
                    for(HelmetBleManagerListener.HelmetBleMessageListener listener : mHelmetBleMessageListener) {
                        listener.onResetDefaultAck(true);
                    }
                }*/
                Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                break;
            case JBleProtocol.MSG_ID_ACK:
                readHelmetAck(msg);
                break;
            default:
        }
    }

    private final HelmetBleService.HelmetBleServiceListener mHelmetBleServiceListener =
            new HelmetBleService.HelmetBleServiceListener() {

                @Override
                public void helmetConnected(String mac) {
                    Log.d(TAG, "helmetConnected");
                    mHelmetBleManager.fetchHelmetSettings();
                    if (mHelmetBleStatusListener != null) {
                        for (HelmetBleManagerListener.HelmetBleStatusListener listener : mHelmetBleStatusListener) {
                            listener.onConnected();
                        }
                    }
                }

                @Override
                public void helmetDisconnected() {
                    if (mBatteryWatchDisposable != null) {
                        mBatteryWatchDisposable.dispose();
                        mBatteryWatchDisposable = null;
                    }
                    if (mDisposableHelmetSettings != null) {
                        mDisposableHelmetSettings.dispose();
                        mDisposableHelmetSettings = null;
                    }
                    if (mHelmetBleStatusListener != null) {
                        for (HelmetBleManagerListener.HelmetBleStatusListener listener : mHelmetBleStatusListener) {
                            listener.onDisconnected();
                        }
                    }
                }

                @Override
                public void helmetConnectionFailure(String error) {
                    Log.d(TAG, "helmet connect failed");
                    if (mHelmetBleStatusListener != null) {
                        for (HelmetBleManagerListener.HelmetBleStatusListener listener : mHelmetBleStatusListener) {
                            listener.onConnectFailed();
                        }
                    }
                }

                @Override
                public void helmetMessageReceived(byte[] msg) {
                    parseHelmetBleMessage(msg);
                }


                @Override
                public void onScanResult(ScanResult result) {
                    if (mHelmetBleStatusListener != null) {
                        for (HelmetBleManagerListener.HelmetBleStatusListener listener : mHelmetBleStatusListener) {
                            listener.onScanResult(result);
                        }
                    }
                }


                @Override
                public void onScanBleFailure() {
                    if (mHelmetBleStatusListener != null) {
                        for (HelmetBleManagerListener.HelmetBleStatusListener listener : mHelmetBleStatusListener) {
                            listener.onScanFailure();
                        }
                    }
                }

                @Override
                public void onScanFinished() {
                    if (mHelmetBleStatusListener != null) {
                        for (HelmetBleManagerListener.HelmetBleStatusListener listener : mHelmetBleStatusListener) {
                            listener.onScanFinished();
                        }
                    }
                }
            };


    /* private void identifyHelmetModel(String serial) {
        Log.d(TAG, "identify helmet from received serial: " + serial);
        String fake = "9860601400000";
        JProduct product = JCandy.identifyProduct(mContext, serial);
        if (product == null) {
            if (mHelmetBleStatusListener != null) {
                for (HelmetBleManagerListener.HelmetBleStatusListener listener : mHelmetBleStatusListener) {
                    listener.onIdentified(false);
                }
            }
            mHelmetBleService.triggerDisconnect();
            return;
        }

        mIsIdentified = true;

         create a helmet and set default settings
        JHelmet helmet = new JHelmet();
        helmet.setProduct(product);
        helmet.setName(JCandy.getHelmetName(helmet.getProduct().getProductType()));

        String photo = JCandy.getYourHelmet(helmet.getProduct().getProductType(),
                helmet.getProduct().getProductColor());

        helmet.setPhoto(photo);
        SharedPreferencesUtil.setYourHelmetPhoto(mContext, photo);

        mJHelmet = helmet;
        if (mHelmetBleStatusListener != null) {
            for (HelmetBleManagerListener.HelmetBleStatusListener listener : mHelmetBleStatusListener) {
                listener.onIdentified(true);
            }
        }
    }*/



    public static boolean isBluetoothEnable() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "not support Bluetooth");
            return false;
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Log.e(TAG, "Bluetooth is not enable");
                return false;
            }
        }
        return true;
    }

}
