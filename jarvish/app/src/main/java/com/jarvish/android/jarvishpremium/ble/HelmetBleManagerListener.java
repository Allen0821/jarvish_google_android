package com.jarvish.android.jarvishpremium.ble;

import com.jarvish.android.jarvishpremium.helmet.SensorFusionData;
import com.jarvish.android.jarvishpremium.model.Helmet;
import com.polidea.rxandroidble2.scan.ScanResult;

public interface HelmetBleManagerListener {
    interface HelmetBleStatusListener {
        void onConnected();
        void onDisconnected();
        void onConnectFailed();
        void onScanResult(final ScanResult result);
        void onScanFinished();
        void onScanFailure();
        void onFetchHelmet(Helmet helmet);
    }

    interface HelmetBleMessageListener {
        void onBatteryLevel(int level);
        void onTailLight(boolean value);
        void onTailLightAck(boolean ack);
        void onBatteryEfficiency(int value);
        void onBatteryEfficiencyAck(boolean ack);
        void onVideoTimestamp(boolean value);
        void onVideoTimestampAck(boolean ack);
        void onVideoRecordingTime(int value);
        void onVideoRecordingTimeAck(boolean ack);
        void onVideoResolution(int value);
        void onVideoResolutionAck(boolean ack);
        void onSyncSystemTimeAck(boolean ack);
        void helmetGyroscope(SensorFusionData data);
        void helmetAccelerometer(SensorFusionData data);
        void helmetMagnetometer(SensorFusionData data);
        void onResetDefaultAck(boolean ack);
    }


    interface HelmetBleSpeechTriggerListener {
        void onSearchPlace();
        void onStopNavigation();
        void onStartTracking();
        void onPauseTracking();
        void onStopTracking();
    }

}
