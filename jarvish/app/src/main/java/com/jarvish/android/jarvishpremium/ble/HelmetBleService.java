package com.jarvish.android.jarvishpremium.ble;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.rx.ReplayingShare;
import com.jarvish.android.jarvishpremium.MainActivity;
import com.jarvish.android.jarvishpremium.R;
import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.RxBleConnection;
import com.polidea.rxandroidble2.RxBleDevice;
import com.polidea.rxandroidble2.scan.ScanFilter;
import com.polidea.rxandroidble2.scan.ScanResult;
import com.polidea.rxandroidble2.scan.ScanSettings;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class HelmetBleService extends Service {
    private static final String TAG = "HelmetBleService";

    // jarvish ble uuid
    private final UUID BLE_READ_NOTIFY = UUID.fromString(JarvishGattAttributes.JARVISH_READ_NOTIFY);
    private final UUID BLE_WRITE = UUID.fromString(JarvishGattAttributes.JARVISH_WRITE);
    private final UUID BLE_CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString(JarvishGattAttributes.CLIENT_CHARACTERISTIC_CONFIG);

    /**
     * The name of the channel for notifications.
     */
    private static final String CHANNEL_ID = "jarvish_channel_02";
    /**
     * The identifier for the notification displayed for the foreground service.
     */
    private static final int NOTIFICATION_ID = 2628;

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    //private final boolean mChangingConfiguration = false;

    private NotificationManager mNotificationManager;

    // Binder given to clients
    private final IBinder mBinder = new HelmetBleBinder();
    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    class HelmetBleBinder extends Binder {
        HelmetBleService getService() {
            // Return this instance of LocalService so clients can call public methods
            return HelmetBleService.this;
        }
    }

    private HelmetBleServiceListener mHelmetBleServiceListener = null;


    private RxBleClient mRxBleClient;
    private RxBleDevice mDevice;
    private final PublishSubject<Boolean> disconnectTriggerSubject = PublishSubject.create();
    private Observable<RxBleConnection> mConnectionObservable;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private Disposable mStateDisposable;
    private Disposable mConnectionDisposable;

    private Disposable mDisposableScan;
    private Disposable mDisposableTimer;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        mRxBleClient = RxBleClient.create(this);
        return mBinder;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        createNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        triggerDisconnect();
        stopScan();
        if (mDisposableTimer != null) {
            mDisposableTimer.dispose();
            mDisposableTimer = null;
        }
        mDevice = null;
        mConnectionObservable = null;
        mCompositeDisposable = null;
        mNotificationManager.cancelAll();
    }


    private void createNotification() {
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }

        startForeground(NOTIFICATION_ID, getNotification());
    }

    /**
     * Returns the {@link Notification} used as part of the foreground service.
     */
    private Notification getNotification() {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        Notification.Builder builder = new Notification.Builder(this)
                .setContentText(getString(R.string.helmet_ble_service))
                .setContentTitle(getString(R.string.app_name))
                .setColor(getResources().getColor(R.color.colorAccent, null))
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_helmet_36dp)
                .setContentIntent(pendingIntent);

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        return builder.build();
    }


    void addHelmetBleServiceListener(HelmetBleServiceListener listener) {
        mHelmetBleServiceListener = listener;
    }


    private void startListeningBleState() {
        mStateDisposable = mDevice.observeConnectionStateChanges()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onConnectionStateChange, this::onConnectionStateChangeFailure);
    }


    private void onConnectionStateChange(RxBleConnection.RxBleConnectionState newState) {
        Log.i(TAG, "Helmet Ble Connection State Change: " + newState.name());
        if (newState.equals(RxBleConnection.RxBleConnectionState.CONNECTED)) {
            if (mHelmetBleServiceListener != null) {
                mHelmetBleServiceListener.helmetConnected(mDevice.getMacAddress());
            }
        }
        if (newState.equals(RxBleConnection.RxBleConnectionState.DISCONNECTED) ||
                newState.equals(RxBleConnection.RxBleConnectionState.DISCONNECTING)) {
            if (mHelmetBleServiceListener != null) {
                mHelmetBleServiceListener.helmetDisconnected();
            }
        }
    }


    private void onConnectionStateChangeFailure(Throwable throwable) {
        Log.e(TAG, "Helmet Ble Connection State Failure: " + throwable.getMessage());
    }


    private Disposable mScanTimerDisposable;
    public void scanHelmet() {
        stopScan();

        mDisposableScan = mRxBleClient.scanBleDevices(
                new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                        .build(),
               new ScanFilter.Builder()
                       .setManufacturerData(0x6655, new byte[]{0x00, (byte)0xF1})
                       .build())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::filterScanResult, this::scanBleFailure);

        mScanTimerDisposable = Observable.timer(10, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Long aLong) -> {
                    if (mDisposableScan != null) {
                        mDisposableScan.dispose();
                    }
                    mHelmetBleServiceListener.onScanFinished();
                }, Throwable::printStackTrace);
    }


    private void filterScanResult(final ScanResult result) {
        Log.d(TAG, "find Ble device: " + result.getBleDevice().getMacAddress());
        mHelmetBleServiceListener.onScanResult(result);
    }


    private void scanBleFailure(Throwable t) {
        Log.e(TAG, "scan Ble failure: " + t.toString());
        stopScan();
        mHelmetBleServiceListener.onScanBleFailure();
    }


    void stopScan() {
        Log.d(TAG, "stop scan");
        if (mDisposableScan != null ) {
            mDisposableScan.dispose();
            mDisposableScan = null;
        }
        if (mScanTimerDisposable != null) {
            mScanTimerDisposable.dispose();
            mScanTimerDisposable = null;
        }
    }

    private Disposable mConnectTimerDisposable;
    public void connectHelmet(String mac) {
        Log.d(TAG, "connect helmet: " + mac);
        stopScan();
        mDevice = mRxBleClient.getBleDevice(mac);
        if (isConnected(mDevice)) {
            triggerDisconnect();
        }
        mConnectionObservable = prepareConnectionObservable();
        mConnectionDisposable =
                mConnectionObservable
                .flatMapSingle(RxBleConnection::discoverServices)
                .flatMapSingle(rxBleDeviceServices -> rxBleDeviceServices.getCharacteristic(BLE_READ_NOTIFY))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onConnectionFinished, this::onConnectionFailure);
        startListeningBleState();

        stopConnectTimer();
        mConnectTimerDisposable = Observable.timer(15, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Long along) -> {
                    Log.d(TAG, "connect timeout");
                    mHelmetBleServiceListener.helmetConnectionFailure("timeout");
                    triggerDisconnect();
                }, Throwable::printStackTrace);
    }

    private boolean isConnected(RxBleDevice device) {
        return device.getConnectionState() == RxBleConnection.RxBleConnectionState.CONNECTED;
    }


    private void stopConnectTimer() {
        if (mConnectTimerDisposable != null) {
            mConnectTimerDisposable.dispose();
            mConnectTimerDisposable = null;
        }
    }


    public void triggerDisconnect() {
        Log.d(TAG, "disconnect helmet");
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
        if (mConnectionDisposable != null) {
            mConnectionDisposable.dispose();
            mConnectionDisposable = null;
        }
        if (mStateDisposable != null) {
            mStateDisposable.dispose();
            mStateDisposable = null;
        }
        stopConnectTimer();
        mHelmetBleServiceListener.helmetDisconnected();
    }


    private Observable<RxBleConnection> prepareConnectionObservable() {
        Log.d(TAG, "prepareConnectionObservable");
        return mDevice
                .establishConnection(false)
                .takeUntil(disconnectTriggerSubject)
                .compose(ReplayingShare.instance());
    }


    private void onConnectionFinished(BluetoothGattCharacteristic characteristic) {
        Log.d(TAG, "onConnectionFinished: " + characteristic.getDescriptor(BLE_CLIENT_CHARACTERISTIC_CONFIG).toString());
        stopConnectTimer();
        setHelmetBleNotify();
    }

    private void onConnectionFailure(Throwable throwable) {
        Log.e(TAG, "onConnectionFailure: " + throwable.toString());
        stopConnectTimer();
        mHelmetBleServiceListener.helmetConnectionFailure(throwable.toString());
    }


    private void setHelmetBleNotify() {
        Log.d(TAG, "set Helmet Ble Notify");
        final Disposable disposable =
                mConnectionObservable
                .flatMap(rxBleConnection -> rxBleConnection.setupNotification(BLE_READ_NOTIFY))
                .flatMap(notificationObservable -> notificationObservable)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onNotificationReceived, this::onNotificationSetupFailure);
        mCompositeDisposable.add(disposable);
        //sendBleCommand(HelmetBleMessage.getHelmetModelNumber());
    }


    private void onNotificationReceived(byte[] bytes) {
        final StringBuilder stringBuilder = new StringBuilder(bytes.length);
        for(byte byteChar : bytes) {
            stringBuilder.append(String.format("%02X ", byteChar));
        }
        Log.d(TAG, "helmet notification received: " + stringBuilder.toString());
        if (mHelmetBleServiceListener != null) {
            mHelmetBleServiceListener.helmetMessageReceived(bytes);
        }
    }


    private void onNotificationSetupFailure(Throwable throwable) {
        Log.e(TAG, "helmet notification setup failure: " + throwable.toString());
    }



    public void sendBleCommand(@NonNull byte[] command) {
        final StringBuilder stringBuilder = new StringBuilder(command.length);
        for(byte byteChar : command) {
            stringBuilder.append(String.format("%02X ", byteChar));
        }
        Log.d(TAG, "send ble command: " + stringBuilder.toString());
        if (mConnectionDisposable != null && mCompositeDisposable != null) {
            final Disposable disposable = mConnectionObservable
                    .firstOrError()
                    .flatMap(rxBleConnection -> rxBleConnection.writeCharacteristic(BLE_WRITE, command))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            bytes -> onSendBleCommandSuccess(),
                            this::onSendBleCommandFailure
                    );
            mCompositeDisposable.add(disposable);
        }
    }


    private void onSendBleCommandSuccess() {
        Log.d(TAG, "send ble command success");
    }


    private void onSendBleCommandFailure(Throwable throwable) {
        Log.e(TAG, "send ble command failure: " + throwable.toString());
    }


    public interface HelmetBleServiceListener {
        void helmetConnected(String mac);
        void helmetDisconnected();
        void helmetConnectionFailure(String error);
        void helmetMessageReceived(byte[] msg);
        void onScanResult(final ScanResult result);
        void onScanBleFailure();
        void onScanFinished();
    }
}
