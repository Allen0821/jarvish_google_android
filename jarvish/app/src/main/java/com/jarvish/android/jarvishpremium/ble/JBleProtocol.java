package com.jarvish.android.jarvishpremium.ble;

import android.util.Log;

import androidx.annotation.NonNull;

import com.jarvish.android.jarvishpremium.helmet.SensorFusionData;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class JBleProtocol {
    private static final String TAG = "JBleProtocol";
    public static final int MSG_ID_WAKE_ON_EVENT = 0x2A00;

    // WIFI related
    // WIFI outgoing message
    public static final int MSG_ID_WIFI_ON_OFF_REQUEST = 0x2A04;
    public static final int MSG_ID_WIFI_AP_SSID = 0x2A08;
    // WIFI incoming message
    public static final int MSG_ID_WIFI_READY_NOTIFY = 0x2A05;
    public static final int MSG_ID_WIFI_AP_SSID_NOTIFY = 0x2A09;


    public static final int MSG_ID_LOCATION_AND_SPEED = 0x2A0B;
    // incoming message
    public static final int MSG_ID_GYROSCOPE = 0x2A0D;
    public static final int MSG_ID_ACCELEROMETER = 0x2A0E;
    public static final int MSG_ID_MAGNETOMETER = 0x2A1E;
    public static final int MSG_ID_SENSOR_FUSION = 0x2A1D;
    // IO message
    public static final int MSG_ID_BATTERY_LEVEL = 0x2A0C;
    public static final int MSG_ID_MODEL_NUMBER = 0x2A02;
    static final int MSG_ID_FIRMWARE_VERSION = 0x2A03;

    public static final int MSG_ID_HELMET_NAME = 0x2A1B;

    public static final int MSG_ID_TAIL_LIGHT = 0x2A13;
    //public static final int MSG_ID_PROXIMITY = 0x2A17;


    static final int MSG_ID_VIDEO_TIMESTAMP = 0x2A20;
    static final int MSG_ID_VIDEO_RESOLUTION = 0x2A14;
    static final int MSG_ID_VIDEO_RECORDING_TIME = 0x2A21;

    static final int MSG_ID_GET_VIDEO_SETTINGS = 0x2A22;

    static final int MSG_ID_BATTERY_EFFICIENCY = 0x2A23;

    static final int MSG_ID_SYS_DATE_TIME  = 0x2A01;



    //public static final int MSG_ID_HELMET_INFO = 0x2A1D;

    // ICP related
    // ICP outgoing
    public static final int MSG_ID_ICP_SERVICE_REQUEST = 0x2A17;
    public static final int MSG_ID_ICP_CONNECT_REQUEST = 0x2A19;
    // ICP incoming
    public static final int MSG_ID_ICP_PAIRING_LIST = 0x2A18;
    public static final int MSG_ID_ICP_CONNECT_COMPLETED = 0x2A1A;
    // ICP IO message
    public static final int MSG_ID_ICP_REMOTE_NAME = 0x2A1B;

    public static final int MSG_ID_RESET_DEFAULT = 0x2A11;


    public static final int MSG_ID_ALEXA_WAKE_ON_EVENT = 0x2A1C;

    static final int MSG_ID_DIRECTION = 0x2A20;

    static final int MSG_ID_ACK = 0x2AFF;



    public static int getHeaderId(byte[] bytes) {
        byte[] header = Arrays.copyOf(bytes, 2);
        ByteBuffer wrapped = ByteBuffer.wrap(header);
        return wrapped.getShort();
    }


    static int getAckIndex(byte[] bytes) {
        byte[] ack = Arrays.copyOfRange(bytes, 3, 5);
        ByteBuffer wrapped = ByteBuffer.wrap(ack);
        return wrapped.getShort();
    }


    static boolean getAckValue(byte[] bytes) {
        int n = Byte.valueOf(bytes[5]).intValue();
        return n == 1;
    }


    private static byte[] buildGetValueCmd(int index) {
        byte[] bytes = new byte[2];
        bytes[0] = (byte)((index >> 8) & 0xFF);
        bytes[1] = (byte)(index & 0xFF);
        return bytes;
    }


    private static byte[] buildGetValueCmd(int index, int size) {
        byte[] bytes = new byte[4];
        bytes[0] = (byte)((index >> 8) & 0xFF);
        bytes[1] = (byte)(index & 0xFF);
        bytes[2] = (byte) size;
        bytes[3] = 0;
        return bytes;
    }


    private static byte[] buildOnOffCmd(int index, int value) {
        byte[] bytes = new byte[5];
        bytes[0] = (byte)((index >> 8) & 0xFF);
        bytes[1] = (byte)(index & 0xFF);
        bytes[2] = (byte) 2;
        bytes[3] = (byte) 1;
        bytes[4] = (byte)value;
        return bytes;
    }


    private static byte[] buildSetValueCmd(int index, int value) {
        byte[] bytes = new byte[4];
        bytes[0] = (byte)((index >> 8) & 0xFF);
        bytes[1] = (byte)(index & 0xFF);
        bytes[2] = 1;
        bytes[3] = (byte) value;
        return bytes;
    }


    @NonNull
    static byte[] getSerialNumber() {
        return buildGetValueCmd(MSG_ID_MODEL_NUMBER);
    }


    @NonNull
    static byte[] getFirmwareVersion() {
        return buildGetValueCmd(MSG_ID_FIRMWARE_VERSION);
    }


    @NonNull
    static byte[] getVideoSetting() {
        return buildGetValueCmd(MSG_ID_GET_VIDEO_SETTINGS);
    }


    @NonNull
    static byte[] getBatteryLevel() {
        return buildGetValueCmd(MSG_ID_BATTERY_LEVEL);
    }


    @NonNull
    static byte[] getTailLight() {
        return buildGetValueCmd(MSG_ID_TAIL_LIGHT, 2);
    }


    @NonNull
    static byte[] setTailLight(boolean enable) {
        int v = enable ? 0 : 1;
        return buildOnOffCmd(MSG_ID_TAIL_LIGHT, v);
    }


    @NonNull
    static byte[] getBatteryEfficiency() {
        return buildGetValueCmd(MSG_ID_BATTERY_EFFICIENCY, 2);
    }


    @NonNull
    static byte[] setBatteryEfficiency(int value) {
        int v = value == 0 ? 0 : 1;
        return buildOnOffCmd(MSG_ID_BATTERY_EFFICIENCY, v);
    }


    @NonNull
    static byte[] setVideoTimestamp(boolean value) {
        int v = value ? 0 : 1;
        return buildSetValueCmd(MSG_ID_VIDEO_TIMESTAMP, v);
    }


    @NonNull
    static byte[] setVideoRecordingTime(int value) {
        int v;
        switch (value) {
            case 1:
                v = 1;
                break;
            case 2:
                v = 2;
                break;
            default:
                v = 0;
        }
        return buildSetValueCmd(MSG_ID_VIDEO_RECORDING_TIME, v);
    }


    @NonNull
    static byte[] setVideoResolution(int value) {
        int v;
        switch (value) {
            case 1:
                v = 1;
                break;
            case 2:
                v = 2;
                break;
            default:
                v = 0;
        }
        return buildSetValueCmd(MSG_ID_VIDEO_RESOLUTION, v);
    }


    @NonNull
    static byte[] resetDefault() {
        return buildSetValueCmd(MSG_ID_RESET_DEFAULT, 1);
    }


    /**
     * ##### sync helmet time with connected device #####
     * 1. @year: year of connected device.
     * 2. @mon: month of connected device.
     * 3. @day: day of connected device.
     * 4. @hr: hour of connected device.
     * 5. @min: minute of connected device.
     * 6. @sec: second of connected device.
     */
    @NonNull
    static byte[] setSystemTime(int year, int mon, int day, int hr, int min, int sec) {
        Log.d(TAG, "year " + year);
        Log.d(TAG, "mon " + mon);
        Log.d(TAG, "day " + day);
        Log.d(TAG, "hr " + hr);
        Log.d(TAG, "min " + min);
        Log.d(TAG, "sec " + sec);

        int n = MSG_ID_SYS_DATE_TIME;
        byte[] ret = new byte[11];
        ret[0] = (byte)((n >> 8) & 0xFF);
        ret[1] = (byte)(n & 0xFF);
        ret[2] = 8;
        ret[3] = 1;
        ret[4] = (byte)((year >> 8) & 0xFF);
        ret[5] = (byte)(year & 0xFF);
        ret[6] = (byte)(mon & 0xFF);
        ret[7] = (byte)(day & 0xFF);
        ret[8] = (byte)(hr & 0xFF);
        ret[9] = (byte)(min & 0xFF);
        ret[10] = (byte)(sec & 0xFF);
        return ret;
    }


    static byte[] setDirectionStep(int direction) {
        Log.d(TAG, "setDirectionStep: " + direction);
        int n = MSG_ID_DIRECTION;
        byte[] ret = new byte[4];
        ret[0] = (byte)((n >> 8) & 0xFF);
        ret[1] = (byte)((n) & 0xFF);
        ret[2] = (byte)((1) & 0xFF);
        ret[3] = (byte)(direction);
        final StringBuilder stringBuilder = new StringBuilder(ret.length);
        for(byte byteChar : ret) {
            stringBuilder.append(String.format("%02X ", byteChar));
        }
        Log.d(TAG, "setDirectionStep: " + stringBuilder.toString());
        return ret;
    }


    static String getBleStringDate(byte[] data) {
        try {
            if(data.length <= 2) {
                return "";
            }
            byte[] number = Arrays.copyOfRange(data, 3, data.length);
            final StringBuilder stringBuilder = new StringBuilder(number.length);
            for(byte byteChar : number) {
                stringBuilder.append(String.format("%02X ", byteChar));
            }
            Log.d(TAG, "number data: " + stringBuilder.toString());
            return new String(number, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    static float bytesToFloat(byte[] b) {
        return ByteBuffer.wrap(b).getFloat();
    }


    static int parseBatteryLevel(byte[] data) {
        return data[3];
    }


    static boolean parseTailLight(byte[] data) {
        return data[3] == 0;
    }


    static int paresGetParameter(byte[] data) {
        int ret = 0;
        ret = (ret << 8) | data[3];
        return ret;
    }


    static int parseVideoResolution(byte[] data) {
        return data[3];
    }


    static boolean parseVideoTimestamp(byte[] data) {
        return data[4] == 0;
    }


    static int parseVideoRecordingTime(byte[] data) {
        return data[5];
    }


    /*public static int convertToString(byte[] b) {
        final StringBuilder stringBuilder = new StringBuilder(b.length);
        for(byte byteChar : b) {
            stringBuilder.append(String.format("%02X ", byteChar));
        }
        Log.i(TAG, "convertToString: " + stringBuilder.toString());
        try {
            String str = new String(b, StandardCharsets.UTF_8);
            Log.i(TAG, "str: " + str);
            Gson gson = new Gson();
            HelmetInfo in = gson.fromJson(str, HelmetInfo.class);
            Log.i(TAG, "Serial: " + in.getSerialNumber());
            Log.i(TAG, "Firmware: " + in.getFirmwareVersion());
            Log.i(TAG, "ICP: " + in.getIcpName());
            Log.i(TAG, "Battery: " + in.getBatteryLevel());
            Log.i(TAG, "Tail: " + in.isTailLight());
            Log.i(TAG, "Proximity: " + in.isProximitySensor());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }*/



    /**
     * ##### turn on/off Sensor Fusion Data #####
     * 1. @enable: true=>turn on, false=>turn off.
     * 2. @interval: the interval of receiving sensor fusion data,
     *                  the unit is millisecond , the minimum interval is 200ms.
     */
    public static byte[] setSensorFusionData(boolean enable, int interval) {
        Log.d(TAG, "set Sensor Fusion Data");
        byte[] command;
        if (enable) {
            if (interval < 2) {
                interval = 2;
            }
            command = convertBleCommand(
                    JBleProtocol.MSG_ID_SENSOR_FUSION,
                    (byte) (2 & 0xFF),
                    (byte) (1 & 0xFF),
                    (byte) (interval & 0xFF));
        } else {
            command = convertBleCommand(
                    JBleProtocol.MSG_ID_SENSOR_FUSION,
                    (byte) (2 & 0xFF),
                    (byte) 0,
                    (byte) 0);
        }
        return command;
    }



    private static byte[] convertBleCommand(int id, byte length, byte flag, byte data) {
        byte[] ret = new byte[5];
        ret[0] = (byte)((id >> 8) & 0xFF);
        ret[1] = (byte)(id & 0xFF);
        ret[2] = length;
        ret[3] = flag;
        ret[4] = data;
        return ret;
    }


    public static SensorFusionData parseSensorFusionData(byte[] bytes) {
        SensorFusionData sensor = new SensorFusionData();
        byte[] data = Arrays.copyOfRange(bytes, 3, bytes.length);
        sensor.setX(JBleProtocol.bytesToFloat(Arrays.copyOfRange(data, 0, 4)));
        sensor.setY(JBleProtocol.bytesToFloat(Arrays.copyOfRange(data, 4, 8)));
        sensor.setZ(JBleProtocol.bytesToFloat(Arrays.copyOfRange(data, 8, 12)));
        return sensor;
    }

}
