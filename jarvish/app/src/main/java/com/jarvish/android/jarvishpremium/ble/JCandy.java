package com.jarvish.android.jarvishpremium.ble;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;


public class JCandy {
    private static final String TAG = "Identify";

    /* helmet style id */
    public static final String HELMET_RAIDEN_EVOS = "01";
    public static final String HELMET_FLASH_1 = "02";
    public static final String HELMET_F2_NEW_MIC = "03";
    public static final String HELMET_RAIDEN_R1_NEW_MIC = "04";
    public static final String HELMET_XTREME_X1_FOR_NEW_MIC = "05";
    public static final String HELMET_MONACO_EVO_S2 = "06";
    public static final String HELMET_A2 = "07";
    public static final String HELMET_X = "08";
    public static final String HELMET_X_AR = "09";
    public static final String HELMET_AT5 = "10";
    public static final String HELMET_VINTAGE = "11";
    public static final String HELMET_ANC_MIC_X_M26 = "12";
    public static final String HELMET_ANC_MIC_FX_M26 = "13";


    /* helmet style name */
    public static final String HELMET_NAME_RAIDEN_EVOS = "Monaco Evo S";
    public static final String HELMET_NAME_FLASH_1 = "FLASH F1";
    public static final String HELMET_NAME_F2_NEW_MIC = "FLASH F2";
    public static final String HELMET_NAME_RAIDEN_R1_NEW_MIC = "RAIDEN R1";
    public static final String HELMET_NAME_XTREME_X1_FOR_NEW_MIC = "XTREME X1";
    public static final String HELMET_NAME_MONACO_EVO_S2 = "Monaco Evo S2";
    public static final String HELMET_NAME_A2 = "A2";
    public static final String HELMET_NAME_X = "JARVISH X";
    public static final String HELMET_NAME_X_AR = "JARVISH X-AR";
    public static final String HELMET_NAME_AT5 = "AT-KIT";
    public static final String HELMET_NAME_VINTAGE = "AN-KIT";
    public static final String HELMET_NAME_ANC_MIC_X_M26 = "JARVISH X";
    public static final String HELMET_NAME_ANC_MIC_FX_M26 = "FLASH X";


    public static final int RESOLUTION_M1 = 0;
    public static final int RESOLUTION_M2526 = 1;


    /*public static Drawable getHelmetPhoto(Context context, String type, String color) {
        Log.d(TAG, "color " + color);
        switch (type) {
            case JCandy.HELMET_RAIDEN_EVOS:
            case JCandy.HELMET_MONACO_EVO_S2:
                switch (color) {
                    case "01":
                        return ContextCompat.getDrawable(context, R.drawable.mes01);
                    case "02":
                        return ContextCompat.getDrawable(context, R.drawable.mes02);
                    case "03":
                        return ContextCompat.getDrawable(context, R.drawable.mes03);
                    case "04":
                        return ContextCompat.getDrawable(context, R.drawable.mes04);
                    case "05":
                        return ContextCompat.getDrawable(context, R.drawable.mes05);
                    case "06":
                        return ContextCompat.getDrawable(context, R.drawable.mes06);
                }
            case JCandy.HELMET_FLASH_1:
                switch (color) {
                    case "07":
                        return ContextCompat.getDrawable(context, R.drawable.f107);
                    case "08":
                        return ContextCompat.getDrawable(context, R.drawable.f108);
                    case "09":
                        return ContextCompat.getDrawable(context, R.drawable.f109);
                    case "10":
                        return ContextCompat.getDrawable(context, R.drawable.f110);
                    case "11":
                        return ContextCompat.getDrawable(context, R.drawable.f111);
                    case "12":
                        return ContextCompat.getDrawable(context, R.drawable.f112);
                    case "13":
                        return ContextCompat.getDrawable(context, R.drawable.f113);
                    case "14":
                        return ContextCompat.getDrawable(context, R.drawable.f114);
                    case "15":
                        return ContextCompat.getDrawable(context, R.drawable.f115);
                }
            case JCandy.HELMET_F2_NEW_MIC:
                switch (color) {
                    case "07":
                        return ContextCompat.getDrawable(context, R.drawable.f107);
                    case "14":
                        return ContextCompat.getDrawable(context, R.drawable.f114);
                }
            case JCandy.HELMET_RAIDEN_R1_NEW_MIC:
                switch (color) {
                    case "19":
                        return ContextCompat.getDrawable(context, R.drawable.r119);
                    case "21":
                        return ContextCompat.getDrawable(context, R.drawable.r121);
                }
            case JCandy.HELMET_XTREME_X1_FOR_NEW_MIC:
                return ContextCompat.getDrawable(context, R.drawable.x120);
            case JCandy.HELMET_A2:
                return ContextCompat.getDrawable(context, R.drawable.a299);
            case JCandy.HELMET_X:
                switch (color) {
                    case "01":
                        return ContextCompat.getDrawable(context, R.drawable.x01);
                    case "02":
                        return ContextCompat.getDrawable(context, R.drawable.x02);
                }
            case JCandy.HELMET_AT5:
                return ContextCompat.getDrawable(context, R.drawable.at599);
            case JCandy.HELMET_X_AR:
                return ContextCompat.getDrawable(context, R.drawable.xar01);
            case JCandy.HELMET_VINTAGE:
                return ContextCompat.getDrawable(context, R.drawable.vintage99);
            case JCandy.HELMET_ANC_MIC_X_M26:
                switch (color) {
                    case "01":
                        return ContextCompat.getDrawable(context, R.drawable.x01);
                    case "02":
                        return ContextCompat.getDrawable(context, R.drawable.x02);
                }
            case JCandy.HELMET_ANC_MIC_FX_M26:
                switch (color) {
                    case "07":
                        return ContextCompat.getDrawable(context, R.drawable.f107);
                    case "14":
                        return ContextCompat.getDrawable(context, R.drawable.f114);
                }
            default:
                return ContextCompat.getDrawable(context, R.drawable.a299);
        }
    }*/


    public static String getHelmetName(String type) {
        switch (type) {
            case JCandy.HELMET_A2:
                return JCandy.HELMET_NAME_A2;
            case JCandy.HELMET_AT5:
                return JCandy.HELMET_NAME_AT5;
            case JCandy.HELMET_F2_NEW_MIC:
                return JCandy.HELMET_NAME_F2_NEW_MIC;
            case JCandy.HELMET_FLASH_1:
                return JCandy.HELMET_NAME_FLASH_1;
            case JCandy.HELMET_MONACO_EVO_S2:
                return JCandy.HELMET_NAME_MONACO_EVO_S2;
            case JCandy.HELMET_RAIDEN_EVOS:
                return JCandy.HELMET_NAME_RAIDEN_EVOS;
            case JCandy.HELMET_RAIDEN_R1_NEW_MIC:
                return JCandy.HELMET_NAME_RAIDEN_R1_NEW_MIC;
            case JCandy.HELMET_XTREME_X1_FOR_NEW_MIC:
                return JCandy.HELMET_NAME_XTREME_X1_FOR_NEW_MIC;
            case JCandy.HELMET_X:
                return JCandy.HELMET_NAME_X;
            case JCandy.HELMET_X_AR:
                return JCandy.HELMET_NAME_X_AR;
            case JCandy.HELMET_VINTAGE:
                return JCandy.HELMET_NAME_VINTAGE;
            case JCandy.HELMET_ANC_MIC_X_M26:
                return JCandy.HELMET_NAME_ANC_MIC_X_M26;
            case JCandy.HELMET_ANC_MIC_FX_M26:
                return JCandy.HELMET_NAME_ANC_MIC_FX_M26;
            default:
                return "";
        }
    }


    public static JProduct identifyProduct(Context context, String serial)  {
        String n = serial;
        if (n == null || n.length() != 13) {
            n = context.getSharedPreferences("JARVISH_BLE", Context.MODE_PRIVATE)
                    .getString("SERIAL_NUMBER", "");
        }

        if (n == null || n.length() != 13) {
            return null;
        }

        return new JProduct(n);
    }


    public static int getVideoResolutionCapacity(String productZza) {
        if (productZza.equals(HELMET_RAIDEN_EVOS) ||
                productZza.equals(HELMET_FLASH_1) ||
                productZza.equals(HELMET_F2_NEW_MIC) ||
                productZza.equals(HELMET_RAIDEN_R1_NEW_MIC) ||
                productZza.equals(HELMET_XTREME_X1_FOR_NEW_MIC) ||
                productZza.equals(HELMET_MONACO_EVO_S2)) {
            return RESOLUTION_M1;
        } else {
            return RESOLUTION_M2526;
        }
    }

    public static String getYourHelmet(String type, String color) {
        Log.d(TAG, "type " + type + ", color " + color);
        switch (type) {
            case JCandy.HELMET_RAIDEN_EVOS:
            case JCandy.HELMET_MONACO_EVO_S2:
                switch (color) {
                    case "02":
                        return "mes02";
                    case "03":
                        return "mes03";
                    case "04":
                        return "mes04";
                    case "05":
                        return "mes05";
                    case "06":
                        return "mes06";
                    default:
                        return "mes01";
                }
            case JCandy.HELMET_FLASH_1:
                switch (color) {
                    case "08":
                        return "f108";
                    case "09":
                        return "f109";
                    case "10":
                        return "f110";
                    case "11":
                        return "f111";
                    case "12":
                        return "f112";
                    case "13":
                        return "f113";
                    case "14":
                        return "f114";
                    case "15":
                        return "f115";
                    default:
                        return "f107";
                }
            case JCandy.HELMET_F2_NEW_MIC:
            case JCandy.HELMET_ANC_MIC_FX_M26:
                if ("14".equals(color)) {
                    return "f114";
                }
                return "f107";
            case JCandy.HELMET_RAIDEN_R1_NEW_MIC:
                if ("21".equals(color)) {
                    return "r121";
                }
                return "r119";
            case JCandy.HELMET_XTREME_X1_FOR_NEW_MIC:
                return "x120";
            case JCandy.HELMET_A2:
                return "a299";
            case JCandy.HELMET_AT5:
                return "at599";
            case JCandy.HELMET_X_AR:
                return "xar01";
            case JCandy.HELMET_VINTAGE:
                return "vintage99";
            case JCandy.HELMET_X:
            case JCandy.HELMET_ANC_MIC_X_M26:
                if ("02".equals(color)) {
                    return "x02";
                }
                return "x01";
            default:
                return "x01";
        }
    }
}
