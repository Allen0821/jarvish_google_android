package com.jarvish.android.jarvishpremium.ble;

import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

public class JHelmet {
    /* product identification */
    private JProduct product;

    private String name;
    private String photo;

    /* Helmet settings */
    //private boolean proximity;
    private boolean tailLight;
    private int batteryEfficiency;
    private int videoResolution;
    private int videoRecordingTime;
    private boolean videoTimestamp;

    private String firmwareVersion;


    public JHelmet() {
        this.tailLight = false;
        this.batteryEfficiency = 0;
        this.videoResolution = 0;
        this.videoRecordingTime = 0;
        this.videoTimestamp = false;
    }


    public JProduct getProduct() {
        return product;
    }

    public void setProduct(JProduct product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public boolean isTailLight() {
        return tailLight;
    }

    public void setTailLight(boolean tailLight) {
        this.tailLight = tailLight;
    }

    public int getBatteryEfficiency() {
        return batteryEfficiency;
    }

    public void setBatteryEfficiency(int batteryEfficiency) {
        this.batteryEfficiency = batteryEfficiency;
    }

    public int getVideoResolution() {
        return videoResolution;
    }

    public void setVideoResolution(int videoResolution) {
        this.videoResolution = videoResolution;
    }

    public int getVideoRecordingTime() {
        return videoRecordingTime;
    }

    public void setVideoRecordingTime(int videoRecordingTime) {
        this.videoRecordingTime = videoRecordingTime;
    }

    public boolean isVideoTimestamp() {
        return videoTimestamp;
    }

    public void setVideoTimestamp(boolean videoTimestamp) {
        this.videoTimestamp = videoTimestamp;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }
}
