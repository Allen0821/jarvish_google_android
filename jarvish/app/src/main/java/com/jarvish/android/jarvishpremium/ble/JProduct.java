/**
 * Created by CupLidSheep on 22,September,2020
 */
package com.jarvish.android.jarvishpremium.ble;

public class JProduct {
    private final String serial;
    private final String year;
    private final String language;
    private final String productType;
    private final String productColor;
    private final String productSize;
    private final String manufacturerNumber;
    private final String verifyCode;


    public JProduct(String serial) {
        this.serial = serial;
        this.year = serial.substring(0, 1);
        this.language = serial.substring(1, 3);
        this.productType = serial.substring(3, 5);
        this.productColor = serial.substring(5, 7);
        this.productSize = serial.substring(7, 8);
        this.manufacturerNumber = serial.substring(8, 12);
        this.verifyCode = serial.substring(12, 13);
    }


    public String getSerial() {
        return serial;
    }

    public String getYear() {
        return year;
    }

    public String getLanguage() {
        return language;
    }

    public String getProductType() {
        return productType;
    }

    public String getProductColor() {
        return productColor;
    }

    public String getProductSize() {
        return productSize;
    }

    public String getManufacturerNumber() {
        return manufacturerNumber;
    }

    public String getVerifyCode() {
        return verifyCode;
    }
}
