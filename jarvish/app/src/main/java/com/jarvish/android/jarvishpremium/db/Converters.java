package com.jarvish.android.jarvishpremium.db;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Converters {

    @TypeConverter
    public static ArrayList<GPSLog> toGPSLogArrayList(String value) {
        Type listType = new TypeToken<ArrayList<GPSLog>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }
    @TypeConverter
    public static String fromGPSLogArrayList(ArrayList<GPSLog> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }


    @TypeConverter
    public static GPSLog toGPSLog(String value) {
        Type type = new TypeToken<GPSLog>() {}.getType();
        return new Gson().fromJson(value, type);
    }
    @TypeConverter
    public static String fromGPSLog(GPSLog gpsLog) {
        Gson gson = new Gson();
        return gson.toJson(gpsLog);
    }


    @TypeConverter
    public static ArrayList<String> toStringArrayList(String value) {
        Type listType = new TypeToken<ArrayList<String>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromStringArrayList(ArrayList<String> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }
}
