package com.jarvish.android.jarvishpremium.db;

public class GPSLog {
    private double latitude;
    private double longitude;
    private double altitude;
    private double speed;
    private long timestamp;


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double distance(GPSLog a, GPSLog b) {
        float pk = (float) (180.f/Math.PI);

        float a1 = (float)a.latitude / pk;
        float a2 = (float)a.longitude / pk;
        float b1 = (float)b.latitude / pk;
        float b2 = (float)b.longitude / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }

    private double ConvertDegreeToRadians(double degrees) {
        return (Math.PI/180) * degrees;
    }
}
