package com.jarvish.android.jarvishpremium.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.ArrayList;

@Entity
public class GPSTrack {
    @PrimaryKey
    private long gpsTrackId;

    @ColumnInfo(name = "gps_track")
    @TypeConverters(Converters.class)
    private ArrayList<GPSLog> gpsTrack;


    public long getGpsTrackId() {
        return gpsTrackId;
    }

    public void setGpsTrackId(long gpsTrackId) {
        this.gpsTrackId = gpsTrackId;
    }

    public ArrayList<GPSLog> getGpsTrack() {
        return gpsTrack;
    }

    public void setGpsTrack(ArrayList<GPSLog> gpsTrack) {
        this.gpsTrack = gpsTrack;
    }
}
