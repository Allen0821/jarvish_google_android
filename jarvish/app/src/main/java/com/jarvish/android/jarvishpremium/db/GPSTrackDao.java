package com.jarvish.android.jarvishpremium.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface GPSTrackDao {
    @Query("SELECT * FROM GPSTrack")
    Flowable<List<GPSTrack>> getAllGPSTrack();


    @Query("SELECT * FROM GPSTrack WHERE gpsTrackId = :gpsTrackId")
    Maybe<GPSTrack> getGPSTrackById(long gpsTrackId);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Maybe<Long> insertGPSTrack(GPSTrack gpsTrack);


    @Update
    void updateGPSTrack(GPSTrack... gpsTracks);


    @Delete
    void deleteGPSTrack(GPSTrack gpsTrack);
}
