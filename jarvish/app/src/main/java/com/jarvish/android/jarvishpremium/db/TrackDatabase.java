package com.jarvish.android.jarvishpremium.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {TrackLog.class, GPSTrack.class}, version = 3, exportSchema = false)
public abstract class TrackDatabase extends RoomDatabase {
    private static volatile TrackDatabase INSTANCE;

    public abstract TrackLogDao trackLogDao();
    public abstract GPSTrackDao gpsTrackDao();
    //public abstract CheckInTrackDao checkInTrackDao();

    public static TrackDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (TrackDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TrackDatabase.class, "track.db")
                            .addMigrations(migration1to2)
                            .addMigrations(migration1to3)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static final Migration migration1to2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE TrackLog ADD COLUMN weather_temperature TEXT");
            database.execSQL("ALTER TABLE TrackLog ADD COLUMN weather_icon INTEGER DEFAULT -10 not null");
            database.execSQL("ALTER TABLE TrackLog ADD COLUMN weather_description TEXT");
        }
    };

    private static final Migration migration1to3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("DROP TABLE CheckInTrack");
            database.execSQL("ALTER TABLE TrackLog ADD COLUMN helmet TEXT");
        }
    };
}
