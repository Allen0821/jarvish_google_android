package com.jarvish.android.jarvishpremium.db;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TrackLog {
    @PrimaryKey(autoGenerate = true)
    private long trackId;

    @ColumnInfo(name = "track_type")
    private int trackType;

    private String title;

    private String content;

    private String cover;

    @ColumnInfo(name = "track_distance")
    private double distance;

    @ColumnInfo(name = "track_duration")
    private long duration;

    @ColumnInfo(name = "average_speed")
    private double averageSpeed;

    @ColumnInfo(name = "maximum_speed")
    private double maxSpeed;

    private long gpsTrackId;

    private long timestamp;

    @ColumnInfo(name = "weather_temperature")
    private String weaTemp;

    @ColumnInfo(name = "weather_icon")
    private int weaIcon;

    @ColumnInfo(name = "weather_description")
    private String weaDesc;

    @ColumnInfo(name = "helmet")
    private String helmet;


    public long getTrackId() {
        return trackId;
    }

    public void setTrackId(long trackId) {
        this.trackId = trackId;
    }


    public int getTrackType() {
        return trackType;
    }

    public void setTrackType(int trackType) {
        this.trackType = trackType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public long getGpsTrackId() {
        return gpsTrackId;
    }

    public void setGpsTrackId(long gpsTrackId) {
        this.gpsTrackId = gpsTrackId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getWeaTemp() {
        return weaTemp;
    }

    public void setWeaTemp(String weaTemp) {
        this.weaTemp = weaTemp;
    }

    public int getWeaIcon() {
        return weaIcon;
    }

    public void setWeaIcon(int weaIcon) {
        this.weaIcon = weaIcon;
    }

    public String getWeaDesc() {
        return weaDesc;
    }

    public void setWeaDesc(String weaDesc) {
        this.weaDesc = weaDesc;
    }

    public String getHelmet() {
        return helmet;
    }

    public void setHelmet(String helmet) {
        this.helmet = helmet;
    }
}
