package com.jarvish.android.jarvishpremium.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public interface TrackLogDao {
    @Query("SELECT * FROM TrackLog")
    Flowable<List<TrackLog>> getAllTrackings();


    @Query("SELECT * FROM TrackLog WHERE trackId = :trackId")
    Maybe<TrackLog> getTrackById(int trackId);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Maybe<Long> insertTrack(TrackLog track);


    @Update
    Single<Integer> updateTrack(TrackLog trackLogs);


    @Delete
    void deleteTrack(TrackLog trackLog);
}
