package com.jarvish.android.jarvishpremium.firestore

import com.google.firebase.firestore.ServerTimestamp
import java.text.SimpleDateFormat
import java.util.*

data class Account(
        val uid: String = "",
        var email: String? = null,
        val emailVerified: Boolean = false,
        var elderId: String? = null,
        var elder: Boolean = false,
        val haveProduct: Boolean = false,
        @ServerTimestamp val created: Date? = null) {


    companion object {

        fun createdFormat(date: Date): String {
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            return format.format(date)
        }
    }
}
