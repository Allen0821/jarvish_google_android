package com.jarvish.android.jarvishpremium.firestore

import com.google.firebase.firestore.ServerTimestamp
import java.io.Serializable
import java.util.*


data class Profile (
        val uid: String = "",
        var name: String? = "",
        var avatarUrl: String? = null,
        val level: Int = 0,
        @ServerTimestamp val created: Date? = null): Serializable {


}