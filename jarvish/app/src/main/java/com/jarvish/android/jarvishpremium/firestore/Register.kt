package com.jarvish.android.jarvishpremium.firestore

import java.util.*
import com.google.firebase.firestore.ServerTimestamp
import java.text.SimpleDateFormat

data class Register(
        val serial: String = "",
        val uid: String = "",
        val name: String = "",
        val category: Int = 0,
        val expired: Boolean = false,
        @ServerTimestamp val date: Date? = null) {

    companion object {

        fun registeredDate(date: Date?): String {
            date?.let {
                val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                return format.format(it)
            }
            return ""
        }

        fun isValidSerial(serial: String?): Boolean {
            return serial != null && serial.length == 13 && serial.matches(Regex("^[0-9]*$"))
        }
    }
}

