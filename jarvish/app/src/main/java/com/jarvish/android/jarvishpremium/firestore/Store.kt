package com.jarvish.android.jarvishpremium.firestore


import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

abstract class Store<T> {

    val db = Firebase.firestore

    abstract fun createDocument(data: T, completed: (result: Boolean) -> Unit)
    abstract fun createDocumentWithID(documentID: String, data: T,  completed: (result: Boolean) -> Unit)

    abstract fun updateDocument(documentID: String, data: HashMap<String, Any?>, completed: (result: Boolean) -> Unit)

    abstract fun getDocument(documentID: String, completed: (data: T?) -> Unit)
    //abstract fun deleteDocument(path: String, data: Any)


    companion object {
        const val profilePath = "profile"
        const val accountPath = "account"
        const val registerPath = "register"
    }
}