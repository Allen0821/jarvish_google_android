package com.jarvish.android.jarvishpremium.firestore

import android.util.Log
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ktx.toObject

private const val TAG = "StoreAccount"

class StoreAccount: Store<Account>() {
    override fun createDocument(data: Account, completed: (result: Boolean) -> Unit) {

    }

    override fun createDocumentWithID(documentID: String, data: Account, completed: (result: Boolean) -> Unit) {
    }

    override fun updateDocument(documentID: String, data: HashMap<String, Any?>, completed: (result: Boolean) -> Unit) {
        db.collection(accountPath).document(documentID)
                .update(data)
                .addOnSuccessListener { completed(true) }
                .addOnFailureListener {
                    it.printStackTrace()
                    completed(false)
                }
    }

    override fun getDocument(documentID: String, completed: (data: Account?) -> Unit) {
        db.collection(accountPath).document(documentID)
                .get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val account = document.toObject<Account>()
                        completed(account)
                    } else {
                        completed(null)
                    }
                }
                .addOnFailureListener {
                    it.printStackTrace()
                    completed(null)
                }

    }

    /*fun listenAccountUpdate(documentID: String, completed: (data: Account?) -> Unit) {
        db.collection(accountPath).document(documentID)
                .addSnapshotListener { snapshot, e ->
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e)
                        return@addSnapshotListener
                    }

                    if (snapshot != null && snapshot.exists()) {
                        val account = snapshot.toObject<Account>()
                        Log.d(TAG, "Current data: $account")
                        completed(account)
                    } else {
                        Log.d(TAG, "Current data: null")
                    }
                }
    }*/

}