package com.jarvish.android.jarvishpremium.firestore

import com.google.firebase.firestore.ktx.toObject


class StoreProfile: Store<Profile>() {

    override fun createDocument(data: Profile, completed: (result: Boolean) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun createDocumentWithID(documentID: String, data: Profile, completed: (result: Boolean) -> Unit) {
        db.collection(profilePath).document(documentID)
                .set(data)
                .addOnSuccessListener {
                    completed(true)
                }
                .addOnFailureListener {
                    it.printStackTrace()
                    completed(false)
                }
    }

    override fun updateDocument(
            documentID: String,
            data: HashMap<String, Any?>,
            completed: (result: Boolean) -> Unit) {

        db.collection(profilePath).document(documentID)
                .update(data)
                .addOnSuccessListener {
                    completed(true)
                }
                .addOnFailureListener {
                    it.printStackTrace()
                    completed(false)
                }
    }

    override fun getDocument(documentID: String, completed: (data: Profile?) -> Unit) {
        db.collection(profilePath).document(documentID)
                .get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val profile = document.toObject<Profile>()
                        completed(profile)
                    } else {
                        completed(null)
                    }
                }
                .addOnFailureListener {
                    it.printStackTrace()
                    completed(null)
                }
    }


}