package com.jarvish.android.jarvishpremium.firestore

import android.util.Log
import androidx.annotation.StringRes
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.toObject
import com.jarvish.android.jarvishpremium.R

enum class RegisterCode(@StringRes val nameResource: Int) {
    NONE(R.string.code_register_serial_number_ok),
    INVALID_SERIAL(R.string.code_invalid_serial_number),
    SERIAL_BEEN_REGISTERED(R.string.code_serial_number_in_use),
    REGISTERED_FAILURE(R.string.code_register_serial_number_failure);
}

private const val TAG = "StoreRegister"

class StoreRegister: Store<Register>() {

    override fun createDocument(data: Register, completed: (result: Boolean) -> Unit) {
        val regRef = db.collection(registerPath).document()
        val userRef = db.collection(accountPath).document(data.uid)

        db.runBatch { batch ->
            batch.set(regRef, data)
            batch.update(userRef, "haveProduct", true)
        }.addOnSuccessListener {
            completed(true)
        }.addOnFailureListener {
            Log.e(registerPath, "error: " + it.localizedMessage)
            completed(false)
        }
    }

    override fun createDocumentWithID(documentID: String, data: Register, completed: (result: Boolean) -> Unit) {
        db.collection(registerPath).document(documentID)
                .set(data)
                .addOnSuccessListener {
                    completed(true)
                }
                .addOnFailureListener {
                    it.printStackTrace()
                    completed(false)
                }
    }

    override fun updateDocument(documentID: String, data: HashMap<String, Any?>, completed: (result: Boolean) -> Unit) {
        TODO("Not yet implemented")
    }


    override fun getDocument(documentID: String, completed: (data: Register?) -> Unit) {
        db.collection(registerPath).document(documentID)
                .get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val register = document.toObject<Register>()
                        completed(register)
                    } else {
                        completed(null)
                    }
                }
                .addOnFailureListener {
                    it.printStackTrace()
                    completed(null)
                }
    }

    fun queryRegistered(uid: String, completed: (date: List<Register>?) -> Unit) {
        db.collection(registerPath)
                .whereEqualTo("uid", uid)
                .orderBy("date", Query.Direction.DESCENDING)
                .get()
                .addOnSuccessListener { documents ->
                    val registers = ArrayList<Register>()
                    for (document in documents) {
                        Log.d(TAG, "${document.id} => ${document.data}")
                        val register = document.toObject<Register>()
                        registers.add(register)
                    }
                    completed(registers)
                }
                .addOnFailureListener { exception ->
                    Log.e(TAG, "Error getting documents: ", exception)
                    completed(null)
                }
    }




}