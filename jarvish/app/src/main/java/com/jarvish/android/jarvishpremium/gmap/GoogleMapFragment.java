package com.jarvish.android.jarvishpremium.gmap;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.JLocation;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.SharedPreferencesUtil;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.databinding.FragmentGoogleMapBinding;
import com.jarvish.android.jarvishpremium.tracking.TrackManager;
import com.jarvish.android.jarvishpremium.weather.core.Weather;
import com.jarvish.android.jarvishpremium.weather.WeatherActivity;
import com.jarvish.android.jarvishpremium.weather.WeatherUtil;
import com.jarvish.android.jarvishpremium.weather.model.Current;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GoogleMapFragment.OnGoogleMapFragmentListener} interface
 * to handle interaction events.
 */
public class GoogleMapFragment extends Fragment implements
        EasyPermissions.PermissionCallbacks,
        OnMapReadyCallback {
    private static final String TAG = "GoogleMapFragment";

    private FragmentGoogleMapBinding mBinding;

    private final static int RC_MAP_PERMISSIONS = 101;
    private static final String[] MAP_PERMISSIONS =
            {Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.FOREGROUND_SERVICE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_WIFI_STATE,
                    Manifest.permission.ACCESS_NETWORK_STATE};

    private GoogleMap mMap;
    private Polyline mPolyline;
    private boolean isPendingDrawMap;
    private List<LatLng> mLatLngs;

    private OnGoogleMapFragmentListener mListener;

    private TrackManager mTrackManager;

    private Disposable mDisposableWeather;

    private int mTrackingState = 0;
    private boolean mMapFocus = false;
    private float mZoomLevel = 17.5f;
    private float mTile = 0;

    private boolean isDestroy = false;

    private Current mCurrent;

    public GoogleMapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        mBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_google_map, container, false);
        View view = mBinding.getRoot();

        mBinding.btClass.setOnClickListener(v -> onClickClass());
        mBinding.ivWeather.setOnClickListener(v -> onClickWeather());
        mBinding.tvTemp.setOnClickListener(v -> onClickWeather());
        mBinding.ivTracking.setOnClickListener(v -> onClickTracking());
        mBinding.ivTracking.setOnLongClickListener(v -> onLongClickTracking());
        mBinding.ivLocation.setOnClickListener(v -> onClickLocation());
        mBinding.ivGMap.setOnClickListener(v -> onClickGMap());


        mBinding.ivWeather.setVisibility(View.GONE);
        mBinding.tvTemp.setVisibility(View.GONE);

        mTrackManager = TrackManager.getInstance();

        if (!SharedPreferencesUtil.getGuide(requireContext())) {
            mBinding.clClass.setVisibility(View.GONE);
            mBinding.ivTracking.setEnabled(true);
        } else {
            mBinding.clClass.setVisibility(View.VISIBLE);
            mBinding.ivTracking.setEnabled(false);
        }

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated");
        requestInitGoogleMap();
    }


    private void requestInitGoogleMap() {
        if (EasyPermissions.hasPermissions(requireContext(), MAP_PERMISSIONS)) {
            initInitGoogleMap();
        } else {
            Log.d(TAG, "Do not have permissions");
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs access to your location and storage",
                    RC_MAP_PERMISSIONS,
                    MAP_PERMISSIONS);
        }
    }


    @AfterPermissionGranted(RC_MAP_PERMISSIONS)
    private void initInitGoogleMap() {
        Log.d(TAG, "initialize map");
        FragmentManager fm = getChildFragmentManager();
        if (getActivity() != null) {
            SupportMapFragment mapFragment = (SupportMapFragment) fm
                    .findFragmentById(R.id.mmap);
            if (mapFragment == null) {
                mapFragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.mmap, mapFragment).commit();
            }
            mapFragment.getMapAsync(this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }


    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnGoogleMapFragmentListener) {
            mListener = (OnGoogleMapFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (mMap != null && mPolyline != null && mLatLngs != null) {
            mPolyline.setPoints(mLatLngs);
        }
        isPendingDrawMap = false;
        getCurrentWeather();
        IntentFilter filter = new IntentFilter();
        filter.addAction("UNITS.CHANGED");
        requireActivity().registerReceiver(mReceiver, filter);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        if (mMap != null && mPolyline != null) {
            mLatLngs = mPolyline.getPoints();
        }
        isPendingDrawMap = true;
        clearUpdateWeatherTask();
        requireActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //mUnbinder.unbind();
        clearUpdateWeatherTask();
        isDestroy = true;
        mTrackManager.stop(requireContext());
        if (mPolyline != null) {
            mPolyline.remove();
        }
        mLatLngs = null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            requireContext(), R.raw.style_json2));
            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
        mMap.getUiSettings().setMapToolbarEnabled(false);
        centerMap();
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setOnCameraMoveStartedListener(mCameraMoveStartedListener);
        mBinding.ivTracking.setVisibility(View.VISIBLE);
        if (SharedPreferencesUtil.getAutoTracking(getContext())) {
            startTracking();
        }

    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int resourceId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, resourceId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        Bitmap icon = Bitmap.createScaledBitmap(bitmap, Util.dpToPx(context, 24), Util.dpToPx(context, 24), true);
        return BitmapDescriptorFactory.fromBitmap(icon);
    }

    private final GoogleMap.OnCameraMoveStartedListener mCameraMoveStartedListener = new GoogleMap.OnCameraMoveStartedListener() {
        @Override
        public void onCameraMoveStarted(int i) {
            Log.d(TAG, "onCameraMoveStarted: " + i);
            if (i == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                mZoomLevel = mMap.getCameraPosition().zoom;
                mTile = mMap.getCameraPosition().tilt;
                Log.d(TAG, "ZoomLevel: " + mZoomLevel + " " + "Tile: " + mTile);
                mMapFocus = false;
            }
        }
    };

    void onClickClass() {
        mBinding.clClass.setVisibility(View.GONE);
        mBinding.ivTracking.setEnabled(true);
        SharedPreferencesUtil.setGuide(requireContext());
    }

    void onClickWeather() {
        Log.d(TAG, "onClickWeather");
        Intent intent = new Intent();
        String observation = new Gson().toJson(mCurrent);
        intent.setClass(requireContext(), WeatherActivity.class);
        intent.putExtra(getString(R.string.key_current_condition), observation);
        startActivity(intent);
    }

    void onClickTracking() {
        Log.d(TAG, "onClickTracking: " + mTrackingState);
        if (mTrackingState == 0) {
            startTracking();
        } else if (mTrackingState == 1) {
            pauseTracking();
        } else if (mTrackingState == 2) {
            resumeTracking();
        }
    }

    boolean onLongClickTracking() {
        //Log.d(TAG, "onLongClickTracking");
        if (mTrackingState != 0) {
            new AlertDialog.Builder(requireContext(), R.style.AppAlertDialog)
                    .setTitle(R.string.hint_stop_track)
                    .setMessage(R.string.hint_store_track_log)
                    .setPositiveButton(R.string.action_confirm, (DialogInterface dialog, int which) -> stopTracking())
                    .setNegativeButton(R.string.action_cancel, null)
                    .show();
        }
        return true;
    }

    void onClickLocation() {
        //Log.d(TAG, "onClickLocation");
        mMapFocus = true;
        mZoomLevel = 17.5f;
        centerMap();
    }

    void onClickGMap() {
        if (isAdded()) {
            Intent mapIntent = new Intent(Intent.ACTION_VIEW);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
                startActivity(mapIntent);
            } else {
                Snackbar.make(mBinding.ivLocation, getString(R.string.no_google_map), Snackbar.LENGTH_SHORT)
                        .setBackgroundTint(getResources().getColor(R.color.color_snackbar_bg, null))
                        .setActionTextColor(getResources().getColor(R.color.white, null))
                        .show();
            }
        }
    }

    private void clearUpdateWeatherTask() {
        if (mDisposableWeather != null) {
            mDisposableWeather.dispose();
            mDisposableWeather = null;
        }
    }

    private void setUpdateWeatherTask(long expired) {
        long interval = expired - System.currentTimeMillis() / 1000L;
        if (interval <= 0) {
            interval = 900000;
        }
        //Log.d(TAG, "expired interval: " + interval);
        clearUpdateWeatherTask();
        mDisposableWeather = Observable.timer(interval, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> getCurrentWeather(), Throwable::printStackTrace);
    }


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "receiver #####");
            if (intent.getAction() != null && intent.getAction().equals("UNITS.CHANGED")) {
                Log.d(TAG, "UNITS.CHANGED #####");
                getCurrentWeather();
            }
        }
    };



    private void getCurrentWeather() {
        JLocation.getLastLocation(requireContext(), new JLocation.Call() {
            @Override
            public void onCompleted(Location location) {
                Weather weather = Weather.getInstance();
                weather.getCurrentConditions(location.getLatitude(), location.getLongitude(),
                        new Weather.OnCurrentConditions() {
                    @Override
                    public void onObservation(Current current) {
                        if (isAdded()) {
                            mBinding.ivWeather.setImageDrawable(WeatherUtil.getWeatherIcon(requireActivity(), current.getWxIcon()));
                            String temp = current.getTemp() + Weather.getInstance().getTemperatureUnits();
                            mBinding.tvTemp.setText(temp);
                            //Log.d(TAG, "current tempture: " + current.getTemp());
                            if (mBinding.ivWeather.getVisibility() == View.GONE ||
                                    mBinding.tvTemp.getVisibility() == View.GONE) {
                                mBinding.ivWeather.setVisibility(View.VISIBLE);
                                mBinding.tvTemp.setVisibility(View.VISIBLE);
                            }
                            setUpdateWeatherTask(current.getExpireTimeGmt());
                            mCurrent = current;
                            SharedPreferences sharedPref = requireActivity().getSharedPreferences(
                                    "track_extra", Context.MODE_PRIVATE);
                            sharedPref.edit()
                                    .putString("weather_temperature", temp)
                                    .putInt("weather_icon", mCurrent.getWxIcon())
                                    .putString("weather_description", mCurrent.getWxPhrase())
                                    .apply();
                        }
                    }

                    @Override
                    public void onFailure(String error) {

                    }
                });
            }

            @Override
            public void onError() {

            }
        });
    }


    private void centerMap() {
        JLocation.getLastLocation(requireContext(), new JLocation.Call() {
            @Override
            public void onCompleted(Location location) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(location.getLatitude(), location.getLongitude()), mZoomLevel));
            }

            @Override
            public void onError() {

            }
        });
    }

    private void startTracking() {
        setTrackingButton(1);
        PolylineOptions polylineOptions = new PolylineOptions().clickable(true);
        mPolyline = mMap.addPolyline(polylineOptions);
        int color = 0xff << 24 | 0xff << 16 | 0xff << 8 | 0xff;
        mPolyline.setColor(color);
        mPolyline.setWidth(18.5f);
       // mTrackingManager.start(getActivity());
        //mTrackingManager.addTrackingListener(mTrackingListener);
        centerMap();
        mTrackManager.start(requireContext(), mTrackManagerListener);
    }


    private void invalidTrackAlert() {
        new AlertDialog.Builder(requireContext(), R.style.AppAlertDialog)
                .setTitle(R.string.track_invalid_title)
                .setMessage(R.string.track_invalid_content_store)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }


    private void stopTracking() {
        mTrackManager.stop(requireContext());
        mPolyline.remove();
        setTrackingButton(0);
    }


    private void pauseTracking() {
        mTrackManager.pause();
        setTrackingButton(2);
    }


    private void resumeTracking() {
        mTrackManager.resume();
        setTrackingButton(1);
    }


    private void setTrackingButton(int state) {
        switch (state) {
            case 0:
                mBinding.tvTrack.setVisibility(View.GONE);
                mBinding.ivTracking.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_moto_white));
                break;
            case 1:
                mBinding.tvTrack.setText(getString(R.string.track_run_tracking));
                mBinding.tvTrack.setVisibility(View.VISIBLE);
                mBinding.ivTracking.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_pause_white));
                break;
            case 2:
                mBinding.tvTrack.setText(getString(R.string.track_pause_tracking));
                mBinding.tvTrack.setVisibility(View.VISIBLE);
                mBinding.ivTracking.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_moto_white));
                break;
        }
        mTrackingState = state;
    }


    private final TrackManager.TrackManagerListener mTrackManagerListener = new TrackManager.TrackManagerListener() {
        @Override
        public void onLocationChanged(Location location) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            Log.d(TAG, "onLocationChanged: " + latLng);
            if (mTrackingState != 1) {
                return;
            }
            if (!isPendingDrawMap) {
                List<LatLng> latLngs = mPolyline.getPoints();
                latLngs.add(latLng);
                mPolyline.setPoints(latLngs);
                if (mMapFocus) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, mZoomLevel));
                }
            } else {
                if (mLatLngs != null) {
                    mLatLngs.add(latLng);
                }
            }
        }

        @Override
        public void onInvalidTrack() {
            if (!isDestroy) {
                invalidTrackAlert();
            }
        }
    };


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnGoogleMapFragmentListener {
        // TODO: Update argument type and name
        void onMap();
    }
}
