/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jarvish.android.jarvishpremium.gmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.Util;

public class IconGenerator {
    private final Context mContext;
    private final View mContentView;
    private final ImageView mImageView;

    /**
     * Creates a new IconGenerator with the default style.
     */
    public IconGenerator(Context context, Drawable drawable) {
        mContext = context;
        mContentView = LayoutInflater.from(mContext).inflate(R.layout.position_icon_view, null);
        mImageView = mContentView.findViewById(R.id.ivHelmet);
        mImageView.setImageDrawable(drawable);
    }

    /**
     * Creates an icon with the current content and style.
     * <p/>
     * This method is useful if a custom view has previously been set, or if text content is not
     * applicable.
     */
    public Bitmap makeIcon() {
        int measureSpec = View.MeasureSpec.makeMeasureSpec(Util.dpToPx(mContext, 64), View.MeasureSpec.UNSPECIFIED);
        mContentView.measure(measureSpec, measureSpec);

        int measuredWidth = mContentView.getMeasuredWidth();
        int measuredHeight = mContentView.getMeasuredHeight();

        mContentView.layout(0, 0, measuredWidth, measuredHeight);

        Bitmap r = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(r);

        mContentView.draw(canvas);
        return r;
    }


}
