package com.jarvish.android.jarvishpremium.gpx;


import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Xml;

import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.db.GPSLog;
import com.jarvish.android.jarvishpremium.db.TrackLog;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;


public class ExportGPXFile {
    private static final String TAG = "ExportGPXFile";

    public static boolean createGPXFile(Context context, Uri uri, TrackLog trackLog, ArrayList<GPSLog> gpsLogs) throws IOException {
        Log.d(TAG, "create gpx file");
        ParcelFileDescriptor gpx = context.getContentResolver().
                openFileDescriptor(uri, "w");
        FileOutputStream fileOutputStream =
                new FileOutputStream(gpx.getFileDescriptor());
        OutputStreamWriter myOutWriter = new OutputStreamWriter(fileOutputStream);
        myOutWriter.append(writeGPXFile(trackLog, gpsLogs));

        myOutWriter.close();
        fileOutputStream.flush();
        fileOutputStream.close();
        return true;
    }


    private static String writeGPXFile(TrackLog trackLog, ArrayList<GPSLog> gpsLogs) {
        Log.d(TAG, "write gpx file");
        try {
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();

            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", false);

            xmlSerializer.startTag("", "gpx");
            xmlSerializer.attribute("", "xmlns", "http://www.topografix.com/GPX/1/1");
            xmlSerializer.attribute("", "xmlns:gpxx", "http://www.jarvish.com/GpxExtensions/v3");
            xmlSerializer.attribute("", "xmlns:gpxtpx", "http://www.jarvish.com/xmlschemas/TrackPointExtension/v1");
            xmlSerializer.attribute("", "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xmlSerializer.attribute("", "creator", "Jarvish");
            xmlSerializer.attribute("", "version", "1.1");
            xmlSerializer.attribute("", "xsi:schemaLocation", "http://www.jarvish.com/GPX/1/1 http://www.here.com/");
            xmlSerializer.startTag("", "trk");

            xmlSerializer.startTag("", GpxPie.name);
            xmlSerializer.text(trackLog.getTitle());
            xmlSerializer.endTag("", GpxPie.name);
            xmlSerializer.startTag("", GpxPie.distance);
            xmlSerializer.text(String.valueOf(trackLog.getDistance()));
            xmlSerializer.endTag("", GpxPie.distance);
            xmlSerializer.startTag("", GpxPie.duration);
            xmlSerializer.text(String.valueOf(trackLog.getDuration()));
            xmlSerializer.endTag("", GpxPie.duration);
            xmlSerializer.startTag("", GpxPie.averageSpeed);
            xmlSerializer.text(String.valueOf(trackLog.getAverageSpeed()));
            xmlSerializer.endTag("", GpxPie.averageSpeed);
            xmlSerializer.startTag("", GpxPie.maximumSpeed);
            xmlSerializer.text(String.valueOf(trackLog.getMaxSpeed()));
            xmlSerializer.endTag("", GpxPie.maximumSpeed);
            xmlSerializer.startTag("", GpxPie.weatherTemperature);
            xmlSerializer.text(trackLog.getWeaTemp());
            xmlSerializer.endTag("", GpxPie.weatherTemperature);
            xmlSerializer.startTag("", GpxPie.weatherIcon);
            xmlSerializer.text(String.valueOf(trackLog.getWeaIcon()));
            xmlSerializer.endTag("", GpxPie.weatherIcon);
            xmlSerializer.startTag("", GpxPie.weatherDescription);
            xmlSerializer.text(trackLog.getWeaDesc());
            xmlSerializer.endTag("", GpxPie.weatherDescription);
            xmlSerializer.startTag("", GpxPie.timestamp);
            xmlSerializer.text(Util.getDateCurrentTimeZone(trackLog.getTimestamp()));
            xmlSerializer.endTag("", GpxPie.timestamp);
            xmlSerializer.startTag("", GpxPie.helmet);
            xmlSerializer.text(trackLog.getHelmet());
            xmlSerializer.endTag("", GpxPie.helmet);


            xmlSerializer.startTag("", "trkseg");
            for (GPSLog gpsLog : gpsLogs) {
                writeGpsLog(xmlSerializer, gpsLog);
            }
            xmlSerializer.endTag("", "trkseg");

            xmlSerializer.endTag("", "trk");
            xmlSerializer.endTag("", "gpx");

            xmlSerializer.endDocument();

            Log.d(TAG, writer.toString());
            return writer.toString();
        } catch (IOException e) {
            e.getStackTrace();
            Log.e(TAG, Objects.requireNonNull(e.getMessage()));
            return null;
        }
    }


    private static void writeGpsLog(XmlSerializer xmlSerializer, GPSLog gpsLog) {
        try {
            xmlSerializer.startTag("", "trkpt");
            xmlSerializer.attribute("", "lat", String.valueOf(gpsLog.getLatitude()));
            xmlSerializer.attribute("", "lon", String.valueOf(gpsLog.getLongitude()));

            xmlSerializer.startTag("", "ele");
            xmlSerializer.text(String.valueOf(gpsLog.getAltitude()));
            xmlSerializer.endTag("", "ele");

            xmlSerializer.startTag("", "time");
            Log.d(TAG, "time: " + Util.getDateCurrentTimeZone(gpsLog.getTimestamp()));
            xmlSerializer.text(Util.getDateCurrentTimeZone(gpsLog.getTimestamp()));
            xmlSerializer.endTag("", "time");

            xmlSerializer.startTag("", "speed");
            xmlSerializer.text(String.valueOf(gpsLog.getSpeed()));
            xmlSerializer.endTag("", "speed");

            xmlSerializer.endTag("", "trkpt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static String getTimeFormat(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format("yyyyMMdd_HHmmss", cal).toString();
    }
}
