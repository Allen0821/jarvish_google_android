package com.jarvish.android.jarvishpremium.gpx;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.util.Xml;

import com.jarvish.android.jarvishpremium.db.GPSLog;
import com.jarvish.android.jarvishpremium.db.GPSTrack;
import com.jarvish.android.jarvishpremium.db.TrackDatabase;
import com.jarvish.android.jarvishpremium.db.TrackLog;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import io.reactivex.MaybeObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ImportGPXFile {
    private static final String TAG = "ImportGPXFile";
    private static final String ns = null;


    public interface ImportFileListener {
        void completed();
        void error(String msg);
    }

    public static void ImportFile( Context context, Uri uri, ImportFileListener listener) {

        try {
            //FileInputStream inputStream = new FileInputStream(file);
            InputStream inputStream = context.getContentResolver().openInputStream(uri);

            if (inputStream == null) {
                throw new IOException("Unable to obtain input stream from URI");
            }


            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(inputStream, null);
            parser.nextTag();

            TrackLog trackLog = new TrackLog();
            ArrayList<GPSLog> gpsLogs = new ArrayList<>();

            parser.require(XmlPullParser.START_TAG, ns, "gpx");
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();
                if (name.equals(GpxPie.name)) {
                    trackLog.setTitle(readPie(parser, GpxPie.name));
                }
                if (name.equals(GpxPie.distance)) {
                    trackLog.setDistance(readDistance(parser));
                }
                if (name.equals(GpxPie.duration)) {
                    long d = readDuration(parser);
                    Log.d(TAG, "duration: " + d);
                    trackLog.setDuration(d);
                }
                if (name.equals(GpxPie.averageSpeed)) {
                    trackLog.setAverageSpeed(readAverageSpeed(parser));
                }
                if (name.equals(GpxPie.maximumSpeed)) {
                    trackLog.setMaxSpeed(readMaxSpeed(parser));
                }
                if (name.equals(GpxPie.weatherTemperature)) {
                    trackLog.setWeaTemp(readPie(parser, GpxPie.weatherTemperature));
                }
                if (name.equals(GpxPie.weatherIcon)) {
                    String i = readPie(parser, GpxPie.weatherIcon);
                    if (!i.isEmpty()) {
                        trackLog.setWeaIcon(Integer.parseInt(i));
                    } else {
                        trackLog.setWeaIcon(-10);
                    }
                }
                if (name.equals(GpxPie.weatherDescription)) {
                    trackLog.setWeaDesc(readPie(parser, GpxPie.weatherDescription));
                }
                if (name.equals(GpxPie.timestamp)) {
                    trackLog.setTimestamp(readTimestamp(parser));
                }
                if (name.equals(GpxPie.helmet)) {
                    trackLog.setHelmet(readPie(parser, GpxPie.helmet));
                }
                if (name.equals("trkseg")) {
                    gpsLogs = readGPSLogs(parser);
                }
            }

            storeInDb(context, trackLog, gpsLogs, listener);
        } catch (Exception e) {
            e.printStackTrace();
            listener.error(e.getMessage());
        }
    }


    private static void storeInDb(Context context, TrackLog trackLog, ArrayList<GPSLog> gpsLogs, ImportFileListener listener) {
        GPSTrack gpsTrack = new GPSTrack();
        gpsTrack.setGpsTrack(gpsLogs);
        gpsTrack.setGpsTrackId(new Date().getTime());
        TrackDatabase.getInstance(context).gpsTrackDao().insertGPSTrack(gpsTrack)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new MaybeObserver<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "store GPS track onSubscribe");
                    }

                    @Override
                    public void onSuccess(Long aLong) {
                        Log.d(TAG, "store GPS track: " + aLong);
                        storeTrackLog(trackLog, aLong, context, listener);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: " + e.getMessage());
                        listener.error(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "store GPS track onComplete");
                    }
                });
    }


    private static void storeTrackLog(TrackLog trackLog, long id, Context context, ImportFileListener listener) {
        trackLog.setGpsTrackId(id);
        TrackDatabase.getInstance(context).trackLogDao().insertTrack(trackLog)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new MaybeObserver<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(Long aLong) {
                        Log.d(TAG, "onSuccess!!!");
                        listener.completed();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "store track log onError: " + e.getMessage());
                        listener.error(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "store track log onComplete");
                    }
                });
    }


    private static String readPie(XmlPullParser parser, String flavor) {
        try {
            parser.require(XmlPullParser.START_TAG, ns, flavor);
            String pie = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, flavor);
            return pie;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    private static String readName(XmlPullParser parser) {
        try {
            parser.require(XmlPullParser.START_TAG, ns, GpxPie.name);
            String name = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, GpxPie.name);
            return name;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    private static double readDistance(XmlPullParser parser) {
        try {
            parser.require(XmlPullParser.START_TAG, ns, GpxPie.distance);
            double distance = Double.parseDouble(readText(parser));
            parser.require(XmlPullParser.END_TAG, ns, GpxPie.distance);
            return distance;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    private static long readDuration(XmlPullParser parser) {
        try {
            parser.require(XmlPullParser.START_TAG, ns, GpxPie.duration);
            long duration = Integer.parseInt(readText(parser));
            parser.require(XmlPullParser.END_TAG, ns, GpxPie.duration);
            return duration;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    private static double readAverageSpeed(XmlPullParser parser) {
        try {
            parser.require(XmlPullParser.START_TAG, ns, GpxPie.averageSpeed);
            double speed = Double.parseDouble(readText(parser));
            parser.require(XmlPullParser.END_TAG, ns, GpxPie.averageSpeed);
            return speed;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    private static double readMaxSpeed(XmlPullParser parser) {
        try {
            parser.require(XmlPullParser.START_TAG, ns, GpxPie.maximumSpeed);
            double speed = Double.parseDouble(readText(parser));
            parser.require(XmlPullParser.END_TAG, ns, GpxPie.maximumSpeed);
            return speed;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    private static long readTimestamp(XmlPullParser parser) {
        try {
            parser.require(XmlPullParser.START_TAG, ns, GpxPie.timestamp);
            String time = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, GpxPie.timestamp);
            return getTime(time);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    private static ArrayList<GPSLog> readGPSLogs(XmlPullParser parser) throws XmlPullParserException, IOException  {
        Log.d(TAG, "read gps logs");
        ArrayList<GPSLog> gpsLogs = new ArrayList<>();
        parser.require(XmlPullParser.START_TAG, ns, "trkseg");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                Log.d(TAG, "@@@@@@@");
                continue;
            }
            String name = parser.getName();
            if (name.equals("trkpt")) {
                gpsLogs.add(readGPSLog(parser));
            }
        }
        Log.d(TAG, "size: " + gpsLogs.size());
        for (int i = 0; i < gpsLogs.size(); i++) {
            Log.d(TAG, "###: " + i + " " + gpsLogs.get(i).getLatitude() + " " + gpsLogs.get(i).getLongitude());
        }
        return gpsLogs;
    }



    private static GPSLog readGPSLog(XmlPullParser parser) throws IOException, XmlPullParserException {
        Log.d(TAG, "@@@ read gps log @@@");
        GPSLog gpsLog = new GPSLog();
        parser.require(XmlPullParser.START_TAG, ns, "trkpt");

        String lat = parser.getAttributeValue(null, "lat");
        String lon = parser.getAttributeValue(null, "lon");
        gpsLog.setLatitude(Double.parseDouble(lat));
        gpsLog.setLongitude(Double.parseDouble(lon));

        Log.d(TAG, "lat: " + lat);
        Log.d(TAG, "lon: " + lon);

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("ele")) {
                parser.require(XmlPullParser.START_TAG, ns, "ele");
                String ele = readText(parser);
                parser.require(XmlPullParser.END_TAG, ns, "ele");
                gpsLog.setAltitude(Double.parseDouble(ele));
            }
            if (name.equals("time")) {
                parser.require(XmlPullParser.START_TAG, ns, "time");
                String time = readText(parser);
                parser.require(XmlPullParser.END_TAG, ns, "time");
                gpsLog.setTimestamp(getTime(time));
            }
            if (name.equals("speed")) {
                parser.require(XmlPullParser.START_TAG, ns, "speed");
                String speed = readText(parser);
                parser.require(XmlPullParser.END_TAG, ns, "speed");
                gpsLog.setSpeed(Double.parseDouble(speed));
            }
        }
        return gpsLog;
    }



    private static long getTime(String time) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
            return dateFormat.parse(time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }


    private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            Log.d(TAG, "readText: " + result);
            parser.nextTag();
        }
        return result;
    }


}
