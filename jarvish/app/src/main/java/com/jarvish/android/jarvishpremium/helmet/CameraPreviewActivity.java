package com.jarvish.android.jarvishpremium.helmet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.icatch.wificam.customer.ICatchWificamSession;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.ActivityCameraPreviewBinding;
import com.jarvish.android.jarvishpremium.icatch.ICatch;


public class CameraPreviewActivity extends AppCompatActivity {
    private static final String TAG = "CameraPreviewActivity";
    //private static final String PREVIEW_URL = "rtsp://192.168.1.1/H264?W=720&H=400&BR=1500000&FPS=15&";
    private ActivityCameraPreviewBinding mBinding;


    private ICatchWificamSession mSession;
    //private boolean mCreateSession;
    private ICatch mICatch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_camera_preview);
        //enableSDKLog();
        if (initSession()) {
            initPlayback();
        } else {
            iCatchInitFailureAlert();
        }

        mBinding.ivClose.setOnClickListener(v -> onClickClose());
    }

    private void iCatchInitFailureAlert() {
        new MaterialAlertDialogBuilder(this, R.style.AppAlertDialog)
                .setTitle(R.string.helmet_icatch_connection_fail_title)
                .setMessage(R.string.helmet_icatch_connection_fail_content)
                .setPositiveButton(R.string.action_confirm,
                        (dialogInterface, i) -> {
                            finish();
                        })
                .show();
    }


    private boolean initSession() {
        Log.d(TAG, "init session");
        mICatch = ICatch.getInstance();
        if (!mICatch.prepareSession()) {
            Log.e(TAG, "create session failed");
            return false;
        }
        mSession = mICatch.getSession();
        return true;
    }

    private void initPlayback() {
        Log.d(TAG, "init playback");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d(TAG, "screen: " + width + " " + height);
        mBinding.cameraPreview.startCameraPreview(mSession, width, height);
    }

    /*public void enableSDKLog() {
        String path = null;
        path = Environment.getExternalStorageDirectory().toString() + "/IcatchSportCamera_SDK_Log";
        Environment.getExternalStorageDirectory();
        Log.d(TAG, "path: " + path);
        ICatchWificamLog.getInstance().setFileLogPath(path);

        Log.d(TAG, "path: " + path);
        if (path != null) {
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        ICatchWificamLog.getInstance().setSystemLogOutput(true);
        ICatchWificamLog.getInstance().setFileLogOutput(true);
        ICatchWificamLog.getInstance().setRtpLog(true);
        ICatchWificamLog.getInstance().setPtpLog(true);
        ICatchWificamLog.getInstance().setRtpLogLevel(ICatchLogLevel.ICH_LOG_LEVEL_INFO);
        ICatchWificamLog.getInstance().setPtpLogLevel(ICatchLogLevel.ICH_LOG_LEVEL_INFO);


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display. getSize(size);

        cameraPreview.setScreen(size.x, size.y);
    }*/

    protected void onClickClose() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        //cameraPreview.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        //cameraPreview.Stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        mBinding.cameraPreview.stop();
        mBinding.cameraPreview.stopCameraPreview();
        mICatch.destroySession();
        mSession = null;
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

}
