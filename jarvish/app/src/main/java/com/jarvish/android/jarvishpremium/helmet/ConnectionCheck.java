package com.jarvish.android.jarvishpremium.helmet;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;


public class ConnectionCheck {

    public static Observable<Boolean> isReachable(final String ip) {
        return Observable.create((ObservableEmitter<Boolean> emitter) -> {
            Process process = java.lang.Runtime.getRuntime().exec("ping -c 1 " + ip);
            int returnVal = process.waitFor();
            process.destroy();
            emitter.onNext(returnVal == 0);
        });
    }
}
