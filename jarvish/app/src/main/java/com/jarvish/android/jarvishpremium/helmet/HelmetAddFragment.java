package com.jarvish.android.jarvishpremium.helmet;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.FragmentHelmetAddBinding;
import com.jarvish.android.jarvishpremium.helmet.viewmodels.HelmetViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HelmetAddFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelmetAddFragment extends Fragment {
   // private static final String TAG = "HelmetAddFragment";

    private FragmentHelmetAddBinding mBinding;

    private HelmetViewModel mViewModel;

    public HelmetAddFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment HelmetAddFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelmetAddFragment newInstance() {
        return new HelmetAddFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_helmet_add, container, false);

        mViewModel = new ViewModelProvider(requireParentFragment()).get(HelmetViewModel.class);
        mBinding.setViewModel(mViewModel);

        return mBinding.getRoot();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}