package com.jarvish.android.jarvishpremium.helmet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.adapter.BleScanResultAdapter;
import com.jarvish.android.jarvishpremium.ble.HelmetBleManager;
import com.jarvish.android.jarvishpremium.databinding.FragmentHelmetConnectBinding;
import com.jarvish.android.jarvishpremium.helmet.viewmodels.HelmetViewModel;
import com.jarvish.android.jarvishpremium.model.HelmetStyle;
import com.polidea.rxandroidble2.scan.ScanResult;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HelmetConnectFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelmetConnectFragment extends Fragment implements BleScanResultAdapter.OnBleScanResultAdapterListener {
    private static final String TAG = "HelmetConnectFragment";

    private FragmentHelmetConnectBinding mBinding;

    private HelmetViewModel mViewModel;

    private HelmetStyle mHelmetStyle;

    private HelmetBleManager mHelmetBleManager;

    private ArrayList<ScanResult> mScanResults;
    private BleScanResultAdapter mAdapter;

    private Animation animation1;
    private Animation animation2;
    private Animation animation3;

    private int mAction = 0;

    public HelmetConnectFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment HelmetConnectFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelmetConnectFragment newInstance() {
        return new HelmetConnectFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_helmet_connect, container, false);
        View view = mBinding.getRoot();
        mViewModel = new ViewModelProvider(requireParentFragment()).get(HelmetViewModel.class);
        mBinding.setViewModel(mViewModel);
        onStopFindAnimation();
        initDeviceScanResultView();

        mHelmetStyle = mViewModel.getSelectedHelmetStyle().getValue();
        Drawable drawable = ResourcesCompat.getDrawable(
                requireContext().getResources(),
                requireContext().getResources()
                        .getIdentifier(mHelmetStyle.getImage(), "drawable",
                                requireContext().getPackageName()),
                null);
        mBinding.ivConnect.setImageDrawable(drawable);
        mBinding.tvTitle.setText(mHelmetStyle.getName());

        mBinding.btFind.setOnClickListener(v1 -> {
            Log.d(TAG, "start scan");
            mScanResults.clear();
            mAdapter.notifyDataSetChanged();
            onStartFindAnimation(ContextCompat.getDrawable(requireContext(), R.drawable.shape_round_find));
            mBinding.btFind.setEnabled(false);
            mAction = 0;
            mHelmetBleManager.startBleScan();
        });

        mHelmetBleManager = HelmetBleManager.getInstance(requireContext());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("SWITCH0");
        intentFilter.addAction("SWITCH1");
        requireContext().registerReceiver(receiver, intentFilter);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        pauseAction();
    }

    public void pauseAction() {
        onStopFindAnimation();
        if (mAction == 0) {
            mHelmetBleManager.stopBleScan();
            mBinding.btFind.setEnabled(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeAction();
    }

    public void resumeAction() {
        if (mAction == 1) {
            onStartFindAnimation(ContextCompat.getDrawable(requireContext(), R.drawable.shape_round_connect));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        requireContext().unregisterReceiver(receiver);
        if (mScanResults != null) {
            mScanResults.clear();
            mScanResults = null;
        }
        mHelmetStyle = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    void onStartFindAnimation(Drawable drawable) {
        if (animation1 == null) {
            animation1 = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_scan);
        } else {
            animation1.reset();
        }
        mBinding.ivScan1.startAnimation(animation1);

        if (animation2 == null) {
            animation2 = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_scan);
        } else {
            animation2.reset();
        }
        animation2.setStartOffset(1200);
        mBinding.ivScan2.startAnimation(animation2);

        if (animation3 == null) {
            animation3 = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_scan);
        } else {
            animation3.reset();
        }
        animation3.setStartOffset(1800);
        mBinding.ivScan3.startAnimation(animation3);

        if (drawable != null) {
            mBinding.ivScan1.setBackground(drawable);
            mBinding.ivScan2.setBackground(drawable);
            mBinding.ivScan3.setBackground(drawable);
        }
        mBinding.ivScan1.setVisibility(View.VISIBLE);
        mBinding.ivScan2.setVisibility(View.VISIBLE);
        mBinding.ivScan3.setVisibility(View.VISIBLE);

        mBinding.ivAdd.setEnabled(false);
    }


    void onStopFindAnimation() {
        mBinding.ivScan1.clearAnimation();
        mBinding.ivScan2.clearAnimation();
        mBinding.ivScan3.clearAnimation();
        mBinding.ivScan1.setVisibility(View.GONE);
        mBinding.ivScan2.setVisibility(View.GONE);
        mBinding.ivScan3.setVisibility(View.GONE);

        mBinding.ivAdd.setEnabled(true);
    }

    private void initDeviceScanResultView() {
        mScanResults = new ArrayList<>();
        mAdapter = new BleScanResultAdapter(mScanResults, this);
        mBinding.rcvResult.setHasFixedSize(true);
        mBinding.rcvResult.setLayoutManager(new LinearLayoutManager(requireContext()));
        mBinding.rcvResult.setAdapter(mAdapter);
    }

    @Override
    public void onSelected(ScanResult scanResult) {
        mScanResults.clear();
        mAdapter.notifyDataSetChanged();
        onStartFindAnimation(ContextCompat.getDrawable(requireContext(), R.drawable.shape_round_connect));
        mBinding.btFind.setVisibility(View.GONE);
        mBinding.rcvResult.setVisibility(View.GONE);
        mBinding.tvContent.setVisibility(View.GONE);
        mBinding.tvConnStatus.setVisibility(View.VISIBLE);
        mBinding.tvConnStatus.setText(R.string.helmet_connect_helmet);
        mAction = 1;
        mHelmetBleManager.connectDevice(scanResult.getBleDevice().getMacAddress());
    }

    void onFeedScanResult(ScanResult scanResult) {
        for (ScanResult result : mScanResults) {
            if (result.getBleDevice().getMacAddress()
                    .equals(scanResult.getBleDevice().getMacAddress())) {
                return;
            }
        }
        Log.d(TAG, "onFeedScanResult: " +
                scanResult.getBleDevice().getName() + " " +
                scanResult.getBleDevice().getMacAddress());
        mScanResults.add(scanResult);
        mAdapter.notifyDataSetChanged();
    }

    void onScanFinished() {
        mBinding.btFind.setEnabled(true);
    }

    void onConnected() {
        mBinding.tvConnStatus.setText(R.string.helmet_fetching_settings);
    }

    void onConnectionFailure() {
        onStopFindAnimation();
        mBinding.btFind.setVisibility(View.VISIBLE);
        mBinding.rcvResult.setVisibility(View.VISIBLE);
        mBinding.tvContent.setVisibility(View.VISIBLE);
        mBinding.tvConnStatus.setVisibility(View.GONE);
        mBinding.btFind.setEnabled(true);
        mAction = 0;
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("SWITCH0")) {
                pauseAction();
            } else if (action != null && action.equals("SWITCH1")) {
                resumeAction();
            }
        }
    };
}