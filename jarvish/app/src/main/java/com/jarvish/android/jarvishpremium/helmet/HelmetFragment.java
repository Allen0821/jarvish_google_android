package com.jarvish.android.jarvishpremium.helmet;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.ble.HelmetBleManager;
import com.jarvish.android.jarvishpremium.ble.HelmetBleManagerListener;
import com.jarvish.android.jarvishpremium.databinding.FragmentHelmetBinding;
import com.jarvish.android.jarvishpremium.helmet.viewmodels.HelmetViewModel;
import com.jarvish.android.jarvishpremium.model.Helmet;
import com.jarvish.android.jarvishpremium.model.HelmetStyle;
import com.polidea.rxandroidble2.scan.ScanResult;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HelmetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelmetFragment extends Fragment {
    private static final String TAG = "HelmetFragment";

    private FragmentHelmetBinding mBinding;

    private HelmetViewModel mViewModel;

    private HelmetAddFragment mHelmetAddFragment;
    private HelmetSelectFragment mHelmetSelectFragment;
    private HelmetConnectFragment mHelmetConnectFragment;
    private HelmetSettingFragment mHelmetSettingFragment;

    private HelmetBleManager mHelmetBleManager;

    private HelmetStyle mHelmetStyle;

    private boolean isConnected;

    private int mStatus = 0;

    private boolean isOnBackground = false;

    public HelmetFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment HelmetFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelmetFragment newInstance(String param1, String param2) {
        return new HelmetFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_helmet, container, false);
        View view = mBinding.getRoot();
        mViewModel = new ViewModelProvider(this).get(HelmetViewModel.class);
        //mBinding.setFragment(this);

        mHelmetBleManager = HelmetBleManager.getInstance(requireContext());
        mHelmetBleManager.addHelmetBleStatusListener(mHelmetBleStatusListener);

        mHelmetStyle = JSettingBox.getAddedHelmet(requireContext());
        if (mHelmetStyle == null) {
            mHelmetAddFragment = HelmetAddFragment.newInstance();
            getChildFragmentManager().beginTransaction()
                    .add(R.id.fmHelmet, mHelmetAddFragment)
                    .commit();
        } else {
            mViewModel.selectHelmetStyle(mHelmetStyle);
            createConnectView();
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //mViewModel = new ViewModelProvider(this).get(HelmetViewModel.class);

        mViewModel.getStatus().observe(getViewLifecycleOwner(), status -> {
            switch (status) {
                case 0:
                    onOpenSelectHelmetStyle();
                    break;
                case 1:
                    onCloseHelmetStyleSelect();
                    break;
                case 2:
                    onRemoveHelmetStyle();
                    break;
                case 3:
                    onDisconnect();
                    break;
                default:
            }
        });


        mViewModel.getSelectedHelmetStyle().observe(getViewLifecycleOwner(), this::onHelmetStyleSelected);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume status: " + mStatus);
        if (mStatus == 0 && isOnBackground) {
            createConnectView();
            mHelmetSettingFragment = null;
        }
        else if (mStatus == 2) {
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.fmHelmet, mHelmetSettingFragment)
                    .commit();
        }
        isOnBackground = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        isOnBackground = true;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHelmetBleManager.destroyHelmetBleManager();
    }

    private void createConnectView() {
        mHelmetConnectFragment = HelmetConnectFragment.newInstance();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.fmHelmet, mHelmetConnectFragment)
                .commit();
    }

    private void createSettingView(Helmet helmet) {
        Log.d(TAG, "onFetchHelmet: " + new Gson().toJson(helmet));
        mViewModel.setConnectedHelmet(helmet);
        mHelmetSettingFragment = HelmetSettingFragment.newInstance();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.fmHelmet, mHelmetSettingFragment)
                .commit();
        isConnected = true;
        mStatus = 3;
    }

    private void onOpenSelectHelmetStyle() {
        if (mHelmetSelectFragment == null) {
            mHelmetSelectFragment = HelmetSelectFragment.newInstance();
        }
        getChildFragmentManager().beginTransaction()
                .hide(mHelmetAddFragment)
                .add(R.id.fmHelmet, mHelmetSelectFragment)
                .commit();
    }

    private void onCloseHelmetStyleSelect() {
        getChildFragmentManager().beginTransaction()
                .remove(mHelmetSelectFragment)
                .show(mHelmetAddFragment)
                .commit();
    }

    private void onHelmetStyleSelected(HelmetStyle helmetStyle) {
        //createConnectView(helmetStyle);
        mHelmetStyle = helmetStyle;
        JSettingBox.setAddedHelmet(requireContext(), mHelmetStyle);
        createConnectView();
        mHelmetSelectFragment = null;
    }

    private void onRemoveHelmetStyle() {
        if (mHelmetAddFragment == null) {
            mHelmetAddFragment = HelmetAddFragment.newInstance();
        }
        getChildFragmentManager().beginTransaction().replace(R.id.fmHelmet, mHelmetAddFragment).commit();
        mHelmetConnectFragment = null;
        JSettingBox.removeAddedHelmet(requireContext());
    }

    private void onDisconnect() {
        createConnectView();
        mHelmetSettingFragment = null;
        mHelmetBleManager.disconnectHelmet();
    }

    private final HelmetBleManagerListener.HelmetBleStatusListener mHelmetBleStatusListener = new HelmetBleManagerListener.HelmetBleStatusListener() {
        @Override
        public void onConnected() {
            mHelmetConnectFragment.onConnected();
            mStatus = 1;
        }

        @Override
        public void onDisconnected() {
            if (isConnected) {
                createConnectView();
                mHelmetSettingFragment = null;
            }
            mStatus = 0;
        }

        @Override
        public void onConnectFailed() {
            if (!isOnBackground) {
                if (isConnected) {
                    createConnectView();
                    mHelmetSettingFragment = null;
                } else {
                    mHelmetConnectFragment.onConnectionFailure();
                }
            }
            mStatus = 0;
        }

        @Override
        public void onScanResult(ScanResult result) {
            Log.d(TAG, "onScanResult: " +
                    result.getBleDevice().getName() + " " +
                    result.getBleDevice().getMacAddress());
            if (mHelmetConnectFragment != null) {
                mHelmetConnectFragment.onFeedScanResult(result);
            }
        }

        @Override
        public void onScanFinished() {
            mHelmetConnectFragment.onStopFindAnimation();
            mHelmetConnectFragment.onScanFinished();
        }

        @Override
        public void onScanFailure() {

        }

        @Override
        public void onFetchHelmet(Helmet helmet) {
            mStatus = 2;
            if (helmet.getProduct() == null || helmet.getProduct().getProductType().isEmpty()) {
                helmet.setStyle(mHelmetStyle.getIndex());
                helmet.setName(mHelmetStyle.getName());
                helmet.setImage(mHelmetStyle.getImage());
            } else {
                mHelmetStyle.setIndex(helmet.getStyle());
                mHelmetStyle.setImage(helmet.getImage());
                mHelmetStyle.setName(helmet.getName());
                JSettingBox.setAddedHelmet(requireContext(), mHelmetStyle);
            }
            createSettingView(helmet);
        }
    };

}