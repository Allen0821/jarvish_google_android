package com.jarvish.android.jarvishpremium.helmet;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.adapter.HelmetSelectAdapter;
import com.jarvish.android.jarvishpremium.databinding.FragmentHelmetSelectBinding;
import com.jarvish.android.jarvishpremium.helmet.viewmodels.HelmetViewModel;
import com.jarvish.android.jarvishpremium.model.HelmetStyle;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HelmetSelectFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelmetSelectFragment extends Fragment implements HelmetSelectAdapter.OnHelmetSelectAdapterListener {
   // private static final String TAG = "HelmetSelectFragment";

    private FragmentHelmetSelectBinding mBinding;

    private HelmetViewModel mViewModel;

    private ArrayList<HelmetStyle> mHelmetStyles;

    public HelmetSelectFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment HelmetSelectFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelmetSelectFragment newInstance() {
        return new HelmetSelectFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_helmet_select, container, false);
        mViewModel = new ViewModelProvider(requireParentFragment()).get(HelmetViewModel.class);
        mBinding.setViewModel(mViewModel);
        View view = mBinding.getRoot();

        buildHelmetStyle();
        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelmetStyles != null) {
            mHelmetStyles.clear();
            mHelmetStyles = null;
        }
    }

    private void buildHelmetStyle() {
        mHelmetStyles = new ArrayList<>();
        mHelmetStyles.addAll(HelmetStyleDefine.buildHelmetStyleList());
        HelmetSelectAdapter adapter = new HelmetSelectAdapter(mHelmetStyles, this);
        mBinding.rcvHelmetStyle.setLayoutManager(new LinearLayoutManager(requireContext()));
        mBinding.rcvHelmetStyle.setAdapter(adapter);
    }

    @Override
    public void onSelected(HelmetStyle helmetStyle) {
        mViewModel.selectHelmetStyle(helmetStyle);
    }

}