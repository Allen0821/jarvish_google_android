package com.jarvish.android.jarvishpremium.helmet;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.ArrayRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.jarvish.android.jarvishpremium.helmet.viewmodels.HelmetViewModel;
import com.jarvish.android.jarvishpremium.ota.OtaActivity;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.ble.HelmetBleManager;
import com.jarvish.android.jarvishpremium.ble.HelmetBleManagerListener;
import com.jarvish.android.jarvishpremium.ble.JCandy;
import com.jarvish.android.jarvishpremium.databinding.FragmentHelmetSettingBinding;
import com.jarvish.android.jarvishpremium.icatch.ICatch;
import com.jarvish.android.jarvishpremium.model.Helmet;
import com.jarvish.android.jarvishpremium.ota.OtaRoles;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HelmetSettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelmetSettingFragment extends Fragment {
    private static final String TAG = "HelmetSettingFragment";

    private FragmentHelmetSettingBinding mBinding;

    private HelmetBleManager mHelmetBleManager;

    private HelmetViewModel mViewModel;
    private Helmet mHelmet;

    private ArrayList<JSetting> mJSettings;
    private JHelmetSettingAdapter mAdapter;

    private int tempChoose;

    private Animation animation;

    public HelmetSettingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment HelmetSettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelmetSettingFragment newInstance() {
        return new HelmetSettingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView");
        mBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_helmet_setting, container, false);
        View view = mBinding.getRoot();
        mViewModel = new ViewModelProvider(requireParentFragment()).get(HelmetViewModel.class);
        mHelmetBleManager = HelmetBleManager.getInstance(requireContext());
        mHelmetBleManager.addHelmetBleMessageListener(mHelmetBleMessageListener);
        mBinding.cpBattery.setVisibility(View.GONE);
        mBinding.btDisconnect.setOnClickListener(v1 -> disconnect());
        initHelmetSettingView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void startProgressAnimation() {
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(requireContext(), R.anim.progress_setting);
        } else {
            animation.reset();
        }
        mBinding.ivProgress.startAnimation(animation);
        mBinding.clProgress.setVisibility(View.VISIBLE);
        requireActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void endProgressAnimation() {
        if (animation != null) {
            mBinding.ivProgress.clearAnimation();
        }
        mBinding.clProgress.setVisibility(View.GONE);
        requireActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        mHelmetBleManager.startBatteryWatch();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        mHelmetBleManager.stopBatteryWatch();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView");
        mHelmetBleManager.removeHelmetBleMessageListener(mHelmetBleMessageListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    private void disconnect() {
        new MaterialAlertDialogBuilder(requireContext(), R.style.AppAlertDialog)
                .setTitle(R.string.helmet_disconnect_title)
                .setPositiveButton(R.string.action_confirm,
                    (dialogInterface, i) -> {
                        mViewModel.disconnectHelmet();
                    })
                .setNegativeButton(R.string.action_cancel, null)
                .show();
    }

    private void initHelmetSettingView() {
        mHelmet = mViewModel.getConnectedHelmet().getValue();
        if (mHelmet == null) {
            mViewModel.disconnectHelmet();
            return;
        }
        mJSettings = JSettingBox.buildSettingBox(requireContext(), mHelmet);
        mAdapter = new JHelmetSettingAdapter(mJSettings, mJHelmetSettingAdapterListener);
        mBinding.rvSettings.setHasFixedSize(true);
        mBinding.rvSettings.setLayoutManager(new LinearLayoutManager(requireContext()));
        mBinding.rvSettings.setAdapter(mAdapter);
        mBinding.tvHelmet.setText(mHelmet.getName());
        Drawable drawable = ResourcesCompat.getDrawable(
                requireContext().getResources(),
                requireContext().getResources()
                        .getIdentifier(mHelmet.getImage(), "drawable",
                                requireContext().getPackageName()),
                null);
        mBinding.ivHelmet.setImageDrawable(drawable);
    }

    private final JHelmetSettingAdapter.JHelmetSettingAdapterListener mJHelmetSettingAdapterListener =
            new JHelmetSettingAdapter.JHelmetSettingAdapterListener() {
                @Override
                public void onSwitchTailLight(boolean value) {
                    startProgressAnimation();
                    mHelmetBleManager.setTailLight(value);
                }

                @Override
                public void onSwitchVideoTimestamp(boolean value) {
                    startProgressAnimation();
                    mHelmetBleManager.setVideoTimestamp(value);
                }

                @Override
                public void onSetVideoResolution(int value) {
                    int itemsId;
                    if (JCandy.getVideoResolutionCapacity(
                            mHelmet.getStyle()) ==
                            JCandy.RESOLUTION_M1) {
                        itemsId = R.array.m1_video_resolution;
                    } else {
                        itemsId = R.array.m2526_video_resolution;
                    }
                    openChooseDialog(
                            R.string.helmet_choose_video_resolution_title,
                            itemsId,
                            JSettingBox.FUNC_VIDEO_RESOLUTION,
                            value);
                }

                @Override
                public void onSetRecordingTime(int value) {
                    openChooseDialog(
                            R.string.helmet_choose_recording_title,
                            R.array.recording_time,
                            JSettingBox.FUNC_RECORDING_TIME,
                            value);
                }

                @Override
                public void onSetBatteryEfficiency(int value) {
                    openChooseDialog(
                            R.string.helmet_choose_battery_efficiency_title,
                            R.array.battery_efficiency,
                            JSettingBox.FUNC_BATTERY_EFFICIENCY,
                            value);
                }

                @Override
                public void onSyncSystemTime() {
                    startProgressAnimation();
                    mHelmetBleManager.setSystemTime();
                }

                @Override
                public void onResetDefault() {
                    startProgressAnimation();
                    mHelmetBleManager.resetDefault();
                }

                @Override
                public void onOpenCameraPreview() {
                    startProgressAnimation();
                    checkConnection(JSettingBox.FUNC_CAMERA_PREVIEW);
                }

                @Override
                public void onOpenHelmetVideo() {
                    startProgressAnimation();
                    checkConnection(JSettingBox.FUNC_HELMET_VIDEO);
                }

                @Override
                public void onOpenBookVideo() {
                    startProgressAnimation();
                    checkConnection(JSettingBox.FUNC_BOOKMARK_VIDEO);
                }

                @Override
                public void onOpenOta() {
                    if (OtaRoles.checkBatteryLevel(mHelmet.getBatteryLevel())) {
                        openOta(mHelmet.getFirmwareVersion(), mHelmet.getStyle(), mHelmet.getLanguage());
                    } else {
                        onOtaLowPowerWarning();
                    }
                }

                @Override
                public void onFormat() {
                    startProgressAnimation();
                    checkConnection(JSettingBox.FUNC_FORMAT);
                }
            };

    void openChooseDialog(@StringRes int titleID, @ArrayRes int itemID, final String func, final int value) {
        new MaterialAlertDialogBuilder(requireContext(), R.style.AppAlertDialog)
                .setTitle(titleID)
                .setSingleChoiceItems(itemID, value, (dialogInterface, i) -> {
                    tempChoose = i;
                    Log.d(TAG, "choose: " + func + " = " + tempChoose);
                })
                .setPositiveButton(R.string.action_confirm, (dialogInterface, i) -> {
                    startProgressAnimation();
                    switch (func) {
                        case JSettingBox.FUNC_VIDEO_RESOLUTION:
                            mHelmetBleManager.setVideoResolution(tempChoose);
                            break;
                        case JSettingBox.FUNC_RECORDING_TIME:
                            mHelmetBleManager.setVideoRecordingTime(tempChoose);
                            break;
                        case JSettingBox.FUNC_BATTERY_EFFICIENCY:
                            mHelmetBleManager.setBatteryEfficiency(tempChoose);
                            break;
                        default:
                            break;
                    }
                })
                .setNegativeButton(R.string.action_cancel, null)
                .show();
    }

    private void updateJSettingEnable(@NonNull String func, boolean enable) {
        for (JSetting jSetting : mJSettings) {
            if (jSetting.getFunc().equals(func)) {
                jSetting.setEnable(enable);
                break;
            }
        }
        mAdapter.notifyDataSetChanged();
    }


    private void updateJSettingValue(@NonNull String func, int value) {
        for (JSetting jSetting : mJSettings) {
            if (jSetting.getFunc().equals(func)) {
                jSetting.setValue(value);
                break;
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private final HelmetBleManagerListener.HelmetBleMessageListener mHelmetBleMessageListener =
            new HelmetBleManagerListener.HelmetBleMessageListener() {
                @Override
                public void onBatteryLevel(int level) {
                    mHelmet.setBatteryLevel(JSettingBox.getBatteryLevel(
                            mHelmet.getStyle(), level));
                    if (mBinding.cpBattery.getVisibility() == View.GONE) {
                        mBinding.cpBattery.setVisibility(View.VISIBLE);
                    }
                    mBinding.cpBattery.setText(String.valueOf(mHelmet.getBatteryLevel()));
                    mBinding.cpBattery.setValue(mHelmet.getBatteryLevel());
                }

                @Override
                public void onTailLight(boolean value) {
                }

                @Override
                public void onTailLightAck(boolean ack) {
                    endProgressAnimation();
                    if (!ack) {
                        mHelmet.setTailLight(!mHelmet.isTailLight());
                        updateJSettingEnable(JSettingBox.FUNC_TAIL_LIGHT, mHelmet.isTailLight());
                        onSettingFailure();
                    }
                }

                @Override
                public void onBatteryEfficiency(int value) {

                }

                @Override
                public void onBatteryEfficiencyAck(boolean ack) {
                    endProgressAnimation();
                    if (ack) {
                        mHelmet.setBatteryEfficiency(tempChoose);
                        updateJSettingValue(JSettingBox.FUNC_BATTERY_EFFICIENCY, mHelmet.getBatteryEfficiency());
                        onRebootHelmet(getString(R.string.helmet_reboot_apply));
                    } else {
                        onSettingFailure();
                    }
                }

                @Override
                public void onVideoTimestamp(boolean value) {
                }

                @Override
                public void onVideoTimestampAck(boolean ack) {
                    endProgressAnimation();
                    if (!ack) {
                        mHelmet.setVideoTimestamp(!mHelmet.isVideoTimestamp());
                        updateJSettingEnable(JSettingBox.FUNC_VIDEO_TIMESTAMP, mHelmet.isVideoTimestamp());
                        onSettingFailure();
                    } else {
                        onRebootHelmet(getString(R.string.helmet_reboot_apply));
                    }
                }

                @Override
                public void onVideoRecordingTime(int value) {

                }

                @Override
                public void onVideoRecordingTimeAck(boolean ack) {
                    endProgressAnimation();
                    if (ack) {
                        mHelmet.setVideoRecordingTime(tempChoose);
                        updateJSettingValue(JSettingBox.FUNC_RECORDING_TIME, mHelmet.getVideoRecordingTime());
                        onRebootHelmet(getString(R.string.helmet_reboot_apply));
                    } else {
                        onSettingFailure();
                    }
                }

                @Override
                public void onVideoResolution(int value) {

                }

                @Override
                public void onVideoResolutionAck(boolean ack) {
                    endProgressAnimation();
                    if (ack) {
                        mHelmet.setVideoResolution(tempChoose);
                        updateJSettingValue(JSettingBox.FUNC_VIDEO_RESOLUTION, mHelmet.getVideoResolution());
                        onRebootHelmet(getString(R.string.helmet_reboot_apply));
                    } else {
                        onSettingFailure();
                    }
                }

                @Override
                public void onSyncSystemTimeAck(boolean ack) {
                    endProgressAnimation();
                    if (ack) {
                        onRebootHelmet(getString(R.string.helmet_reboot_apply));
                    } else {
                        onSettingFailure();
                    }
                }

                @Override
                public void helmetGyroscope(SensorFusionData data) {

                }

                @Override
                public void helmetAccelerometer(SensorFusionData data) {

                }

                @Override
                public void helmetMagnetometer(SensorFusionData data) {

                }

                @Override
                public void onResetDefaultAck(boolean ack) {
                    endProgressAnimation();
                    if (ack) {
                        onRebootHelmet(getString(R.string.helmet_reboot_apply));
                    } else {
                        onSettingFailure();
                    }
                }
            };

    private void checkConnection(final String func) {
        ConnectionCheck.isReachable("192.168.1.1")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Boolean aBoolean) {
                        Log.d(TAG, "onNext: " + aBoolean);
                        endProgressAnimation();
                        if (aBoolean) {
                            switch (func) {
                                case JSettingBox.FUNC_CAMERA_PREVIEW:
                                    openCameraPreview();
                                    break;
                                case JSettingBox.FUNC_HELMET_VIDEO:
                                    openHelmetVideo();
                                    break;
                                case JSettingBox.FUNC_BOOKMARK_VIDEO:
                                    openBookmarkVideo();
                                    break;
                                case JSettingBox.FUNC_FORMAT:
                                    onFormatWarning();
                                    break;
                            }
                        } else {
                            onConnectWifiWarning();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "check connection error: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void openCameraPreview() {
        Intent intent = new Intent();
        intent.setClass(requireContext(), CameraPreviewActivity.class);
        startActivity(intent);
    }

    void openHelmetVideo() {
        Intent intent = new Intent();
        intent.putExtra(getString(R.string.key_video_index), 0);
        intent.setClass(requireContext(), HelmetVideoActivity.class);
        startActivity(intent);
    }

    void openBookmarkVideo() {
        Intent intent = new Intent();
        intent.putExtra(getString(R.string.key_video_index), 1);
        intent.setClass(requireContext(), HelmetVideoActivity.class);
        startActivity(intent);
    }

    void openOta(String version, String type, String language) {
        Intent intent = new Intent();
        intent.putExtra(getString(R.string.key_helmet_fw_version), version);
        intent.putExtra(getString(R.string.key_helmet_model_number), type);
        intent.putExtra(getString(R.string.key_helmet_model_language), language);
        intent.setClass(requireContext(), OtaActivity.class);
        startActivityForResult(intent, 2266);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2266 && resultCode == Activity.RESULT_OK && data != null) {
            boolean res = data.getBooleanExtra("OTA", false);
            if (res) {
                mViewModel.disconnectHelmet();
            }
        }
    }

    void confirmHelmetFormat() {
        startProgressAnimation();
        Observable.create((ObservableOnSubscribe<Boolean>) emitter -> {
            if (ICatch.getInstance().prepareSession()) {
                if (ICatch.getInstance().formatStorage()) {
                    Log.d(TAG, "format success");
                    emitter.onNext(true);
                } else {
                    Log.e(TAG, "format failed");
                    emitter.onNext(false);
                }
            } else {
                emitter.onNext(false);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onNext(Boolean o) {
                        if (o) {
                            endProgressAnimation();
                            onRebootHelmet(getString(R.string.helmet_format_success));
                        } else {
                            endProgressAnimation();
                            onSettingFailure();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        endProgressAnimation();
                        onSettingFailure();
                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                });
    }

    void onRebootHelmet(String message) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_SHORT)
                .setTextColor( requireActivity().getResources().getColor(R.color.white, null))
                .setBackgroundTint( requireActivity().getResources().getColor(R.color.color_primary_01, null))
                .setActionTextColor( requireActivity().getResources().getColor(R.color.white, null))
                .show();
    }

    void onSettingFailure() {
        new MaterialAlertDialogBuilder(requireContext(), R.style.AppAlertDialog)
                .setTitle(R.string.helmet_setting_failure_title)
                .setMessage(R.string.helmet_setting_failure_content)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }

    public void onConnectWifiWarning() {
        new MaterialAlertDialogBuilder(requireContext(), R.style.AppAlertDialog)
                .setTitle(R.string.helmet_setting_connect_wifi_title)
                .setMessage(R.string.helmet_setting_connect_wifi_content)
                .setPositiveButton(R.string.action_confirm, (DialogInterface dialog, int which) -> {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intent);
                })
                .setNegativeButton(R.string.action_cancel, null)
                .show();
    }

    public void onFormatWarning() {
        new AlertDialog.Builder(requireContext(), R.style.AppAlertDialog)
                .setTitle(R.string.helmet_setting_format_title)
                .setMessage(R.string.helmet_setting_format_content)
                .setPositiveButton(R.string.action_confirm, (DialogInterface dialog, int which) -> {
                    confirmHelmetFormat();
                })
                .setNegativeButton(R.string.action_cancel, null)
                .show();
    }

    public void onOtaLowPowerWarning() {
        new MaterialAlertDialogBuilder(requireContext(), R.style.AppAlertDialog)
                .setMessage(R.string.ota_battery_warning)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }

}