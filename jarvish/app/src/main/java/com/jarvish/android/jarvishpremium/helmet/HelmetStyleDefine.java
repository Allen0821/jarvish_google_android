/**
 * Created by CupLidSheep on 29,December,2020
 */
package com.jarvish.android.jarvishpremium.helmet;

import com.jarvish.android.jarvishpremium.model.HelmetStyle;

import java.util.ArrayList;

class HelmetStyleDefine {

    public static final String HELMET_NAME_RAIDEN_EVOS = "Monaco Evo S";
    public static final String HELMET_NAME_FLASH_1 = "FLASH F1";
    public static final String HELMET_NAME_F2_NEW_MIC = "FLASH F2";
    public static final String HELMET_NAME_RAIDEN_R1_NEW_MIC = "RAIDEN R1";
    public static final String HELMET_NAME_XTREME_X1_FOR_NEW_MIC = "XTREME X1";
    public static final String HELMET_NAME_MONACO_EVO_S2 = "Monaco Evo S2";
    public static final String HELMET_NAME_A2 = "A2";
    public static final String HELMET_NAME_X = "JARVISH X";
    public static final String HELMET_NAME_X_AR = "JARVISH X-AR";
    public static final String HELMET_NAME_AT5 = "AT-KIT";
    public static final String HELMET_NAME_VINTAGE = "AN-KIT";
    public static final String HELMET_NAME_ANC_MIC_X_M26 = "JARVISH X";
    public static final String HELMET_NAME_ANC_MIC_FX_M26 = "FLASH X";

    private static HelmetStyle getMonacoEvoS() {
        return new HelmetStyle("Monaco Evo S", "", "mes01", "01");
    }

    private static HelmetStyle getFlashF1() {
        return new HelmetStyle("FLASH F1", "", "f107", "02");
    }

    private static HelmetStyle getFlashF2() {
        return new HelmetStyle("FLASH F2", "", "f107", "03");
    }

    private static HelmetStyle getRaidenR1() {
        return new HelmetStyle("RAIDEN R1", "", "r119", "04");
    }

    private static HelmetStyle getXtremeX1() {
        return new HelmetStyle("XTREME X1", "", "x120", "05");
    }

    private static HelmetStyle getMonacoEvoS2() {
        return new HelmetStyle("Monaco Evo S2", "", "mes01", "06");
    }

    private static HelmetStyle getA2() {
        return new HelmetStyle("A2", "", "a299", "07");
    }

    private static HelmetStyle getJarvishX() {
        return new HelmetStyle("JARVISH X", "", "x01", "08");
    }

    private static HelmetStyle getJarvishXAR() {
        return new HelmetStyle("JARVISH X-AR", "", "xar01", "09");
    }

    private static HelmetStyle getAT5() {
        return new HelmetStyle("AT5", "", "at599", "10");
    }

    private static HelmetStyle getVintage() {
        return new HelmetStyle("VINTAGE", "", "vintage99", "11");
    }

    private static HelmetStyle getJarvishX2() {
        return new HelmetStyle("JARVISH X", "", "x01", "12");
    }

    private static HelmetStyle getFlashX() {
        return new HelmetStyle("FLASH X", "", "f107", "13");
    }


    public static ArrayList<HelmetStyle> buildHelmetStyleList() {
        ArrayList<HelmetStyle> list = new ArrayList<>();
        list.add(getMonacoEvoS());
        list.add(getFlashF1());
        list.add(getFlashF2());
        list.add(getRaidenR1());
        list.add(getXtremeX1());
        list.add(getMonacoEvoS2());
        list.add(getA2());
        list.add(getJarvishX());
        list.add(getJarvishXAR());
        list.add(getAT5());
        list.add(getVintage());
        list.add(getFlashX());
        return list;
    }
}
