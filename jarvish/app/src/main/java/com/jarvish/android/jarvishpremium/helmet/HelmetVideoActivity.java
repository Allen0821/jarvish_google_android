package com.jarvish.android.jarvishpremium.helmet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.LruCache;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.icatch.wificam.customer.ICatchWificamPlayback;
import com.icatch.wificam.customer.ICatchWificamSession;
import com.icatch.wificam.customer.ICatchWificamVideoPlayback;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.type.ICatchFile;
import com.icatch.wificam.customer.type.ICatchFileType;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.adapter.VideoStorageAdapter;
import com.jarvish.android.jarvishpremium.databinding.ActivityHelmetVideoBindingImpl;
import com.jarvish.android.jarvishpremium.helmet.video.HelmetFile;
import com.jarvish.android.jarvishpremium.helmet.video.HelmetVideoOperation;
import com.jarvish.android.jarvishpremium.helmet.video.PlayHelmetVideoActivity;
import com.jarvish.android.jarvishpremium.helmet.viewmodels.VideoActivityViewModel;
import com.jarvish.android.jarvishpremium.helmet.viewmodels.VideoStorage;
import com.jarvish.android.jarvishpremium.icatch.ICatch;
import com.jarvish.android.jarvishpremium.view.HelmetVideoSectionView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;


public class HelmetVideoActivity extends AppCompatActivity implements
        HelmetVideoSectionView.OnHelmetVideoSectionViewListener {
    private static final String TAG = "HelmetVideoActivity";

    private ActivityHelmetVideoBindingImpl mBinding;
    private VideoActivityViewModel mViewModel;

    private ICatchWificamSession mSession;
    private ICatchWificamPlayback mPlayback;
    private ICatchWificamVideoPlayback mVideoPlayback;
    //private ICatchWificamPlayback mCameraPlayback;
    private LruCache<Integer, Bitmap> mLruCache;
    private final ArrayList<HelmetFile> mHelmetFiles = new ArrayList<>();
    private Handler handler;
    private HelmetVideoOperation mHelmetVideoOperation;

    private SectionedRecyclerViewAdapter mSectionAdapter;
    private final SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());


    private Set<BitmapWorkerTask> mTaskCollection;

    private ICatch mICatch;
    private int mVideoIndex = 0;

    private Animation animation;

    private final ArrayList<VideoStorage> mVideoStorages = new ArrayList<>();
    private VideoStorageAdapter mVideoStorageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_helmet_video);
        mViewModel = new ViewModelProvider(this).get(VideoActivityViewModel.class);
        mBinding.setViewModel(mViewModel);
        mBinding.setLifecycleOwner(this);
        mVideoIndex = getIntent()
                .getIntExtra(getString(R.string.key_video_index), 0);
        Log.d(TAG, "video index: " + mVideoIndex);
        initVideoStorage();
        mBinding.rvStorage.setVisibility(View.GONE);
        setTopAppbar();
        mTaskCollection = new HashSet<>();
        mSectionAdapter = new SectionedRecyclerViewAdapter();
        int maxMemory = (int) Runtime.getRuntime().maxMemory();
        int cacheMemory = maxMemory / 8;
        mLruCache = new LruCache<>(cacheMemory);
        initView();
        handler = new Handler();
        if (initSession()) {
            getHelmetVideos();
        } else {
            Log.e(TAG, "get helmet videos failed");
            iCatchInitFailureAlert();
        }
    }

    private void iCatchInitFailureAlert() {
        new MaterialAlertDialogBuilder(this, R.style.AppAlertDialog)
                .setTitle(R.string.helmet_icatch_connection_fail_title)
                .setMessage(R.string.helmet_icatch_connection_fail_content)
                .setPositiveButton(R.string.action_confirm,
                        (dialogInterface, i) -> {
                            finish();
                        })
                .show();
    }

    private void initVideoStorage() {
        mVideoStorages.add(new VideoStorage(0, "0", getString(R.string.helmet_number_of_videos)));
        mVideoStorages.add(new VideoStorage(1, "0", getString(R.string.helmet_total_videos_size)));
        mVideoStorages.add(new VideoStorage(2, "0", getString(R.string.helmet_free_space)));
        mVideoStorageAdapter = new VideoStorageAdapter(mVideoStorages);
        mBinding.rvStorage.setLayoutManager(new GridLayoutManager(
                HelmetVideoActivity.this,
                3));
        mBinding.rvStorage.setHasFixedSize(true);
        mBinding.rvStorage.setAdapter(mVideoStorageAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        loadBitmaps(mHelmetFiles.size());
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelAllTasks();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        mHelmetFiles.clear();
        mICatch.destroySession();
        mSession = null;
    }


    private void setTopAppbar() {
        if (mVideoIndex == 0) {
            mBinding.tbVideo.setTitle(R.string.helmet_video_title);
        } else {
            mBinding.tbVideo.setTitle(R.string.helmet_video_bookmark_title);
        }
        mBinding.tbVideo.setNavigationOnClickListener((View v) -> finish());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void initView() {
        GridLayoutManager layoutManage = new GridLayoutManager(this, 3);
        layoutManage.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (mSectionAdapter.getSectionItemViewType(position) == SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER) {
                    return 3;
                }
                return 1;
            }
        });
        mBinding.rvHelmetVideo.setLayoutManager(layoutManage);
        mBinding.rvHelmetVideo.setAdapter(mSectionAdapter);
    }


    private boolean initSession() {
        mICatch = ICatch.getInstance();
        if (!mICatch.prepareSession()) {
            Log.e(TAG, "create session failed");
            return false;
        }
        mSession = mICatch.getSession();

        try {
            mPlayback = mSession.getPlaybackClient();
            mHelmetVideoOperation = new HelmetVideoOperation(mPlayback);
        } catch (IchInvalidSessionException e) {
            e.printStackTrace();
            return false;
        }

        try {
            mVideoPlayback = mSession.getVideoPlaybackClient();
        } catch (IchInvalidSessionException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    private void setCountTitle() {
        mVideoStorages.get(0).setValue(String.valueOf(mHelmetFiles.size()));

        long size = ICatch.getInstance().getFreeSpaceInImages();
        long use = 0;
        for(HelmetFile file : mHelmetFiles) {
            use = use + file.getiCatchFile().getFileSize();
        }
        mVideoStorages.get(1).setValue(Util.getDataSize(use));
        mVideoStorages.get(2).setValue(Util.getDataSize(size * 1024 * 1024));
        mVideoStorageAdapter.notifyDataSetChanged();
        mBinding.rvStorage.setVisibility(View.VISIBLE);
    }


    private void startLoadingAnimation() {
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(this, R.anim.moto_loading_1);
        } else {
            animation.reset();
        }
        mBinding.ivLoading.startAnimation(animation);
        mBinding.ivLoading.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void endLoadingAnimation() {
        if (animation != null) {
            mBinding.ivLoading.clearAnimation();
        }
        mBinding.ivLoading.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void getHelmetVideos() {
        try {
            List<ICatchFile> files = mPlayback.listFiles(ICatchFileType.ICH_TYPE_VIDEO);
            Log.d(TAG, "files= " + files.size());
            mHelmetFiles.clear();
            int i = 0;
            for (ICatchFile iCatchFile : files) {
                if (mVideoIndex == 0) {
                    if (iCatchFile.getFileName().contains("M.M")) {
                        continue;
                    }
                } else {
                    if (!iCatchFile.getFileName().contains("M.M")) {
                        continue;
                    }
                }
                Log.d(TAG, "file name: " + iCatchFile.getFileName());
                HelmetFile helmetFile = new HelmetFile();
                helmetFile.setiCatchFile(iCatchFile);
                helmetFile.setIndex(i);
                i++;
                mHelmetFiles.add(helmetFile);
            }
            setCountTitle();

            Collections.sort(mHelmetFiles, (o1, o2) -> {
                Date d1 = convertStringToDate(o1.getiCatchFile().getFileDate());
                Date d2 = convertStringToDate(o2.getiCatchFile().getFileDate());
                if (d1 == null || d2 == null) {
                    return 0;
                }
                if (d1.before(d2)) {
                    return 1;
                } else if (d1.after(d2)) {
                    return -1;
                } else {
                    return 0;
                }
            });
            setHelmetVideosView(mHelmetFiles);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void setHelmetVideosView(ArrayList<HelmetFile> helmetFiles) {
        LinkedHashMap<String, ArrayList<HelmetFile>> hashMap = new LinkedHashMap<>();
        for (HelmetFile f : helmetFiles) {
            //Log.d(TAG, "name= " + f.getiCatchFile().getFileName());
            String date = convertVideoStringDate(f.getiCatchFile().getFileDate());
            //Log.d(TAG, "date= " + date);
            int position = date.indexOf(" ");
            String key = date.substring(0, position);
            if (hashMap.containsKey(key)) {
                //Log.d(TAG, "key= " + key);
                if (hashMap.get(key) != null) {
                    hashMap.get(key).add(f);
                }
            } else {
                ArrayList<HelmetFile> list = new ArrayList<>();
                list.add(f);
                hashMap.put(key, list);
            }
        }


        mSectionAdapter.removeAllSections();
        for (String key : hashMap.keySet()) {
            mSectionAdapter.addSection(key,
                    new HelmetVideoSectionView(key, hashMap.get(key), this));
        }
        mSectionAdapter.notifyDataSetChanged();
        //rvHelmetVideo.scrollToPosition(mHelmetFile.size() - 1);
        loadBitmaps(mHelmetFiles.size());
    }


    private void loadBitmaps(int count) {
        try {
            for (int i = 0; i < count; i++) {
                Bitmap bitmap = mHelmetFiles.get(i).getBitmap();
                if (bitmap == null) {
                    bitmap = mLruCache.get(i);
                    mHelmetFiles.get(i).setBitmap(bitmap);
                }
                if (bitmap == null) {
                    BitmapWorkerTask task = new BitmapWorkerTask();
                    mTaskCollection.add(task);
                    task.execute(mHelmetFiles.get(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public  void  cancelAllTasks() {
        Log.d(TAG, "cancelAllTasks");
        if (mTaskCollection !=  null) {
            for (BitmapWorkerTask task : mTaskCollection) {
                task.cancel( true );
            }
        }
    }



    private String convertVideoStringDate(String strDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.getDefault());
        try {
            Date date = format.parse(strDate);
            if (date != null) {
                return mSimpleDateFormat.format(date);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "unknown";
    }


    public static Date convertStringToDate(String strDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.getDefault());
        try{
            return format.parse(strDate);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onClickItem(String tag, int position, HelmetFile file) {
        Log.d(TAG, "onClickItem: " + file.getiCatchFile().getFileName());
        openVideo(file);
    }


    private void openVideo(HelmetFile file) {
        final MaterialDialog dialog = new MaterialDialog.Builder(HelmetVideoActivity.this)
                .customView(R.layout.helmet_video_dialog,false)
                .show();
        View customView = dialog.getCustomView();
        ImageView img = customView.findViewById(R.id.ivVideo);
        img.setImageBitmap(file.getBitmap());
        TextView title = customView.findViewById(R.id.tvVideoName);
        title.setText(file.getiCatchFile().getFileName());
        TextView date = customView.findViewById(R.id.tvVideoDate);
        date.setText(Util.getHelmetVideoDate(file.getiCatchFile().getFileDate()));
        TextView size = customView.findViewById(R.id.tvVideoSize);
        size.setText(Util.getDataSize(file.getiCatchFile().getFileSize()));
        ImageView close = customView.findViewById(R.id.ivClose);
        close.setOnClickListener(v -> dialog.dismiss());
        ImageView play = customView.findViewById(R.id.ivPlay);
        play.setOnClickListener(v -> {
            Log.d(TAG, "play");
            cancelAllTasks();
            playVideo(file);
        });


        TextView delete = customView.findViewById(R.id.btDelete);
        delete.setOnClickListener((View v) -> {
            Log.d(TAG, "delete");
            cancelAllTasks();
            deleteVideo(file);
            dialog.dismiss();
        });


        TextView copy = customView.findViewById(R.id.btCopy);
        copy.setOnClickListener((View v) -> {
            Log.d(TAG, "copy");
            cancelAllTasks();
            downloadVideo(file.getiCatchFile());
            dialog.dismiss();
        });
    }


    private void playVideo(HelmetFile file) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(() -> endLoadingAnimation());
                Intent intent = new Intent();
                intent.setClass(HelmetVideoActivity.this, PlayHelmetVideoActivity.class);
                intent.putExtra(getString(R.string.key_helmet_video_file), new Gson().toJson(file));
                startActivity(intent);
            }
        }, 500);
        startLoadingAnimation();
    }


    private MaterialDialog mDownloadDialog;
    private void downloadVideo(ICatchFile iCatchFile) {
        if (mDownloadDialog == null) {
            mDownloadDialog = createDownloadDialog(iCatchFile.getFileName());
            mDownloadDialog.show();
        } else {
            mDownloadDialog.setContent(iCatchFile.getFileName());
            mDownloadDialog.setProgress(0);
            mDownloadDialog.setCancelable(false);
            mDownloadDialog.show();
        }
        mHelmetVideoOperation.startDownloadVideo(iCatchFile, new HelmetVideoOperation.HelmetVideoDownloadListener() {
            @Override
            public void onCompleted(File file) {
                mDownloadDialog.setContent(getString(R.string.helmet_video_download_completed));
                Log.d(TAG, "onCompleted");
                scanVideo(file);
            }

            @Override
            public void onFailure() {
                mDownloadDialog.setContent(getString(R.string.helmet_video_download_failed));
                mDownloadDialog.setCancelable(true);
                mDownloadDialog.setCanceledOnTouchOutside(true);
                Log.d(TAG, "onFailure");
            }

            @Override
            public void onError() {
                mDownloadDialog.setContent(getString(R.string.helmet_video_download_failed));
                mDownloadDialog.setCancelable(true);
                mDownloadDialog.setCanceledOnTouchOutside(true);
                Log.d(TAG, "onError");
            }

            @Override
            public void onProgress(int p) {
                mDownloadDialog.setProgress(p);
                Log.d(TAG, "onProgress: " + p);
            }
        });
    }


    private void scanVideo(File file) {
        Log.d(TAG, "scanVideo: " + file.getAbsolutePath());
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        Log.d(TAG, "contentUri: " + contentUri.getPath());
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        mDownloadDialog.setCancelable(true);
        mDownloadDialog.setCanceledOnTouchOutside(true);
    }


    private MaterialDialog createDownloadDialog(String name) {
        return new MaterialDialog.Builder(this)
                .title(getString(R.string.action_download_video))
                .content(name)
                .progress(false, 100, true)
                .cancelable(false)
                .build();
    }


    private void deleteVideo(HelmetFile helmetFile) {
        mHelmetVideoOperation.startDeleteVideo(helmetFile.getiCatchFile(), new HelmetVideoOperation.HelmetVideoDeleteListener() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "delete onCompleted");
                mHelmetFiles.remove(helmetFile);
                setHelmetVideosView(mHelmetFiles);
                setCountTitle();
            }

            @Override
            public void onFailure() {
                Log.e(TAG, "onFailure onCompleted");
            }
        });
    }


    /**
     * Add one Bitmap to Memory LruCache
     *
     * @param key : the key use in LruCache
     * @param bitmap : the Bitmap which we get from Camera
     */
    public  void  addBitmapToMemoryCache(int key, Bitmap bitmap) {
        Log.d("MPhotoWallAdapter", " add bitmap to memory cache start");
        if (key < mHelmetFiles.size()) {
            mHelmetFiles.get(key).setBitmap(bitmap);
            mLruCache.put(key, bitmap);
            //mSectionAdapter.notifyItemChanged(key);
            mSectionAdapter.notifyDataSetChanged();
        }
    }





    class BitmapWorkerTask extends AsyncTask<HelmetFile, Void, Bitmap> {
        private int fileID;

        @Override
        protected Bitmap doInBackground(HelmetFile... helmetFiles) {
            if (isCancelled()) {
                Log.d(TAG, "is cancelled");
                return null;
            }
            fileID = helmetFiles[0].getIndex();
            return  downloadBitmap(helmetFiles[0].getiCatchFile());
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            Log.d(TAG, "onPostExecute");
            if  (bitmap !=  null ) {
                // To save the picture in LrcCache
                Log.d(TAG, "fileID: " + fileID);
                addBitmapToMemoryCache(mHelmetFiles.get(fileID).getIndex(), bitmap);
                mSectionAdapter.notifyItemChanged(fileID);
            }
        }

        /**
         * Create download request to get the thumbnail Bitmap。
         *
         * @param file
         * Picture Bitmap
         * @return Bitmap Object
         */
        private  Bitmap downloadBitmap(ICatchFile file) {
            Bitmap bitmap =  null ;
            ICatchFrameBuffer buffer = null;
            int datalength = 0;
            try {
                buffer = mPlayback.getThumbnail(file);
                datalength = buffer.getFrameSize();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (datalength > 0) {
                bitmap = BitmapFactory.decodeByteArray(buffer.getBuffer(), 0, datalength);
            }

            return  bitmap;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
