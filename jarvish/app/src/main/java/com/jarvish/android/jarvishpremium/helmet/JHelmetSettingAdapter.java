/**
 * Created by CupLidSheep on 25,September,2020
 */
package com.jarvish.android.jarvishpremium.helmet;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.adapter.PersonalFunctionAdapter;
import com.jarvish.android.jarvishpremium.databinding.PersonalFunctionViewBinding;
import com.jarvish.android.jarvishpremium.databinding.SettingInfoViewBinding;
import com.jarvish.android.jarvishpremium.databinding.SettingSectionViewBinding;
import com.jarvish.android.jarvishpremium.databinding.SettingSwichViewBinding;
import com.jarvish.android.jarvishpremium.databinding.SettingTapViewBinding;
import com.jarvish.android.jarvishpremium.helmet.viewholder.InfoViewHolder;
import com.jarvish.android.jarvishpremium.helmet.viewholder.SectionViewHolder;
import com.jarvish.android.jarvishpremium.helmet.viewholder.SwitchViewHolder;
import com.jarvish.android.jarvishpremium.helmet.viewholder.TapViewHolder;

import java.util.ArrayList;


public class JHelmetSettingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "JHelmetAdapter";
    // A menu item view type.
    private static final int SECTION_TYPE = 0;
    private static final int SWITCH_TYPE = 1;
    private static final int TAP_TYPE = 2;
    private static final int INFO_TYPE = 3;

    private final ArrayList<JSetting> mJSettings;
    private final JHelmetSettingAdapterListener mListener;

    public JHelmetSettingAdapter(ArrayList<JSetting> jSettings, JHelmetSettingAdapterListener listener) {
        mJSettings = jSettings;
        mListener = listener;
    }


    @Override
    public int getItemViewType(int position) {
        int type = mJSettings.get(position).getType();
        Log.d(TAG, "get item type: " + type);
        switch (type) {
            case JSettingBox.S_SECTION:
                return SECTION_TYPE;
            case JSettingBox.S_SWITCH_CONTROL:
                return SWITCH_TYPE;
            case JSettingBox.S_TAP_CONTROL:
                return TAP_TYPE;
            default:
                return INFO_TYPE;
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "view type: " + viewType);
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case SECTION_TYPE:
              return new SectionViewHolder(SettingSectionViewBinding
                      .inflate(layoutInflater, parent, false));
            case SWITCH_TYPE:
                return new SwitchViewHolder(SettingSwichViewBinding
                        .inflate(layoutInflater, parent, false));
            case TAP_TYPE:
                return new TapViewHolder(SettingTapViewBinding
                        .inflate(layoutInflater, parent, false));
            default:
                return new InfoViewHolder(SettingInfoViewBinding
                        .inflate(layoutInflater, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case SECTION_TYPE:
                SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
                sectionViewHolder.bind(mJSettings.get(position));
                break;
            case SWITCH_TYPE:
                SwitchViewHolder switchViewHolder = (SwitchViewHolder) holder;
                switchViewHolder.bind(mJSettings.get(position), mListener);
                break;
            case TAP_TYPE:
                TapViewHolder tapViewHolder = (TapViewHolder) holder;
                tapViewHolder.bind(mJSettings.get(position), mListener);
                break;
            default:
                InfoViewHolder infoViewHolder = (InfoViewHolder) holder;
                infoViewHolder.bind(mJSettings.get(position), mListener);
                break;
        }
    }


    @Override
    public int getItemCount() {
        return mJSettings.size();
    }


    public interface JHelmetSettingAdapterListener {
        void onSwitchTailLight(boolean value);
        void onSwitchVideoTimestamp(boolean value);
        void onSetVideoResolution(int value);
        void onSetRecordingTime(int value);
        void onSetBatteryEfficiency(int value);
        void onSyncSystemTime();
        void onResetDefault();
        void onOpenCameraPreview();
        void onOpenHelmetVideo();
        void onOpenBookVideo();
        void onOpenOta();
        void onFormat();
    }
}
