/**
 * Created by CupLidSheep on 25,September,2020
 */
package com.jarvish.android.jarvishpremium.helmet;

import android.graphics.drawable.Drawable;

public class JSetting {
    private int type;
    private String title;
    private String content;
    private Drawable icon;
    private boolean enable;
    private int value;
    private String func;

    public JSetting(int type, String title, String content, Drawable icon, boolean enable, int value, String func) {
        this.type = type;
        this.title = title;
        this.content = content;
        this.icon = icon;
        this.enable = enable;
        this.value = value;
        this.func = func;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getFunc() {
        return func;
    }

    public void setFunc(String func) {
        this.func = func;
    }
}
