/*
 * Created by CupLidSheep on 25,September,2020
 */
package com.jarvish.android.jarvishpremium.helmet;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.ble.JCandy;
import com.jarvish.android.jarvishpremium.ble.JHelmet;
import com.jarvish.android.jarvishpremium.model.Helmet;
import com.jarvish.android.jarvishpremium.model.HelmetStyle;

import java.util.ArrayList;

public class JSettingBox {
    private static final String TAG = "JSettingBox";

    public static final int S_SECTION = 0;
    public static final int S_SWITCH_CONTROL = 1;
    public static final int S_TAP_CONTROL = 2;
    public static final int S_INFO = 3;

    public static final String FUNC_TAIL_LIGHT = "FUNC_TAIL_LIGHT";
    public static final String FUNC_VIDEO_TIMESTAMP = "FUNC_VIDEO_TIMESTAMP";
    public static final String FUNC_VIDEO_RESOLUTION = "FUNC_VIDEO_RESOLUTION";
    public static final String FUNC_RECORDING_TIME = "FUNC_RECORDING_TIME";
    public static final String FUNC_BATTERY_EFFICIENCY = "FUNC_BATTERY_EFFICIENCY";
    public static final String FUNC_SYNC_SYSTEM_TIME = "FUNC_SYNC_SYSTEM_TIME";
    public static final String FUNC_RESET_DEFAULT = "FUNC_RESET_DEFAULT";
    public static final String FUNC_CAMERA_PREVIEW = "FUNC_CAMERA_PREVIEW";
    public static final String FUNC_HELMET_VIDEO = "FUNC_HELMET_VIDEO";
    public static final String FUNC_BOOKMARK_VIDEO = "FUNC_BOOKMARK_VIDEO";
    public static final String FUNC_FORMAT = "FUNC_FORMAT";
    public static final String FUNC_FW_UPDATE = "FUNC_FW_UPDATE";
    public static final String FUNC_SERIAL_NUMBER = "FUNC_SERIAL_NUMBER";
    public static final String FUNC_FW_VERSION = "FUNC_FW_VERSION";


    public static ArrayList<JSetting> buildSettingBox(Context context, @NonNull Helmet helmet) {
        ArrayList<JSetting> jSettings = new ArrayList<>();

        switch (helmet.getStyle()) {
            case JCandy.HELMET_RAIDEN_EVOS:
            case JCandy.HELMET_MONACO_EVO_S2:
                jSettings.add(addSettingTitle(context));
                jSettings.add(addVideoTimestamp(context, helmet.isVideoTimestamp()));
                jSettings.add(addVideoResolution(context, helmet.getVideoResolution()));
                jSettings.add(addRecordingTime(context, helmet.getVideoRecordingTime()));
                jSettings.add(addBatteryEfficiency(context, helmet.getBatteryEfficiency()));
                jSettings.add(addSyncSystemTime(context));
                jSettings.add(addResetDefault(context));

                jSettings.addAll(addWifiFunctions(context));
                break;
            case JCandy.HELMET_FLASH_1:
            case JCandy.HELMET_F2_NEW_MIC:
            case JCandy.HELMET_RAIDEN_R1_NEW_MIC:
            case JCandy.HELMET_XTREME_X1_FOR_NEW_MIC:
            case JCandy.HELMET_A2:
            case JCandy.HELMET_X:
            case JCandy.HELMET_ANC_MIC_X_M26:
            case JCandy.HELMET_ANC_MIC_FX_M26:
            case JCandy.HELMET_X_AR:
                jSettings.add(addSettingTitle(context));
                jSettings.add(addTailLight(context, helmet.isTailLight()));
                jSettings.add(addVideoTimestamp(context, helmet.isVideoTimestamp()));
                jSettings.add(addVideoResolution(context, helmet.getVideoResolution()));
                jSettings.add(addRecordingTime(context, helmet.getVideoRecordingTime()));
                jSettings.add(addBatteryEfficiency(context, helmet.getBatteryEfficiency()));
                jSettings.add(addSyncSystemTime(context));
                jSettings.add(addResetDefault(context));

                jSettings.addAll(addWifiFunctions(context));
                break;
            case JCandy.HELMET_AT5:
            case JCandy.HELMET_VINTAGE:
                Log.d(TAG, "AT5, VINTAGE");
                break;
            default:
                break;
        }
        jSettings.add(addInformationTitle(context));
        jSettings.add(addSerialNumber(context, helmet.getSerialNumber()));
        jSettings.add(addFirmwareVersion(context, helmet.getFirmwareVersion()));

        return jSettings;
    }

    public static ArrayList<JSetting> buildJSettingBox(Context context, JHelmet helmet) {
        ArrayList<JSetting> jSettings = new ArrayList<>();

        switch (helmet.getProduct().getProductType()) {
            case JCandy.HELMET_RAIDEN_EVOS:
            case JCandy.HELMET_MONACO_EVO_S2:
                //jSettings.add(addSettingTitle(context));
                //jSettings.add(addVideoTimestamp(context, helmet.isVideoTimestamp()));
                //jSettings.add(addVideoResolution(context, helmet.getVideoResolution()));
                //jSettings.add(addRecordingTime(context, helmet.getVideoRecordingTime()));
                //jSettings.addAll(addCommonFunctions(context, helmet));
                jSettings.addAll(addWifiFunctions(context));
                break;
            case JCandy.HELMET_FLASH_1:
            case JCandy.HELMET_F2_NEW_MIC:
            case JCandy.HELMET_RAIDEN_R1_NEW_MIC:
            case JCandy.HELMET_XTREME_X1_FOR_NEW_MIC:
            case JCandy.HELMET_A2:
            case JCandy.HELMET_X:
            case JCandy.HELMET_ANC_MIC_X_M26:
            case JCandy.HELMET_ANC_MIC_FX_M26:
            case JCandy.HELMET_X_AR:
                jSettings.add(addSettingTitle(context));
                jSettings.add(addTailLight(context, helmet.isTailLight()));
                jSettings.add(addVideoTimestamp(context, helmet.isVideoTimestamp()));
                jSettings.add(addVideoResolution(context, helmet.getVideoResolution()));
                jSettings.add(addRecordingTime(context, helmet.getVideoRecordingTime()));
                jSettings.addAll(addCommonFunctions(context, helmet));
                jSettings.addAll(addWifiFunctions(context));
                break;
            case JCandy.HELMET_AT5:
            case JCandy.HELMET_VINTAGE:
                Log.d(TAG, "AT5, VINTAGE");
                break;
            default:
                break;
        }
        jSettings.add(addInformationTitle(context));
        jSettings.add(addSerialNumber(context, helmet.getProduct().getSerial()));
        jSettings.add(addFirmwareVersion(context, helmet.getFirmwareVersion()));

        return jSettings;
    }


    public static int getBatteryLevel(String type, int level) {
        if (type.equals(JCandy.HELMET_AT5) ||
                type.equals(JCandy.HELMET_VINTAGE)) {
            switch (level) {
                case 4:
                    return 100;
                case 3:
                    return 80;
                case 2:
                    return 60;
                case 1:
                    return 40;
                case 0:
                    return 20;
                default:
                    return 0;
            }
        }
        return level;
    }


    /* add common functions */
    private static ArrayList<JSetting> addCommonFunctions(Context context, JHelmet helmet) {
        ArrayList<JSetting> jSettings = new ArrayList<>();
        jSettings.add(addBatteryEfficiency(context, helmet.getBatteryEfficiency()));
        jSettings.add(addSyncSystemTime(context));
        jSettings.add(addResetDefault(context));
        return jSettings;
    }


    /* add wifi functions */
    private static ArrayList<JSetting> addWifiFunctions(Context context) {
        ArrayList<JSetting> jSettings = new ArrayList<>();
        jSettings.add(addWifiSettingTitle(context));
        jSettings.add(addCameraPreview(context));
        jSettings.add(addHelmetVideo(context));
        jSettings.add(addBookmarkVideo(context));
        jSettings.add(addFormat(context));
        jSettings.add(addFirmwareUpdate(context));
        return jSettings;
    }


    /* add setting title */
    @NonNull
    private static JSetting addSettingTitle(Context context) {
        return new JSetting(
                S_SECTION,
                context.getString(R.string.helmet_setting_control_title),
                "",
                null,
                false,
                0,
                "");
    }


    /* add tail light */
    @NonNull
    private static JSetting addTailLight(Context context, boolean enable) {
        return new JSetting(
                S_SWITCH_CONTROL,
                context.getString(R.string.helmet_setting_tail_light),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_tail_light),
                enable,
                0,
                FUNC_TAIL_LIGHT);
    }


    /* add video timestamp */
    @NonNull
    private static JSetting addVideoTimestamp(Context context, boolean enable) {
        return new JSetting(
                S_SWITCH_CONTROL,
                context.getString(R.string.helmet_setting_video_timestamp),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_video_timestamp),
                enable,
                0,
                FUNC_VIDEO_TIMESTAMP);
    }


    /* add video resolution */
    @NonNull
    private static JSetting addVideoResolution(Context context, int value) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_video_resolution),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_video_resolution),
                true,
                value,
                FUNC_VIDEO_RESOLUTION);
    }


    /* add recording time */
    @NonNull
    private static JSetting addRecordingTime(Context context, int value) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_recording_time),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_recording_time),
                true,
                value,
                FUNC_RECORDING_TIME);
    }


    /* add battery efficiency */
    @NonNull
    private static JSetting addBatteryEfficiency(Context context, int value) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_battery_efficiency),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_battery_efficiency),
                true,
                value,
                FUNC_BATTERY_EFFICIENCY);
    }


    /* add sync system time */
    @NonNull
    private static JSetting addSyncSystemTime(Context context) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_sync_system_time),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_sync_system),
                true,
                0,
                FUNC_SYNC_SYSTEM_TIME);
    }


    /* add reset_default */
    @NonNull
    private static JSetting addResetDefault(Context context) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_reset_default),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_reset),
                true,
                0,
                FUNC_RESET_DEFAULT);
    }


    /* add need wifi title */
    @NonNull
    private static JSetting addWifiSettingTitle(Context context) {
        return new JSetting(
                S_SECTION,
                context.getString(R.string.helmet_setting_need_wifi_title),
                "",
                null,
                true,
                0,
                "");
    }


    /* add need camera preview */
    @NonNull
    private static JSetting addCameraPreview(Context context) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_camera_preview),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_preview),
                true,
                0,
                FUNC_CAMERA_PREVIEW);
    }


    /* add helmet video */
    @NonNull
    private static JSetting addHelmetVideo(Context context) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_helmet_video),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_view_video),
                true,
                0,
                FUNC_HELMET_VIDEO);
    }


    /* add helmet bookmark video */
    @NonNull
    private static JSetting addBookmarkVideo(Context context) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_helmet_bookmark_video),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_bookmark_video),
                true,
                0,
                FUNC_BOOKMARK_VIDEO);
    }


    /* add format */
    @NonNull
    private static JSetting addFormat(Context context) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_format),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_format_helmet),
                true,
                0,
                FUNC_FORMAT);
    }


    /* add fw update */
    @NonNull
    private static JSetting addFirmwareUpdate(Context context) {
        return new JSetting(
                S_TAP_CONTROL,
                context.getString(R.string.helmet_setting_fw_update),
                "",
                ContextCompat.getDrawable(context, R.drawable.ic_update_helmet),
                true,
                0,
                FUNC_FW_UPDATE);
    }


    /* add information title */
    @NonNull
    private static JSetting addInformationTitle(Context context) {
        return new JSetting(
                S_SECTION,
                context.getString(R.string.helmet_setting_information_title),
                "",
                null,
                false,
                0,
                "");
    }


    /* add serial number */
    @NonNull
    private static JSetting addSerialNumber(Context context, String content) {
        return new JSetting(
                S_INFO,
                context.getString(R.string.helmet_setting_serial_number),
                content,
                ContextCompat.getDrawable(context, R.drawable.ic_serial_number),
                false,
                0,
                FUNC_SERIAL_NUMBER);
    }


    /* add firmware version */
    @NonNull
    private static JSetting addFirmwareVersion(Context context, String content) {
        return new JSetting(
                S_INFO,
                context.getString(R.string.helmet_setting_firmware_version),
                content,
                ContextCompat.getDrawable(context, R.drawable.ic_firmware_version),
                false,
                0,
                FUNC_FW_VERSION);
    }

    public static void setAddedHelmet(Context context, HelmetStyle style) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                "ADDED_HELMET_STYLE", Context.MODE_PRIVATE);
        sharedPref.edit().putString("HELMET_STYLE", new Gson().toJson(style)).apply();
    }

    public static HelmetStyle getAddedHelmet(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                "ADDED_HELMET_STYLE", Context.MODE_PRIVATE);
        String s = sharedPref.getString("HELMET_STYLE", "");
        if (s.isEmpty()) {
            return null;
        } else {
            return new Gson().fromJson(s, HelmetStyle.class);
        }
    }

    public static void removeAddedHelmet(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                "ADDED_HELMET_STYLE", Context.MODE_PRIVATE);
        sharedPref.edit().remove("HELMET_STYLE").apply();
    }
}
