package com.jarvish.android.jarvishpremium.helmet;

import androidx.annotation.NonNull;

public class ProductDefine {

    public static final String NAME_RAIDEN_EVOS = "Monaco Evo S";
    public static final String NAME_FLASH_1 = "FLASH F1";
    public static final String NAME_F2_NEW_MIC = "FLASH F2";
    public static final String NAME_RAIDEN_R1_NEW_MIC = "RAIDEN R1";
    public static final String NAME_XTREME_X1_FOR_NEW_MIC = "XTREME X1";
    public static final String NAME_MONACO_EVO_S2 = "Monaco Evo S2";
    public static final String NAME_A2 = "A2";
    public static final String NAME_X = "JARVISH X";
    public static final String NAME_X_AR = "JARVISH X-AR";
    public static final String NAME_AT5 = "AT-KIT";
    public static final String NAME_VINTAGE = "AN-KIT";
    public static final String NAME_ANC_MIC_X_M26 = "JARVISH X";
    public static final String NAME_ANC_MIC_FX_M26 = "FLASH X";


    public static final String SERIAL_RAIDEN_EVOS = "9860101400000";
    public static final String SERIAL_FLASH_1 = "9860207400000";
    public static final String SERIAL_F2_NEW_MIC = "9860307400000";
    public static final String SERIAL_RAIDEN_R1_NEW_MIC = "9860419400000";
    public static final String SERIAL_XTREME_X1_FOR_NEW_MIC = "9860500400000";
    public static final String SERIAL_MONACO_EVO_S2 = "9860601400000";
    public static final String SERIAL_A2 = "9860700400000";
    public static final String SERIAL_X = "9860801400000";
    public static final String SERIAL_X_AR = "9860900400000";
    public static final String SERIAL_AT5 = "9861000400000";
    public static final String SERIAL_VINTAGE = "9861100400000";
    public static final String SERIAL_ANC_MIC_X_M26 = "9861201400000";
    public static final String SERIAL_ANC_MIC_FX_M26 = "9861307400000";


}
