package com.jarvish.android.jarvishpremium.helmet;

public class SensorData {
    private int accelerometerX;
    private int accelerometerY;
    private int accelerometerZ;

    private int gyroscopeX;
    private int gyroscopeY;
    private int gyroscopeZ;

    private int magnetometerX;
    private int magnetometerY;
    private int magnetometerZ;


    public int getAccelerometerX() {
        return accelerometerX;
    }

    public void setAccelerometerX(int accelerometerX) {
        this.accelerometerX = accelerometerX;
    }

    public int getAccelerometerY() {
        return accelerometerY;
    }

    public void setAccelerometerY(int accelerometerY) {
        this.accelerometerY = accelerometerY;
    }

    public int getAccelerometerZ() {
        return accelerometerZ;
    }

    public void setAccelerometerZ(int accelerometerZ) {
        this.accelerometerZ = accelerometerZ;
    }

    public int getGyroscopeX() {
        return gyroscopeX;
    }

    public void setGyroscopeX(int gyroscopeX) {
        this.gyroscopeX = gyroscopeX;
    }

    public int getGyroscopeY() {
        return gyroscopeY;
    }

    public void setGyroscopeY(int gyroscopeY) {
        this.gyroscopeY = gyroscopeY;
    }

    public int getGyroscopeZ() {
        return gyroscopeZ;
    }

    public void setGyroscopeZ(int gyroscopeZ) {
        this.gyroscopeZ = gyroscopeZ;
    }

    public int getMagnetometerX() {
        return magnetometerX;
    }

    public void setMagnetometerX(int magnetometerX) {
        this.magnetometerX = magnetometerX;
    }

    public int getMagnetometerY() {
        return magnetometerY;
    }

    public void setMagnetometerY(int magnetometerY) {
        this.magnetometerY = magnetometerY;
    }

    public int getMagnetometerZ() {
        return magnetometerZ;
    }

    public void setMagnetometerZ(int magnetometerZ) {
        this.magnetometerZ = magnetometerZ;
    }
}
