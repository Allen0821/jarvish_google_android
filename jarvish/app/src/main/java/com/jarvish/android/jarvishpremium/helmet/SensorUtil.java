package com.jarvish.android.jarvishpremium.helmet;

public class SensorUtil {

    public static float transformGyroscopeAxis(int param) {
        Double d;
        d = (param / 57.29577951 / 1000.0);
        return d.floatValue();
    }



    public static float transformAccelerometerAxis(int param) {
        Double d;
        d = (param / 1000.0 * 9.81);
        return d.floatValue();
    }


    public static float transformMagnetometerAxis(int param) {
        Double d;
        d = (param / 10.0);
        return d.floatValue();
    }
}
