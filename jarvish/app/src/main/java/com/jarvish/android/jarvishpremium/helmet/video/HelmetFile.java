package com.jarvish.android.jarvishpremium.helmet.video;

import android.graphics.Bitmap;

import com.icatch.wificam.customer.type.ICatchFile;

public class HelmetFile {
    private int index;
    private ICatchFile iCatchFile;
    private Bitmap bitmap;


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public ICatchFile getiCatchFile() {
        return iCatchFile;
    }

    public void setiCatchFile(ICatchFile iCatchFile) {
        this.iCatchFile = iCatchFile;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
