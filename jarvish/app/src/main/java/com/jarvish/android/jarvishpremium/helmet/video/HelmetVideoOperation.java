package com.jarvish.android.jarvishpremium.helmet.video;

import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;

import com.icatch.wificam.customer.ICatchWificamPlayback;
import com.icatch.wificam.customer.type.ICatchFile;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class HelmetVideoOperation {
    private static final String TAG = "HelmetVideoOperation";
    private ICatchFile mICatchFile;
    private File mCurrentFile;
    public static final String DOWNLOAD_PATH = "JARVISH";
    private final ICatchWificamPlayback mPlayback;
    private HelmetVideoOperation.HelmetVideoDownloadListener mDownloadListener;
    private HelmetVideoOperation.HelmetVideoDeleteListener mDeleteListener;
    private Disposable mProgressDisposable;


    public HelmetVideoOperation(ICatchWificamPlayback playback) {
        mPlayback = playback;
    }


    public void startDownloadVideo(ICatchFile iCatchFile, HelmetVideoOperation.HelmetVideoDownloadListener listener) {
        mICatchFile = iCatchFile;
        mDownloadListener = listener;
        Observable.create((ObservableOnSubscribe<Integer>) emitter -> {
            try {
                if (downloadFile(iCatchFile)) {
                    emitter.onComplete();
                } else {
                    emitter.onError(new Exception("error"));
                }
            } catch (Exception e) {
                emitter.onError(e);
            }
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull Integer integer) {

            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
                mDownloadListener.onError();
            }

            @Override
            public void onComplete() {
                Log.e(TAG, "onComplete");
                mDownloadListener.onCompleted(mCurrentFile);
            }
        });
    }



    /*public void cancelDownloadVideo() {
        try {
            mPlayback.cancelFileDownload();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mProgressDisposable.dispose();

        if (mCurrentFile.exists()) {
            mCurrentFile.delete();
        }
    }*/


    private boolean downloadFile(ICatchFile iCatchFile) throws Exception {
        mCurrentFile = createFile(iCatchFile.getFileDate());
        String path = mCurrentFile.getAbsolutePath();
        Log.d(TAG, "download to = " + path);

        if (mPlayback == null) {
            Log.e(TAG, "XXXXXX");
        }
        mProgressDisposable = downloadProgress();
        if (mPlayback.downloadFile(iCatchFile, path)) {
            mDownloadListener.onProgress(100);
            mProgressDisposable.dispose();
            return true;
        } else {
            mProgressDisposable.dispose();
            return false;
        }
    }


    private File createFile(String strDate) {
        File folder = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), DOWNLOAD_PATH);
       if(!folder.exists()) {
            folder.mkdirs();
        }
        Log.d(TAG, "folder path: " + folder.getPath());
        String name;
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.ENGLISH);
        try {
            Date date = format.parse(strDate);
            name = date.getTime() + ".MOV";
        } catch (Exception e) {
            e.printStackTrace();
            name = "unknown" + ".MOV";
        }

        File file = new File(folder.getAbsolutePath() + File.separator + name);
        if (file.exists()) {
           file.delete();
        }
        return file;
    }


    private Disposable downloadProgress() {
        Log.d(TAG, "start download progress");
        return Observable.interval(1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        if (mCurrentFile != null) {
                            if (mICatchFile.getFileSize() == mCurrentFile.length()) {
                                mDownloadListener.onProgress(100);
                            } else {
                                long l = mCurrentFile.length() * 100 / mICatchFile.getFileSize();
                                mDownloadListener.onProgress((int) l);
                            }
                        } else {
                            mDownloadListener.onProgress(0);
                        }
                    }
                });
    }


    public void startDeleteVideo(ICatchFile iCatchFile, HelmetVideoOperation.HelmetVideoDeleteListener listener) {
        mDeleteListener = listener;
        Observable.create((ObservableOnSubscribe<Integer>) emitter -> {
            if (deleteFile(iCatchFile)) {
                emitter.onComplete();
            } else {
                emitter.onError(new Exception("error"));
            }
        })
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull Integer integer) {

            }

            @Override
            public void onError(@NonNull Throwable e) {
                mDeleteListener.onFailure();
            }

            @Override
            public void onComplete() {
                mDeleteListener.onCompleted();
            }
        });
    }


    private boolean deleteFile(ICatchFile iCatchFile) throws Exception {
        Log.d(TAG, "delete file = " + iCatchFile.getFileName());
        if (mPlayback == null) {
            Log.e(TAG, "playback null");
        }
        return mPlayback.deleteFile(iCatchFile);
    }


    public interface HelmetVideoDownloadListener {
        void onCompleted(File file);
        void onFailure();
        void onError();
        void onProgress(int p);
    }

    public interface HelmetVideoDeleteListener {
        void onCompleted();
        void onFailure();
    }

}
