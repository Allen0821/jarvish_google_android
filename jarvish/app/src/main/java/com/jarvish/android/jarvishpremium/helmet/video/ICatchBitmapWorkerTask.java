package com.jarvish.android.jarvishpremium.helmet.video;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;

import com.icatch.wificam.customer.ICatchWificamPlayback;
import com.icatch.wificam.customer.exception.IchBufferTooSmallException;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchDeviceException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchNoSuchFileException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.type.ICatchFile;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;

public class ICatchBitmapWorkerTask extends AsyncTask<ICatchFile, Void, Bitmap> {
    private static final String TAG = "ICatchBitmapWorkerTask";
    private final ICatchWificamPlayback mPlayback;
    private  String mFileID;	//use the file name to be the ImageView Tag
    private LruCache<String, Bitmap> mMemoryCache;

    ICatchBitmapWorkerTask(ICatchWificamPlayback playback) {
        this.mPlayback = playback;
    }

    @Override
    protected Bitmap doInBackground(ICatchFile... iCatchFiles) {
        mFileID = iCatchFiles[0].getFileName();
        // download picture in background
        Bitmap bitmap = downloadBitmap(iCatchFiles[0]);
        if  (bitmap !=  null ) {
            // To save the picture in LrcCache
            addBitmapToMemoryCache(iCatchFiles[0].getFileName(), bitmap);
        }
        return  bitmap;
    }


    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
    }


    public  void  addBitmapToMemoryCache(String key, Bitmap bitmap) {
        Log.d(TAG, " add bitmap to memory cache start");
        if  (getBitmapFromMemoryCache(key) ==  null ) {
            mMemoryCache.put(key, bitmap);
        }
    }


    public  Bitmap getBitmapFromMemoryCache(String key) {
        return  mMemoryCache.get(key);
    }

    /**
     * Create download request to get the thumbnail Bitmap。
     *
     * @param file
     * Picture Bitmap
     * @return Bitmap Object
     */
    private Bitmap downloadBitmap(ICatchFile file) {
        Bitmap bitmap =  null ;
        // Log.d("BitmapWorkerTask", " downloadBitmap start");
        ICatchFrameBuffer buffer = null;

        //Log.d("BitmapWorkerTask", " downloadBitmap try to get thumbnail! ");
        try {
            buffer = this.mPlayback.getThumbnail(file);
        } catch (IchNoSuchFileException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchSocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchCameraModeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchBufferTooSmallException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchInvalidSessionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchDeviceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int datalength = buffer.getFrameSize();
        if (datalength > 0) {
            bitmap = BitmapFactory.decodeByteArray(buffer.getBuffer(), 0, datalength);
        }
        return  bitmap;
    }
}
