package com.jarvish.android.jarvishpremium.helmet.video;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.SeekBar;


import com.google.gson.Gson;
import com.icatch.wificam.customer.ICatchWificamControl;
import com.icatch.wificam.customer.ICatchWificamListener;
import com.icatch.wificam.customer.ICatchWificamSession;

import com.icatch.wificam.customer.type.ICatchEvent;
import com.icatch.wificam.customer.type.ICatchEventID;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.TimeTool;
import com.jarvish.android.jarvishpremium.databinding.ActivityPlayHelmetVideoBinding;
import com.jarvish.android.jarvishpremium.icatch.ICatch;


import java.util.concurrent.TimeUnit;


import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PlayHelmetVideoActivity extends AppCompatActivity {
    private static final String TAG = "PlayHelmetVideoActivity";

    private ActivityPlayHelmetVideoBinding mBinding;

    private ICatchWificamSession mSession;
    //private ICatchWificamVideoPlayback mPlayback;
    //private boolean mCreateSession;
    private HelmetFile mHelmetFile;


    private ICatch mICatch;
    private boolean mIsPlaying = true;

    private ICatchWificamControl mICatchWificamControl;

    private Disposable mDisposable;

    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_play_helmet_video);

        mHelmetFile = new Gson().fromJson(getIntent()
                .getStringExtra(getString(R.string.key_helmet_video_file)), HelmetFile.class);
        mBinding.tvName.setText(mHelmetFile.getiCatchFile().getFileName());
        mBinding.sbVideo.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d(TAG, "seekBar onProgressChanged: " + progress);
                setTimeLapsedValue(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBarOnStartTrackingTouch();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBarOnStopTrackingTouch();
            }
        });
        initPlayback();
        setHideControlTimer();

        mBinding.surfaceView.setOnClickListener(v -> onClickSurfaceView());
        mBinding.ivClose.setOnClickListener(v -> onClickClose());
        mBinding.ivPlayVideo.setOnClickListener(v -> onClickPlayVideo());
    }


    private void startLoadingAnimation() {
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(this, R.anim.moto_loading_1);
        } else {
            animation.reset();
        }
        mBinding.ivLoading.startAnimation(animation);
        mBinding.ivLoading.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void endLoadingAnimation() {
        if (animation != null) {
            mBinding.ivLoading.clearAnimation();
        }
        mBinding.ivLoading.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void initPlayback() {
        Log.d(TAG, "init Playback");
        mICatch = ICatch.getInstance();
        if (!mICatch.prepareSession()) {
            Log.e(TAG, "create session failed");
            return;
        }
        mSession = mICatch.getSession();

        try {
            mICatchWificamControl = mSession.getControlClient();
            mICatchWificamControl.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_PLAYBACK_CACHING_CHANGED, mCacheStateChangedListener);
            mICatchWificamControl.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_PLAYBACK_CACHING_PROGRESS, mCacheProgressListener);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "index: " + mHelmetFile.getIndex());
        Log.d(TAG, "name: " + mHelmetFile.getiCatchFile().getFileName());
        Log.d(TAG, "path: " + mHelmetFile.getiCatchFile().getFilePath());
        mBinding.surfaceView.startPlayBack(mSession, mHelmetFile);
        int duration = mBinding.surfaceView.getDuration();
        mBinding.tvEnd.setText(TimeTool.secondsToMinuteOrHours(duration / 100));
        mBinding.sbVideo.setMax(duration);
    }


    protected void onClickSurfaceView() {
        Log.d(TAG, "onClickSurfaceView");
        mBinding.tvName.setVisibility(View.VISIBLE);
        mBinding.tvStart.setVisibility(View.VISIBLE);
        mBinding.tvEnd.setVisibility(View.VISIBLE);
        mBinding.ivPlayVideo.setVisibility(View.VISIBLE);
        mBinding.sbVideo.setVisibility(View.VISIBLE);
        mBinding.ivClose.setVisibility(View.VISIBLE);
        setHideControlTimer();
    }


    private void setHideControlTimer() {
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
        mDisposable = Observable.timer(7, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Long aLong) -> {
                    mBinding.tvName.setVisibility(View.GONE);
                    mBinding.tvStart.setVisibility(View.GONE);
                    mBinding.tvEnd.setVisibility(View.GONE);
                    mBinding.ivPlayVideo.setVisibility(View.GONE);
                    mBinding.sbVideo.setVisibility(View.GONE);
                    mBinding.ivClose.setVisibility(View.GONE);
                }, Throwable::printStackTrace);
    }


    public void setTimeLapsedValue(int progress) {
        mBinding.tvStart.setText(TimeTool.secondsToMinuteOrHours(progress / 100));
    }


    private void seekBarOnStopTrackingTouch() {
        Log.d(TAG, "seek video");
        Observable.create((ObservableOnSubscribe<Boolean>) emitter -> {
            double position = mBinding.sbVideo.getProgress() / 100.0;
            emitter.onNext(mBinding.surfaceView.videoSeek(position));
        })
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onNext(Boolean value) {
                endLoadingAnimation();
                mBinding.ivPlayVideo.setEnabled(true);
                if (value) {
                    Log.d(TAG, "seek success");
                    mBinding.surfaceView.resumePlayback();
                    mBinding.ivPlayVideo.setImageDrawable(
                            ContextCompat.getDrawable(
                                    PlayHelmetVideoActivity.this,
                                    R.drawable.ic_pause_black_32dp));
                    mIsPlaying = true;
                } else {
                    Log.e(TAG, "seek failed");
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "seek error: " + e.getMessage());
            }
        });
    }


    public void seekBarOnStartTrackingTouch() {
        startLoadingAnimation();
        mIsPlaying = false;
        mBinding.surfaceView.pausePlayback();
        mBinding.ivPlayVideo.setImageDrawable(
                ContextCompat.getDrawable(
                        PlayHelmetVideoActivity.this,
                        R.drawable.ic_play_arrow_black_32dp));
        mBinding.ivPlayVideo.setEnabled(false);
    }


    protected void onClickPlayVideo() {
        if (mIsPlaying) {
            mBinding.surfaceView.pausePlayback();
            mIsPlaying = false;
            mBinding.ivPlayVideo.setImageDrawable(ContextCompat.getDrawable(
                    PlayHelmetVideoActivity.this,R.drawable.ic_play_arrow_black_32dp));
        } else {
            mBinding.surfaceView.resumePlayback();
            mIsPlaying = true;
            mBinding.ivPlayVideo.setImageDrawable(ContextCompat.getDrawable(
                    PlayHelmetVideoActivity.this,R.drawable.ic_pause_black_32dp));
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBinding.surfaceView.stopPlayBack();
        mBinding.surfaceView.stop();
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
    }


    void onClickClose() {
        finish();
    }


    private final ICatchWificamListener mCacheStateChangedListener = iCatchEvent -> {
        int temp = Double.valueOf(iCatchEvent.getDoubleValue1() * 100).intValue();
        Log.d(TAG, "CacheStateChangedListener: " + temp);
    };


    private final ICatchWificamListener mCacheProgressListener = new ICatchWificamListener() {
        @Override
        public void eventNotify(ICatchEvent iCatchEvent) {
            //int temp = new Double(iCatchEvent.getDoubleValue1() * 100).intValue();
            int temp = Double.valueOf(iCatchEvent.getDoubleValue1() * 100).intValue();
            Log.d(TAG, "CacheProgressListener: " + temp);
            mBinding.sbVideo.setProgress(temp);
        }
    };


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }


    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }
}
