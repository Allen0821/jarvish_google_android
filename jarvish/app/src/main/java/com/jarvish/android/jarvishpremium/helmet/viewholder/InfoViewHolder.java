/**
 * Created by CupLidSheep on 25,September,2020
 */
package com.jarvish.android.jarvishpremium.helmet.viewholder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.SettingInfoViewBinding;
import com.jarvish.android.jarvishpremium.helmet.JHelmetSettingAdapter;
import com.jarvish.android.jarvishpremium.helmet.JSetting;


public class InfoViewHolder extends RecyclerView.ViewHolder {
    private final SettingInfoViewBinding mBinding;

    public InfoViewHolder(@NonNull SettingInfoViewBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(JSetting setting, JHelmetSettingAdapter.JHelmetSettingAdapterListener listener) {
        mBinding.tvTitle.setText(setting.getTitle());
        mBinding.tvInfo.setText(setting.getContent());
        mBinding.ivIcon.setImageDrawable(setting.getIcon());
    }
}
