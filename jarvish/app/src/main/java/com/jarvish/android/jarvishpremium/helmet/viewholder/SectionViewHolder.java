/**
 * Created by CupLidSheep on 25,September,2020
 */
package com.jarvish.android.jarvishpremium.helmet.viewholder;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.SettingSectionViewBinding;
import com.jarvish.android.jarvishpremium.helmet.JSetting;


public class SectionViewHolder extends RecyclerView.ViewHolder {
    private SettingSectionViewBinding mBinding;

    public SectionViewHolder(@NonNull SettingSectionViewBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(JSetting setting) {
        mBinding.tvTitle.setText(setting.getTitle());
    }
}
