/**
 * Created by CupLidSheep on 25,September,2020
 */
package com.jarvish.android.jarvishpremium.helmet.viewholder;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.SettingSwichViewBinding;
import com.jarvish.android.jarvishpremium.helmet.JHelmetSettingAdapter;
import com.jarvish.android.jarvishpremium.helmet.JSetting;
import com.jarvish.android.jarvishpremium.helmet.JSettingBox;


public class SwitchViewHolder extends RecyclerView.ViewHolder {
    private final SettingSwichViewBinding mBinding;

    public SwitchViewHolder(@NonNull SettingSwichViewBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(@NonNull JSetting setting, @NonNull JHelmetSettingAdapter.JHelmetSettingAdapterListener listener) {
        mBinding.tvTitle.setText(setting.getTitle());
        mBinding.ivIcon.setImageDrawable(setting.getIcon());
        mBinding.swControl.setChecked(setting.isEnable());

        mBinding.swControl.setOnCheckedChangeListener((compoundButton, b) -> {
            if (!compoundButton.isPressed()) {
                return;
            }
            switch (setting.getFunc()) {
                case JSettingBox.FUNC_TAIL_LIGHT:
                    listener.onSwitchTailLight(b);
                    break;
                case JSettingBox.FUNC_VIDEO_TIMESTAMP:
                    listener.onSwitchVideoTimestamp(b);
                    break;
                default:
                    break;
            }
        });
    }
}
