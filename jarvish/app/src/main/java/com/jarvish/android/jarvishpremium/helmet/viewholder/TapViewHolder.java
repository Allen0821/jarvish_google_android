/*
 * Created by CupLidSheep on 25,September,2020
 */
package com.jarvish.android.jarvishpremium.helmet.viewholder;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.SettingTapViewBinding;
import com.jarvish.android.jarvishpremium.helmet.JHelmetSettingAdapter;
import com.jarvish.android.jarvishpremium.helmet.JSetting;
import com.jarvish.android.jarvishpremium.helmet.JSettingBox;


public class TapViewHolder extends RecyclerView.ViewHolder {
    private final SettingTapViewBinding mBinding;

    public TapViewHolder(@NonNull SettingTapViewBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(@NonNull JSetting setting, @NonNull JHelmetSettingAdapter.JHelmetSettingAdapterListener listener) {
        mBinding.tvTitle.setText(setting.getTitle());
        mBinding.ivIcon.setImageDrawable(setting.getIcon());

        itemView.setOnClickListener(view -> {
            switch (setting.getFunc()) {
                case JSettingBox.FUNC_VIDEO_RESOLUTION:
                    listener.onSetVideoResolution(setting.getValue());
                    break;
                case JSettingBox.FUNC_RECORDING_TIME:
                    listener.onSetRecordingTime(setting.getValue());
                    break;
                case JSettingBox.FUNC_BATTERY_EFFICIENCY:
                    listener.onSetBatteryEfficiency(setting.getValue());
                    break;
                case JSettingBox.FUNC_SYNC_SYSTEM_TIME:
                    listener.onSyncSystemTime();
                    break;
                case JSettingBox.FUNC_RESET_DEFAULT:
                    listener.onResetDefault();
                    break;
                case JSettingBox.FUNC_CAMERA_PREVIEW:
                    listener.onOpenCameraPreview();
                    break;
                case JSettingBox.FUNC_HELMET_VIDEO:
                    listener.onOpenHelmetVideo();
                    break;
                case JSettingBox.FUNC_BOOKMARK_VIDEO:
                    listener.onOpenBookVideo();
                    break;
                case JSettingBox.FUNC_FW_UPDATE:
                    listener.onOpenOta();
                    break;
                case JSettingBox.FUNC_FORMAT:
                    listener.onFormat();
                    break;
                default:
                    break;
            }
        });
    }
}
