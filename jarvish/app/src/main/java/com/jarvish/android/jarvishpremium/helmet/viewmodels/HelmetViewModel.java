/**
 * Created by CupLidSheep on 28,January,2021
 */
package com.jarvish.android.jarvishpremium.helmet.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.jarvish.android.jarvishpremium.model.Helmet;
import com.jarvish.android.jarvishpremium.model.HelmetStyle;

public class HelmetViewModel extends ViewModel {

    private final MutableLiveData<Integer> status = new MutableLiveData<>();

    private final MutableLiveData<HelmetStyle> selectedHelmetStyle = new MutableLiveData<>();

    private final MutableLiveData<Helmet> connectedHelmet = new MutableLiveData<>();

    public MutableLiveData<Integer> getStatus() {
        return status;
    }

    public void selectHelmetStyle(HelmetStyle helmetStyle) {
        selectedHelmetStyle.setValue(helmetStyle);
    }

    public MutableLiveData<HelmetStyle> getSelectedHelmetStyle() {
        return selectedHelmetStyle;
    }

    public void setConnectedHelmet(Helmet helmet) {
        connectedHelmet.setValue(helmet);
    }

    public MutableLiveData<Helmet> getConnectedHelmet() {
        return connectedHelmet;
    }

    public void openSelectHelmet() {
        status.setValue(0);
    }

    public void closeSelectHelmet() {
        status.setValue(1);
    }

    public void removeSelectedHelmet() {
        status.setValue(2);
    }

    public void disconnectHelmet() {
        status.setValue(3);
    }
}
