/**
 * Created by CupLidSheep on 10,March,2021
 */
package com.jarvish.android.jarvishpremium.helmet.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.jarvish.android.jarvishpremium.model.HelmetStyle;

public class VideoActivityViewModel extends ViewModel {
    public final MutableLiveData<String> numOfVideos = new MutableLiveData<>();
    public final MutableLiveData<String> freeSpace = new MutableLiveData<>();

    public void setNumOfVideos(String number) {
        numOfVideos.setValue(number);
    }

    public void setFreeSpace(String space) {
        freeSpace.setValue(space);
    }
}
