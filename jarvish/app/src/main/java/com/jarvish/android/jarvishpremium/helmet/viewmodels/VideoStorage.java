/*
 * Created by CupLidSheep on 12,March,2021
 */
package com.jarvish.android.jarvishpremium.helmet.viewmodels;

public class VideoStorage {
    private final int index;
    private String value;
    private final String title;

    public VideoStorage(int index, String value, String title) {
        this.index = index;
        this.value = value;
        this.title = title;
    }

    public int getIndex() {
        return index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

}
