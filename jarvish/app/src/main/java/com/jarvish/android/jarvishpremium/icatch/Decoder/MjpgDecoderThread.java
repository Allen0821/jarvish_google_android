package com.jarvish.android.jarvishpremium.icatch.Decoder;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;
import android.view.SurfaceHolder;


import com.icatch.wificam.customer.ICatchWificamPreview;
import com.icatch.wificam.customer.ICatchWificamVideoPlayback;
import com.icatch.wificam.customer.exception.IchTryAgainException;
import com.icatch.wificam.customer.type.ICatchAudioFormat;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.icatch.wificam.customer.type.ICatchVideoFormat;
import com.jarvish.android.jarvishpremium.icatch.MediaPlayerView;
import com.jarvish.android.jarvishpremium.icatch.ScaleTool;

import java.nio.ByteBuffer;

import static com.jarvish.android.jarvishpremium.icatch.MediaPlayerView.CAMERA_PREVIEW_MODE;
import static com.jarvish.android.jarvishpremium.icatch.MediaPlayerView.VIDEO_PLAY_MODE;

/**
 * Created by zhang yanhu C001012 on 2015/12/4 11:03.
 */
public class MjpgDecoderThread {
    private static final String TAG = "MjpgDecoderThread";
    private final ICatchWificamPreview mWifiCamPreview;
    private final ICatchWificamVideoPlayback mWifiCamVideoPlayback;
    private final MediaPlayerView mMediaPlayerView;
    private Bitmap videoFrameBitmap;
    private int frameWidth;
    private int frameHeight;
    private final SurfaceHolder surfaceHolder;
    private AudioThread audioThread;
    private VideoThread videoThread;
    private final int mPreviewLaunchMode;
    private VideoFramePtsChangedListener videoPbUpdateBarLitener = null;
    private Rect drawFrameRect;
    private final ICatchVideoFormat videoFormat;
    private OnDecodeTimeListener onDecodeTimeListener;

    public void setOnDecodeTimeListener(OnDecodeTimeListener onDecodeTimeListener) {
        this.onDecodeTimeListener = onDecodeTimeListener;
    }

    public MjpgDecoderThread(SurfaceHolder holder,
                             MediaPlayerView mediaPlayerView,
                             int previewLaunchMode,
                             ICatchWificamPreview wifiCamPreview,
                             ICatchWificamVideoPlayback wifiCamVideoPlayback,
                             ICatchVideoFormat iCatchVideoFormat,
                             VideoFramePtsChangedListener videoPbUpdateBarLitener) {
        this.surfaceHolder = holder;
        this.mMediaPlayerView = mediaPlayerView;
        this.mPreviewLaunchMode = previewLaunchMode;
        this.mWifiCamPreview = wifiCamPreview;
        this.mWifiCamVideoPlayback = wifiCamVideoPlayback;
        this.videoFormat = iCatchVideoFormat;
        if (videoFormat != null) {
            frameWidth = videoFormat.getVideoW();
            frameHeight = videoFormat.getVideoH();
        }
        holder.setFormat(PixelFormat.RGBA_8888);
        this.videoPbUpdateBarLitener = videoPbUpdateBarLitener;
    }

    public void start(boolean enableAudio, boolean enableVideo) {
        Log.i(TAG, "start");
        if (enableAudio) {
            audioThread = new AudioThread();
            audioThread.start();
        }
        if (enableVideo) {
            videoThread = new VideoThread();
            videoThread.start();
        }
    }

    public boolean isAlive() {
        if (videoThread != null && videoThread.isAlive()) {
            return true;
        }
        else return audioThread != null && audioThread.isAlive();
    }

    public void stop() {
        if (audioThread != null) {
            audioThread.requestExitAndWait();
        }
        if (videoThread != null) {
            videoThread.requestExitAndWait();
        }
    }

    private class VideoThread extends Thread {
        private boolean done;
        private final ByteBuffer bmpBuf;
        private final byte[] pixelBuf;

        VideoThread() {
            super();
            done = false;
            pixelBuf = new byte[frameWidth * frameHeight * 4];
            bmpBuf = ByteBuffer.wrap(pixelBuf);
            // Trigger onDraw with those initialize parameters
            videoFrameBitmap = Bitmap.createBitmap(frameWidth, frameHeight, Bitmap.Config.ARGB_8888);
            drawFrameRect = new Rect(0, 0, frameWidth, frameHeight);
        }

        @Override
        public void run() {
            Log.i(TAG, "start running video thread");
            ICatchFrameBuffer buffer = new ICatchFrameBuffer(frameWidth * frameHeight * 4);
            buffer.setBuffer(pixelBuf);
            boolean temp = false;
            boolean isSaveBitmapToDb = false;
            boolean isFirstFrame = true;
            boolean isStartGet = true;
            long lastTime = System.currentTimeMillis();
            while (!done) {
                temp = false;
                try {
                    if (mPreviewLaunchMode == CAMERA_PREVIEW_MODE) {
                        temp = mWifiCamPreview.getNextVideoFrame(buffer);
                    } else {
                        temp = mWifiCamVideoPlayback.getNextVideoFrame(buffer);
                        Log.i(TAG, "videoPbControl " + temp);
                    }
                } catch (IchTryAgainException e) {
                    Log.e(TAG, "IchTryAgainException: " + e.getMessage());
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    continue;
                } catch (Exception ex) {
                    Log.e(TAG, "getNextVideoFrame " + ex.getMessage());
                    //ex.printStackTrace();
                    return;
                }
                if (!temp) {
                    Log.e(TAG,"getNextVideoFrame failed\n");
                    continue;
                }
                if (buffer == null || buffer.getFrameSize() == 0) {
                    Log.e(TAG, "getNextVideoFrame buffer == null\n");
                    continue;
                }

                bmpBuf.rewind();
                if (videoFrameBitmap == null) {
                    continue;
                }
                if (isFirstFrame) {
                    isFirstFrame = false;
                    Log.i(TAG, "get first Frame");
                }
                videoFrameBitmap.copyPixelsFromBuffer(bmpBuf);

                if (!isSaveBitmapToDb) {
                    if (videoFrameBitmap != null && mPreviewLaunchMode == CAMERA_PREVIEW_MODE) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                //DatabaseHelper.updateCameraPhoto(GlobalInfo.curSlotId, videoFrameBitmap);
                            }
                        }).start();
                        isSaveBitmapToDb = true;
                    }
                }

                Canvas canvas = surfaceHolder.lockCanvas();
                if (canvas == null) {
                    continue;
                }
                int w = mMediaPlayerView.getWidth();
                int h = mMediaPlayerView.getHeight();
                drawFrameRect = ScaleTool.getScaledPosition(frameWidth, frameHeight, w, h);
                canvas.drawBitmap(videoFrameBitmap, null, drawFrameRect, null);
                surfaceHolder.unlockCanvasAndPost(canvas);
                if (onDecodeTimeListener != null && buffer != null) {
                    if (System.currentTimeMillis() - lastTime > 500) {
                        lastTime = System.currentTimeMillis();
                        long decodeTime = buffer.getDecodeTime();
                        onDecodeTimeListener.decodeTime(decodeTime);
                    }
                }
                if (mPreviewLaunchMode == VIDEO_PLAY_MODE && videoPbUpdateBarLitener != null) {
                    videoPbUpdateBarLitener.onFramePtsChanged(buffer.getPresentationTime());
                }
            }
            Log.i(TAG, "stopMPreview video thread");
        }

        public void requestExitAndWait() {
            // 把这个线程标记为完成，并合并到主程序线程
            done = true;
            /*try {
                join();
            } catch (InterruptedException ex) {
            }*/
        }
    }

    public void redrawBitmap() {
        Log.i(TAG, "redrawBitmap");
        if (videoFrameBitmap != null) {
            Canvas canvas = surfaceHolder.lockCanvas();
            if (canvas == null) {
                return;
            }
            int w = mMediaPlayerView.getWidth();
            int h = mMediaPlayerView.getHeight();
            Log.i(TAG, "redrawBitmap mPreview.getWidth()=" + mMediaPlayerView.getWidth());
            Log.i(TAG, "redrawBitmap mPreview.getHeight()=" + mMediaPlayerView.getHeight());
            Rect drawFrameRect = ScaleTool.getScaledPosition(frameWidth, frameHeight, w, h);
            canvas.drawBitmap(videoFrameBitmap, null, drawFrameRect, null);
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private class AudioThread extends Thread {
        private boolean done = false;
        private AudioTrack audioTrack;

        public void run() {
            Log.i(TAG, "Run AudioThread");
            ICatchAudioFormat audioFormat = null;
            if (mPreviewLaunchMode == CAMERA_PREVIEW_MODE) {
                try {
                    audioFormat = mWifiCamPreview.getAudioFormat();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    audioFormat = mWifiCamVideoPlayback.getAudioFormat();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (audioFormat == null) {
                Log.e(TAG, "Run AudioThread audioFormat is null!");
                return;
            }
            int bufferSize = AudioTrack.getMinBufferSize(audioFormat.getFrequency(), audioFormat.getNChannels() == 2 ? AudioFormat.CHANNEL_IN_STEREO
                    : AudioFormat.CHANNEL_IN_LEFT, audioFormat.getSampleBits() == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT);

            audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, audioFormat.getFrequency(), audioFormat.getNChannels() == 2 ? AudioFormat.CHANNEL_IN_STEREO
                    : AudioFormat.CHANNEL_IN_LEFT, audioFormat.getSampleBits() == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT,
                    bufferSize, AudioTrack.MODE_STREAM);

            audioTrack.play();
            Log.i(TAG, "Run AudioThread 3");
            byte[] audioBuffer = new byte[1024 * 50];
            ICatchFrameBuffer icatchBuffer = new ICatchFrameBuffer(1024 * 50);
            icatchBuffer.setBuffer(audioBuffer);
            boolean temp = false;
            while (!done) {
                temp = false;
                try {
                    if (mPreviewLaunchMode == CAMERA_PREVIEW_MODE) {
                        temp = mWifiCamPreview.getNextAudioFrame(icatchBuffer);
                    } else {
                        temp = mWifiCamVideoPlayback.getNextAudioFrame(icatchBuffer);
                    }
                } catch (IchTryAgainException e) {
                    //AppLog.e(TAG, "getNextAudioFrame IchTryAgainException");
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    continue;
                } catch (Exception ex) {
                    Log.e(TAG, "getNextVideoFrame " + ex.getClass().getSimpleName());
                    ex.printStackTrace();
                    return;
                }
                if (false == temp) {
                    continue;
                }
                audioTrack.write(icatchBuffer.getBuffer(), 0, icatchBuffer.getFrameSize());
            }
            audioTrack.stop();
            audioTrack.release();
            Log.i(TAG, "stopMPreview audio thread");

        }

        public void requestExitAndWait() {
            done = true;
            try {
                join();
            } catch (InterruptedException ex) {
            }
        }
    }

    public void redrawBitmap(SurfaceHolder holder, int w, int h) {
        SurfaceHolder surfaceHolder = holder;
        Log.d(TAG, "redrawBitmap w=" + w + " h=" + h);
        Log.d(TAG, "redrawBitmap frameWidth=" + frameWidth + " frameHeight=" + frameHeight);
        Log.d(TAG, "redrawBitmap videoFrameBitmap=" + videoFrameBitmap);
        if (videoFrameBitmap != null) {
            Canvas canvas = surfaceHolder.lockCanvas();
            if (canvas != null) {
                drawFrameRect = ScaleTool.getScaledPosition(frameWidth, frameHeight, w, h);
                canvas.drawBitmap(videoFrameBitmap, null, drawFrameRect, null);
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }
}
