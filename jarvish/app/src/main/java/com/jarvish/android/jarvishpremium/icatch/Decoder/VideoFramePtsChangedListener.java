package com.jarvish.android.jarvishpremium.icatch.Decoder;

/**
 * Created by b.jiang on 2015/12/25.
 */
public interface VideoFramePtsChangedListener {
    void onFramePtsChanged(double pts);
}
