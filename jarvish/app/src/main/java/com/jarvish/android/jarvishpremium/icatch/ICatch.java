package com.jarvish.android.jarvishpremium.icatch;


import android.util.Log;

import com.icatch.wificam.customer.ICatchWificamAssist;
import com.icatch.wificam.customer.ICatchWificamConfig;
import com.icatch.wificam.customer.ICatchWificamControl;
import com.icatch.wificam.customer.ICatchWificamPlayback;
import com.icatch.wificam.customer.ICatchWificamProperty;
import com.icatch.wificam.customer.ICatchWificamSession;


public class ICatch {
    private static final String TAG = "ICatch";
    private static volatile ICatch mICatch;
    private ICatchWificamSession mSession;
    private boolean mSessionPrepared;


    private ICatch(){
        if (mICatch != null){
            throw new RuntimeException("Use getInstance() method to get ICatch instance.");
        }
        Log.d(TAG, "init ICatch");
        init();
    }


    public static ICatch getInstance() {
        if (mICatch == null) {
            synchronized (ICatch.class) {
                if (mICatch == null) {
                    mICatch = new ICatch();
                }
            }
        }
        return mICatch;
    }


    protected ICatch readResolve() {
        return getInstance();
    }


    private void init() {
        //ICatchWificamConfig.getInstance().enablePTPIP();
        //mSession = new ICatchWificamSession();
        mSessionPrepared = false;
    }


    public void destroyICatch() {
        destroySession();
    }


    public boolean prepareSession() {
        if (!mSessionPrepared) {
            ICatchWificamConfig.getInstance().enablePTPIP();
            mSession = new ICatchWificamSession();
            try {
                if (mSession.prepareSession("192.168.1.1", "anonymous", "anonymous@icatchtek.com")) {
                    mSessionPrepared = true;
                }
            } catch (Exception e) {
                Log.e(TAG, "prepare session exception: " + e.getMessage());
                e.printStackTrace();
            }
        }
        return mSessionPrepared;
    }


    public void destroySession() {
        try {
            if (mSession != null) {
                mSession.destroySession();
                mSession = null;
            }
        } catch (Exception e) {
            Log.e(TAG, "destroy ICatch exception: " + e.getMessage());
            e.printStackTrace();
        }
        mSessionPrepared = false;
    }


    public boolean formatStorage() {
        try {
            ICatchWificamControl camControl = mSession.getControlClient();
            return camControl.formatStorage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getFreeSpaceInImages() {
        try {
            ICatchWificamControl camControl = mSession.getControlClient();
            return camControl.getFreeSpaceInImages();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    public boolean uploadFile(String localPath, String fileName){
        boolean retValue = false;
        try {
            if (prepareSession()) {
                ICatchWificamPlayback playback = mSession.getPlaybackClient();
                retValue = playback.uploadFile(localPath, fileName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retValue;
    }


    public boolean updateFirmware(String fileName){
        boolean retValue = false;
        try {
            ICatchWificamAssist assist = ICatchWificamAssist.getInstance();
            if (prepareSession()) {
                retValue = assist.updateFw(mSession, fileName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retValue;
    }


    public String getFirmwareVersion(){
        String retValue = "";
        try {
            if (prepareSession()) {
                ICatchWificamProperty property = mSession.getPropertyClient();
                retValue = property.getCurrentStringPropertyValue(0xD803);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retValue;
    }


    public String getDeviceID(){
        String retValue = "";
        try {
            if (prepareSession()) {
                ICatchWificamProperty property = mSession.getPropertyClient();
                retValue = property.getCurrentStringPropertyValue(0xD804);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retValue;
    }


    public ICatchWificamSession getSession() {
        return mSession;
    }
}
