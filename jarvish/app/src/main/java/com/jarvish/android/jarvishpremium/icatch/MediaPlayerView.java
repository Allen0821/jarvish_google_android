package com.jarvish.android.jarvishpremium.icatch;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.icatch.wificam.customer.ICatchWificamPreview;
import com.icatch.wificam.customer.ICatchWificamSession;
import com.icatch.wificam.customer.ICatchWificamVideoPlayback;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.exception.IchStreamNotRunningException;
import com.icatch.wificam.customer.exception.IchStreamNotSupportException;
import com.icatch.wificam.customer.type.ICatchCodec;
import com.icatch.wificam.customer.type.ICatchMJPGStreamParam;
import com.icatch.wificam.customer.type.ICatchPreviewMode;
import com.icatch.wificam.customer.type.ICatchVideoFormat;
import com.jarvish.android.jarvishpremium.helmet.video.HelmetFile;
import com.jarvish.android.jarvishpremium.icatch.Decoder.H264DecoderThread;
import com.jarvish.android.jarvishpremium.icatch.Decoder.MjpgDecoderThread;
import com.jarvish.android.jarvishpremium.icatch.Decoder.OnDecodeTimeListener;
import com.jarvish.android.jarvishpremium.icatch.Decoder.VideoFramePtsChangedListener;

public class MediaPlayerView extends SurfaceView implements SurfaceHolder.Callback {
    private static final String TAG = "MediaPlayerView";

    public static final int VIDEO_PLAY_MODE = 1;
    public static final int CAMERA_PREVIEW_MODE = 2;

    private ICatchWificamSession mSession;
    private ICatchWificamPreview mPreview;
    private ICatchWificamVideoPlayback mVideoPlayback;
    private ICatchVideoFormat mVideoFormat;
    private final SurfaceHolder mHolder;
    private int previewCodec;
    private int mFrameW = 0;
    private int mFrameH = 0;
    private boolean mNeedStart = false;
    private boolean mHasSurface = false;
    private int mPreviewLaunchMode;
    private MjpgDecoderThread mJpgDecoderThread;
    private H264DecoderThread h264DecoderThread;

    private final VideoFramePtsChangedListener mVideoPbUpdateBarListener = null;
    private OnDecodeTimeListener mOnDecodeTimeListener;



    public MediaPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mHolder = this.getHolder();
        mHolder.addCallback(this);
    }



    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated hasSurface = " + mHasSurface);
        mHasSurface = true;
        if (mNeedStart) {
            startDecoderThread(mVideoFormat, mPreviewLaunchMode);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, " redrawBitmap");
        Log.d(TAG, "start startDecoderThread.I'm coming......");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "surfaceDestroyed");
        mHasSurface = false;
        stop();
    }


    public boolean startCameraPreview(ICatchWificamSession session, int width, int height) {
        mSession = session;
        try {
            mPreview = mSession.getPreviewClient();
            if (mPreview == null) {
                Log.e(TAG, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            }
            //mPreview.changePreviewMode(ICatchPreviewMode.ICH_VIDEO_PREVIEW_MODE);
            ICatchMJPGStreamParam param = new ICatchMJPGStreamParam(720, 480);
            mPreview.start(param, ICatchPreviewMode.ICH_VIDEO_PREVIEW_MODE);
            mVideoFormat = mPreview.getVideoFormat();
        } catch (IchSocketException e) {
            Log.e(TAG, "IchSocketException");
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchCameraModeException e) {
            Log.e(TAG, "IchCameraModeException");
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchInvalidSessionException e) {
            Log.e(TAG, "IchInvalidSessionException");
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchStreamNotRunningException e) {
            Log.e(TAG, "IchStreamNotRunningException");
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchStreamNotSupportException e) {
            Log.e(TAG, "IchStreamNotSupportException");
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchInvalidArgumentException e) {
            Log.e(TAG, "IchInvalidArgumentException");
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (mVideoFormat != null) {
            mFrameW = mVideoFormat.getVideoW();
            mFrameH = mVideoFormat.getVideoH();
        } else {
            Log.e(TAG, "start camera preview error");
            return false;
        }

        mPreviewLaunchMode = CAMERA_PREVIEW_MODE;
        startDecoderThread(mVideoFormat, mPreviewLaunchMode);
        return true;
    }


    public boolean startPlayBack(ICatchWificamSession session, HelmetFile file) {
        Log.d(TAG, "start " + file.getiCatchFile().getFileName());
        mSession = session;
        try {
            mVideoPlayback = mSession.getVideoPlaybackClient();
            mVideoPlayback.play(file.getiCatchFile());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


        int times = 0;
        while (mFrameW == 0 || mFrameH == 0) {
            if (times > 100) {
                break;
            }
            try {
                mVideoFormat = mVideoPlayback.getVideoFormat();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (mVideoFormat != null) {
                mFrameW = mVideoFormat.getVideoW();
                mFrameH = mVideoFormat.getVideoH();
            } else {
                Log.e(TAG, "xxxxxx");
            }
            try {
                Thread.sleep(33);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "times: " + times);
            times++;
        }
        if (!mHasSurface) {
            mNeedStart = true;
            return false;
        }
        Log.d(TAG, "frameW: " + mFrameW + " frameH: " + mFrameH);
        mPreviewLaunchMode = VIDEO_PLAY_MODE;
        startDecoderThread(mVideoFormat, mPreviewLaunchMode);
        return true;
    }


    public boolean stop() {
        if (mJpgDecoderThread != null) {
            mJpgDecoderThread.stop();
            postInvalidate();
            Log.d(TAG, "start mjpgDecoderThread.isAlive() = " + mJpgDecoderThread.isAlive());
        }
        if (h264DecoderThread != null) {
            h264DecoderThread.stop();
        }
        mVideoFormat = null;
        Log.i(TAG, "end play");
        mNeedStart = false;
        return true;
    }


    public void stopCameraPreview() {
        try {
            mPreview.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void stopPlayBack() {
        Log.d(TAG, "stop playback");
        try {
            mVideoPlayback.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void resumePlayback() {
        Log.d(TAG, "resume playback");
        try {
            mVideoPlayback.resume();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void pausePlayback() {
        Log.d(TAG, "pause playback");
        try {
            mVideoPlayback.pause();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public int getDuration() {
        Log.d(TAG, "begin getVideoDuration");
        double temp = 0;
        try {
            temp = mVideoPlayback.getLength();
        } catch (Exception e) {
            e.printStackTrace();
        }
        int duration = Double.valueOf(temp * 100).intValue();
        Log.d(TAG, "end getVideoDuration length =" + duration);
        return duration;
    }


    public boolean videoSeek(double position) {
        Log.d(TAG, "begin videoSeek position = " + position);
        boolean retValue = false;
        try {
            retValue = mVideoPlayback.seek(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retValue;
    }


    public void startDecoderThread(ICatchVideoFormat videoFormat, int mode) {
        Log.d(TAG, "start startDecoderThread");
        boolean enableAudio = true;
        if (videoFormat == null) {
            Log.d(TAG, "start startDecoderThread videoFormat=" + videoFormat);
            return;
        }
        previewCodec = videoFormat.getCodec();
        enableAudio = true;
        Log.d(TAG, "start startDecoderThread previewCodec=" + previewCodec + " enableAudio=" + enableAudio);
        switch (previewCodec) {
            case ICatchCodec.ICH_CODEC_RGBA_8888:
                mJpgDecoderThread = new MjpgDecoderThread(
                        mHolder,
                        this,
                        mode,
                        mPreview,
                        mVideoPlayback,
                        videoFormat,
                        mVideoPbUpdateBarListener);

                mJpgDecoderThread.setOnDecodeTimeListener(mOnDecodeTimeListener);
                mJpgDecoderThread.start(enableAudio, true);
                setSurfaceViewArea();
                break;
            case ICatchCodec.ICH_CODEC_H264:
                h264DecoderThread = new H264DecoderThread(
                        mHolder,
                        mode,
                        mPreview,
                        mVideoPlayback,
                        videoFormat,
                        mVideoPbUpdateBarListener);

                h264DecoderThread.setOnDecodeTimeListener(mOnDecodeTimeListener);
                h264DecoderThread.start(enableAudio, true);
                setSurfaceViewArea();
                break;
            default:
        }
    }


    public void setSurfaceViewArea() {
        if (mFrameW == 0 || mFrameH == 0) {
            return;
        }
        View parentView = (View) this.getParent();
        int mWidth = parentView.getWidth();
        int mHeigth = parentView.getHeight();
        Log.d(TAG, "start setSurfaceViewArea frmW=" + mFrameW + " frmH=" + mFrameH +
                " mWidth=" + mWidth + " mHeigth=" + mHeigth);

        if (mFrameH <= 0 || mFrameW <= 0) {
            Log.e(TAG, "setSurfaceViewArea frmW or frmH <= 0!!!");
            mHolder.setFixedSize(mWidth, mWidth * 9 / 16);
            return;
        }
        if (mWidth == 0 || mHeigth == 0) {
            return;
        }

        //JIRA IC-666
        if (previewCodec == ICatchCodec.ICH_CODEC_RGBA_8888) {
            if (mJpgDecoderThread != null) {
                mJpgDecoderThread.redrawBitmap(mHolder, mWidth, mHeigth);
            }
        } else if (previewCodec == ICatchCodec.ICH_CODEC_H264) {
            if (mWidth * mFrameH / mFrameW <= mHeigth) {
                mHolder.setFixedSize(mWidth, mWidth * mFrameH / mFrameW);
            } else {
                mHolder.setFixedSize(mHeigth * mFrameH / mFrameW, mHeigth);
            }
        }
        Log.d(TAG, "end setSurfaceViewArea");
    }


    public void setOnDecodeTimeListener(OnDecodeTimeListener onDecodeTimeListener) {
        this.mOnDecodeTimeListener = onDecodeTimeListener;
    }
}