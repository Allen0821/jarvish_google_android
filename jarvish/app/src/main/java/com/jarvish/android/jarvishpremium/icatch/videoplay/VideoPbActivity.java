package com.jarvish.android.jarvishpremium.icatch.videoplay;

import androidx.appcompat.app.AppCompatActivity;

public class VideoPbActivity extends AppCompatActivity {
    /*private static final String TAG = "VideoPbActivity";
    private TextView timeLapsed;
    private TextView timeDuration;
    private SeekBar seekBar;
    private ImageButton play;
    private ImageButton back;
    private ImageButton deleteBtn;
    private ImageButton downloadBtn;
    private ImageButton stopBtn;
    private LinearLayout topBar;
    private RelativeLayout bottomBar;
    private TextView videoNameTxv;
    private MPreview videoPbView;
    private boolean isShowBar = true;
    private ProgressWheel progressWheel;
    private VideoPbPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_pb);
        timeLapsed = (TextView) findViewById(R.id.video_pb_time_lapsed);
        timeDuration = (TextView) findViewById(R.id.video_pb_time_duration);
        seekBar = (SeekBar) findViewById(R.id.video_pb_seekBar);
        play = (ImageButton) findViewById(R.id.video_pb_play_btn);
        back = (ImageButton) findViewById(R.id.video_pb_back);
        stopBtn = (ImageButton) findViewById(R.id.video_pb_stop_btn);

        topBar = (LinearLayout) findViewById(R.id.video_pb_top_layout);
        bottomBar = (RelativeLayout) findViewById(R.id.video_pb_bottom_layout);
        videoPbView = (MPreview) findViewById(R.id.video_pb_view);
        videoNameTxv = (TextView) findViewById(R.id.video_pb_video_name);
        progressWheel = (ProgressWheel) findViewById(R.id.video_pb_spinner);
        deleteBtn = (ImageButton) findViewById(R.id.delete);
        downloadBtn = (ImageButton) findViewById(R.id.download);
        presenter = new VideoPbPresenter(this);
        presenter.setView(this);

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        // do not display menu bar
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        videoPbView.addVideoFramePtsChangedListener(new VideoFramePtsChangedListener() {
            @Override
            public void onFramePtsChanged(double pts) {
                presenter.updatePbSeekbar(pts);
            }
        });
        videoPbView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                presenter.showBar(topBar.getVisibility() == View.VISIBLE ? true : false);

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.stopVideoPb();
                presenter.removeEventListener();
                finish();
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.play();
            }
        });

        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.stopVideoPb();
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.delete();
            }
        });

        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.download();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                presenter.setTimeLapsedValue(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                presenter.seekBarOnStartTrackingTouch();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                presenter.seekBarOnStopTrackingTouch();
            }
        });
    }*/
}
