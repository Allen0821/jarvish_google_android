package com.jarvish.android.jarvishpremium.login

import android.graphics.Color
import android.util.Log
import android.util.Patterns
import android.widget.Button
import androidx.databinding.BindingAdapter
import com.jarvish.android.jarvishpremium.R


@BindingAdapter("isButtonEnable")
fun Button.isButtonEnable(email: String) {
    Log.d("BindingAdapter", "email= $email")
    if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
        isEnabled = true
        setBackgroundColor(resources.getColor(R.color.color_primary_01, null))
    } else {
        isEnabled = false
        setBackgroundColor(Color.GRAY)
    }
}