package com.jarvish.android.jarvishpremium.login.model

import androidx.annotation.StringRes
import com.jarvish.android.jarvishpremium.R

enum class AuthErrorCode(@StringRes val nameResource: Int) {
    NONE(R.string.error_none),
    INVALID_EMAIL_ADDRESS(R.string.error_invalid_email_address),
    ERROR_SEND_SIGN_IN_LINK(R.string.error_send_email_link),
    ERROR_EMAIL_LINK_AUTH(R.string.error_email_link_auth),
    ERROR_SIGN_IN(R.string.error_sign_in),
    ERROR_COLLISION_AUTH(R.string.error_collision_auth);
    //
    /*WEAK_PASSWORD(R.string.error_weak_password),
    PASSWORD_NOT_MATCH(R.string.code_elder_password_not_match),
    EMAIL_ALREADY_IN_USE(R.string.error_email_already_in_use),
    ERROR_USER_NOT_FOUND(R.string.error_user_not_found),
    SEND_VERIFICATION_CODE_FAILURE(R.string.code_elder_send_verification_code_failure),
    SEND_VERIFICATION_LINK_FAILURE(R.string.error_send_verification_link_failure),
    INVALID_VERIFICATION_CODE(R.string.code_elder_invalid_verification_code),
    RESET_PASSWORD_FAILURE(R.string.code_elder_reset_password_failed),
    SIGN_IN_FAILURE(R.string.error_sign_in_failed),
    SIGN_UP_FAILURE(R.string.error_sign_up_failed),
    TRANSFER_ACCOUNT_FAILURE(R.string.error_transfer_account_failure);*/

}