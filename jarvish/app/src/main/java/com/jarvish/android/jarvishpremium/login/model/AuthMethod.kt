package com.jarvish.android.jarvishpremium.login.model

import android.content.Context
import android.util.Log
import com.facebook.AccessToken
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.actionCodeSettings
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.jarvish.android.jarvishpremium.firestore.Account
import com.jarvish.android.jarvishpremium.firestore.Profile
import com.jarvish.android.jarvishpremium.firestore.StoreProfile

private const val TAG = "Auth"
private const val AUTH_SHARED_PREFS = "Auth_shared_prefs"
private const val AUTH_EMAIL_ADDRESS = "auth_email_address"
private const val AUTH_METHOD_ID = "auth_method_id"

class AuthMethod {

    private val auth = Firebase.auth

    fun sendSignInLink(email: String, completed: (AuthErrorCode) -> Unit) {
        Log.d(TAG, "send sign in link to: $email")

        val actionCodeSettings = actionCodeSettings {
            url = "https://jarvish-24e27.firebaseapp.com"
            handleCodeInApp = true
            setAndroidPackageName(
                    "com.jarvish.android.jarvishpremium",
                    false, /* installIfNotAvailable */
                    "null" /* minimumVersion */
            )
        }

        auth.useAppLanguage()
        auth.sendSignInLinkToEmail(email, actionCodeSettings)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "Email sent.")
                        completed(AuthErrorCode.NONE)
                    } else {
                        Log.d(TAG, "error: ${task.exception?.localizedMessage}")
                        completed(AuthErrorCode.ERROR_SEND_SIGN_IN_LINK)
                    }
                }
    }

    fun signInWithEmailLink(email: String, link: String, completed: (AuthErrorCode) -> Unit) {
        Log.d(TAG, "sign in email: $email")
        Log.d(TAG, "sign in link: $link")

        if (auth.isSignInWithEmailLink(link)) {
            auth.signInWithEmailLink(email, link)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Log.d(TAG, "Successfully signed in with email link!")
                            val result = task.result
                            createNewUser(result.user, result.additionalUserInfo) {
                                completed(AuthErrorCode.NONE)
                            }
                        } else {
                            Log.e(TAG, "error signed in with email link: ", task.exception)
                            completed(AuthErrorCode.ERROR_EMAIL_LINK_AUTH)
                        }
                    }
        }
    }

    fun signInWithFacebook(token: AccessToken, completed: (errorCode: AuthErrorCode)-> Unit) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        /*auth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                Log.d(TAG, "Successfully signed in with facebook!")
                val result = it.result
                createNewUser(result.user, result.additionalUserInfo) {
                    completed(AuthErrorCode.NONE)
                }
            } else {
                Log.e(TAG, "error signed in with facebook: ", it.exception)
                completed(AuthErrorCode.ERROR_FACEBOOK_AUTH)
            }
        }*/
        signIn(credential) {
            completed(it)
        }
    }

    fun signInWithGoogle(idToken: String, completed: (errorCode: AuthErrorCode)-> Unit) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        /*auth.signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Log.d(TAG, "Successfully signed in with google!")
                        val result = it.result
                        createNewUser(result.user, result.additionalUserInfo) {
                            completed(AuthErrorCode.NONE)
                        }
                    } else {
                        Log.e(TAG, "error signed in with google: ", it.exception)
                        completed(AuthErrorCode.ERROR_GOOGLE_AUTH)
                    }
                }*/
        signIn(credential) {
            completed(it)
        }
    }

    private fun signIn(credential: AuthCredential, completed: (errorCode: AuthErrorCode)-> Unit) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Log.d(TAG, "Successfully signed in!")
                        val result = it.result
                        createNewUser(result.user, result.additionalUserInfo) {
                            completed(AuthErrorCode.NONE)
                        }
                    } else {
                        Log.e(TAG, "error signed in: ", it.exception)
                        when(it.exception) {
                            is FirebaseAuthUserCollisionException ->
                                completed(AuthErrorCode.ERROR_COLLISION_AUTH)
                            else ->
                                completed(AuthErrorCode.ERROR_SIGN_IN)
                        }
                    }
                }
    }

    private fun createNewUser(user: FirebaseUser?, info: AdditionalUserInfo?, completed: () -> Unit) {
        if (user == null || info == null) {
            completed()
            return
        }

        Log.d(TAG, "isNewUser: " + info.isNewUser)
        if (info.isNewUser) {
            var avatarUri: String? = null
            user.photoUrl?.let { avatarUri = it.toString() }

            val profile = Profile(user.uid, user.displayName, avatarUri)
            val account = Account(uid = user.uid, email = user.email)
            Log.d(TAG, "account: $account")

            val db = Firebase.firestore
            val profileRef = db.collection("profile").document(user.uid)
            val accountRef = db.collection("account").document(user.uid)
            db.runBatch { batch ->
                batch.set(profileRef, profile)
                batch.set(accountRef, account)
            }.addOnCompleteListener {
                completed()
            }
        } else {
            completed()
        }
    }


    companion object {
        fun saveAuthEmailAddress(context: Context, emailAddress: String?) {
            val sharedPref = context.getSharedPreferences(AUTH_SHARED_PREFS, Context.MODE_PRIVATE)
            sharedPref.edit().putString(AUTH_EMAIL_ADDRESS, emailAddress).apply()
        }

        fun getAuthEmailAddress(context: Context): String? {
            val sharedPref = context.getSharedPreferences(AUTH_SHARED_PREFS, Context.MODE_PRIVATE)
            return sharedPref.getString(AUTH_EMAIL_ADDRESS, null)
        }

        fun emailVerificationRedirection(link: String): Boolean
            = link.contains("jarvish-24e27.firebaseapp.com")
    }
}