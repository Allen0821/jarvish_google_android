package com.jarvish.android.jarvishpremium.login.old;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.FragmentForgetBinding;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ForgetFragment.OnForgetFragmentListener} interface
 * to handle interaction events.
 */
public class ForgetFragment extends Fragment {
    //private static final String TAG = "ForgetFragment";

    private OnForgetFragmentListener mListener;

    private String mEmail;

    public ForgetFragment() {
        // Required empty public constructor
    }


    static ForgetFragment newInstance() {
        return new ForgetFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentForgetBinding binding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_forget, container, false);
        View view = binding.getRoot();

        binding.btSubmitReset.setOnClickListener(v -> onClickSubmitReset());
        binding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mEmail = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnForgetFragmentListener) {
            mListener = (OnForgetFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    protected void onClickSubmitReset() {
        if (LoginTool.verifyEmail(mEmail)) {
            mListener.onRequestPasswordFromEmail(mEmail);
        } else {
            new AlertDialog.Builder(requireContext(), R.style.AppAlertDialog)
                    .setTitle(R.string.login_email_invalid)
                    .setNegativeButton(R.string.action_confirm, null)
                    .show();
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnForgetFragmentListener {
        // TODO: Update argument type and name
        void onRequestPasswordFromEmail(String email);
    }



}
