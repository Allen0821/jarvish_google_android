package com.jarvish.android.jarvishpremium.login.old;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.FragmentEmailLoginBinding;
import com.jarvish.android.jarvishpremium.model.JarvishLogin;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginEmailFragment.OnLoginFragmentListener} interface
 * to handle interaction events.
 */
public class LoginEmailFragment extends Fragment {
    private static final String TAG = "LoginFragment";

    private OnLoginFragmentListener mListener;

    private JarvishLogin mJarvishLogin;


    public LoginEmailFragment() {
        // Required empty public constructor
    }


    static LoginEmailFragment newInstance() {
        //Bundle args = new Bundle();
        return new LoginEmailFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentEmailLoginBinding mBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_email_login, container, false);
        View view = mBinding.getRoot();
        mJarvishLogin = new JarvishLogin();

        mBinding.tvForget.setOnClickListener(v -> onClickForget());
        mBinding.btSignIn.setOnClickListener(v -> onClickLogin());
        mBinding.etLoginEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mJarvishLogin.setEmail(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mBinding.etLoginPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mJarvishLogin.setPassword(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentListener) {
            mListener = (OnLoginFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    protected void onClickForget() {
        if (mListener != null) {
            mListener.onForgetPassword();
        }
    }

    protected void onClickLogin() {
        if (checkInformation() && mListener != null) {
            mListener.onLoginWithEmail(mJarvishLogin);
        }
    }


    private boolean checkInformation() {
        if (!LoginTool.verifyEmail(mJarvishLogin.getEmail())) {
            loginAlert(getString(R.string.login_email_invalid));
            return false;
        }


        if (!LoginTool.verifyPassword(mJarvishLogin.getPassword())) {
            loginAlert(getString(R.string.login_password_invalid));
            return false;
        }

        return true;
    }


    private void loginAlert(String title) {
        new AlertDialog.Builder(requireContext(), R.style.AppAlertDialog)
                .setTitle(title)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLoginFragmentListener {
        void onLoginWithEmail(JarvishLogin jarvishLogin);
        void onForgetPassword();
    }
}
