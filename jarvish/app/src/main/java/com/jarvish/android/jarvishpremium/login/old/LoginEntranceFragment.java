package com.jarvish.android.jarvishpremium.login.old;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.FragmentLoginEntranceBinding;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginEntranceFragment.OnLoginEntranceListener} interface
 * to handle interaction events.
 */
public class LoginEntranceFragment extends Fragment {
    //private static final String TAG = "LoginEntranceFragment";

    private OnLoginEntranceListener mListener;

    public LoginEntranceFragment() {
        // Required empty public constructor
    }


    static LoginEntranceFragment newInstance() {
        //Bundle args = new Bundle();
        return new LoginEntranceFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentLoginEntranceBinding binding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_login_entrance, container, false);
        binding.btEmailLogin.setOnClickListener(v -> onClickEmailLogin());
        binding.btFBLogin.setOnClickListener(v -> onClickFBLogin());
        binding.tvRegister.setOnClickListener(v -> onClickRegister());
        return binding.getRoot();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginEntranceListener) {
            mListener = (OnLoginEntranceListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    void onClickEmailLogin() {
        if (mListener != null) {
            mListener.onLoginWithEmail();
        }
    }


    void onClickFBLogin() {
        if (mListener != null) {
            mListener.onLoginWithFb();
        }
    }


    void onClickRegister() {
        if (mListener != null) {
            mListener.onCreateAccount();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLoginEntranceListener {
        // TODO: Update argument type and name
        void onLoginWithEmail();
        void onLoginWithFb();
        void onCreateAccount();
    }
}
