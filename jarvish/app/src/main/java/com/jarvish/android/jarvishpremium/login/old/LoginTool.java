package com.jarvish.android.jarvishpremium.login.old;

import android.util.Log;

import static android.util.Patterns.EMAIL_ADDRESS;

public class LoginTool {
    //private static final String TAG = "LoginTool";

    public static boolean verifyEmail(String email) {
        return email != null && !email.isEmpty() && EMAIL_ADDRESS.matcher(email).matches();
    }


    static boolean verifyPassword(String pwd) {
        if (pwd == null) {
            return false;
        }

        return !pwd.isEmpty();
    }
}
