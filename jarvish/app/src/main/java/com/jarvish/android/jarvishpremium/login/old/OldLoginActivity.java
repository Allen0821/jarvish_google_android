package com.jarvish.android.jarvishpremium.login.old;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.SharedPreferencesUtil;
import com.jarvish.android.jarvishpremium.databinding.ActivityOldLoginBinding;
import com.jarvish.android.jarvishpremium.model.JarvishLogin;
import com.jarvish.android.jarvishpremium.model.JarvishProfile;
import com.jarvish.android.jarvishpremium.model.JarvishRegister;
import com.jarvish.android.jarvishpremium.model.JarvishResetPassword;
import com.jarvish.android.jarvishpremium.model.JarvishSession;
import com.jarvish.android.jarvishpremium.network.JarvishApi;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;


import static com.facebook.login.LoginBehavior.WEB_ONLY;

public class OldLoginActivity extends AppCompatActivity implements
        LoginEntranceFragment.OnLoginEntranceListener,
        LoginEmailFragment.OnLoginFragmentListener,
        ForgetFragment.OnForgetFragmentListener,
        RegisterFragment.OnRegisterFragmentListener,
        ResetPasswordFragment.OnResetPasswordFragmentListener {
    private static final String TAG = "OldLoginActivity";

    private ActivityOldLoginBinding mBinding;

    private final FragmentManager mFragmentManager = getSupportFragmentManager();
    private LoginEntranceFragment mLoginEntranceFragment;
    private LoginEmailFragment mLoginEmailFragment;
    private ForgetFragment mForgetFragment;
    private RegisterFragment mRegisterFragment;
    private ResetPasswordFragment mResetPasswordFragment;


    private int mState = 0;
    private static final int STATE_ENTRANCE = 0;
    private static final int STATE_SIGNIN_EMAIL = 1;
    private static final int STATE_SIGNUP = 2;
    private static final int STATE_FORGET = 3;
    private static final int STATE_RESET = 4;

    private boolean mIsFbLogin = true;
    private String mFbAvatar = "";


    private CallbackManager mCallbackManager;

    private Animation animation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_old_login);

        initFacebookLogin();
        mLoginEntranceFragment = LoginEntranceFragment.newInstance();
        mLoginEmailFragment = LoginEmailFragment.newInstance();
        mForgetFragment = ForgetFragment.newInstance();
        mRegisterFragment = RegisterFragment.newInstance();
        mResetPasswordFragment = ResetPasswordFragment.newInstance();
        mFragmentManager.beginTransaction()
                .add(R.id.fmLogin, mLoginEntranceFragment)
                .commit();

        mBinding.ivBack.setOnClickListener(v -> onClickBack());
    }

    private void startLoadingAnimation() {
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(this, R.anim.moto_loading_1);
        } else {
            animation.reset();
        }
        mBinding.ivLoading.startAnimation(animation);
        mBinding.clLoading.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void endLoadingAnimation() {
        if (animation != null) {
            mBinding.ivLoading.clearAnimation();
        }
        mBinding.clLoading.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onClickBack() {
        stateChange();
    }

    @Override
    public void onBackPressed() {
        stateChange();
    }

    private void stateChange() {
        switch (mState) {
            case STATE_ENTRANCE:
                finish();
                break;
            case STATE_SIGNIN_EMAIL:
                mFragmentManager.beginTransaction()
                        .remove(mLoginEmailFragment)
                        .show(mLoginEntranceFragment)
                        .commit();
                mState = STATE_ENTRANCE;
                break;
            case STATE_SIGNUP:
                mFragmentManager.beginTransaction()
                        .remove(mRegisterFragment)
                        .show(mLoginEntranceFragment)
                        .commit();
                mState = STATE_ENTRANCE;
                break;
            case STATE_FORGET:
                mFragmentManager.beginTransaction()
                        .remove(mForgetFragment)
                        .show(mLoginEmailFragment)
                        .commit();
                mState = STATE_SIGNIN_EMAIL;
                break;
            case STATE_RESET:
                mFragmentManager.beginTransaction()
                        .remove(mResetPasswordFragment)
                        .show(mForgetFragment)
                        .commit();
                mState = STATE_FORGET;
                break;
            default:

        }
    }


    @Override
    public void onLoginWithEmail() {
        Log.d(TAG, "onLoginWithEmail");
        mFragmentManager.beginTransaction()
                .add(R.id.fmLogin, mLoginEmailFragment)
                .hide(mLoginEntranceFragment)
                .commit();
        mState = STATE_SIGNIN_EMAIL;
        mBinding.ivBack.setVisibility(View.VISIBLE);
    }


    @Override
    public void onLoginWithFb() {
        mIsFbLogin = true;
        startLoginFacebook();
    }


    @Override
    public void onCreateAccount() {
        mFragmentManager.beginTransaction()
                .add(R.id.fmLogin, mRegisterFragment)
                .hide(mLoginEntranceFragment)
                .commit();
        mState = STATE_SIGNUP;
        mBinding.ivBack.setVisibility(View.VISIBLE);
    }


    @Override
    public void onLoginWithEmail(JarvishLogin jarvishLogin) {
        //startProgressBar();
        loginWithEmail(jarvishLogin);
    }


    void requestResetPassword(String email) {
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.key_reset_pwd_email), email);
        mResetPasswordFragment.setArguments(bundle);
        mFragmentManager.beginTransaction()
                .add(R.id.fmLogin, mResetPasswordFragment)
                .hide(mForgetFragment)
                .commit();
        mState = STATE_RESET;
    }


    @Override
    public void onRequestPasswordFromEmail(final String email) {
        startLoadingAnimation();
        JarvishApi.getInstance().jarvishRequestResetPassword(email, new JarvishApi.JarvishApiHandler.resetPasswordHandler() {
            @Override
            public void onSuccess() {
                endLoadingAnimation();
                requestResetPassword(email);
            }

            @Override
            public void onError(int error) {
                endLoadingAnimation();
                Log.e(TAG, "request error: " + error);
            }

            @Override
            public void onFailure(String msg) {
                endLoadingAnimation();
                Log.e(TAG, "request failure: " + msg);
            }
        });
    }


    @Override
    public void onForgetPassword() {
        mFragmentManager.beginTransaction()
                .add(R.id.fmLogin, mForgetFragment)
                .hide(mLoginEmailFragment)
                .commit();
        mState = STATE_FORGET;
    }


    /*@Override
    public void onRegister() {
        mState = STATE_SIGNUP;
        mFragmentManager.beginTransaction()
                .add(R.id.fmLogin, mRegisterFragment)
                .hide(mLoginEmailFragment)
                .commit();
    }*/

    @Override
    public void onSignUpWithEmail(JarvishRegister register) {
        signUpWithEmail(register);
    }

    @Override
    public void onSignUpWithFacebook() {
        mIsFbLogin = false;
        startLoginFacebook();
    }


    @Override
    public void onResetPassword(JarvishResetPassword resetPassword) {
        startLoadingAnimation();
        JarvishApi.getInstance().jarvishResetPassword(resetPassword,
                new JarvishApi.JarvishApiHandler.resetPasswordHandler() {
            @Override
            public void onSuccess() {
                endLoadingAnimation();
                resetSuccessAlert();
            }

            @Override
            public void onError(int error) {
                Log.e(TAG, "request error: " + error);
                endLoadingAnimation();
                resetAlert(getString(R.string.hint_reset_password_failed));
            }

            @Override
            public void onFailure(String msg) {
                Log.e(TAG, "request failure: " + msg);
                endLoadingAnimation();
                resetAlert(getString(R.string.hint_reset_password_failed));
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initFacebookLogin() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().setLoginBehavior(WEB_ONLY)
                .registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                Log.e(TAG, "facebook:onSuccess:" + loginResult);
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (JSONObject object, GraphResponse response) -> {
                            Log.d(TAG, "onCompleted object:" + object.toString());
                            try {
                                JSONObject picture = object.getJSONObject("picture").getJSONObject("data");
                                mFbAvatar = picture.getString("url");
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                            if (mIsFbLogin) {
                                loginWithFacebook(loginResult.getAccessToken());
                            } else {
                                registerWithFacebook(loginResult.getAccessToken(), object);
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,birthday,picture.height(500)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, "facebook:onError", error);
            }
        });
    }


    private void loginWithEmail(JarvishLogin jarvishLogin) {
        startLoadingAnimation();
        JarvishApi.getInstance().jarvishLogin(jarvishLogin, new JarvishApi.JarvishApiHandler.loginHandler() {
            @Override
            public void onSuccess(JarvishSession session) {
                String data = new Gson().toJson(session);
                Log.d(TAG, "loginWithEmail:" + data);
                SharedPreferencesUtil.storeLoginData(getApplicationContext(), session.getSession(), session.getMemberId());
                getUserProfile(session);
            }

            @Override
            public void onError(int error) {
                Log.e(TAG, "Login with email error: " + error);
                loginFailure();
                loginAlert(getString(R.string.login_failure),
                        getString(R.string.login_failure_wrong_pwd_id));
            }

            @Override
            public void onFailure(String msg) {
                Log.e(TAG, "Login with email failure: " + msg);
                loginFailure();
                loginAlert(getString(R.string.login_failure),
                        getString(R.string.login_failure_timeout));
            }
        });
    }


    private void loginWithFacebook(AccessToken token) {
        startLoadingAnimation();
        JarvishApi.getInstance().jarvishFacebookLogin(token.getToken(), new JarvishApi.JarvishApiHandler.loginHandler() {
            @Override
            public void onSuccess(JarvishSession session) {
                String data = new Gson().toJson(session);
                Log.d(TAG, "loginWithFacebook:" + data);
                SharedPreferencesUtil.storeLoginData(getApplicationContext(), session.getSession(), session.getMemberId());
                getUserProfile(session);
            }

            @Override
            public void onError(int error) {
                Log.e(TAG, "login with facebook error:" + error);
                loginFailure();
                loginAlert(getString(R.string.login_failure),
                        getString(R.string.login_failure_wrong));
            }

            @Override
            public void onFailure(String msg) {
                Log.e(TAG, "login with facebook failure:" + msg);
                loginFailure();
                loginAlert(getString(R.string.login_failure),
                        getString(R.string.login_failure_wrong));
            }
        });
    }

    private void signUpWithEmail(JarvishRegister register) {
        startLoadingAnimation();
        JarvishApi.getInstance().jarvishRegister(register, new JarvishApi.JarvishApiHandler.registerHandler() {
            @Override
            public void onSuccess() {
                endLoadingAnimation();
                new MaterialAlertDialogBuilder(OldLoginActivity.this, R.style.AppAlertDialog)
                        .setTitle(R.string.hint_register_success)
                        .setMessage(R.string.hint_register_success_confirm)
                        .setNegativeButton(R.string.action_confirm, (DialogInterface dialog, int which) -> stateChange())
                        .show();
            }

            @Override
            public void onError(int error) {
                endLoadingAnimation();
                if (error == 403) {
                    registerAlert(getString(R.string.register_failure),
                            getString(R.string.register_failure_403));
                }
            }

            @Override
            public void onFailure(String msg) {
                endLoadingAnimation();
                registerAlert(getString(R.string.register_failure),
                        getString(R.string.login_failure_wrong));
            }
        });
    }

    private void registerWithFacebook(AccessToken token, JSONObject object) {
        Log.d(TAG, "sign up With Facebook");
        startLoadingAnimation();
        try {
            String email = object.getString("email");
            String name = object.getString("name");
            HashMap<String, Object> map = new HashMap<>();
            map.put("token", token);
            map.put("email", email);
            map.put("first_name", name);
            String body = new Gson().toJson(map);
            JarvishApi.getInstance().jarvishFacebookRegister(body, new JarvishApi.JarvishApiHandler.registerHandler() {
                @Override
                public void onSuccess() {
                    loginWithFacebook(token);
                }

                @Override
                public void onError(int error) {
                    Log.e(TAG, "registerWithFacebook error:" + error);
                    loginFailure();
                    if (error == 403) {
                        loginAlert(getString(R.string.register_failure),
                                getString(R.string.register_failure_403));
                    }
                }

                @Override
                public void onFailure(String msg) {
                    Log.e(TAG, "registerWithFacebook failure:" + msg);
                    loginFailure();
                    loginAlert(getString(R.string.register_failure),
                            getString(R.string.login_failure_wrong));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            loginFailure();
            loginAlert(getString(R.string.register_failure),
                    getString(R.string.login_failure_wrong));
        }
    }


    private void getUserProfile(JarvishSession session) {
        JarvishApi.getInstance().jarvishGetProfile(session.getSession(), session.getMemberId(), new JarvishApi.JarvishApiHandler.getProfileHandler() {
            @Override
            public void onSuccess(JarvishProfile profile) {
                String data = new Gson().toJson(profile);
                Log.d(TAG, "getUserProfile:" + data);
                SharedPreferencesUtil.storeUserProfileAvatar(getApplicationContext(), mFbAvatar);
                SharedPreferencesUtil.storeUserProfileName(getApplicationContext(), profile.getUserName());
                loginCompleted();
            }

            @Override
            public void onError(int error) {
                loginFailure();
            }

            @Override
            public void onFailure(String msg) {
                loginFailure();
            }
        });
    }


    private void loginFailure() {
        LoginManager.getInstance().logOut();
        endLoadingAnimation();
    }

    private void loginCompleted() {
        Log.d(TAG, "login completed");
        Intent intent = new Intent();
        intent.setAction("RELOAD.FUNC");
        sendBroadcast(intent);
        endLoadingAnimation();
        finish();
    }


    private void startLoginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
    }


    private void loginAlert(String title, String content) {
        new AlertDialog.Builder(OldLoginActivity.this, R.style.AppAlertDialog)
                .setTitle(title)
                .setMessage(content)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }


    private void registerAlert(String title, String content) {
        new MaterialAlertDialogBuilder(OldLoginActivity.this, R.style.AppAlertDialog)
                .setTitle(title)
                .setMessage(content)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }

    private void resetAlert(String content) {
        new MaterialAlertDialogBuilder(OldLoginActivity.this, R.style.AppAlertDialog)
                .setTitle(content)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }

    private void resetSuccessAlert() {
        new MaterialAlertDialogBuilder(OldLoginActivity.this, R.style.AppAlertDialog)
                .setTitle(R.string.reset_password_success)
                .setNegativeButton(R.string.action_confirm, (dialogInterface, i) -> {
                    mFragmentManager.beginTransaction()
                            .show(mLoginEmailFragment)
                            .remove(mResetPasswordFragment)
                            .commit();
                    mState = STATE_SIGNIN_EMAIL;
                })
                .show();
    }
    /* void saveFbAvatar() {
        Glide.with(this)
                .asBitmap()
                .load(mFbAvatar)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        try (FileOutputStream out = new FileOutputStream("")) {
                            resource.compress(Bitmap.CompressFormat.PNG, 100, out);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
    }*/

}
