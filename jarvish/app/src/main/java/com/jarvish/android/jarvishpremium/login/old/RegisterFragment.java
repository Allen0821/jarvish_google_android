package com.jarvish.android.jarvishpremium.login.old;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.FragmentRegisterBinding;
import com.jarvish.android.jarvishpremium.model.JarvishRegister;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment.OnRegisterFragmentListener} interface
 * to handle interaction events.
 */
public class RegisterFragment extends Fragment {
    private static final String TAG = "RegisterFragment";

    private OnRegisterFragmentListener mListener;

    private JarvishRegister mRegister;
    private boolean mIsConfirmPwd = false;


    public RegisterFragment() {
        // Required empty public constructor
    }


    static RegisterFragment newInstance() {
        return new RegisterFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentRegisterBinding binding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_register, container, false);
        View view = binding.getRoot();
        mRegister = new JarvishRegister();

        binding.etRegisterName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mRegister.setFirstName(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.etRegisterEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mRegister.setEmail(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.etRegisterPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mRegister.setPassword(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.etRegisterConfirmPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mIsConfirmPwd = charSequence.toString().equals(mRegister.getPassword());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.btSignUp.setOnClickListener(v -> onClickSignUp());
        binding.btFBRegister.setOnClickListener(v -> onClickFacebookRegister());

        return view;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnRegisterFragmentListener) {
            mListener = (OnRegisterFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    protected void onClickSignUp() {
        if (checkInformation()) {
            Log.d(TAG, "password not same");
            new MaterialAlertDialogBuilder(requireContext(), R.style.AppAlertDialog)
                    .setTitle(R.string.hint_register_warning)
                    .setMessage(R.string.hint_register_email_warning)
                    .setPositiveButton(R.string.action_confirm, (DialogInterface dialog, int which) -> {
                        if (mListener != null) {
                            mListener.onSignUpWithEmail(mRegister);
                        }
                    })
                    .setNegativeButton(R.string.action_cancel, null)
                    .show();
        }
    }


    protected void onClickFacebookRegister() {
        if (mListener != null) {
            mListener.onSignUpWithFacebook();
        }
    }


    private boolean checkInformation() {
        if (!LoginTool.verifyEmail(mRegister.getEmail())) {
            loginAlert(getString(R.string.login_email_invalid));
            return false;
        }


        if (!mIsConfirmPwd || !LoginTool.verifyPassword(mRegister.getPassword())) {
            loginAlert(getString(R.string.login_password_invalid));
            return false;
        }

        return true;
    }


    private void loginAlert(String title) {
        new MaterialAlertDialogBuilder(requireContext(), R.style.AppAlertDialog)
                .setTitle(title)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }


    /*private void startSignUp() {
        startLoadingAnimation();
        JarvishApi.getInstance().jarvishRegister(mRegister, new JarvishApi.JarvishApiHandler.registerHandler() {
            @Override
            public void onSuccess() {
                endLoadingAnimation();
                new MaterialAlertDialogBuilder(requireContext(), R.style.MyAlertDialogTheme)
                        .setTitle(R.string.hint_register_success)
                        .setMessage(R.string.hint_register_success_confirm)
                        .setNegativeButton(R.string.action_confirm, (DialogInterface dialog, int which) -> {
                            if(mListener != null) {
                                mListener.onRegisterCompleted();
                            }
                        })
                        .show();
            }

            @Override
            public void onError(int error) {
                endLoadingAnimation();
                if (error == 403) {
                    registerAlert(getString(R.string.register_failure),
                            getString(R.string.register_failure_403));
                }
            }

            @Override
            public void onFailure(String msg) {
                endLoadingAnimation();
                registerAlert(getString(R.string.register_failure),
                        getString(R.string.login_failure_wrong));
            }
        });
    }*/


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnRegisterFragmentListener {
        void onSignUpWithEmail(JarvishRegister register);
        void onSignUpWithFacebook();
    }

}
