package com.jarvish.android.jarvishpremium.login.old;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.FragmentResetPasswordBinding;
import com.jarvish.android.jarvishpremium.model.JarvishResetPassword;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ResetPasswordFragment.OnResetPasswordFragmentListener} interface
 * to handle interaction events.
 */
public class ResetPasswordFragment extends Fragment {
    private static final String TAG = "ResetPasswordFragment";
    private OnResetPasswordFragmentListener mListener;

    private JarvishResetPassword mJarvishResetPassword;
    private String mRePwd;
    private String mEmail;

    public ResetPasswordFragment() {
        // Required empty public constructor
    }


    static ResetPasswordFragment newInstance() {
        return new ResetPasswordFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentResetPasswordBinding binding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_reset_password, container, false);
        View view = binding.getRoot();

        mJarvishResetPassword = new JarvishResetPassword();
        if (getArguments() != null) {
            mEmail = getArguments().getString(getString(R.string.key_reset_pwd_email));
        }
        Log.d(TAG, "email: " + mEmail);
        mJarvishResetPassword.setEmail(mEmail);

        binding.etVerification.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mJarvishResetPassword.setVerifyCode(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.etNewPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mJarvishResetPassword.setPassword(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.etNewPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mRePwd = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.btResetSubmit.setOnClickListener(v -> onClickResetSubmit());

        return view;
    }



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnResetPasswordFragmentListener) {
            mListener = (OnResetPasswordFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    protected void onClickResetSubmit() {
        if (checkPassword()) {
            if (mListener != null) {
                mListener.onResetPassword(mJarvishResetPassword);
            }
        }
    }


    private void resetAlert(String content) {
        new MaterialAlertDialogBuilder(requireContext(), R.style.AppAlertDialog)
                .setTitle(content)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }


    private boolean checkPassword() {
        if (mJarvishResetPassword.getVerifyCode() == null || mJarvishResetPassword.getVerifyCode().isEmpty()) {
            resetAlert(getString(R.string.hint_input_verification_code));
            return false;
        }
        if (!LoginTool.verifyPassword(mJarvishResetPassword.getPassword())) {
            resetAlert(getString(R.string.login_password_invalid));
            return false;
        }
        if (!mJarvishResetPassword.getPassword().equals(mRePwd)) {
            resetAlert(getString(R.string.login_password_invalid));
            return false;
        }
        return true;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnResetPasswordFragmentListener {
        // TODO: Update argument type and name
        void onResetPassword(JarvishResetPassword resetPassword);
    }
}
