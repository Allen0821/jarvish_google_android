package com.jarvish.android.jarvishpremium.login.ui

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.jarvish.android.jarvishpremium.MainActivity
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.ViewUtil
import com.jarvish.android.jarvishpremium.databinding.ActivityAuthenticationBinding
import com.jarvish.android.jarvishpremium.login.model.AuthErrorCode
import com.jarvish.android.jarvishpremium.login.viewmodel.AuthenticationViewModel

private const val TAG = "Auth"

class AuthenticationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAuthenticationBinding
    private lateinit var viewModel: AuthenticationViewModel

    private var canBack = true

    private lateinit var callbackManager: CallbackManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_authentication)
        binding.activity = this
        viewModel = AuthenticationViewModel()
        binding.viewModel = viewModel

        binding.lifecycleOwner = this


        viewModel.isOpen = intent.getBooleanExtra(getString(R.string.auth_open_app), false)
        Log.d(TAG, "is open: ${viewModel.isOpen}")

        if (viewModel.isOpen) {
            val email = intent.getStringExtra(getString(R.string.auth_email_address))
            val link = intent.getStringExtra(getString(R.string.auth_link))
            Log.d(TAG, "email address: $email")
            Log.d(TAG, "auth link: $link")
            viewModel.signInWithEmailLink(email, link)
        }

        viewModel.signInResult.observe(this) { event ->
            canBack = true
            event.getContentIfNotHandled()?.let {
                when (it) {
                    AuthErrorCode.NONE -> {
                        onSkip(RESULT_OK)
                    }
                    else -> ViewUtil.alertView(this, it.nameResource)
                }
            }
        }
    }

    fun onEmailLinkAuth() {
        startActivity(Intent(this, EmailLinkAuthActivity::class.java))
    }

    fun onFacebookAuth() {
        callbackManager = CallbackManager.Factory.create()
        val loginManager = LoginManager.getInstance()
        loginManager.logOut()
        loginManager
                .setLoginBehavior(LoginBehavior.WEB_ONLY)
                .registerCallback(callbackManager,
                        object : FacebookCallback<LoginResult> {
                            override fun onSuccess(result: LoginResult?) {
                                if (result != null) {
                                    canBack = false
                                    viewModel.signInWithFacebook(result.accessToken)
                                }
                            }

                            override fun onCancel() {
                                Log.d(TAG, "cancel")
                            }

                            override fun onError(error: FacebookException?) {
                                if (error != null) {
                                    Log.d(TAG, "error: " + error.localizedMessage)
                                }
                            }

                        })
        loginManager
                .logInWithReadPermissions(this, listOf("email", "public_profile"))
    }

    private var sigInWithGoogleReq =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(it.data)
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    val account = task.getResult(ApiException::class.java)!!
                    Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
                    viewModel.signInWithGoogle(account.idToken!!)
                } catch (e: ApiException) {
                    Log.e(TAG, "Google sign in failed " + e.localizedMessage)
                    Log.e(TAG, "Google sign in failed " + e)
                    ViewUtil.alertView(this, AuthErrorCode.ERROR_SIGN_IN.nameResource)
                }
    }

    fun onSigInWithGoogle() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("1093439405302-gm08c4ql23rv03k4m8ocdm087jvql58d.apps.googleusercontent.com")
                .requestEmail()
                .build()
        sigInWithGoogleReq.launch(GoogleSignIn.getClient(this, gso).signInIntent)
    }

    fun onSkip(requestCode: Int) {
        if (viewModel.isOpen) {
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            setResult(requestCode)
        }
        finish()


    }


    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 64206) {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onBackPressed() {
        if (viewModel.isOpen) {
            moveTaskToBack(true)
        } else {
            setResult(RESULT_CANCELED)
            finish()
        }
    }
}