package com.jarvish.android.jarvishpremium.login.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.HintRequest
import com.google.android.gms.auth.api.credentials.IdentityProviders
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.ViewUtil
import com.jarvish.android.jarvishpremium.databinding.ActivityEmailLinkAuthBinding
import com.jarvish.android.jarvishpremium.login.model.AuthErrorCode
import com.jarvish.android.jarvishpremium.login.viewmodel.EmailLinkAuthViewModel

private const val TAG = "Auth"

class EmailLinkAuthActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEmailLinkAuthBinding
    private lateinit var viewModel: EmailLinkAuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_email_link_auth)
        viewModel = EmailLinkAuthViewModel()

        binding.activity = this
        binding.viewModel = viewModel

        binding.sendLinkView.activity = this
        binding.sendLinkView.viewModel = viewModel

        binding.linkSentView.viewModel = viewModel

        binding.lifecycleOwner = this

        viewModel.sendAuthLinkResult.observe(this) { it ->
            it.getContentIfNotHandled()?.let {
                if (it != AuthErrorCode.NONE)
                ViewUtil.alertView(this, it.nameResource)
            }
        }

        emailSuggestions()
    }

    private var resultHintRequest = registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) {
        if (it.resultCode == RESULT_OK && it.data != null) {
            val credential = it.data!!.getParcelableExtra<Credential>(Credential.EXTRA_KEY)
            if (credential != null && credential.id.isNotEmpty()) {
                Log.d(TAG, "on selected Email " + credential.id)
                viewModel.emailAddress.value = credential.id
            } else
                Log.e(TAG, "No Selection")
        }
    }

    private fun emailSuggestions() {
        val hintRequest = HintRequest.Builder()
                .setEmailAddressIdentifierSupported(true)
                .setAccountTypes(IdentityProviders.GOOGLE)
                .build()
        val intent = Credentials.getClient(this).getHintPickerIntent(hintRequest)
        resultHintRequest.launch(IntentSenderRequest.Builder(intent).build())
    }

    fun onSendSignInLink() {
        ViewUtil.hideKeyboard(this, binding.root)
        viewModel.onSendSignInLink(this)
    }

}