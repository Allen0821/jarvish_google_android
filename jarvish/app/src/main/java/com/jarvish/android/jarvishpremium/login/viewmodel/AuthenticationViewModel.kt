package com.jarvish.android.jarvishpremium.login.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.facebook.AccessToken
import com.jarvish.android.jarvishpremium.login.Event
import com.jarvish.android.jarvishpremium.login.model.*


class AuthenticationViewModel: ViewModel() {
    val authMethod = AuthMethod()
    var isOpen: Boolean = false
    val isProgress = MutableLiveData<Boolean>().apply { value = false }


    private val _signInResult = MutableLiveData<Event<AuthErrorCode>>()
    val signInResult: LiveData<Event<AuthErrorCode>>
        get() = _signInResult


    fun signInWithEmailLink(email: String?, link: String?) {
        if (email is String && link is String) {
            isProgress.value = true
            authMethod.signInWithEmailLink(email, link) {
                _signInResult.value = Event(it)
                if (it != AuthErrorCode.NONE) {
                    isProgress.value = false
                }
            }
        }
    }

    fun signInWithFacebook(token: AccessToken) {
        isProgress.value = true
        authMethod.signInWithFacebook(token) {
            _signInResult.value = Event(it)
            if (it != AuthErrorCode.NONE) {
                isProgress.value = false
            }

        }
    }

    fun signInWithGoogle(idToken: String) {
        isProgress.value = true
        authMethod.signInWithGoogle(idToken) {
            _signInResult.value = Event(it)
            if (it != AuthErrorCode.NONE) {
                isProgress.value = false
            }

        }
    }

}