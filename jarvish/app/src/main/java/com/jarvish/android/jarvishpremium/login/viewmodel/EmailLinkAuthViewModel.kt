package com.jarvish.android.jarvishpremium.login.viewmodel

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jarvish.android.jarvishpremium.ViewUtil
import com.jarvish.android.jarvishpremium.login.Event
import com.jarvish.android.jarvishpremium.login.model.AuthErrorCode
import com.jarvish.android.jarvishpremium.login.model.AuthMethod

private const val TAG = "Auth"

class EmailLinkAuthViewModel: ViewModel() {
    var isSent = MutableLiveData<Boolean>().apply { value = false }

    var isProgress = MutableLiveData<Boolean>().apply { value = false }

    var emailAddress = MutableLiveData<String>().apply { value = "" }

    private val _sendAuthLinkResult = MutableLiveData<Event<AuthErrorCode>>()
    val sendAuthLinkResult: LiveData<Event<AuthErrorCode>>
        get() = _sendAuthLinkResult

    private val authMethod = AuthMethod()

    fun onSendSignInLink(context: Context) {
        isProgress.value = true
        emailAddress.value?.let { it ->
            authMethod.sendSignInLink(it) {
                if (it == AuthErrorCode.NONE) {
                    AuthMethod.saveAuthEmailAddress(context, emailAddress.value)
                    isSent.value = true
                }
                isProgress.value = false
                _sendAuthLinkResult.value = Event(it)
            }
        }
    }

    fun onReSendSignInLink(context: Context) {
        isProgress.value = true
        emailAddress.value?.let { it ->
            authMethod.sendSignInLink(it) {
                if (it == AuthErrorCode.NONE) {
                    AuthMethod.saveAuthEmailAddress(context, emailAddress.value)
                }
                isProgress.value = false
                _sendAuthLinkResult.value = Event(it)
            }
        }
    }

    fun openEmailBox(context: Context) {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_APP_EMAIL)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }
}