/**
 * Created by CupLidSheep on 30,December,2020
 */
package com.jarvish.android.jarvishpremium.model;

import androidx.annotation.NonNull;

public class Helmet {
    private String name;
    private String style;
    private String image;
    private String serialNumber;
    private String firmwareVersion;
    private String language = "";

    /* Helmet settings */
    //private boolean proximity;
    private boolean tailLight;
    private int batteryEfficiency;
    private int videoResolution;
    private int videoRecordingTime;
    private boolean videoTimestamp;
    private int batteryLevel;

    /* product identification */
    private Product product;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isTailLight() {
        return tailLight;
    }

    public void setTailLight(boolean tailLight) {
        this.tailLight = tailLight;
    }

    public int getBatteryEfficiency() {
        return batteryEfficiency;
    }

    public void setBatteryEfficiency(int batteryEfficiency) {
        this.batteryEfficiency = batteryEfficiency;
    }

    public int getVideoResolution() {
        return videoResolution;
    }

    public void setVideoResolution(int videoResolution) {
        this.videoResolution = videoResolution;
    }

    public int getVideoRecordingTime() {
        return videoRecordingTime;
    }

    public void setVideoRecordingTime(int videoRecordingTime) {
        this.videoRecordingTime = videoRecordingTime;
    }

    public boolean isVideoTimestamp() {
        return videoTimestamp;
    }

    public void setVideoTimestamp(boolean videoTimestamp) {
        this.videoTimestamp = videoTimestamp;
    }

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String paresLanguage(@NonNull String serial) {
        String language;
        if (serial.startsWith("78861")) {
            language = "86";
        } else {
            language =  serial.substring(1, 3);
        }
        return language;
    }

}
