package com.jarvish.android.jarvishpremium.model;

import com.google.gson.annotations.SerializedName;

public class HelmetInfo {
    @SerializedName("Serial Number")
    private String serialNumber;

    @SerializedName("Firmware Version")
    private String firmwareVersion;

    @SerializedName("ICP Name")
    private String icpName;


    @SerializedName("Battery Level")
    private int batteryLevel;


    @SerializedName("Tail Light")
    private boolean tailLight;


    @SerializedName("Proximity Sensor")
    private boolean proximitySensor;


    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public String getIcpName() {
        return icpName;
    }

    public void setIcpName(String icpName) {
        this.icpName = icpName;
    }

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public boolean isTailLight() {
        return tailLight;
    }

    public void setTailLight(boolean tailLight) {
        this.tailLight = tailLight;
    }

    public boolean isProximitySensor() {
        return proximitySensor;
    }

    public void setProximitySensor(boolean proximitySensor) {
        this.proximitySensor = proximitySensor;
    }
}
