/**
 * Created by CupLidSheep on 29,December,2020
 */
package com.jarvish.android.jarvishpremium.model;

public class HelmetStyle {
    private String name;
    private final String information;
    private String image;
    private String index;

    public HelmetStyle(String name, String information, String image, String index) {
        this.name = name;
        this.information = information;
        this.image = image;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return information;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
