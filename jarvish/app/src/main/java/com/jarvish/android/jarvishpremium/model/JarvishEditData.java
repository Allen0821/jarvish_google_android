package com.jarvish.android.jarvishpremium.model;

public class JarvishEditData {
    private String op;
    private String field;
    private String fieldType;
    private String value;

    public JarvishEditData(String field, String value) {
        this.op = "replace";
        this.fieldType = "String";
        this.field = field;
        this.value = value;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
