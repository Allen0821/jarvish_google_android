package com.jarvish.android.jarvishpremium.model;

import com.google.gson.annotations.SerializedName;

public class JarvishProfile {
    @SerializedName("id")
    private String id;

    @SerializedName("user_name")
    private String userName;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("middle_name")
    private String middleName;

    @SerializedName("birthday")
    private String birthday;

    @SerializedName("email")
    private String email;

    @SerializedName("gender")
    private String gender;

    @SerializedName("country_code")
    private String countryCode;

    @SerializedName("zip_code")
    private String zipCode;

    @SerializedName("address")
    private String address;

    @SerializedName("reg_source")
    private String regSource;

    @SerializedName("social_id")
    private String socialId;

    @SerializedName("phone1")
    private String phone1;

    @SerializedName("phone2")
    private String phone2;

    @SerializedName("line_id")
    private String lineId;

    @SerializedName("wechat_id")
    private String wechatId;

    @SerializedName("emergency_contact")
    private String emergencyContact;

    @SerializedName("emergency_contact_phone")
    private String emergencyContactPhone;

    @SerializedName("created")
    private String created;

    @SerializedName("city")
    private String city;

    @SerializedName("head_shot")
    private String headShot;

    @SerializedName("headSize")
    private String headSize;

    @SerializedName("head_hsize")
    private String headLength;

    @SerializedName("head_vsize")
    private String headWidth;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegSource() {
        return regSource;
    }

    public void setRegSource(String regSource) {
        this.regSource = regSource;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getWechatId() {
        return wechatId;
    }

    public void setWechatId(String wechatId) {
        this.wechatId = wechatId;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHeadShot() {
        return headShot;
    }

    public void setHeadShot(String headShot) {
        this.headShot = headShot;
    }

    public String getHeadSize() {
        return headSize;
    }

    public void setHeadSize(String headSize) {
        this.headSize = headSize;
    }

    public String getHeadLength() {
        return headLength;
    }

    public void setHeadLength(String headLength) {
        this.headLength = headLength;
    }

    public String getHeadWidth() {
        return headWidth;
    }

    public void setHeadWidth(String headWidth) {
        this.headWidth = headWidth;
    }
}
