package com.jarvish.android.jarvishpremium.model;

import com.google.gson.annotations.SerializedName;

public class JarvishRegisterHelmet {
    @SerializedName("memberId")
    private int uid;

    @SerializedName("serialNumber")
    private String serialNumber;


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
