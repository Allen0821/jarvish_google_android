package com.jarvish.android.jarvishpremium.model;

import com.google.gson.annotations.SerializedName;

public class JarvishResetPassword {
    @SerializedName("password")
    private String password;
    @SerializedName("email")
    private String email;
    @SerializedName("verificationCode")
    private String verifyCode;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }
}
