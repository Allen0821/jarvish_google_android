/**
 * Created by CupLidSheep on 05,March,2021
 */
package com.jarvish.android.jarvishpremium.model;

public class Product {
    private static final String TAG = "Product";
    private String year;
    private String language;
    private final String productType;
    private String productColor;
    private String productSize;
    private String manufacturerNumber;
    private String verifyCode;

    public Product(String serial) {
        String productType1;
        if (serial == null) {
            productType1 = "";
        } else {
            if (serial.startsWith("78861")) {
                productType1 = "01";
            } else {
                if (serial.length() != 13) {
                    productType1 = "";
                } else {
                    this.year = serial.substring(0, 1);
                    this.language = serial.substring(1, 3);
                    productType1 = serial.substring(3, 5);
                    this.productColor = serial.substring(5, 7);
                    this.productSize = serial.substring(7, 8);
                    this.manufacturerNumber = serial.substring(8, 12);
                    this.verifyCode = serial.substring(12, 13);

                    int type = Integer.parseInt(productType1);
                    if (type > 13 || type < 1) {
                        productType1 = "";
                    }
                }
            }
        }
        this.productType = productType1;
    }

    public String getYear() {
        return year;
    }

    public String getLanguage() {
        return language;
    }

    public String getProductType() {
        return productType;
    }

    public String getProductColor() {
        return productColor;
    }

    public String getProductSize() {
        return productSize;
    }

    public String getManufacturerNumber() {
        return manufacturerNumber;
    }

    public String getVerifyCode() {
        return verifyCode;
    }
}
