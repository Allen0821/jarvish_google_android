package com.jarvish.android.jarvishpremium.model;

import android.graphics.drawable.Drawable;

public class PzzaFuc {
    private String title;
    private Drawable icon;
    private int type;


    public PzzaFuc(String title, Drawable icon, int type) {
        this.title = title;
        this.icon = icon;
        this.type = type;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
