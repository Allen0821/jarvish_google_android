package com.jarvish.android.jarvishpremium.model;

public class UserManual {
    private String name;
    private String link;

    public UserManual(String name, String link) {
        this.name = name;
        this.link = link;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
