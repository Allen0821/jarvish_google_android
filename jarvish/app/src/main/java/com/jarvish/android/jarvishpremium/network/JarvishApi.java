package com.jarvish.android.jarvishpremium.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.jarvish.android.jarvishpremium.model.JarvishLogin;
import com.jarvish.android.jarvishpremium.model.JarvishProfile;
import com.jarvish.android.jarvishpremium.model.JarvishRegister;
import com.jarvish.android.jarvishpremium.model.JarvishRegisterHelmet;
import com.jarvish.android.jarvishpremium.model.JarvishResetPassword;
import com.jarvish.android.jarvishpremium.model.JarvishSession;

import org.json.JSONObject;

import java.security.cert.CertificateException;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.reactivex.annotations.NonNull;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class JarvishApi {
    private static final String TAG = "JarvishApi";
    private static volatile JarvishApi mJarvishApi;
    private static final String JARVISH_URL = "https://developer.jarvish.com/";
    private static final String JARVISH_API_KEY = "JarvishAPIKey";


    private final JarvishApiInterface mJarvishApiInterface;


    private JarvishApi() {
        if (mJarvishApi != null){
            throw new RuntimeException("Use getInstance() method to get JarvishApi instance.");
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(JARVISH_URL)
                .client(getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        mJarvishApiInterface = retrofit.create(JarvishApiInterface.class);
    }


    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public static JarvishApi getInstance() {
        if (mJarvishApi == null) {
            synchronized (JarvishApi.class) {
                if (mJarvishApi == null) {
                    mJarvishApi = new JarvishApi();
                }
            }
        }
        return mJarvishApi;
    }



    public void jarvishLogin(JarvishLogin login, JarvishApiHandler.loginHandler handler) {
        String body = new Gson().toJson(login);
        Log.d(TAG, "jarvishLogin body: " + body);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);
        mJarvishApiInterface.loginUser(requestBody).enqueue(new Callback<JarvishSession>() {
            @Override
            public void onResponse(Call<JarvishSession> call, Response<JarvishSession> response) {
                Log.d(TAG, "jarvishLogin onResponse: " + response.toString());
                if (response.isSuccessful() && response.code() == 200) {
                    handler.onSuccess(response.body());
                } else {
                    handler.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<JarvishSession> call, Throwable t) {
                handler.onFailure(t.getMessage());
            }
        });
    }


    public void jarvishFacebookLogin(@NonNull String token, JarvishApiHandler.loginHandler handler) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("provider", "facebook");
        map.put("token", token);
        String body = new Gson().toJson(map);
        Log.d(TAG, "jarvishLoginFacebook body: " + body);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);
        mJarvishApiInterface.loginFacebookUser(requestBody).enqueue(new Callback<JarvishSession>() {
            @Override
            public void onResponse(Call<JarvishSession> call, Response<JarvishSession> response) {
                Log.d(TAG, "jarvishLoginFacebook onResponse: " + response.toString());
                if (response.isSuccessful() && response.code() == 200) {
                    handler.onSuccess(response.body());
                } else {
                    handler.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<JarvishSession> call, Throwable t) {
                handler.onFailure(t.getMessage());
            }
        });
    }


    public void jarvishRegister(JarvishRegister register, JarvishApiHandler.registerHandler handler) {
        String body = new Gson().toJson(register);
        Log.d(TAG, "jarvishRegister body: " + body);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);
        mJarvishApiInterface.registerUser(requestBody).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                Log.d(TAG, "sign up onResponse: " + response.toString());
                if (response.isSuccessful() && response.code() == 201) {
                    handler.onSuccess();
                } else {
                    handler.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handler.onFailure(t.getMessage());
            }
        });
    }


    public void jarvishFacebookRegister(String body, JarvishApiHandler.registerHandler handler) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);
        mJarvishApiInterface.registerFacebookUser(requestBody).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, "sign up with fb onResponse: " + response.toString());
                if (response.isSuccessful() && response.code() == 201) {
                    handler.onSuccess();
                } else {
                    handler.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handler.onFailure(t.getMessage());
            }
        });
    }


    public void jarvishGetProfile(String session, String uid, JarvishApiHandler.getProfileHandler handler) {
        Log.d(TAG, "jarvishGetProfile session: " + session);
        Log.d(TAG, "jarvishGetProfile uid: " + uid);
        mJarvishApiInterface.getUserProfile(session, uid).enqueue(new Callback<JarvishProfile>() {
            @Override
            public void onResponse(Call<JarvishProfile> call, Response<JarvishProfile> response) {
                Log.d(TAG, "jarvishGetProfile onResponse: " + response.toString());
                if (response.isSuccessful() && response.code() == 200) {
                    handler.onSuccess(response.body());
                } else {
                    handler.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<JarvishProfile> call, Throwable t) {
                Log.d(TAG, "jarvishGetProfile onFailure: " + t.getMessage());
                handler.onFailure(t.getMessage());
            }
        });
    }


    public void jarvishUpdateProfile(String session, String uid, String data, JarvishApiHandler.getProfileHandler handler) {
        Log.d(TAG, "jarvishUpdateProfile session: " + session);
        Log.d(TAG, "jarvishUpdateProfile uid: " + uid);
        Log.d(TAG, "jarvishUpdateProfile body: " + data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), data);
        mJarvishApiInterface.updateUserProfile(session, uid, requestBody).enqueue(new Callback<JarvishProfile>() {
            @Override
            public void onResponse(Call<JarvishProfile> call, Response<JarvishProfile> response) {
                Log.d(TAG, "jarvishUpdateProfile onResponse: " + response.toString());
                if (response.isSuccessful() && response.code() == 200) {
                    handler.onSuccess(response.body());
                } else {
                    handler.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<JarvishProfile> call, Throwable t) {
                Log.d(TAG, "jarvishUpdateProfile onFailure: " + t.getMessage());
                handler.onFailure(t.getMessage());
            }
        });
    }



    public void jarvishRegisterHelmet(String session, JarvishRegisterHelmet registerHelmet, JarvishApiHandler.OnRegisterHelmet handler) {
        String data = new Gson().toJson(registerHelmet);
        Log.d(TAG, "jarvishRegisterHelmet body: " + data);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), data);
        mJarvishApiInterface.registerHelmet(session, requestBody).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.d(TAG, "jarvishRegisterHelmet onResponse: " + response.toString());
                if (response.isSuccessful() && response.code() == 201) {
                    handler.onSuccess();
                } else {
                    try {
                        JSONObject object = new JSONObject(response.errorBody().string());
                        Log.d(TAG, "jarvishRegisterHelmet errorCode: " + object.getString("errorCode"));
                        handler.onError(object.getString("errorCode"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        handler.onFailure(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                handler.onFailure(t.getMessage());
            }
        });
    }


    public void jarvishRequestResetPassword(String email, JarvishApiHandler.resetPasswordHandler handler) {
        Log.d(TAG, "jarvishRequestResetPassword email: " + email);
        mJarvishApiInterface.requestResetPassword(email).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, "jarvishRequestResetPassword onResponse: " + response.toString());
                if (response.isSuccessful() && response.code() == 200) {
                    handler.onSuccess();
                } else {
                    handler.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handler.onFailure(t.getMessage());
            }
        });
    }



    public void jarvishResetPassword(JarvishResetPassword resetPassword, JarvishApiHandler.resetPasswordHandler handler) {
        String body = new Gson().toJson(resetPassword);
        Log.d(TAG, "jarvishRegister body: " + body);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);
        mJarvishApiInterface.resetPassword(requestBody).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, "jarvishResetPassword onResponse: " + response.toString());
                if (response.isSuccessful() && response.code() == 200) {
                    handler.onSuccess();
                } else {
                    handler.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handler.onFailure(t.getMessage());
            }
        });
    }


    public interface JarvishApiHandler {
        interface loginHandler {
            void onSuccess(JarvishSession session);
            void onError(int error);
            void onFailure(String msg);
        }

        interface registerHandler {
            void onSuccess();
            void onError(int error);
            void onFailure(String msg);
        }

        interface getProfileHandler {
            void onSuccess(JarvishProfile profile);
            void onError(int error);
            void onFailure(String msg);
        }


        interface resetPasswordHandler {
            void onSuccess();
            void onError(int error);
            void onFailure(String msg);
        }

        interface OnRegisterHelmet {
            void onSuccess();
            void onError(String error);
            void onFailure(String msg);
        }
    }
}
