package com.jarvish.android.jarvishpremium.network;

import com.google.gson.JsonObject;
import com.jarvish.android.jarvishpremium.model.JarvishProfile;
import com.jarvish.android.jarvishpremium.model.JarvishSession;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface JarvishApiInterface {

    @Headers("APIKey: JarvishAPIKey")
    @POST("apis/v1/login")
    Call<JarvishSession> loginUser(@Body RequestBody body);


    @Headers("APIKey: JarvishAPIKey")
    @POST("apis/v1/login")
    Call<JarvishSession> loginFacebookUser(@Body RequestBody body);


    @Headers("APIKey: JarvishAPIKey")
    @POST("apis/v1/registration")
    Call<Void> registerUser(@Body RequestBody body);


    @Headers("APIKey: JarvishAPIKey")
    @POST("apis/v1/registration")
    Call<Void> registerFacebookUser(@Body RequestBody body);


    @Headers("APIKey: JarvishAPIKey")
    @GET("apis/v1/profile/{user_id}")
    Call<JarvishProfile> getUserProfile(@Header("session") String session, @Path("user_id") String uid);


    @Headers("APIKey: JarvishAPIKey")
    @PATCH("apis/v1/profile/{user_id}")
    Call<JarvishProfile> updateUserProfile(@Header("session") String session, @Path("user_id") String uid, @Body RequestBody body);


    @Headers("APIKey: JarvishAPIKey")
    @POST("apis/v1/product")
    Call<JsonObject> registerHelmet(@Header("session") String session, @Body RequestBody body);


    @Headers("APIKey: JarvishAPIKey")
    @POST("apis/v1/member/{email}/verificationcode")
    Call<Void> requestResetPassword(@Path("email") String email);


    @Headers("APIKey: JarvishAPIKey")
    @PUT("apis/v1/member/password/reset")
    Call<Void> resetPassword(@Body RequestBody body);
}
