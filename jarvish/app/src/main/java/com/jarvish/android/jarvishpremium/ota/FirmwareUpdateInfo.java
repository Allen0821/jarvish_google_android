package com.jarvish.android.jarvishpremium.ota;

import com.google.gson.annotations.SerializedName;

public class FirmwareUpdateInfo {
    @SerializedName("cmdModelFileName")
    private String cmdModelFileName;

    @SerializedName("cmdModelLink")
    private String cmdModelLink;

    @SerializedName("cmdModelMD5")
    private String cmdModelMD5;

    @SerializedName("cmdModelVersion")
    private String cmdModelVersion;

    @SerializedName("firmwareFileName")
    private String firmwareFileName;

    @SerializedName("firmwareLgVersion")
    private String firmwareLgVersion;

    @SerializedName("firmwareLink")
    private String firmwareLink;

    @SerializedName("firmwareMD5")
    private String firmwareMD5;

    @SerializedName("firmwareVersion")
    private String firmwareVersion;

    @SerializedName("langCode")
    private String langCode;

    @SerializedName("mcuFileName")
    private String mcuFileName;

    @SerializedName("mcuLink")
    private String mcuLink;

    @SerializedName("mcuMD5")
    private String mcuMD5;

    @SerializedName("mcuVersion")
    private String mcuVersion;

    @SerializedName("releaseNote")
    private String releaseNote;


    public String getCmdModelFileName() {
        return cmdModelFileName;
    }

    public void setCmdModelFileName(String cmdModelFileName) {
        this.cmdModelFileName = cmdModelFileName;
    }

    public String getCmdModelLink() {
        return cmdModelLink;
    }

    public void setCmdModelLink(String cmdModelLink) {
        this.cmdModelLink = cmdModelLink;
    }

    public String getCmdModelMD5() {
        return cmdModelMD5;
    }

    public void setCmdModelMD5(String cmdModelMD5) {
        this.cmdModelMD5 = cmdModelMD5;
    }

    public String getCmdModelVersion() {
        return cmdModelVersion;
    }

    public void setCmdModelVersion(String cmdModelVersion) {
        this.cmdModelVersion = cmdModelVersion;
    }

    public String getFirmwareFileName() {
        return firmwareFileName;
    }

    public void setFirmwareFileName(String firmwareFileName) {
        this.firmwareFileName = firmwareFileName;
    }

    public String getFirmwareLgVersion() {
        return firmwareLgVersion;
    }

    public void setFirmwareLgVersion(String firmwareLgVersion) {
        this.firmwareLgVersion = firmwareLgVersion;
    }

    public String getFirmwareLink() {
        return firmwareLink;
    }

    public void setFirmwareLink(String firmwareLink) {
        this.firmwareLink = firmwareLink;
    }

    public String getFirmwareMD5() {
        return firmwareMD5;
    }

    public void setFirmwareMD5(String firmwareMD5) {
        this.firmwareMD5 = firmwareMD5;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getMcuFileName() {
        return mcuFileName;
    }

    public void setMcuFileName(String mcuFileName) {
        this.mcuFileName = mcuFileName;
    }

    public String getMcuLink() {
        return mcuLink;
    }

    public void setMcuLink(String mcuLink) {
        this.mcuLink = mcuLink;
    }

    public String getMcuMD5() {
        return mcuMD5;
    }

    public void setMcuMD5(String mcuMD5) {
        this.mcuMD5 = mcuMD5;
    }

    public String getMcuVersion() {
        return mcuVersion;
    }

    public void setMcuVersion(String mcuVersion) {
        this.mcuVersion = mcuVersion;
    }

    public String getReleaseNote() {
        return releaseNote;
    }

    public void setReleaseNote(String releaseNote) {
        this.releaseNote = releaseNote;
    }
}
