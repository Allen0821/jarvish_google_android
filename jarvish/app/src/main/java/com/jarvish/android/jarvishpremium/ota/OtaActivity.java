package com.jarvish.android.jarvishpremium.ota;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.ActivityOtaBinding;
import com.jarvish.android.jarvishpremium.helmet.ConnectionCheck;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OtaActivity extends AppCompatActivity implements OtaManagerListener {
    private static final String TAG = "OtaActivity";

    private ActivityOtaBinding mBinding;
    private OtaActivityViewModel mViewModel;

    private String mHelmetId;
    private OtaManager mOtaManager;
    private int mAction = 0;
    private boolean mHaveNew = false;

    private String mOtaFmVer;


    private OtaInfo mOtaInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_ota);
        mViewModel = new OtaActivityViewModel();
        mBinding.setViewModel(mViewModel);
        mBinding.setActivity(this);
        mBinding.setLifecycleOwner(this);

        mHelmetId = getIntent().getStringExtra(getString(R.string.key_helmet_model_number));
        String language = getIntent().getStringExtra(getString(R.string.key_helmet_model_language));
        String firmwareVersion = getIntent().getStringExtra(getString(R.string.key_helmet_fw_version));
        if (language == null || language.isEmpty()) {
            language = OtaRoles.setLanguageCode();
        }
        mOtaFmVer = OtaRoles.getUpdateVersion(mHelmetId, language, firmwareVersion);
        Log.d(TAG, "real ver: " + mOtaFmVer);

        mViewModel.setCancelVisible(false);
        mViewModel.setConfirmButton(false, "");

        mOtaManager = OtaManager.getInstance();
        mOtaManager.addOtaManagerListener(this);
        getUpgradeInfo();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mOtaManager != null) {
            mOtaManager.removeOtaManagerListener(this);
            mOtaManager.destroy();
            mOtaManager = null;
        }
    }

    public void onClickCancel() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("OTA", false);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    public void onClickConfirm() {
        if (mAction == 0) {
            if (!mHaveNew) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("OTA", false);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        }

        if (mAction == 1) {
            downloadFirmware();
        }

        if (mAction == 2) {
            uploadFirmware();
        }

        if (mAction == 3) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra("OTA", true);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }
    }


    private void getUpgradeInfo() {
        if (!isFinishing()) {
            mViewModel.setOtaMessage(getString(R.string.ota_get_info));
            //tvOtaHint.setText(R.string.ota_get_info);
            mBinding.pbOta.setVisibility(View.VISIBLE);
            ConnectionCheck.isReachable("8.8.8.8")
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(Boolean aBoolean) {
                            Log.d(TAG, "onNext: " + aBoolean);
                            if (aBoolean) {
                                mOtaManager.checkHelmetFirmwareUpdate(OtaActivity.this, mHelmetId, mOtaFmVer);
                            } else {
                                mBinding.pbOta.setVisibility(View.GONE);
                                mViewModel.setOtaMessage(getString(R.string.ota_get_info_no_internet));
                                //tvOtaHint.setText(R.string.ota_get_info_no_internet);
                                mViewModel.setCancelVisible(false);
                                mViewModel.setConfirmButton(true, getString(R.string.action_confirm));
                                mAction = 0;
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            mBinding.pbOta.setVisibility(View.GONE);
                        }

                        @Override
                        public void onComplete() {
                            mBinding.pbOta.setVisibility(View.GONE);
                        }
                    });
        }
    }

    private void downloadFirmware() {
        mBinding.pbOta.setVisibility(View.VISIBLE);
        mViewModel.setCancelVisible(false);
        mViewModel.setConfirmButton(false, "");
        mViewModel.setOtaMessage(getString(R.string.ota_download));
        //tvOtaHint.setText(R.string.ota_download);
        mOtaManager.downloadFirmware(getApplicationContext(), mOtaInfo);
    }


    private void uploadFirmware() {
        mBinding.pbOta.setVisibility(View.VISIBLE);
        mViewModel.setCancelVisible(false);
        mViewModel.setConfirmButton(false, "");
        mViewModel.setOtaMessage(getString(R.string.ota_update_helmet));
        //tvOtaHint.setText(R.string.ota_update_helmet);
        ConnectionCheck.isReachable("192.168.1.1")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            mOtaManager.updateHelmetFirmware();
                        } else {
                            updateHelmetFailed();
                            openConnectWifiHint();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    private void updateHelmetFailed() {
        mBinding.pbOta.setVisibility(View.GONE);
        mViewModel.setOtaMessage(getString(R.string.ota_update_error));
        //tvOtaHint.setText(R.string.ota_update_error);
        mViewModel.setCancelVisible(true);
        mViewModel.setConfirmButton(true, getString(R.string.action_update));
        mAction = 2;
    }


    private void openConnectWifiHint() {
        new AlertDialog.Builder(OtaActivity.this, R.style.AppAlertDialog)
                .setTitle(R.string.helmet_setting_need_wifi_title)
                .setMessage(R.string.helmet_setting_connect_wifi_content)
                .setPositiveButton(R.string.action_confirm, (DialogInterface dialog, int which) -> {
                    Intent intentWifi = new Intent();
                    intentWifi.setAction(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intentWifi);
                })
                .setNegativeButton(R.string.action_cancel, null)
                .show();
    }


    @Override
    public void onLocalFirmwareExist() {
        Log.d(TAG, "onLocalFirmwareExist");
        mBinding.pbOta.setVisibility(View.GONE);
        mViewModel.setOtaMessage(getString(R.string.ota_download_complete));
        //tvOtaHint.setText(R.string.ota_download_complete);
        mViewModel.setCancelVisible(true);
        mViewModel.setConfirmButton(true, getString(R.string.action_update));
        mAction = 2;
    }


    @Override
    public void onGetRemoteFirmwareInfo() {
        mBinding.pbOta.setVisibility(View.VISIBLE);
        mViewModel.setCancelVisible(false);
        mViewModel.setConfirmButton(false, "");
        mViewModel.setOtaMessage(getString(R.string.ota_get_info));
        //tvOtaHint.setText(R.string.ota_get_info);
    }

    @Override
    public void onRemoteFirmwareInfo(OtaInfo info) {
        Log.d(TAG, "OtaInfo: " + info.getLatest().getReleaseNote());
        mViewModel.setOtaMessage(getString(R.string.ota_get_info_success,
                info.getLatest().getReleaseNote()));
        //tvOtaHint.setText( getString(R.string.ota_get_info_success,
                //info.getLatest().getReleaseNote()));
        mViewModel.setCancelVisible(true);
        mViewModel.setConfirmButton(true, getString(R.string.action_download));
        mOtaInfo = info;
        mBinding.pbOta.setVisibility(View.GONE);
        mAction = 1;
        mHaveNew = true;
    }


    @Override
    public void onGetRemoteFirmwareInfoError() {
        mViewModel.setOtaMessage(getString(R.string.ota_get_info_error));
        //tvOtaHint.setText(R.string.ota_get_info_error);
        mViewModel.setCancelVisible(false);
        mViewModel.setConfirmButton(true, getString(R.string.action_confirm));
        mBinding.pbOta.setVisibility(View.GONE);
        mAction = 0;
        mHaveNew = false;
    }

    @Override
    public void onDownloadFirmwareSuccess() {
        Log.d(TAG, "onDownloadFirmwareSuccess");
        mBinding.pbOta.setVisibility(View.GONE);
        mViewModel.setOtaMessage(getString(R.string.ota_download_complete));
        //tvOtaHint.setText(R.string.ota_download_complete);
        mViewModel.setCancelVisible(true);
        mViewModel.setConfirmButton(true, getString(R.string.action_update));
        mAction = 2;
    }


    @Override
    public void onDownloadFirmwareError() {
        Log.d(TAG, "onDownloadFirmwareError");
        mBinding.pbOta.setVisibility(View.GONE);
        mViewModel.setOtaMessage(getString(R.string.ota_download_error));
        //tvOtaHint.setText(R.string.ota_download_error);
        mViewModel.setCancelVisible(true);
        mViewModel.setConfirmButton(true, getString(R.string.action_confirm));
        mAction = 0;
    }


    @Override
    public void onUpdateFirmwareSuccess() {
        mBinding.pbOta.setVisibility(View.GONE);
        mViewModel.setOtaMessage(getString(R.string.ota_updating_disconnect_message));
        //tvOtaHint.setText(R.string.ota_updating_disconnect_message);
        mViewModel.setConfirmButton(true, getString(R.string.action_confirm));

        mAction = 3;
    }


    @Override
    public void onUpdateFirmwareError() {
        updateHelmetFailed();
    }


    @Override
    public void onFileLoss() {
        Log.d(TAG, "onDownloadFirmwareError");
        mBinding.pbOta.setVisibility(View.GONE);
        mViewModel.setOtaMessage(getString(R.string.ota_download_error));
        //tvOtaHint.setText(R.string.ota_download_error);

        mViewModel.setCancelVisible(true);

        mViewModel.setConfirmButton(true, getString(R.string.action_confirm));

        mAction = 1;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
