/**
 * Created by CupLidSheep on 22,January,2021
 */
package com.jarvish.android.jarvishpremium.ota;

import androidx.annotation.StringRes;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class OtaActivityViewModel extends ViewModel {
    public MutableLiveData<String> otaMessage;
    public MutableLiveData<Boolean> cancelVisible;
    public MutableLiveData<Boolean> confirmVisible;
    public MutableLiveData<String> confirmText;

    public OtaActivityViewModel() {
        otaMessage = new MutableLiveData<>();
        cancelVisible = new MutableLiveData<>();
        confirmVisible = new MutableLiveData<>();
        confirmText = new MutableLiveData<>();
    }

    public void setOtaMessage(String message) {
        this.otaMessage.setValue(message);
    }

    public void setCancelVisible(Boolean visible) {
        this.cancelVisible.setValue(visible);
    }

    public void setConfirmButton(Boolean visible, String text) {
        this.confirmVisible.setValue(visible);
        this.confirmText.setValue(text);
    }
}
