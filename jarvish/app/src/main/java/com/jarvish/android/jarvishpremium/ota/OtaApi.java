package com.jarvish.android.jarvishpremium.ota;

import android.util.Log;

import androidx.annotation.NonNull;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class OtaApi {
    private static final String TAG = "OtaApi";
    private static final String OTA_URL = "https://developer.jarvish.com";
    private final OtaApiInterface mOtaApiInterface;
    private final OtaApiListener mOtaApiListener;

    OtaApi(OtaApiListener listener) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(OTA_URL)
                .client(getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        mOtaApiInterface = retrofit.create(OtaApiInterface.class);
        mOtaApiListener = listener;
    }


    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    void getUpdateInfo(String model, String boardInfo, String fwVersion, String langCode, String langSubVersion,
                              String mcu_version, String cmd_model) {
        mOtaApiInterface.getUpdateInfo(model, boardInfo, fwVersion, langCode, langSubVersion,
                mcu_version, cmd_model).enqueue(new Callback<OtaInfo>() {
            @Override
            public void onResponse(@NonNull Call<OtaInfo> call, @NonNull Response<OtaInfo> response) {
                Log.d(TAG, "getUpdateInfo onResponse: " + response.toString());
                if (response.code() == 200 && response.body() != null) {
                    Log.d(TAG, "getUpdateInfo onResponse: " + response.body());
                    mOtaApiListener.onGetOtaInfo(response.body());
                } else {
                    mOtaApiListener.onGetOtaInfoError();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OtaInfo> call, @NonNull Throwable t) {
                Log.e(TAG, "getUpdateInfo error: " + t.getMessage());
                mOtaApiListener.onGetOtaInfoError();
            }
        });
    }


    void downloadFirmware(String name, String url) {
        Log.d(TAG, "download: " + name);
        Disposable disposable = mOtaApiInterface.downloadFirmware(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> responseBodyResponse) throws Exception {
                        mOtaApiListener.onDownloadSuccess(name, responseBodyResponse.body());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mOtaApiListener.onDownloadError();
                    }
                });
    }


    interface OtaApiListener {
        void onGetOtaInfo(OtaInfo info);
        void onGetOtaInfoError();
        void onDownloadSuccess(String name, ResponseBody responseBody);
        void onDownloadError();
    }
}
