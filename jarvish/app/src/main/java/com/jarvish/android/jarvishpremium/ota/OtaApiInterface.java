package com.jarvish.android.jarvishpremium.ota;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface OtaApiInterface {

    //@Headers("publishtest: a373bb72-0472-4461-8c1f-9695187a7de2")
    @GET("/api/v2/ota/{model}/{boardInfo}/{fwVersion}/{langCode}/{langSubVersion}")
    Call<OtaInfo> getUpdateInfo(@Path("model") String model, @Path("boardInfo") String boardInfo,
                                @Path("fwVersion") String fwVersion, @Path("langCode") String langCode,
                                @Path("langSubVersion") String langSubVersion,
                                @Query("mcu_version") String mcu_version, @Query("cmd_model") String cmd_model);

    //@Headers("publishtest: a373bb72-0472-4461-8c1f-9695187a7de2")
    @GET
    Observable<Response<ResponseBody>> downloadFirmware(@Url String fileUrl);
}
