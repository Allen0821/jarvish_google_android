package com.jarvish.android.jarvishpremium.ota;

import com.google.gson.annotations.SerializedName;

public class OtaInfo {
    @SerializedName("boardInfo")
    private String boardInfo;

    @SerializedName("helmetModel")
    private String helmetModel;

    @SerializedName("helmetModelName")
    private String helmetModelName;

    @SerializedName("isLatest")
    private String isLatest;

    @SerializedName("langCode")
    private String langCode;

    @SerializedName("latest")
    private FirmwareUpdateInfo latest;


    public String getBoardInfo() {
        return boardInfo;
    }

    public void setBoardInfo(String boardInfo) {
        this.boardInfo = boardInfo;
    }

    public String getHelmetModel() {
        return helmetModel;
    }

    public void setHelmetModel(String helmetModel) {
        this.helmetModel = helmetModel;
    }

    public String getHelmetModelName() {
        return helmetModelName;
    }

    public void setHelmetModelName(String helmetModelName) {
        this.helmetModelName = helmetModelName;
    }

    public String getIsLatest() {
        return isLatest;
    }

    public void setIsLatest(String isLatest) {
        this.isLatest = isLatest;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public FirmwareUpdateInfo getLatest() {
        return latest;
    }

    public void setLatest(FirmwareUpdateInfo latest) {
        this.latest = latest;
    }
}
