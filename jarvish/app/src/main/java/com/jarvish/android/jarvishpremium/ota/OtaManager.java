package com.jarvish.android.jarvishpremium.ota;

import android.content.Context;
import android.util.Log;

import com.jarvish.android.jarvishpremium.icatch.ICatch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;

public class OtaManager {
    private static final String TAG = "OTAManager";
    private static volatile OtaManager mOtaManager;
    private OtaApi mOtaApi;
    private ArrayList<OtaManagerListener> mOtaManagerListener;

    private Context mContext;
    private String mHelmetId;
    private String mLocalFwPath = ""; //local file path
    private static final String FW_PATH = "firmware"; //local fw folder

    private static final String CMD = "STM32F411CE_CMD_MD.bin";
    private static final String FW = "SPHOST.BRN";
    private static final String MCU = "STM32F411CE_MCU_FW.bin";
    private static final String CHECKSUM_FILE = "checksum.txt";

    private OtaManager() {
        if (mOtaManager != null){
            throw new RuntimeException("Use getInstance() method to get OTAManager instance.");
        }
        iniOtaManager();
    }


    public static OtaManager getInstance() {
        if (mOtaManager == null) {
            synchronized (OtaManager.class) {
                if (mOtaManager == null) {
                    mOtaManager = new OtaManager();
                }
            }
        }
        return mOtaManager;
    }


    protected OtaManager readResolve() {
        return getInstance();
    }


    private void iniOtaManager() {
        mOtaApi = new OtaApi(new OtaApi.OtaApiListener() {
            @Override
            public void onGetOtaInfo(OtaInfo info) {
                for (OtaManagerListener listener : mOtaManagerListener) {
                    listener.onRemoteFirmwareInfo(info);
                }
            }

            @Override
            public void onGetOtaInfoError() {
                for (OtaManagerListener listener : mOtaManagerListener) {
                    listener.onGetRemoteFirmwareInfoError();
                }
            }


            @Override
            public void onDownloadSuccess(String name, ResponseBody responseBody) {
                if (createFirmware(name, responseBody)) {
                    if (name.equals(CMD)) {
                        mOtaApi.downloadFirmware(MCU, mOtaInfo.getLatest().getMcuLink());
                        return;
                    } else if (name.equals(MCU)) {
                        mOtaApi.downloadFirmware(FW, mOtaInfo.getLatest().getFirmwareLink());
                        return;
                    } else if (name.equals(FW)) {
                        if (createChecksumFile()) {
                            for (OtaManagerListener listener : mOtaManagerListener) {
                                listener.onDownloadFirmwareSuccess();
                            }
                            return;
                        }
                    }
                }
                deleteOldFolder();
                for (OtaManagerListener listener : mOtaManagerListener) {
                    listener.onDownloadFirmwareError();
                }
            }


            @Override
            public void onDownloadError() {
                for (OtaManagerListener listener : mOtaManagerListener) {
                    listener.onDownloadFirmwareError();
                }
                deleteOldFolder();
            }
        });
        mOtaManagerListener = new ArrayList<>();
    }


    public void destroy() {
        mContext = null;
        if (mOtaManagerListener != null) {
            mOtaManagerListener.clear();
            mOtaManagerListener = null;
        }
        mOtaApi = null;
        mOtaManager = null;
    }


    public void addOtaManagerListener(OtaManagerListener listener) {
        if (mOtaManagerListener != null) {
            mOtaManagerListener.add(listener);
        }
    }


    public void removeOtaManagerListener(OtaManagerListener listener) {
        if (mOtaManagerListener != null) {
            mOtaManagerListener.remove(listener);
        }
    }


    public void checkHelmetFirmwareUpdate(Context context, String helmetId, String helmetFwVersion) {
        mContext = context.getApplicationContext();
        mHelmetId = helmetId;
        mLocalFwPath = "";
        String fwVersion;
        String mcuVersion;
        String fwLang;
        String fwLangVer;

        Log.d(TAG, "check update: " + helmetId + " " + helmetFwVersion);

        if (helmetFwVersion == null || helmetFwVersion.isEmpty()) {
            for (OtaManagerListener listener : mOtaManagerListener) {
                listener.onGetRemoteFirmwareInfoError();
            }
            return;
        }

        String[] infos = helmetFwVersion.split("\\.");
        if (infos.length != 4) {
            for (OtaManagerListener listener : mOtaManagerListener) {
                listener.onGetRemoteFirmwareInfoError();
            }
            return;
        }

        fwVersion = infos[0];
        mcuVersion = infos[1];
        fwLang = infos[2];
        fwLangVer = infos[3];

        String local = checkLocalFirmwareExist(helmetId, fwVersion);
        Log.d(TAG, "local firmware path: " + local);
        if (local != null) {
            mLocalFwPath = local;
            for (OtaManagerListener listener : mOtaManagerListener) {
                listener.onLocalFirmwareExist();
            }
        } else {
            for (OtaManagerListener listener : mOtaManagerListener) {
                listener.onGetRemoteFirmwareInfo();
            }

            getUpdateInfo(
                    helmetId,
                    "4",
                    fwVersion,
                    fwLang,
                    fwLangVer,
                    mcuVersion,
                    "1234");
        }
    }


    private OtaInfo mOtaInfo;
    public void downloadFirmware(Context context, OtaInfo info) {
        if (info != null) {
            mOtaInfo = info;
        } else {
            return;
        }
        mContext = context.getApplicationContext();
        mLocalFwPath = FW_PATH + File.separator +
                mHelmetId + File.separator +
                mOtaInfo.getLatest().getFirmwareVersion() + File.separator;
        File folder = new File(mContext.getExternalFilesDir(null), mLocalFwPath);
        if (!folder.exists()) {
            if (!folder.mkdirs()) {
                for (OtaManagerListener listener : mOtaManagerListener) {
                    listener.onDownloadFirmwareError();
                }
                deleteOldFolder();
                return;
            }
        }
        mOtaApi.downloadFirmware(CMD, mOtaInfo.getLatest().getCmdModelLink());
    }


    public void updateHelmetFirmware() {
        if (mLocalFwPath.isEmpty()) {
            for (OtaManagerListener listener : mOtaManagerListener) {
                listener.onUpdateFirmwareError();
            }
        } else {
            Log.d(TAG, "upload file: " + mLocalFwPath + File.separator + CMD);
            uploadFile(CMD, CMD, CMD);
        }
    }


    private void getUpdateInfo(String model, String boardInfo, String fwVersion, String langCode, String langSubVersion,
                               String mcu_version, String cmd_model) {
        Log.d(TAG, "model: " + model);
        Log.d(TAG, "boardInfo: " + boardInfo);
        Log.d(TAG, "fwVersion: " + fwVersion);
        Log.d(TAG, "langCode: " + langCode);
        Log.d(TAG, "langSubVersion: " + langSubVersion);
        Log.d(TAG, "mcu_version: " + mcu_version);
        Log.d(TAG, "cmd_model: " + cmd_model);
        mOtaApi.getUpdateInfo(model,
                boardInfo,
                fwVersion,
                langCode,
                langSubVersion,
                mcu_version,
                cmd_model);
    }


    private String checkLocalFirmwareExist(String helmetId, String curVersion) {
        File fwFolder = new File(mContext.getExternalFilesDir(null), FW_PATH);
        if (!fwFolder.exists()) {
            if (fwFolder.mkdir()) {
                Log.e(TAG, "mkdir failed: " + FW_PATH);
            }
            return null;
        }

        String path = FW_PATH + File.separator + helmetId;
                File folder = new File(mContext.getExternalFilesDir(null), path);
        if (!folder.exists()) {
            if (folder.mkdir()) {
                Log.e(TAG, "mkdir failed: " + path);
            }
            return null;
        }

        File[] files = folder.listFiles();
        for (File file : files) {
            String name = file.getName();
            if (Integer.parseInt(name) > Integer.parseInt(curVersion)) {
                Log.d(TAG, "find local files");
                return path + File.separator + name;
            } else {
                Log.d(TAG, "delete old local files");
                deleteOldFile(file);
            }
        }

        return null;
    }


    private String checkFileExists(String name) {
        File file = new File(mContext.getExternalFilesDir(null), mLocalFwPath + File.separator + name);
        if (file.exists()) {
            Log.d(TAG, "check path: " + file.getAbsolutePath());
            return file.getAbsolutePath();
        }
        Log.e(TAG, "check path failed: " + file.getAbsolutePath());
        return null;
    }


    private boolean createFirmware(String name, ResponseBody responseBody) {
        Log.d(TAG, "create firmware: " + name);
        String fileName;
        String md5;
        if (name.equals(CMD)) {
            fileName = CMD;
            md5 = mOtaInfo.getLatest().getCmdModelMD5();
        } else if (name.equals(MCU)) {
            fileName = MCU;
            md5 = mOtaInfo.getLatest().getMcuMD5();
        } else if (name.equals(FW)) {
            fileName = FW;
            md5 = mOtaInfo.getLatest().getFirmwareMD5();
        } else {
            return false;
        }

        File file = createFile(fileName, responseBody);
        if (file == null) {
            return false;
        }

        return checkFileMD5(file, md5);
    }


    private File createFile(String name, ResponseBody responseBody) {
        String path = mLocalFwPath + name;
        Log.d(TAG, "create file: " + path);
        File file = new File(mContext.getExternalFilesDir(null), path);
        try {
            BufferedSink sink = Okio.buffer(Okio.sink(file));
            sink.writeAll(responseBody.source());
            sink.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private boolean checkFileMD5(File file, String md5) {
        if (!file.isFile()) {
            return false;
        }
        MessageDigest digest;
        FileInputStream in;
        byte[] buffer = new byte[1024];
        int len;
        try {
            digest = MessageDigest.getInstance("MD5");
            in = new FileInputStream(file);
            while ((len = in.read(buffer, 0, 1024)) != -1) {
                digest.update(buffer, 0, len);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        String fileMd5 = bytesToHexString(digest.digest());

        return !(fileMd5 == null || !fileMd5.equals(md5));
    }


    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }
        for (byte b : src) {
            int v = b & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }


    private boolean createChecksumFile() {
        Log.d(TAG, "create checksum");
        Log.d(TAG, "checksum FW " + mOtaInfo.getLatest().getFirmwareMD5());
        Log.d(TAG, "checksum CMD " + mOtaInfo.getLatest().getCmdModelMD5());
        Log.d(TAG, "checksum MCU " + mOtaInfo.getLatest().getMcuMD5());


        File checksumFile = new File(mContext.getExternalFilesDir(null), mLocalFwPath + CHECKSUM_FILE);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(checksumFile, false));
            writer.write("[" + FW + "] = [" + mOtaInfo.getLatest().getFirmwareMD5() + "]");
            writer.newLine();
            writer.write("[" + CMD + "] = [" + mOtaInfo.getLatest().getCmdModelMD5() + "]");
            writer.newLine();
            writer.write("[" + MCU + "] = [" + mOtaInfo.getLatest().getMcuMD5() + "]");
            writer.newLine();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    private void uploadFile(final String index, final String fileName, final String remotePath) {
        String path = checkFileExists(fileName);
        if (path == null) {
            deleteOldFolder();
            for (OtaManagerListener listener : mOtaManagerListener) {
                listener.onFileLoss();
            }
            return;
        }

        Observable.create((ObservableEmitter<Boolean> emitter) -> {
            Log.d(TAG, "upload file path: " + path);
            Log.d(TAG, "upload file: " + remotePath);
            emitter.onNext(ICatch.getInstance().uploadFile(path, remotePath));
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
                Log.d(TAG, "Disposable");
            }

            @Override
            public void onNext(Boolean o) {
                Log.d(TAG, "uploadFile onNext: " + o);
                if (o) {
                    switch (index) {
                        case CMD:
                            uploadFile(MCU, MCU, MCU);
                            return;
                        case MCU:
                            uploadFile(CHECKSUM_FILE, CHECKSUM_FILE, CHECKSUM_FILE);
                            return;
                        /*case FW:
                            uploadFile(CHECKSUM_FILE, mLocalPath + "/" + CHECKSUM_FILE, CHECKSUM_FILE);
                            return;*/
                        case CHECKSUM_FILE:
                            uploadFirmware();
                            return;
                    }
                }
                for (OtaManagerListener listener : mOtaManagerListener) {
                    listener.onUpdateFirmwareError();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "uploadFile onError: " + e.getMessage());
                for (OtaManagerListener listener : mOtaManagerListener) {
                    listener.onUpdateFirmwareError();
                }
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "uploadFile onComplete");
            }
        });
    }


    private void uploadFirmware() {
        String path = checkFileExists(FW);
        if (path == null) {
            deleteOldFolder();
            for (OtaManagerListener listener : mOtaManagerListener) {
                listener.onFileLoss();
            }
            return;
        }

        Observable.create((ObservableEmitter<Boolean> emitter) -> {
            Log.d(TAG, "upload firmware path: " + path);
            emitter.onNext(ICatch.getInstance().updateFirmware(path));
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Boolean o) {
                Log.d(TAG, "uploadFirmware onNext: " + o);
                if (o) {
                    deleteOldFolder();
                    for (OtaManagerListener listener : mOtaManagerListener) {
                        listener.onUpdateFirmwareSuccess();
                    }
                } else {
                    for (OtaManagerListener listener : mOtaManagerListener) {
                        listener.onUpdateFirmwareError();
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "uploadFile onError: " + e.getMessage());
                for (OtaManagerListener listener : mOtaManagerListener) {
                    listener.onUpdateFirmwareError();
                }
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "uploadFile onComplete");
            }
        });
    }


    private void deleteOldFolder() {
        Log.d(TAG, "delete file: " + mLocalFwPath);
        File file = new File(mContext.getExternalFilesDir(null), mLocalFwPath);
        if (!file.exists()) {
            Log.e(TAG, "file " + mLocalFwPath + " doesn't exists!");
        } else {
            deleteOldFile(file);
        }
    }


    private void deleteOldFile(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File child : files) {
                child.delete();
            }
        }
        file.delete();
    }
}
