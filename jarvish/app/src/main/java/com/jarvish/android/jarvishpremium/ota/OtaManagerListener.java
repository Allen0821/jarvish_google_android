package com.jarvish.android.jarvishpremium.ota;

public interface OtaManagerListener {

    void onLocalFirmwareExist();
    void onGetRemoteFirmwareInfo();
    void onRemoteFirmwareInfo(OtaInfo info);
    void onGetRemoteFirmwareInfoError();
    void onDownloadFirmwareSuccess();
    void onDownloadFirmwareError();
    void onUpdateFirmwareSuccess();
    void onUpdateFirmwareError();
    void onFileLoss();
}
