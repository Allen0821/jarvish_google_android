/**
 * Created by CupLidSheep on 14,September,2020
 */
package com.jarvish.android.jarvishpremium.ota;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Locale;

public class OtaRoles {

    public static String getUpdateVersion(@NonNull String id, @NonNull String language, String fmVer) {
        Log.d("OTA", id + " " + language + " " + fmVer);
        if (id.equals("08") && language.equals("86")) {
            if (fmVer == null || fmVer.isEmpty() || !fmVer.contains("2540")) {
                return "2522.178.86.01";
            }
            if (fmVer.contains("2540")) {
                return "";
            }
        }

        if ((id.equals("01") || id.equals("02") || id.equals("03") ||
                id.equals("04") || id.equals("05") || id.equals("06")) &&
                language.equals("86")) {
            if (fmVer == null || fmVer.isEmpty() ||
                    (!fmVer.contains("712") && !fmVer.contains("530"))) {
                return "702.178.86.01";
            } else {
                return "";
            }
        }

        return fmVer;
    }

    /*
     * use device default Language to set OTA update Language code
     * if app can't get Language code from helmet.
     */
    public static String setLanguageCode() {
        Locale locale = Locale.getDefault();
        if (locale.getCountry().equals("TW")) {
            return "86";
        } else {
            return "01";
        }
    }

    public static boolean checkBatteryLevel(int level) {
        Log.d("OTA", String.valueOf(level));
        return level >= 10;
    }

}
