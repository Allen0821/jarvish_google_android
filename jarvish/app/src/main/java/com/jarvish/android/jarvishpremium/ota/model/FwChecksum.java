/**
 * Created by CupLidSheep on 04,February,2021
 */
package com.jarvish.android.jarvishpremium.ota.model;

import android.util.Base64;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;

public class FwChecksum {


    public static String getHexString(File file) {
        if (file.isFile()) {
            MessageDigest digest;
            FileInputStream in;
            byte[] buffer = new byte[1024];
            int len;
            try {
                digest = MessageDigest.getInstance("MD5");
                in = new FileInputStream(file);
                while ((len = in.read(buffer, 0, 1024)) != -1) {
                    digest.update(buffer, 0, len);
                }
                in.close();
                return convertHexString(digest.digest());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static String getHexString(String hash) {
        byte[] bytes = Base64.decode(hash, Base64.NO_WRAP);
        return convertHexString(bytes);
    }


    private static String convertHexString(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder();
        if (bytes == null || bytes.length <= 0) {
            return null;
        }
        for (byte b : bytes) {
            int v = b & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /*public static String getBase64String(MessageDigest digest) {
        byte[] bytes = digest.digest();
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }*/

}
