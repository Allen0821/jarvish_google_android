/*
 * Created by CupLidSheep on 08,February,2021
 */
package com.jarvish.android.jarvishpremium.ota.model;


import android.content.Context;
import android.util.Log;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FwDownloadTask {
    private static final String TAG = "[DW]";
    private final Context context;
    private String serverPath;
    private final FwDownloadTaskCallback callback;
    private int step;

    private String cmdMd5;
    private String mcuMd5;
    private String fwMd5;

    FirebaseStorage storage = FirebaseStorage.getInstance();

    FwDownloadTask(Context context, FwDownloadTaskCallback callback) {
        this.context = context;
        this.callback = callback;
    }


    public void startDownload(String serverPath) {
        step = 0;
        this.serverPath = serverPath;
        download();
    }

    private void download() {
        String fileName;
        switch (step) {
            case 0:
                fileName = OtaClient.CMD;
                break;
            case 1:
                fileName = OtaClient.MCU;
                break;
            case 2:
                fileName = OtaClient.FW;
                break;
            default:
                return;
        }

        Log.d(TAG, "download: " + fileName);

        final String storagePath = serverPath + File.separator + fileName;
        final File file = new File(createFolder(), fileName);
        StorageReference storageRef = storage.getReference();
        StorageReference pathReference = storageRef.child(storagePath);
        //Log.d(TAG, "store path: " + file.getAbsolutePath());
        pathReference.getFile(file)
                .addOnSuccessListener(taskSnapshot -> verifyChecksum(storagePath, file))
                .addOnFailureListener(exception -> {
                    exception.printStackTrace();
                    callback.onDownloadTaskFailure();
        });
    }

    private void verifyChecksum(final String storagePath, final File file) {
        Log.d(TAG, "verify checksum: " + file.getName());
        StorageReference storageRef = storage.getReference();
        StorageReference pathReference = storageRef.child(storagePath);
        pathReference.getMetadata().addOnSuccessListener(storageMetadata -> {
            String filehash = FwChecksum.getHexString(file.getAbsoluteFile());
            String matadata = FwChecksum.getHexString(storageMetadata.getMd5Hash());
            //Log.d(TAG, "filehash: " + filehash);
            //Log.d(TAG, "matadata: " + matadata);
            if (filehash != null && filehash.equals(matadata)) {
                switch (step) {
                    case 0:
                        step = 1;
                        cmdMd5 = filehash;
                        download();
                        return;
                    case 1:
                        step = 2;
                        mcuMd5 = filehash;
                        download();
                        return;
                    case 2:
                        fwMd5 = filehash;
                        createChecksumFile();
                        return;
                    default:
                        callback.onDownloadTaskFailure();
                }
            } else {
                callback.onDownloadTaskFailure();
            }
        }).addOnFailureListener(exception -> callback.onDownloadTaskFailure());
    }

    private File createFolder() {
        File f = new File(this.context.getExternalFilesDir(null), "ota");
        if(!f.exists()) {
            f.mkdirs();
        }
        return f;
    }

    private void createChecksumFile() {
        Log.d(TAG, "cmd hash: " + cmdMd5);
        Log.d(TAG, "mcu hash: " + mcuMd5);
        Log.d(TAG, "fw hash: " + fwMd5);
        File checksumFile = new File(createFolder(), OtaClient.CHECKSUM_FILE);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(checksumFile, false));
            writer.write("[" + OtaClient.FW + "] = [" + fwMd5 + "]");
            writer.newLine();
            writer.write("[" + OtaClient.CMD + "] = [" + cmdMd5 + "]");
            writer.newLine();
            writer.write("[" + OtaClient.MCU + "] = [" + mcuMd5 + "]");
            writer.newLine();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            callback.onDownloadTaskFailure();
        }
        callback.onDownloadTaskCompleted();
    }

    public interface FwDownloadTaskCallback {
        void onDownloadTaskCompleted();
        void onDownloadTaskFailure();
    }

}
