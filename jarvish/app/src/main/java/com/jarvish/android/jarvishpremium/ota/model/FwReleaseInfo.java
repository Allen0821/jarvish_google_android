/**
 * Created by CupLidSheep on 29,January,2021
 */
package com.jarvish.android.jarvishpremium.ota.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class FwReleaseInfo {
    private String helmetStyle;
    private String languageCode;
    private int versionCode;
    private String versionName;
    private String note;
    private String path;
    private @ServerTimestamp Date create;

    public String getHelmetStyle() {
        return helmetStyle;
    }

    public void setHelmetStyle(String helmetStyle) {
        this.helmetStyle = helmetStyle;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getCreate() {
        return create;
    }

    public void setCreate(Date create) {
        this.create = create;
    }
}
