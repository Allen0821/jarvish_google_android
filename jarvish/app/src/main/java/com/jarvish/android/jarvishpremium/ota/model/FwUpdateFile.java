/**
 * Created by CupLidSheep on 18,March,2021
 */
package com.jarvish.android.jarvishpremium.ota.model;

public class FwUpdateFile {
    private String cmdPath;
    private String cmdMD5;
    private String mcuPath;
    private String mcuMD5;
    private String fwPath;
    private String fwMD5;
    private String checksumPath;

    public String getCmdPath() {
        return cmdPath;
    }

    public void setCmdPath(String cmdPath) {
        this.cmdPath = cmdPath;
    }

    public String getCmdMD5() {
        return cmdMD5;
    }

    public void setCmdMD5(String cmdMD5) {
        this.cmdMD5 = cmdMD5;
    }

    public String getMcuPath() {
        return mcuPath;
    }

    public void setMcuPath(String mcuPath) {
        this.mcuPath = mcuPath;
    }

    public String getMcuMD5() {
        return mcuMD5;
    }

    public void setMcuMD5(String mcuMD5) {
        this.mcuMD5 = mcuMD5;
    }

    public String getFwPath() {
        return fwPath;
    }

    public void setFwPath(String fwPath) {
        this.fwPath = fwPath;
    }

    public String getFwMD5() {
        return fwMD5;
    }

    public void setFwMD5(String fwMD5) {
        this.fwMD5 = fwMD5;
    }

    public String getChecksumPath() {
        return checksumPath;
    }

    public void setChecksumPath(String checksumPath) {
        this.checksumPath = checksumPath;
    }
}
