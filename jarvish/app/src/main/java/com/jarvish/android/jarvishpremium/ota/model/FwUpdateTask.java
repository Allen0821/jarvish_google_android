/**
 * Created by CupLidSheep on 04,February,2021
 */
package com.jarvish.android.jarvishpremium.ota.model;

import android.content.Context;
import android.util.Log;

import com.jarvish.android.jarvishpremium.icatch.ICatch;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FwUpdateTask {
    private static final String TAG = "[UP]";
    private final Context context;
    private final FwUpdateTaskCallback fwUpdateTaskCallback;
    private int step;

    public FwUpdateTask(Context context, FwUpdateTaskCallback callback) {
        this.context = context;
        this.fwUpdateTaskCallback = callback;
    }

    public void startUpdate() {
        step = 0;
        upload();
    }

    private void upload() {
        String fileName;
        switch (step) {
            case 0:
                fileName = OtaClient.CMD;
                uploadFile(fileName);
                break;
            case 1:
                fileName = OtaClient.MCU;
                uploadFile(fileName);
                break;
            case 2:
                fileName = OtaClient.CHECKSUM_FILE;
                uploadFile(fileName);
                break;
            case 3:
                uploadFirmware();
                break;
            default:
                fwUpdateTaskCallback.onUpdateFailure();
        }
    }

    private void uploadFile(String fileName) {
        String filePath = getFilePath(fileName);
        Log.d(TAG, "upload file path: " + filePath);
        if (filePath.isEmpty()) {
            fwUpdateTaskCallback.onUpdateFailure();
            return;
        }

        Observable.create((ObservableOnSubscribe<Boolean>) emitter -> {
            emitter.onNext(ICatch.getInstance().uploadFile(filePath, fileName));
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull Boolean result) {
                if (result) {
                    step = step + 1;
                    upload();
                } else {
                    fwUpdateTaskCallback.onUpdateFailure();
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                e.printStackTrace();
                fwUpdateTaskCallback.onUpdateFailure();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void uploadFirmware() {
        String filePath = getFilePath(OtaClient.FW);
        Log.d(TAG, "upload firmware path: " + filePath);
        if (filePath.isEmpty()) {
            fwUpdateTaskCallback.onUpdateFailure();
            return;
        }

        Observable.create((ObservableOnSubscribe<Boolean>) emitter -> {
            emitter.onNext(ICatch.getInstance().updateFirmware(filePath));
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Boolean>() {

            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull Boolean result) {
                if (result) {
                    fwUpdateTaskCallback.onUpdateCompleted();
                } else {
                    fwUpdateTaskCallback.onUpdateFailure();
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                e.printStackTrace();
                fwUpdateTaskCallback.onUpdateFailure();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private String getFilePath(String fileName) {
        File f = new File(this.context.getExternalFilesDir(null), "ota" + File.separator + fileName);
        if (f.exists()) {
            Log.d(TAG, "file exists: " + f.getAbsolutePath());
            return f.getAbsolutePath();
        }
        Log.e(TAG, "file does not exists: " + f.getAbsolutePath());
        return "";
    }

    public interface UploadFileListener {
        void onSuccess();
        void onFailure();
    }

    public interface UploadFirmwareListener {
        void onSuccess();
        void onFailure();
    }

    public interface FwUpdateTaskCallback {
        void onUpdateCompleted();
        void onUpdateFailure();
    }
}
