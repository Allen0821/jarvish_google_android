/**
 * Created by CupLidSheep on 29,January,2021
 */
package com.jarvish.android.jarvishpremium.ota.model;

import androidx.annotation.NonNull;

public class FwVersionCheck {

    /**
     * verify that the version name string.
     *
     * @param version Helmet version string.
     */
    public static Boolean isValidVersionName(String version) {
        if (version == null || version.isEmpty()) {
            return false;
        }

        String[] infos = version.split("\\.");
        if (infos.length != 4) {
            return false;
        }

        try {
            Integer.parseInt(infos[0]);
        } catch(NumberFormatException e) {
            return false;
        }

        try {
            Integer.parseInt(infos[1]);
        } catch(NumberFormatException e) {
            return false;
        }

        try {
            Integer.parseInt(infos[3]);
        } catch(NumberFormatException e) {
            return false;
        }

        return true;
    }


    /**
     * check that release version of Jarvish X from server is newer than current helmet version.
     *
     * @param helmetVersionName Jarvish X version name string.
     * @param serverVersionCode Jarvish X release version code from server.
     */
    public static boolean isNewReleaseForJarvishX(String helmetVersionName, int serverVersionCode) {

        if (!isValidVersionName(helmetVersionName)) {
            return true;
        }

        String[] hVer = helmetVersionName.split("\\.");

        return serverVersionCode > Integer.parseInt(hVer[0]);
    }


    /**
     * check release version of F1, F2, EVO S1, EVO S2, R1 or X1 from server
     * is newer than current helmet version.
     *
     * @param helmetVersionName Helmet version name string.
     * @param serverVersionCode Helmet release version code from server.
     */
    public static boolean isNewReleaseForFEVoRX(String helmetVersionName, int serverVersionCode) {

        if (!isValidVersionName(helmetVersionName)) {
            return true;
        }

        String[] hVer = helmetVersionName.split("\\.");
        int hVersionCode = Integer.parseInt(hVer[0]);

        if (hVersionCode <= 503) {
            return false;
        }

        return serverVersionCode > hVersionCode;
    }
}
