/*
 * Created by CupLidSheep on 29,January,2021
 */
package com.jarvish.android.jarvishpremium.ota.model;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.ota.OtaRoles;
import com.jarvish.android.jarvishpremium.ota.model.FwDownloadTask;
import com.jarvish.android.jarvishpremium.ota.model.FwReleaseInfo;
import com.jarvish.android.jarvishpremium.ota.model.FwUpdateFile;
import com.jarvish.android.jarvishpremium.ota.model.FwUpdateTask;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class OtaClient implements FwDownloadTask.FwDownloadTaskCallback, FwUpdateTask.FwUpdateTaskCallback {
    private static final String TAG = "OtaClient";

    private final OtaClientListener mOtaClientListener;

    private final FirebaseStorage storage = FirebaseStorage.getInstance();

    private final Context mContext;

    static final String CMD = "STM32F411CE_CMD_MD.bin";
    static final String FW = "SPHOST.BRN";
    static final String MCU = "STM32F411CE_MCU_FW.bin";
    static final String CHECKSUM_FILE = "checksum.txt";

    //private final FwUpdateFile fwUpdateFile;

    private final String boardID;
    private final String countryCode;

    private String serverPath = "";

    public OtaClient(Context context, @NonNull String helmetStyle, @NonNull String languageCode, OtaClientListener listener) {
        this.mContext = context;
        this.mOtaClientListener = listener;
        this.boardID = helmetStyle;
        this.countryCode = getCountryCode(languageCode);
    }

    public void stop() {
        deleteFile();
    }

    /**
     * Get firmware release information of helmet from firestore.
     */
    public void getReleaseInformation(final String version) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        String path = "ota/" + this.boardID + "/" + this.countryCode;
        Log.d(TAG, "path: " + path);
        db.collection(path)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        FwReleaseInfo fwReleaseInfo =
                                task.getResult().getDocuments().get(0).toObject(FwReleaseInfo.class);
                        if (isLatest(version, fwReleaseInfo.getVersionCode())) {
                            serverPath = fwReleaseInfo.getPath();
                            mOtaClientListener.onHaveReleaseVersion(fwReleaseInfo);
                        } else {
                            mOtaClientListener.onNoReleaseVersion();
                        }
                    } else {
                        Log.e(TAG, "Error getting release documents: ", task.getException());
                        mOtaClientListener.onNoReleaseVersion();
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error getting release documents: " + e.getMessage());
                    mOtaClientListener.onNoReleaseVersion();
                });
    }

    public void downloadFiles() {
        FwDownloadTask fwDownloadTask = new FwDownloadTask(mContext, this);
        fwDownloadTask.startDownload(serverPath);
    }


    public void uploadFilesToHelmet() {
        FwUpdateTask fwUpdateTask = new FwUpdateTask(mContext, this);
        fwUpdateTask.startUpdate();
    }

    private void deleteFile() {
        File folder = new File(mContext.getExternalFilesDir(null), "ota");
        if (folder.exists() && folder.isDirectory()) {
            File[] files = folder.listFiles();
            for (File child : files) {
                child.delete();
            }
        }
        folder.delete();
    }

    public String getCountryCode(String languageCode) {
        Log.d(TAG, "languageCode: " + languageCode);
        String code;
        if (languageCode == null || languageCode.isEmpty()) {
            code = OtaRoles.setLanguageCode();
        } else {
            code = languageCode;
        }
        if (code.equals("86")) {
            return "tw";
        } else {
            return "ww";
        }
    }

    public boolean isLatest(String version, int latest) {
        Log.d(TAG, "current: " + version + " latest: " + latest);
        if (version == null || version.isEmpty()) {
            return true;
        }
        String[] vName = version.split("\\.");
        if (vName.length != 4) {
            return true;
        }
        int verCode = Integer.parseInt(vName[0]);
        Log.d(TAG, "current verCode: " + verCode);
        return latest > verCode;
    }

    @Override
    public void onDownloadTaskCompleted() {
        mOtaClientListener.onDownloadCompleted();
    }

    @Override
    public void onDownloadTaskFailure() {
        mOtaClientListener.onDownloadFailure();
    }

    @Override
    public void onUpdateCompleted() {
        mOtaClientListener.onUploadCompleted();
    }

    @Override
    public void onUpdateFailure() {
        mOtaClientListener.onUploadFailure();
    }

    public interface OtaClientListener {
        void onHaveReleaseVersion(FwReleaseInfo fwReleaseInfo);
        void onNoReleaseVersion();
        void onDownloadCompleted();
        void onDownloadFailure();
        void onUploadCompleted();
        void onUploadFailure();
    }
}
