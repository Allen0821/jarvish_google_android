package com.jarvish.android.jarvishpremium.ota.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.ActivityOtaUpdateBinding;
import com.jarvish.android.jarvishpremium.helmet.ConnectionCheck;
import com.jarvish.android.jarvishpremium.ota.model.OtaClient;
import com.jarvish.android.jarvishpremium.ota.model.FwReleaseInfo;
import com.jarvish.android.jarvishpremium.ota.viewModel.OtaUpdateViewModel;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OtaUpdateActivity extends AppCompatActivity {
    private static final String TAG = "OtaUpdateActivity";

    private ActivityOtaUpdateBinding mBinding;
    private OtaUpdateViewModel mViewModel;

    private OtaClient otaClient;
    private FwReleaseInfo mFwReleaseInfo;

    private String firmwareVersion;
    private int step = 0;

    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_ota_update);
        mViewModel = new OtaUpdateViewModel();
        mBinding.setViewModel(mViewModel);
        mBinding.setActivity(this);
        mBinding.setLifecycleOwner(this);

        String style = getIntent().getStringExtra(getString(R.string.key_helmet_model_number));
        String language = getIntent().getStringExtra(getString(R.string.key_helmet_model_language));
        firmwareVersion = getIntent().getStringExtra(getString(R.string.key_helmet_fw_version));

        otaClient = new OtaClient(OtaUpdateActivity.this, style, language, otaClientListener);
        getReleaseInformation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (otaClient != null) {
            otaClient.stop();
            otaClient = null;
        }
        endProgressAnimation();
    }

    private void getReleaseInformation() {
        if (!isFinishing()) {
            mViewModel.setOtaMessage(getString(R.string.ota_get_info));
            startProgressAnimation();
            mViewModel.isCancelBtnVisible.setValue(false);
            mViewModel.isNextBtnVisible.setValue(false);
            ConnectionCheck.isReachable("8.8.8.8")
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(@NonNull Boolean aBoolean) {
                            Log.d(TAG, "onNext: " + aBoolean);
                            if (aBoolean) {
                                otaClient.getReleaseInformation(firmwareVersion);
                            } else {
                                mViewModel.setOtaMessage(getString(R.string.ota_get_info_no_internet));
                                endProgressAnimation();
                                mViewModel.isCancelBtnVisible.setValue(true);
                                mViewModel.isNextBtnVisible.setValue(true);
                                step = 0;
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            mViewModel.setOtaMessage(getString(R.string.ota_get_info_no_internet));
                            endProgressAnimation();
                            mViewModel.isCancelBtnVisible.setValue(true);
                            mViewModel.isNextBtnVisible.setValue(true);
                            step = 0;
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
    }

    public void onNext() {
        if (step == 0) {
            getReleaseInformation();
        } else if (step == 1) {
            startProgressAnimation();
            mViewModel.isCancelBtnVisible.setValue(false);
            mViewModel.isNextBtnVisible.setValue(false);
            mViewModel.setOtaMessage(getString(R.string.ota_download));
            mViewModel.isReleaseNoteVisible.setValue(false);
            otaClient.downloadFiles();
        } else if (step == 2) {
            startProgressAnimation();
            mViewModel.isCancelBtnVisible.setValue(false);
            mViewModel.isNextBtnVisible.setValue(false);
            mViewModel.setOtaMessage(getString(R.string.ota_update_helmet));
            uploadFirmware();
        }
    }

    public void onLeaveOta() {
        finish();
    }

    private void startProgressAnimation() {
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(this, R.anim.progress_setting);
        } else {
            animation.reset();
        }
        mBinding.pbOta.startAnimation(animation);
        mViewModel.isProgressVisible.setValue(true);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void endProgressAnimation() {
        if (animation != null) {
            mBinding.pbOta.clearAnimation();
        }
        mViewModel.isProgressVisible.setValue(false);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void onReadReleaseNote() {
        String release = mFwReleaseInfo.getNote().replace("\\n", "\n");
        new MaterialAlertDialogBuilder(this, R.style.AppAlertDialog)
                .setMessage(release)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }


    private void uploadFirmware() {
        ConnectionCheck.isReachable("192.168.1.1")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull Boolean aBoolean) {
                        if (aBoolean) {
                            Log.d(TAG, "has connected to helmet");
                            otaClient.uploadFilesToHelmet();
                        } else {
                            Log.e(TAG, "no connected to helmet");
                            updateHelmetFailed();
                            openConnectWifiHint();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        updateHelmetFailed();
                        openConnectWifiHint();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void openConnectWifiHint() {
        new AlertDialog.Builder(OtaUpdateActivity.this, R.style.AppAlertDialog)
                .setTitle(R.string.helmet_setting_need_wifi_title)
                .setMessage(R.string.helmet_setting_connect_wifi_content)
                .setPositiveButton(R.string.action_confirm, (DialogInterface dialog, int which) -> {
                    Intent intentWifi = new Intent();
                    intentWifi.setAction(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intentWifi);
                })
                .setNegativeButton(R.string.action_cancel, null)
                .show();
    }

    private void updateHelmetFailed() {
        endProgressAnimation();
        mViewModel.setOtaMessage(getString(R.string.ota_update_error));
        mViewModel.isCancelBtnVisible.setValue(true);
        mViewModel.isNextBtnVisible.setValue(true);
        step = 2;
    }

    private final OtaClient.OtaClientListener otaClientListener = new OtaClient.OtaClientListener() {
        @Override
        public void onHaveReleaseVersion(FwReleaseInfo fwReleaseInfo) {
            Log.d(TAG, "fwReleaseInfo: " + new Gson().toJson(fwReleaseInfo));
            mFwReleaseInfo = fwReleaseInfo;
            mViewModel.setOtaMessage(getString(R.string.ota_have_new_release,
                    fwReleaseInfo.getVersionName()));
            endProgressAnimation();
            mViewModel.isNextBtnVisible.setValue(true);
            mViewModel.isCancelBtnVisible.setValue(true);
            mViewModel.isReleaseNoteVisible.setValue(true);
            step = 1;
        }

        @Override
        public void onNoReleaseVersion() {
            mViewModel.setOtaMessage(getString(R.string.ota_get_info_error));
            endProgressAnimation();
            mViewModel.isNextBtnVisible.setValue(true);
            mViewModel.isCancelBtnVisible.setValue(true);
        }

        @Override
        public void onDownloadCompleted() {
            endProgressAnimation();
            mViewModel.isCancelBtnVisible.setValue(true);
            mViewModel.isNextBtnVisible.setValue(true);
            mViewModel.setOtaMessage(getString(R.string.ota_download_complete));
            step = 2;
        }

        @Override
        public void onDownloadFailure() {
            endProgressAnimation();
            mViewModel.isNextBtnVisible.setValue(true);
            mViewModel.isCancelBtnVisible.setValue(true);
            step = 1;
            mViewModel.setOtaMessage(getString(R.string.ota_download_error));
        }

        @Override
        public void onUploadCompleted() {
            endProgressAnimation();
            Intent resultIntent = new Intent();
            resultIntent.putExtra("OTA", true);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }

        @Override
        public void onUploadFailure() {
            updateHelmetFailed();
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}