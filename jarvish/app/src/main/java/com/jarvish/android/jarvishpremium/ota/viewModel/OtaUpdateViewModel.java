/**
 * Created by CupLidSheep on 05,February,2021
 */
package com.jarvish.android.jarvishpremium.ota.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class OtaUpdateViewModel extends ViewModel {
    public MutableLiveData<String> otaMessage;
    public MutableLiveData<Boolean> isReleaseNoteVisible;
    public MutableLiveData<Boolean> isCancelBtnVisible;
    public MutableLiveData<Boolean> isNextBtnVisible;
    public MutableLiveData<Boolean> isProgressVisible;

    public MutableLiveData<Integer> otaStep;


    public OtaUpdateViewModel() {
        otaMessage = new MutableLiveData<>();
        isReleaseNoteVisible = new MutableLiveData<>();
        isReleaseNoteVisible.setValue(false);
        isCancelBtnVisible = new MutableLiveData<>();
        isCancelBtnVisible.setValue(false);
        isNextBtnVisible = new MutableLiveData<>();
        isNextBtnVisible.setValue(false);
        isProgressVisible = new MutableLiveData<>();
        isProgressVisible.setValue(true);
        otaStep = new MutableLiveData<>();
        otaStep.setValue(0);
    }

    public void setOtaMessage(String message) {
        this.otaMessage.setValue(message);
    }
}
