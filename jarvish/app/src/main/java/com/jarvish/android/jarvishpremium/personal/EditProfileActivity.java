package com.jarvish.android.jarvishpremium.personal;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.SharedPreferencesUtil;
import com.jarvish.android.jarvishpremium.databinding.ActivityEditProfileBinding;
import com.jarvish.android.jarvishpremium.model.JarvishEditData;
import com.jarvish.android.jarvishpremium.model.JarvishProfile;
import com.jarvish.android.jarvishpremium.network.JarvishApi;

import java.util.ArrayList;
import java.util.Locale;


public class EditProfileActivity extends AppCompatActivity {
    private static final String TAG = "EditProfileActivity";

    private ActivityEditProfileBinding mBinding;


    private String mSession;
    private String mUid;
    private JarvishProfile mProfile;
    private AlertDialog mAlertDialog;
    private boolean mIsHaveProfile = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        mSession = SharedPreferencesUtil.getLoginSession(this);
        mUid = SharedPreferencesUtil.getLoginUid(this);

        mBinding.ivCancel.setOnClickListener(v -> onClickCancel());
        mBinding.ivConfirm.setOnClickListener(v -> onClickConfirm());
        mBinding.tvMeasure.setOnClickListener(v -> onClickMeasure());

        getProfile();
    }


    private void getProfile() {
        JarvishApi.getInstance().jarvishGetProfile(mSession, mUid, new JarvishApi.JarvishApiHandler.getProfileHandler() {
            @Override
            public void onSuccess(JarvishProfile profile) {
                String data = new Gson().toJson(profile);
                Log.d(TAG, "get profile onSuccess: " + data);
                mProfile = profile;
                showProfile();
            }

            @Override
            public void onError(int error) {
                Log.e(TAG, "get profile error: " + error);
            }

            @Override
            public void onFailure(String msg) {
                Log.e(TAG, "get profile failure: " + msg);
            }
        });

    }


    private void showProfile() {
        if (mProfile != null) {
            mBinding.etName.setText(mProfile.getUserName());
            mBinding.etPhone.setText(mProfile.getPhone1());
            mBinding.etCity.setText(mProfile.getCity());
            mBinding.etLength.setText(mProfile.getHeadLength());
            mBinding.etVsize.setText(mProfile.getHeadWidth());
            mIsHaveProfile = true;
        }
    }

    protected void onClickCancel() {
        finish();
    }

    protected void onClickConfirm() {
        if (!mIsHaveProfile) {
            return;
        }
        startProgressBar(getString(R.string.action_update_profile));
        String body = new Gson().toJson(loadEditData());
        JarvishApi.getInstance().jarvishUpdateProfile(mSession, mUid, body, new JarvishApi.JarvishApiHandler.getProfileHandler() {
            @Override
            public void onSuccess(JarvishProfile profile) {
                Log.d(TAG, "update profile onSuccess: " + profile);
                SharedPreferencesUtil.storeUserProfileName(getApplicationContext(), profile.getUserName());
                stopProgressBar();
                Intent intent = new Intent();
                intent.setAction("RELOAD.FUNC");
                sendBroadcast(intent);
                finish();
            }

            @Override
            public void onError(int error) {
                Log.e(TAG, "update profile error: " + error);
                stopProgressBar();
            }

            @Override
            public void onFailure(String msg) {
                Log.e(TAG, "update profile failure: " + msg);
                stopProgressBar();
            }
        });
    }

    protected void onClickMeasure() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(getMeasureUrl()));
        startActivity(browserIntent);
    }

    private String getMeasureUrl() {
        Locale locale = Locale.getDefault();
        if (locale.getLanguage().equals("zh")) {
            if (locale.getCountry().equals("TW")) {
                return "https://drive.google.com/uc?export=view&id=17NyhN27sOaIWX4uGWr7MsxKN0A11fyY9";
            }
        }
        return "https://drive.google.com/uc?export=view&id=1U1oq6wt_1pRlVSXAwHHYQiiL5U5W7iot";
    }


    private ArrayList<JarvishEditData> loadEditData() {
        ArrayList<JarvishEditData> update = new ArrayList<>();
        if (mBinding.etName.getText() != null && !mBinding.etName.getText().toString().isEmpty()) {
            update.add(new JarvishEditData("user_name", mBinding.etName.getText().toString()));
        }
        if (mBinding.etPhone.getText() != null && !mBinding.etPhone.getText().toString().isEmpty()) {
            update.add(new JarvishEditData("phone1", mBinding.etPhone.getText().toString()));
        }
        if (mBinding.etCity.getText() != null && !mBinding.etCity.getText().toString().isEmpty()) {
            update.add(new JarvishEditData("city", mBinding.etCity.getText().toString()));
        }
        if (mBinding.etLength.getText() != null && !mBinding.etLength.getText().toString().isEmpty()) {
            update.add(new JarvishEditData("head_hsize", mBinding.etLength.getText().toString()));
        }
        if (mBinding.etVsize.getText() != null && !mBinding.etVsize.getText().toString().isEmpty()) {
            update.add(new JarvishEditData("head_vsize", mBinding.etVsize.getText().toString()));
        }
        /*if (etVsize.getText() != null && !etVsize.getText().toString().isEmpty()) {
           // update.add(new JarvishEdi.]y;\tData("head_shot", "xxxxx"));
        }*/
        return update;
    }


    private void startProgressBar(String title) {
        Log.d(TAG, "start progress bar");
        mAlertDialog = null;
        mAlertDialog = new AlertDialog.Builder(this, R.style.AppAlertDialog)
                .setTitle(title)
                .setView(R.layout.process_alert_dialog)
                .setCancelable(false)
                .create();
        mAlertDialog.show();
    }


    private void stopProgressBar() {
        //progressBar.setProgress(0);
        Log.d(TAG, "stop progress bar");
        mAlertDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
