package com.jarvish.android.jarvishpremium.personal;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.jarvish.android.jarvishpremium.ManualSetting;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.SharedPreferencesUtil;
import com.jarvish.android.jarvishpremium.adapter.PersonalFunctionAdapter;
import com.jarvish.android.jarvishpremium.databinding.FragmentPersonalBinding;
import com.jarvish.android.jarvishpremium.firestore.Profile;
import com.jarvish.android.jarvishpremium.firestore.StoreProfile;
import com.jarvish.android.jarvishpremium.login.ui.AuthenticationActivity;
import com.jarvish.android.jarvishpremium.model.PzzaFuc;
import com.jarvish.android.jarvishpremium.user.UserAccountActivity;
import com.jarvish.android.jarvishpremium.user.UserProfileActivity;
import com.jarvish.android.jarvishpremium.user.helmetregistration.HelmetRegistrationActivity;
import com.jarvish.android.jarvishpremium.weather.core.Weather;

import java.util.ArrayList;
import java.util.Date;


import kotlin.Unit;
import kotlin.jvm.functions.Function1;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class PersonalFragment extends Fragment {
    private static final String TAG = "PersonalFragment";

    private FragmentPersonalBinding mBinding;


    private ArrayList<PzzaFuc> mFunctionList;
    private PersonalFunctionAdapter mFunctionListAdapter;

    private Profile currentProfile;

    public PersonalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_personal, container, false);
        View view = mBinding.getRoot();

        initView();


        //setItem();
        IntentFilter filter = new IntentFilter();
        filter.addAction("RELOAD.FUNC");
        requireActivity().registerReceiver(mReceiver, filter);
        mBinding.btSignIn.setOnClickListener(v -> onClickSignIn());


        return view;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "receiver #####");
            if (intent.getAction() != null && intent.getAction().equals("RELOAD.FUNC")) {
                Log.d(TAG, "RELOAD.FUNC #####");
                setItem();
            }
        }
    };


    private void setUserProfileView() {
        String name = SharedPreferencesUtil.getUserProfileName(requireContext());
        String avatar = SharedPreferencesUtil.getUserProfileAvatar(requireContext());
        mBinding.tvUserName.setText(name);
        Glide.with(this)
                    .load(avatar)
                    .placeholder(R.drawable.ic_user)
                    .error(R.drawable.ic_user)
                    .into(mBinding.ivAvatar);
    }

    private void queryUserProfile() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        mBinding.tvUserName.setText(user.getDisplayName());
        Glide.with(requireActivity())
                .load(user.getPhotoUrl())
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .into(mBinding.ivAvatar);
        StoreProfile storeProfile = new StoreProfile();
        storeProfile.getDocument(user.getUid(), new Function1<Profile, Unit>() {
            @Override
            public Unit invoke(Profile profile) {
                currentProfile = profile;
                mBinding.tvUserName.setText(currentProfile.getName());
                Glide.with(requireActivity())
                        .load(currentProfile.getAvatarUrl())
                        .placeholder(R.drawable.ic_user)
                        .error(R.drawable.ic_user)
                        .into(mBinding.ivAvatar);
                return null;
            }
        });
    }

    /*private void displayUser() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        mBinding.tvUserName.setText(user.getDisplayName());
        Glide.with(requireActivity())
                .load(user.getPhotoUrl())
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .into(mBinding.ivAvatar);
    }*/


    private String getAppVersion() {
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager()
                    .getPackageInfo(getApplicationContext().getPackageName(), 0);
           return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }


    private void initView() {
        mFunctionList = new ArrayList<>();
        mBinding.rcPersonal.setHasFixedSize(true);
        mBinding.rcPersonal.setLayoutManager(new LinearLayoutManager(getContext()));
        mFunctionListAdapter = new PersonalFunctionAdapter(mFunctionList, mOnPersonalFunctionListener);
        mBinding.rcPersonal.setAdapter(mFunctionListAdapter);
    }


    private boolean isLogin()  {
        if (SharedPreferencesUtil.getLoginSession(requireContext()).equals("NO") ||
                SharedPreferencesUtil.getLoginUid(requireContext()).equals("NO")) {
            return false;
        }
        Long login = SharedPreferencesUtil.getLoginDate(requireContext());
        if ((new Date().getTime() - login) > 2*24*60*60*1000) {
            SharedPreferencesUtil.removeLoginData(requireContext());
            return false;
        }
        return true;
    }

    private boolean isSignedIn() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            Log.d("Auth", "user: " + user.getUid() + " " + user.getDisplayName());
            return true;
        } else {
            Log.e("Auth", "user: " + "no user");
            return false;
        }
    }


    private void setItem() {
        if (mFunctionList == null) {
            mFunctionList = new ArrayList<>();
        } else {
            mFunctionList.clear();
        }

        /*mFunctionList.add(new PzzaFuc(
                getString(R.string.personal_version, getAppVersion()),
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_version), 0));*/

        if (!isSignedIn()/*!isLogin()*/) {
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_app_units),
                    ContextCompat.getDrawable(requireContext(), R.drawable.switch_distance_units), 1));
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_user_manual),
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_user_manual), 1));
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_policy),
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_privacy_policy), 1));
            mBinding.btSignIn.setVisibility(View.VISIBLE);
            mBinding.ivAvatar.setVisibility(View.GONE);
            mBinding.tvUserName.setVisibility(View.GONE);
        } else {
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_edit_profile),
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_edit_profile), 1));
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_account_info),
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_version), 1));
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_register_helmet),
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_register_helmet), 1));
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_app_units),
                    ContextCompat.getDrawable(requireContext(), R.drawable.switch_distance_units), 1));
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_user_manual),
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_user_manual), 1));
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_policy),
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_privacy_policy), 1));
            mFunctionList.add(new PzzaFuc(getString(R.string.personal_sign_out),
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_log_out), 1));
            mBinding.btSignIn.setVisibility(View.GONE);
            mBinding.ivAvatar.setVisibility(View.VISIBLE);
            mBinding.tvUserName.setVisibility(View.VISIBLE);
            //setUserProfileView();
            queryUserProfile();
        }

        mFunctionListAdapter.notifyDataSetChanged();
        mBinding.tvVersion.setText(getString(R.string.personal_version, getAppVersion()));
    }


    private int mUnits;
    private void setUnits() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext(), R.style.AppAlertDialog);
        builder.setTitle(R.string.personal_app_units)
                .setSingleChoiceItems(R.array.app_units,
                        SharedPreferencesUtil.getAppUnits(getApplicationContext()),
                        (DialogInterface dialog, int which) -> {
                            Log.d(TAG, "which: " + which);
                            mUnits = which;
                        })
                .setPositiveButton(R.string.action_confirm, (DialogInterface dialog, int which) -> {
                    SharedPreferencesUtil.setAppUnits(getApplicationContext(), mUnits);
                    Weather.getInstance().setUnits(getApplicationContext(), mUnits);
                    Intent intent = new Intent();
                    intent.setAction("UNITS.CHANGED");
                    getApplicationContext().sendBroadcast(intent);
                })
                .setNegativeButton(R.string.action_cancel,(DialogInterface dialog, int which) -> {
                })
                .show();
    }


    private final PersonalFunctionAdapter.OnPersonalFunctionListener mOnPersonalFunctionListener = (String title) -> {
        if (title.equals(getString(R.string.personal_edit_profile))) {
            //editProfile();
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            Intent profile = new Intent();
            profile.setClass(requireContext(), UserProfileActivity.class);
            profile.putExtra(getString(R.string.user_id), user.getUid());
            profile.putExtra("EXTRA_PROFILE", currentProfile);
            startActivity(profile);
        } else if (title.equals(getString(R.string.personal_account_info))) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            Intent account = new Intent();
            account.setClass(requireContext(), UserAccountActivity.class);
            account.putExtra(getString(R.string.user_id), user.getUid());
            startActivity(account);
        } else if (title.equals(getString(R.string.personal_register_helmet))) {
            registerHelmet();
        } else if (title.equals(getString(R.string.personal_user_manual))) {
            Intent intent = new Intent();
            intent.setClass(requireActivity(), UserManualActivity.class);
            startActivity(intent);
        } else if (title.equals(getString(R.string.personal_app_units))) {
            setUnits();
        } else if (title.equals(getString(R.string.personal_policy))) {
            Intent intent = new Intent();
            intent.setClass(requireActivity(), WebViewActivity.class);
            intent.putExtra(getString(R.string.key_web_title), getString(R.string.personal_policy));
            intent.putExtra(getString(R.string.key_web_url), ManualSetting.getPrivacyPolicyLink());
            startActivity(intent);
        } else if (title.equals(getString(R.string.personal_sign_out))) {
            signOut();
        }
    };


    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        /*if (context instanceof OnPersonalFragmentListener) {
            mListener = (OnPersonalFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPersonalFragmentListener");
        }*/
    }


    @Override
    public void onResume() {
        super.onResume();
        setItem();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        requireActivity().unregisterReceiver(mReceiver);
    }


    private void editProfile() {
        /*if (isLogin()) {
            Intent intent = new Intent();
            intent.setClass(requireActivity(), EditProfileActivity.class);
            startActivity(intent);
        } else {
            new AlertDialog.Builder(requireContext(), R.style.AppAlertDialog)
                    .setTitle(R.string.hint_sign_in_expired)
                    .setMessage(R.string.hint_sign_in_again)
                    .setNegativeButton(R.string.action_confirm, (dialogInterface, i) -> {
                        SharedPreferencesUtil.removeLoginData(requireContext());
                        setItem();
                    })
                    .show();
        }*/
    }

    void onClickSignIn() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            Intent signInIntent = new Intent(getContext(), AuthenticationActivity.class);
            signInLauncher.launch(signInIntent);
        }
    }

    private void onSignOut() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            for (UserInfo profile : user.getProviderData()) {
                String providerId = profile.getProviderId();
                Log.d("Auth", "User is signed in providerId: " + providerId);
                signOutProvider(providerId);
            }
            FirebaseAuth.getInstance().signOut();
        }

        /*Intent i = new Intent();
        i.setComponent(new ComponentName("com.cuplidsheep.obd2reader", "com.cuplidsheep.obd2reader.obd2.Obd2BtService"));
        requireContext().startForegroundService(i);*/



    }

    private void signOutProvider(String providerId) {
        if (providerId.equals("google.com")) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            GoogleSignIn.getClient(requireActivity(), gso).signOut();
        } else if (providerId.equals("facebook.com")) {
            LoginManager.getInstance().logOut();
        }
    }


    private void registerHelmet() {
        /*Intent intent = new Intent();
        intent.setClass(requireActivity(), RegisterHelmetActivity.class);
        startActivity(intent);*/
        Intent intent = new Intent();
        intent.setClass(requireActivity(), HelmetRegistrationActivity.class);
        startActivity(intent);

    }


    private void signOut() {
        Log.d(TAG, "Sign out");
        new AlertDialog.Builder(requireContext(), R.style.AppAlertDialog)
                .setTitle(R.string.hint_sign_out)
                .setMessage("")
                .setPositiveButton(R.string.action_confirm,
                        (DialogInterface dialog, int which) -> {
                            //SharedPreferencesUtil.removeLoginData(getApplicationContext());
                            onSignOut();
                            setItem();
                        })
                .setNegativeButton(R.string.action_cancel, null)
                .show();
    }

    private final ActivityResultLauncher<Intent> signInLauncher =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            result -> {
                Log.d("Auth", "sign in Result Code = " + result.getResultCode());
                if (result.getResultCode() == Activity.RESULT_OK) {
                    setItem();
                }
            });
}
