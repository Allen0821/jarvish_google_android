package com.jarvish.android.jarvishpremium.personal;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.ScanerActivity;
import com.jarvish.android.jarvishpremium.SharedPreferencesUtil;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.databinding.ActivityRegisterHelmetBinding;
import com.jarvish.android.jarvishpremium.model.JarvishRegisterHelmet;
import com.jarvish.android.jarvishpremium.network.JarvishApi;


public class RegisterHelmetActivity extends AppCompatActivity {
    private static final String TAG = "RegisterHelmetActivity";

    private ActivityRegisterHelmetBinding mBinding;

    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_register_helmet);
        setTopAppBar();
        mBinding.btRegisterHelmet.setOnClickListener(v -> onClickRegisterHelmet());
        mBinding.ivScan.setOnClickListener(v -> onClickScan());
    }

    private void setTopAppBar() {
        mBinding.tbRegisterHelmet.setTitle(R.string.helmet_register_title);
        mBinding.tbRegisterHelmet.setNavigationOnClickListener((View v) -> finish());
    }


    private void startLoadingAnimation() {
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(RegisterHelmetActivity.this, R.anim.moto_loading_1);
        } else {
            animation.reset();
        }
        mBinding.ivLoading.startAnimation(animation);
        mBinding.ivLoading.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void endLoadingAnimation() {
        if (animation != null) {
            mBinding.ivLoading.clearAnimation();
        }
        mBinding.ivLoading.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    protected void onClickScan() {
        IntentIntegrator integrator = new IntentIntegrator(RegisterHelmetActivity.this);
        integrator.setCaptureActivity(ScanerActivity.class);
        integrator.setOrientationLocked(false);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    protected void onClickRegisterHelmet() {
        Util.hideSoftKeyboard(RegisterHelmetActivity.this);
        String number = mBinding.etSN.getText().toString();
        if (!Util.verifySerialNumber(number)) {
            setSerialNumberAlert(getString(R.string.helmet_enter_serial_number_invalid));
            return;
        }
        registerHelmet(number);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.e(TAG, "no result");
            } else {
                Log.d(TAG, "result: " + result.getContents());
                mBinding.etSN.setText(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setSerialNumberAlert(String title) {
        new AlertDialog.Builder(RegisterHelmetActivity.this, R.style.AppAlertDialog)
                .setTitle(title)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }


    private void registerHelmet(String number) {
        startLoadingAnimation();
        JarvishRegisterHelmet registerHelmet = new JarvishRegisterHelmet();
        int id = Integer.parseInt(SharedPreferencesUtil.getLoginUid(RegisterHelmetActivity.this));
        registerHelmet.setUid(id);
        registerHelmet.setSerialNumber(number.replace(" ", ""));
        JarvishApi.getInstance().jarvishRegisterHelmet(SharedPreferencesUtil.getLoginSession(RegisterHelmetActivity.this), registerHelmet,
                new JarvishApi.JarvishApiHandler.OnRegisterHelmet() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "register helmet onSuccess");
                        registerResultAlert(getString(R.string.helmet_register_success_title), true);
                    }

                    @Override
                    public void onError(String error) {
                        Log.d(TAG, "register helmet onError " + error);
                        if (error.equals("04-302")) {
                            registerResultAlert(getString(R.string.helmet_register_fail_been_registered), false);
                        } else if (error.equals("04-301")) {
                            registerResultAlert(getString(R.string.helmet_register_fail_invalid), false);
                        } else {
                            registerResultAlert(getString(R.string.helmet_register_fail_title), false);
                        }
                    }

                    @Override
                    public void onFailure(String msg) {
                        Log.d(TAG, "register helmet onFailure " + msg);
                        registerResultAlert(getString(R.string.helmet_register_fail_title), false);
                    }
                });
    }


    private void registerResultAlert(String title, final Boolean result) {
        endLoadingAnimation();
        new MaterialAlertDialogBuilder(RegisterHelmetActivity.this, R.style.AppAlertDialog)
                .setTitle(title)
                .setPositiveButton(R.string.action_confirm, (dialogInterface, i) -> {
                   if (result) {
                       finish();
                   }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}