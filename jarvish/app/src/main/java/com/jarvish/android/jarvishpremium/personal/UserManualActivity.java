package com.jarvish.android.jarvishpremium.personal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.jarvish.android.jarvishpremium.ManualSetting;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.adapter.UserManualAdapter;
import com.jarvish.android.jarvishpremium.databinding.ActivityUserManualBinding;
import com.jarvish.android.jarvishpremium.model.UserManual;

import java.util.ArrayList;


public class UserManualActivity extends AppCompatActivity implements UserManualAdapter.OnUserManualListener {
    //private static final String TAG = "UserManualActivity";
    private ActivityUserManualBinding mBinding;

    private ArrayList<UserManual> mUserManualList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_manual);

        setToolbar();
        setFunctionList();
    }


    private void setFunctionList() {
        //setItem();
        if (mUserManualList != null) {
            mUserManualList.clear();
        } else {
            mUserManualList = new ArrayList<>();
        }
        mUserManualList.addAll(ManualSetting.getUserGuideLink());
        mBinding.rcManual.setHasFixedSize(true);
        mBinding.rcManual.setLayoutManager(new LinearLayoutManager(UserManualActivity.this));
        UserManualAdapter mUserManualAdapter = new UserManualAdapter(mUserManualList, this);
        mBinding.rcManual.setAdapter(mUserManualAdapter);
    }


    private void setToolbar() {
        mBinding.tbManual.setTitle(getString(R.string.user_manual_title));
        mBinding.tbManual.setTitleTextColor(getResources().getColor(R.color.colorAccent, null));
        mBinding.tbManual.setNavigationOnClickListener((View v) -> finish());
    }


    @Override
    public void onSelectedManual(UserManual manual) {
        /*Intent intent = new Intent();
        intent.setClass(UserManualActivity.this, WebViewActivity.class);
        intent.putExtra(getString(R.string.key_web_title), manual.getName());
        intent.putExtra(getString(R.string.key_web_url), manual.getLink());
        startActivity(intent);*/
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(manual.getLink()));
        startActivity(browserIntent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
