package com.jarvish.android.jarvishpremium.personal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebViewClient;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.ActivityWebViewBinding;


public class WebViewActivity extends AppCompatActivity {
    //private static final String TAG = "WebViewActivity";

    private ActivityWebViewBinding mBinding;


    private String mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);

        mTitle = getIntent().getStringExtra(getString(R.string.key_web_title));
        String url = getIntent().getStringExtra(getString(R.string.key_web_url));

        setTopAppBar();
        mBinding.webView.setWebViewClient(new WebViewClient());
        if (url != null) {
            mBinding.webView.loadUrl(url);
        }
    }


    private void setTopAppBar() {
        mBinding.tbWeb.setTitle(mTitle);
        mBinding.tbWeb.setTitleTextColor(getResources().getColor(R.color.color_primary_01, null));
        mBinding.tbWeb.setNavigationOnClickListener((View v) -> finish());
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
