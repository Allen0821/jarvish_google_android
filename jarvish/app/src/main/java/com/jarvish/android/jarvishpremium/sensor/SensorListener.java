package com.jarvish.android.jarvishpremium.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import androidx.annotation.NonNull;

public class SensorListener implements  SensorEventListener {
    private static final String TAG = "SensorListener";
    private final SensorManager mSensorManager;

    //private Sensor mAccelerometerSensor;
    //private Sensor mMagneticSensor;
    //private Sensor mGyroscopeSensor;

    private final Sensor mRotationVectorSensor;



    @NonNull
    private final SensorValueListener mSensorValueListener;



    public SensorListener(Context context, @NonNull SensorValueListener listener) {
        this.mSensorValueListener = listener;
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        //mAccelerometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //mMagneticSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        //mGyroscopeSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mRotationVectorSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
    }


    public void start() {
        Log.d(TAG, "start sensor listener");
        //mSensorManager.registerListener(this, mAccelerometerSensor, SensorManager.SENSOR_DELAY_FASTEST);
        //mSensorManager.registerListener(this, mMagneticSensor, SensorManager.SENSOR_DELAY_FASTEST);
        //mSensorManager.registerListener(this, mGyroscopeSensor, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mRotationVectorSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void stop() {
        Log.d(TAG, "stop sensor listener");
        mSensorManager.unregisterListener(this);
    }

    private static final float HEADING_OFFSET = 0;
    private void calculateHeading(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            float[] rotationVectorValue;
            float[] rotationMatrix = new float[16];
            float[] orientation = new float[3];
            rotationVectorValue = event.values.clone();
            SensorManager.getRotationMatrixFromVector(rotationMatrix, rotationVectorValue);
            SensorManager.getOrientation(rotationMatrix, orientation);
            final float heading = (float) (orientation[0] * 180 / Math.PI - HEADING_OFFSET);
            Log.d(TAG, "heading: " + heading);
            mSensorValueListener.onHeadingChanged(heading);

        }
    }


    /*private float[] mGravity = new float[3];
    private float[] mGeomagnetic = new float[3];
    private float[] R = new float[9];
    private float[] I = new float[9];


    private float azimuth;
    private float azimuthFix;

    private void calculateOrientation(SensorEvent event) {
        Log.d(TAG, "calculate orientation");
        final float alpha = 0.97f;

        synchronized (this) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                mGravity[0] = alpha * mGravity[0] + (1 - alpha) * event.values[0];
                mGravity[1] = alpha * mGravity[1] + (1 - alpha) * event.values[1];
                mGravity[2] = alpha * mGravity[2] + (1 - alpha) * event.values[2];
            }

            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                // mGeomagnetic = event.values;
                mGeomagnetic[0] = alpha * mGeomagnetic[0] + (1 - alpha) * event.values[0];
                mGeomagnetic[1] = alpha * mGeomagnetic[1] + (1 - alpha) * event.values[1];
                mGeomagnetic[2] = alpha * mGeomagnetic[2] + (1 - alpha) * event.values[2];

                // Log.e(TAG, Float.toString(event.values[0]));
                float magneticField = (float) Math.sqrt(mGeomagnetic[0] * mGeomagnetic[0]
                        + mGeomagnetic[1] * mGeomagnetic[1]
                        + mGeomagnetic[2] * mGeomagnetic[2]);

                //mOnValueChangedListener.onMagneticFieldChanged(magneticField);
            }


            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            Log.d(TAG, "get RotationMatrix: " + success);
            if (success) {
                float[] orientation = new float[3];
                SensorManager.getOrientation(R, orientation);
                float azimuth = (float) Math.toDegrees(orientation[0]);
                azimuth = (azimuth + azimuthFix + 360) % 360 - 180;
                if (azimuth < 0) {
                    azimuth = azimuth + 360;
                }
                float pitch = (float) Math.toDegrees(orientation[1]);
                float roll = (float) Math.toDegrees(orientation[2]);
                Log.d(TAG, "azimuth: " + azimuth + ", pitch: " + pitch + ", roll: " + roll);
                mSensorValueListener.onRotationChanged(azimuth, pitch, roll);
            }
        }
    }*/


    @Override
    public void onSensorChanged(SensorEvent event) {
        //calculateOrientation(event);
        calculateHeading(event);
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
