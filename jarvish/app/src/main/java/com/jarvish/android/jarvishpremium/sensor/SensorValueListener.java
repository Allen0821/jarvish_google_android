package com.jarvish.android.jarvishpremium.sensor;

public interface SensorValueListener {
    //void onRotationChanged(float azimuth, float pitch, float roll);

    void onHeadingChanged(float heading);
}
