package com.jarvish.android.jarvishpremium.speech;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import com.jarvish.android.jarvishpremium.R;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static android.content.Context.AUDIO_SERVICE;

public class SimpleSpeechRecognizer implements RecognitionListener {
    private static final String TAG = "SimpleSpeechRecognizer";
    private final Context mContext;
    private final SpeechRecognizer mSpeechRecognizer;
    private final Intent mRecognizerIntent;
    private OnSimpleSpeechRecognizerListener mOnSimpleSpeechRecognizerListener;
    private boolean isListening = false;


    private MediaPlayer mAudioCueStartSpeaking;
    private MediaPlayer mAudioCueStopSpeaking;
    private TextToSpeech mTtl;

    private boolean startNavigation = false;

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();


    public SimpleSpeechRecognizer(Context context, OnSimpleSpeechRecognizerListener listener) {
        mContext = context;
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(mContext);
        mSpeechRecognizer.setRecognitionListener(this);
        mRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, Locale.getDefault());
        mRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, context.getPackageName());
        mRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mRecognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);

        mTtl = new TextToSpeech(mContext, mOnInitListener);

        mOnSimpleSpeechRecognizerListener = listener;

        mAudioCueStartSpeaking = MediaPlayer.create(mContext, R.raw.med_ui_wakesound_touch);
        mAudioCueStopSpeaking = MediaPlayer.create(mContext, R.raw.med_ui_endpointing_touch);
    }



    public void destroySimpleSpeechRecognizer() {
        if ( mAudioCueStartSpeaking != null ) {
            mAudioCueStartSpeaking.release();
            mAudioCueStartSpeaking = null;
        }
        if ( mAudioCueStopSpeaking != null ) {
            mAudioCueStopSpeaking.release();
            mAudioCueStopSpeaking = null;
        }
        if ( mCompositeDisposable != null ) {
            mCompositeDisposable.clear();
            mCompositeDisposable = null;
        }
        mOnSimpleSpeechRecognizerListener= null;
        if(mTtl != null) {
            mTtl.stop();
            mTtl.shutdown();
            mTtl = null;
            Log.d(TAG, "TTS Destroyed");
        }
        mSpeechRecognizer.destroy();
    }



    public void onStartSpeaking() {
        if (!isListening) {
            Log.d(TAG, "onStartSpeaking");
            isListening = true;
            mAudioCueStartSpeaking.start();
            setBluetoothMic(true);
            Disposable disposable = Observable.timer(1500, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            aLong -> mSpeechRecognizer.startListening(mRecognizerIntent),
                            Throwable::printStackTrace);
            mCompositeDisposable.add(disposable);
        }
    }


    private void onStopSpeaking() {
        if (isListening) {
            Log.d(TAG, "onStopSpeaking");
            mSpeechRecognizer.stopListening();
            mAudioCueStartSpeaking.stop();
            setBluetoothMic(false);
            isListening = false;
            startNavigation = false;
            Disposable disposable = Observable.timer(1500, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        //setBluetoothMic(false);
                    }, Throwable::printStackTrace);
            mCompositeDisposable.add(disposable);
        }
    }



    private boolean isConnectedBluetooth() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter != null && adapter.isEnabled()) {
            int[] profiles = {BluetoothProfile.A2DP, BluetoothProfile.HEADSET, BluetoothProfile.HEALTH};
            boolean connectionExists = false;
            for (int profileId : profiles) {
                if (adapter.getProfileConnectionState(profileId) ==
                        BluetoothProfile.STATE_CONNECTED) {
                    connectionExists = true;
                    Log.d(TAG, "bluetooth profile id:" + profileId);
                    break;
                }
            }
            Log.d(TAG, "bluetooth profile connected:" + connectionExists);
            return connectionExists;
        }
        return false;
    }


    private void setBluetoothMic(boolean enable) {
        if (isConnectedBluetooth()) {
            AudioManager audioManager = (AudioManager) mContext.getSystemService(AUDIO_SERVICE);
            if (enable) {
                Log.d(TAG, "enable bluetooth Mic");
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
                audioManager.startBluetoothSco();
                audioManager.setBluetoothScoOn(true);
            } else {
                Log.d(TAG, "disable bluetooth Mic");
                audioManager.setMode(AudioManager.MODE_NORMAL);
                audioManager.stopBluetoothSco();
                audioManager.setBluetoothScoOn(false);
            }
        }
    }


    public void onTextToSpeech(String text) {
        if (mTtl != null) {
            Log.d(TAG, "speaking text: " + text);
            mTtl.speak("要前往" + text + "嗎", TextToSpeech.QUEUE_FLUSH, null, "UniqueID");
            startNavigation = true;
            //onStartSpeaking();
            //mOnSimpleSpeechRecognizerListener.onStartNavigation();
        }
    }


    @Override
    public void onBeginningOfSpeech() {

    }


    @Override
    public void onBufferReceived(byte[] buffer) {

    }


    @Override
    public void onEndOfSpeech() {

    }


    @Override
    public void onError(int error) {
        Log.e(TAG, "onError: " + error);
        onStopSpeaking();
    }


    @Override
    public void onEvent(int eventType, Bundle params) {

    }


    @Override
    public void onPartialResults(Bundle partialResults) {

    }


    @Override
    public void onReadyForSpeech(Bundle params) {

    }


    @Override
    public void onResults(Bundle results) {
        Log.d(TAG, "onResults");
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        for (String result : matches){
            Log.d(TAG, "match: " + result);
        }
        if (startNavigation) {
            isStartingNavigation(matches.get(0));
        } else {
            mOnSimpleSpeechRecognizerListener.onSearchPlace(matches.get(0));
        }
        onStopSpeaking();
    }


    @Override
    public void onRmsChanged(float rmsdB) {

    }


    private final TextToSpeech.OnInitListener mOnInitListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if(status == TextToSpeech.SUCCESS) {
                Log.d(TAG, "init TextToSpeech success");
                mTtl.setLanguage(Locale.getDefault());
                mTtl.setOnUtteranceProgressListener(mUtteranceProgressListener);
            } else {
                Log.e(TAG, "init TextToSpeech error: " + status);
            }
        }
    };


    private final UtteranceProgressListener mUtteranceProgressListener = new UtteranceProgressListener() {
        @Override
        public void onStart(String utteranceId) {
            Log.d(TAG, "UtteranceProgressListener start");
        }

        @Override
        public void onDone(String utteranceId) {
            Log.d(TAG, "UtteranceProgressListener done");
            Observable.timer(3, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Long>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Long aLong) {
                            onStartSpeaking();
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }

        @Override
        public void onError(String utteranceId) {
            Log.d(TAG, "UtteranceProgressListener error");
        }
    };


    private void isStartingNavigation(String text) {
        Log.d(TAG, "isStartingNavigation");
        if (text.equals("ok") || text.equals("是") || text.equals("對") || text.equals("好")) {
            Log.d(TAG, "go!!!!!!!!");
            mOnSimpleSpeechRecognizerListener.onStartNavigation();
        }
    }


    public interface OnSimpleSpeechRecognizerListener {
        void onSearchPlace(String place);
        void onGoHome();
        void onGoToOffice();
        void onStartNavigation();
        void onStartTracking();
        void onPauseTracking();
        void onStopTracking();

    }
}
