package com.jarvish.android.jarvishpremium.track;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.ActivityEditTrackBinding;
import com.jarvish.android.jarvishpremium.db.TrackDatabase;
import com.jarvish.android.jarvishpremium.db.TrackLog;


import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class EditTrackActivity extends AppCompatActivity {
    private static final String TAG = "EditTrackActivity";
    private ActivityEditTrackBinding mBinding;

    private TrackLog mTrackLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_track);

        String tracklog = getIntent().getStringExtra(getString(R.string.key_track_edit_index));
        mTrackLog = new Gson().fromJson(tracklog, TrackLog.class);
        Log.d(TAG, "Track: " + mTrackLog);

        mBinding.etTitle.setText(mTrackLog.getTitle());
        mBinding.ivCancel.setOnClickListener(v -> onClickCancel());
        mBinding.ivConfirm.setOnClickListener(v -> onClickConfirm());
    }

    protected void onClickCancel() {
        finish();
    }


    protected void onClickConfirm() {
        if (mBinding.etTitle.getText() != null && !mBinding.etTitle.getText().toString().isEmpty()) {
            mTrackLog.setTitle(mBinding.etTitle.getText().toString());
        }
        update();
    }


    private void update() {
        Log.d(TAG, "update track");
        TrackDatabase.getInstance(this).trackLogDao()
                .updateTrack(mTrackLog)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull Integer integer) {
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra(getString(R.string.key_track_edit_result_index), mBinding.etTitle.getText().toString());
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "update error:" + e.getMessage());
                        e.printStackTrace();
                    }
                });

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
