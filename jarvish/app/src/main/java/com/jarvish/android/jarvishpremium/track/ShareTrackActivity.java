package com.jarvish.android.jarvishpremium.track;

import androidx.appcompat.app.AppCompatActivity;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;


import android.content.res.Resources;
import android.graphics.Bitmap;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.libraries.maps.CameraUpdate;
import com.google.android.libraries.maps.CameraUpdateFactory;
import com.google.android.libraries.maps.GoogleMap;
import com.google.android.libraries.maps.OnMapReadyCallback;
import com.google.android.libraries.maps.SupportMapFragment;
import com.google.android.libraries.maps.model.LatLng;
import com.google.android.libraries.maps.model.LatLngBounds;
import com.google.android.libraries.maps.model.MapStyleOptions;
import com.google.android.libraries.maps.model.PolylineOptions;
import com.google.android.libraries.maps.model.StyleSpan;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.adapter.ShareTrackAdapter;
import com.jarvish.android.jarvishpremium.databinding.ActivityShareTrackBinding;
import com.jarvish.android.jarvishpremium.db.GPSLog;

import java.util.ArrayList;

public class ShareTrackActivity extends AppCompatActivity implements OnMapReadyCallback,
        ShareTrackContract, ShareTrackAdapter.OnShareTrackListener {

    private static final String TAG = "ShareTrackActivity";

    private ActivityShareTrackBinding mBinding;

    private ShareTrackPresenter mPresenter;
    private GoogleMap mMap;
    private ShareTrackAdapter mShareTrackAdapter;
    private Bitmap mBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_share_track);

        setTopAppBar();
        initInitGoogleMap();
        mPresenter = new ShareTrackPresenter(this);
        long id = getIntent().getLongExtra(getString(R.string.key_gps_track_log_id), 0);
        Log.d(TAG, "track id: " + id);
        mPresenter.getTrackGPSLogs(this, id);
        setAdapter();
    }

    private void setTopAppBar() {
        mBinding.tbShare.setTitle(R.string.track_share_title);
        mBinding.tbShare.setNavigationOnClickListener((View v) -> finish());
    }

    private void setAdapter() {
        mShareTrackAdapter = new ShareTrackAdapter(mPresenter.buildShareTrackItem(this), this);
        mBinding.rcvShare.setHasFixedSize(true);
        mBinding.rcvShare.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rcvShare.setAdapter(mShareTrackAdapter);
    }

    private void initInitGoogleMap() {
        Log.d(TAG, "initialize map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json2));
            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
            mMap.getUiSettings().setAllGesturesEnabled(false);
            mMap.getUiSettings().setCompassEnabled(false);
            mMap.setOnMarkerClickListener(marker -> true);
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }


    @Override
    public void onFeedGpsLog(ArrayList<GPSLog> gpsLogs) {
        mMap.addMarker(mPresenter.createMarker(this, new LatLng(
                        gpsLogs.get(0).getLatitude(),
                        gpsLogs.get(0).getLongitude()),
                getString(R.string.track_departure),
                R.drawable.ic_start));


        mMap.addMarker(mPresenter.createMarker(this, new LatLng(
                        gpsLogs.get(gpsLogs.size() - 1).getLatitude(),
                        gpsLogs.get(gpsLogs.size() - 1).getLongitude()),
                getString(R.string.track_destination),
                R.drawable.ic_end));

        mPresenter.drawPolyline(gpsLogs);
    }

    @Override
    public void onInvalidGpsLog() {

    }

    @Override
    public void onErrorGpsLog(String e) {

    }

    @Override
    public void onDrawPolyline(LatLngBounds bounds, ArrayList<LatLng> latLngs, ArrayList<StyleSpan> styleSpans) {
        mMap.addPolyline(new PolylineOptions()
                .addAll(latLngs)
                .addAllSpans(styleSpans)
                .width(17.5f));
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 128);
        mMap.moveCamera(cu);
    }

    @Override
    public void onSelectedItem(int index) {
        mMap.snapshot(bitmap -> {
            Log.d(TAG, "snap shot ready");
            if (index == 0) {
                mPresenter.shareToFacebook(ShareTrackActivity.this, bitmap);
            } else {
                mPresenter.shareToApp(ShareTrackActivity.this, bitmap);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}