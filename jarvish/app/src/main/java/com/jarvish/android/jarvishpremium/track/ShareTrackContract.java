package com.jarvish.android.jarvishpremium.track;

import com.google.android.libraries.maps.model.LatLng;
import com.google.android.libraries.maps.model.LatLngBounds;
import com.google.android.libraries.maps.model.StyleSpan;
import com.jarvish.android.jarvishpremium.db.GPSLog;

import java.util.ArrayList;

interface ShareTrackContract {
    void onFeedGpsLog(ArrayList<GPSLog> gpsLogs);
    void onInvalidGpsLog();
    void onErrorGpsLog(String e);
    void onDrawPolyline(LatLngBounds bounds, ArrayList<LatLng> latLngs, ArrayList<StyleSpan> styleSpans);
}
