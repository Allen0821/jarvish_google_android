/**
 * Created by CupLidSheep on 11,November,2020
 */
package com.jarvish.android.jarvishpremium.track;

import android.graphics.drawable.Drawable;

public class ShareTrackItem {
    private String name;
    private Drawable icon;
    private int index;

    public ShareTrackItem(String name, Drawable icon, int index) {
        this.name = name;
        this.icon = icon;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
