/**
 * Created by CupLidSheep on 10,November,2020
 */
package com.jarvish.android.jarvishpremium.track;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.libraries.maps.model.BitmapDescriptor;
import com.google.android.libraries.maps.model.BitmapDescriptorFactory;
import com.google.android.libraries.maps.model.LatLng;
import com.google.android.libraries.maps.model.LatLngBounds;
import com.google.android.libraries.maps.model.MarkerOptions;
import com.google.android.libraries.maps.model.StrokeStyle;
import com.google.android.libraries.maps.model.StyleSpan;
import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.db.GPSLog;
import com.jarvish.android.jarvishpremium.db.GPSTrack;
import com.jarvish.android.jarvishpremium.db.TrackDatabase;
import com.jarvish.android.jarvishpremium.model.PzzaFuc;

import java.util.ArrayList;
import java.util.Date;

import io.reactivex.MaybeObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class ShareTrackPresenter {
    private static final String TAG = "ShareTrackPresenter";
    private final ShareTrackContract mContract;

    ShareTrackPresenter(ShareTrackContract contract) {
        mContract = contract;
    }

    ArrayList<ShareTrackItem> buildShareTrackItem(Context context) {
        ArrayList<ShareTrackItem> shareTrackItems = new ArrayList<>();

        shareTrackItems.add(new ShareTrackItem("Facebook",
                ContextCompat.getDrawable(context, R.drawable.ic_facebook_logo_blue), 0));
        shareTrackItems.add(new ShareTrackItem(context.getString(R.string.action_more),
                ContextCompat.getDrawable(context, R.drawable.ic_more_horiz_24), 1));

        return shareTrackItems;
    }

    void getTrackGPSLogs(Context context, long trackId) {
        Log.d(TAG, "get GPS Track id => " + trackId);
        TrackDatabase.getInstance(context).gpsTrackDao().getGPSTrackById(trackId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new MaybeObserver<GPSTrack>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull GPSTrack gpsTrack) {
                        ArrayList<GPSLog> gpsLogs = gpsTrack.getGpsTrack();
                        Log.d(TAG, "onSuccess");
                        if (gpsLogs.size() < 2) {
                            mContract.onInvalidGpsLog();
                        } else {
                            mContract.onFeedGpsLog(gpsLogs);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "throwable: " + e.getMessage());
                        mContract.onErrorGpsLog(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    MarkerOptions createMarker(Context context, @NonNull LatLng latLng, String title, @DrawableRes int resourceId) {
        return new MarkerOptions()
                .position(latLng)
                .icon(TrackHelper.bitmapDescriptorFromVector(context, resourceId))
                .anchor(0.5f, 0.5f)
                .title(title);
    }


    void drawPolyline(ArrayList<GPSLog> gpsLogs) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        ArrayList<LatLng> latLngs = new ArrayList<>();
        ArrayList<StyleSpan> styleSpans = new ArrayList<>();
        int i = 0;
        for (GPSLog log : gpsLogs) {
            LatLng latLng = new LatLng(log.getLatitude(), log.getLongitude());
            builder.include(latLng);
            latLngs.add(latLng);
            if (i < gpsLogs.size() - 1) {
                styleSpans.add(
                        new StyleSpan(StrokeStyle.gradientBuilder(
                                TrackHelper.getSpeedColor(gpsLogs.get(i).getSpeed()),
                                TrackHelper.getSpeedColor(gpsLogs.get(i + 1).getSpeed())).build()));
            }
            i++;
        }
        LatLngBounds bounds = builder.build();
        mContract.onDrawPolyline(bounds, latLngs, styleSpans);
    }

    public void shareToFacebook(Activity activity, Bitmap bitmap) {
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(bitmap)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareDialog shareDialog = new ShareDialog(activity);
            shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
        }
    }

    public void shareToApp(Activity activity, Bitmap bitmap) {
        String name = "JARVISH_" + new Date().getTime() + ".png";
        String path = MediaStore.Images.Media.insertImage(
                activity.getContentResolver(), bitmap, name, "JARVISH premium");
        Uri uri = Uri.parse(path);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("image/png");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        activity.startActivity(Intent.createChooser(shareIntent, activity.getString(R.string.track_share_title)));
    }
}
