package com.jarvish.android.jarvishpremium.track;

import android.content.Context;
import android.os.Build;
import android.text.SpannableString;
import android.util.Log;

import com.jarvish.android.jarvishpremium.SharedPreferencesUtil;
import com.jarvish.android.jarvishpremium.TimeTool;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.databinding.TrackContentViewBinding;
import com.jarvish.android.jarvishpremium.db.GPSLog;
import com.jarvish.android.jarvishpremium.db.TrackLog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class TrackContentView {

    private final Context mContext;
    private final TrackContentViewBinding mBinding;


    TrackContentView(Context context, TrackContentViewBinding binding) {
        mContext = context;
        mBinding = binding;
        //mTrackDetailActivity = trackDetailActivity;
    }


    void setContentView(TrackLog trackLog, GPSLog departure, GPSLog arrival) {

        mBinding.tvDistance.setText(getDistanceString(trackLog.getDistance()));

        mBinding.tvSpeed.setText(getSpeedString(trackLog.getAverageSpeed()));

        Log.d("TAG", "duration: " + trackLog.getDuration());
        String duration = TimeTool.calculateDuration(trackLog.getDuration());
        mBinding.tvDuration.setText(duration);

        mBinding.tvDeparture.setText(
                Util.getTrackPointTimeFormat(departure.getTimestamp()));
        mBinding.tvDestination.setText(
                Util.getTrackPointTimeFormat(arrival.getTimestamp()));

        mBinding.tvTime.setText(showDateAndTime(
                trackLog.getTimestamp(), Util.getDateTimeFormat()));
    }


    private SpannableString getDistanceString(double m) {
        if (SharedPreferencesUtil.getAppUnits(mContext) == 0) {
            return Util.calculateSpannableStringDistanceKm(m);
        } else {
            return Util.calculateSpannableStringDistanceMi(m);
        }
    }


    private SpannableString getSpeedString(double m) {
        if (SharedPreferencesUtil.getAppUnits(mContext) == 0) {
            return Util.calculateSpannableStringAverageSpeedKm(m);
        } else {
            return Util.calculateSpannableStringAverageSpeedMi(m);
        }
    }


    private String showDateAndTime(long timestamp, String format) {
        try{
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
                DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.getDefault());
                return dateFormat.format(new Date(timestamp));
            } else {
                SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());
                return formatter.format(new Date(timestamp));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
