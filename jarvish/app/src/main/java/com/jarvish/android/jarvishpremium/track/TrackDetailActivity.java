package com.jarvish.android.jarvishpremium.track;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.libraries.maps.CameraUpdate;
import com.google.android.libraries.maps.CameraUpdateFactory;
import com.google.android.libraries.maps.GoogleMap;
import com.google.android.libraries.maps.OnMapReadyCallback;
import com.google.android.libraries.maps.SupportMapFragment;
import com.google.android.libraries.maps.model.CameraPosition;
import com.google.android.libraries.maps.model.LatLng;
import com.google.android.libraries.maps.model.LatLngBounds;
import com.google.android.libraries.maps.model.MapStyleOptions;
import com.google.android.libraries.maps.model.Marker;
import com.google.android.libraries.maps.model.PolylineOptions;
import com.google.android.libraries.maps.model.StyleSpan;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.databinding.ActivityTrackDetailBinding;
import com.jarvish.android.jarvishpremium.db.GPSLog;
import com.jarvish.android.jarvishpremium.db.TrackLog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class TrackDetailActivity extends AppCompatActivity implements
        TrackDetailContract,
        EasyPermissions.PermissionCallbacks,
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener {
    private static final String TAG = "TrackDetailActivity";
    private ActivityTrackDetailBinding mBinding;

    public static final int EDIT_TRACK_CODE = 1788;
    public static final int EXPORT_GPX_CODE = 1799;

    private final static int RC_MAP_PERMISSIONS = 101;
    private static final String[] MAP_PERMISSIONS =
            {   Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_WIFI_STATE,
                    Manifest.permission.ACCESS_NETWORK_STATE };

    private GoogleMap mMap;

    private TrackDetailPresenter mTrackDetailPresenter;

    private TrackLog mTrackLog = null;
    private ArrayList<GPSLog> mGPSLogs;
    private ArrayList<LatLng> mLatLngs;

    private TrackContentView mTrackContentView;

    private float mZoomLevel = 17.5f;
    private float mTile = 55.0f;
    private boolean mFocus = true;

    private Animation animation;

    private Marker mTrackingMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_track_detail);

        mTrackDetailPresenter = new TrackDetailPresenter(this);

        startLoadingAnimation();
        mBinding.clWea.setVisibility(View.GONE);

        requestInitGoogleMap();

        mTrackContentView = new TrackContentView(this, mBinding.trackContent);

        getLocalGpx(getIntent().getStringExtra(getString(R.string.key_track_log)));

        setTopAppBar();
        setBottomSheet();
        mBinding.ivPlayHistory.setOnClickListener(v -> onClickPlayHistory());
        mBinding.ivFocus.setOnClickListener(v -> onClickFocus());
    }


    private void requestInitGoogleMap() {
        if (EasyPermissions.hasPermissions(TrackDetailActivity.this, MAP_PERMISSIONS)) {
            initInitGoogleMap();
        } else {
            Log.d(TAG, "Do not have permissions");
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs access to your location and storage",
                    RC_MAP_PERMISSIONS,
                    MAP_PERMISSIONS);
        }
    }


    @AfterPermissionGranted(RC_MAP_PERMISSIONS)
    private void initInitGoogleMap() {
        Log.d(TAG, "initialize map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json2));
            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnCameraMoveStartedListener(mCameraMoveStartedListener);
        mMap.setOnMapLoadedCallback(this::getGpsTrack);
    }

    private void setTopAppBar() {
        mBinding.toolbar.setTitle(mTrackLog.getTitle());
        mBinding.toolbar.setNavigationOnClickListener((View v) -> finish());
        mBinding.toolbar.inflateMenu(R.menu.tracking_menu);
        mBinding.toolbar.setOnMenuItemClickListener((MenuItem item) -> {
            switch (item.getItemId()) {
                case R.id.itShare:
                    startSnapShot();
                    break;
                case R.id.itExport:
                    exportGpxFile();
                    break;
                case R.id.itEdit:
                    Intent intent = new Intent();
                    intent.putExtra(getString(R.string.key_track_edit_index), 0);
                    String tracklog = new Gson().toJson(mTrackLog);
                    intent.putExtra(getString(R.string.key_track_edit_index), tracklog);
                    intent.setClass(this, EditTrackActivity.class);
                    startActivityForResult(intent, EDIT_TRACK_CODE);
                    break;
                default:
            }
            return true;
        });
    }


    private void exportGpxFile() {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/gpx");
        String name = "jarvish_" + getTimeFormat(new Date().getTime()) + ".gpx";
        intent.putExtra(Intent.EXTRA_TITLE, name);
        startActivityForResult(intent, EXPORT_GPX_CODE);
    }

    private static String getTimeFormat(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format("yyyyMMdd_HHmmss", cal).toString();
    }


    private void setBottomSheet() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        ViewGroup.LayoutParams params = mBinding.bottomSheet.getLayoutParams();
        params.height = height - Util.getViewPixel(this, 64);
        mBinding.bottomSheet.setLayoutParams(params);
    }


    private void startLoadingAnimation() {
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(this, R.anim.moto_loading_1);
        } else {
            animation.reset();
        }
        mBinding.ivLoading.startAnimation(animation);
        mBinding.clCover.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void endLoadingAnimation() {
        if (animation != null) {
            mBinding.ivLoading.clearAnimation();
        }
        mBinding.clCover.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }


    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    protected void  onClickFocus() {
        mZoomLevel = 17.5f;
        mFocus = true;
        mTrackDetailPresenter.zoomToCurrentPoint();
    }


    void onClickPlayHistory() {
        mTrackDetailPresenter.setPlayTrack();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mLatLngs != null) {
            mLatLngs.clear();
            mLatLngs = null;
        }

        mTrackDetailPresenter.destroy();
        mMap.clear();
    }

    private void getLocalGpx(String tracklog) {
        if (tracklog != null && !tracklog.isEmpty()) {
            Log.d(TAG, tracklog);
            mTrackLog = new Gson().fromJson(tracklog, TrackLog.class);
            mTrackDetailPresenter.getWeatherInformation(
                    this,
                    mTrackLog.getWeaTemp(),
                    mTrackLog.getWeaIcon());
        }
    }


    void getGpsTrack() {
        Log.d(TAG, "get Gps Track => " + mTrackLog.getGpsTrackId());
        mTrackDetailPresenter.getTrackGPSLogs(this, mTrackLog.getGpsTrackId());
    }


    private final GoogleMap.OnCameraMoveStartedListener mCameraMoveStartedListener = new GoogleMap.OnCameraMoveStartedListener() {
        @Override
        public void onCameraMoveStarted(int i) {
            if (i == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                mZoomLevel = mMap.getCameraPosition().zoom;
                mTile = mMap.getCameraPosition().tilt;
                //Log.d(TAG, "ZoomLevel: " + mZoomLevel + " " + "Tile: " + mTile);
                mFocus = false;
            }
        }
    };


    private void drawTracking(ArrayList<GPSLog> gpsLogs) {
        mGPSLogs = gpsLogs;
        Log.d(TAG, "gpslog size: " + mGPSLogs.size());

        mTrackContentView.setContentView(mTrackLog, mGPSLogs.get(0), mGPSLogs.get(mGPSLogs.size() - 1));


        mMap.addMarker(mTrackDetailPresenter.createMarker(this, new LatLng(
                        mGPSLogs.get(0).getLatitude(),
                        mGPSLogs.get(0).getLongitude()),
                getString(R.string.track_departure),
                R.drawable.ic_start));

        mMap.addMarker(mTrackDetailPresenter.createMarker(this, new LatLng(
                mGPSLogs.get(mGPSLogs.size() - 1).getLatitude(),
                mGPSLogs.get(mGPSLogs.size() - 1).getLongitude()),
                getString(R.string.track_destination),
                R.drawable.ic_end));

        mTrackingMarker = mMap.addMarker(
                mTrackDetailPresenter.createGPSPointIcon(
                        TrackDetailActivity.this,
                        new LatLng(mGPSLogs.get(0).getLatitude(),
                                mGPSLogs.get(0).getLongitude()),
                        mTrackLog.getHelmet()));

        mTrackDetailPresenter.drawPolyline(gpsLogs);
    }


    private void startSnapShot() {
        Log.d(TAG, "start snap shot");
        Intent intent = new Intent();
        intent.setClass(this, ShareTrackActivity.class);
        Log.d(TAG, "track id: " + mTrackLog.getTrackId());
        intent.putExtra(getString(R.string.key_gps_track_log_id), mTrackLog.getGpsTrackId());
        startActivity(intent);
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_TRACK_CODE && resultCode == Activity.RESULT_OK) {
            assert data != null;
            String newTitle = data.getStringExtra(getString(R.string.key_track_edit_result_index));
            mBinding.toolbar.setTitle(newTitle);
            mTrackLog.setTitle(newTitle);
            Intent intent = new Intent();
            intent.setAction("UNITS.CHANGED");
            sendBroadcast(intent);
        } else if (requestCode == EXPORT_GPX_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri;
            if (data != null) {
                uri = data.getData();
                Log.d(TAG, "create gpx file: " + uri.getPath());
                mTrackDetailPresenter.exportTrack(getApplicationContext(), uri, mTrackLog, mGPSLogs);
            }
        }
    }


    @Override
    public void onFeedGPSLogs(@NonNull ArrayList<GPSLog> gpsLogs) {
        Log.d(TAG, "on feed: " + new Gson().toJson(gpsLogs));
        mGPSLogs = gpsLogs;
        drawTracking(gpsLogs);
    }

    @Override
    public void onDrawPolyline(LatLngBounds bounds, ArrayList<LatLng> latLngs, ArrayList<StyleSpan> styleSpans) {
        mMap.addPolyline(new PolylineOptions()
                .addAll(latLngs)
                .addAllSpans(styleSpans)
                .width(17.5f));
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 128);
        mMap.moveCamera(cu);
        mLatLngs = latLngs;

        mTrackDetailPresenter.playTrack();

        endLoadingAnimation();
    }

    @Override
    public void onWeatherInfo(String temperature, Drawable icon) {
        if (temperature != null && icon != null) {
            mBinding.tvWeaTemp.setText(temperature);
            mBinding.ivWeaIcon.setImageDrawable(icon);
            mBinding.clWea.setVisibility(View.VISIBLE);
        } else {
            mBinding.clWea.setVisibility(View.GONE);
        }
    }

    @Override
    public void onZoomCameraToTrackPoint(LatLng latLng, float heading) {
        mTrackingMarker.setPosition(latLng);
        if (mFocus) {
            CameraPosition cameraPosition =
                    new CameraPosition.Builder()
                            .target(latLng)
                            .zoom(mZoomLevel)
                            .bearing(heading)
                            .tilt(mTile)
                            .build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void onZoomLatLng(LatLng latLng) {
        if (latLng != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, mZoomLevel));
        }
    }

    @Override
    public void onPlayingStatus(boolean playing) {
        if (playing) {
            mBinding.ivPlayHistory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pause_black_32dp));
        } else {
            mBinding.ivPlayHistory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play_arrow_black_32dp));
        }
        mBinding.ivPlayHistory.setColorFilter(getResources().getColor(R.color.white, null));
    }

    @Override
    public void onInvalidGPSLogs() {
        new MaterialAlertDialogBuilder(TrackDetailActivity.this, R.style.AppAlertDialog)
                .setTitle(R.string.track_invalid_title)
                .setMessage(R.string.track_invalid_content_open)
                .setNegativeButton(R.string.action_confirm, null)
                .show();
    }

    @Override
    public void onGPSLogsError(String msg) {

    }

    @Override
    public void onExportResult(boolean result) {
        String message;
        if (result) {
            message = getString(R.string.track_export_gpx_completed);
        } else {
            message = getString(R.string.track_export_gpx_fail);
        }
        Snackbar.make(mBinding.bottomSheet, message, Snackbar.LENGTH_SHORT)
                .setBackgroundTint(getResources().getColor(R.color.color_snackbar_bg, null))
                .setActionTextColor(getResources().getColor(R.color.white, null))
                .show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
