package com.jarvish.android.jarvishpremium.track;

import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

import com.google.android.libraries.maps.model.CameraPosition;
import com.google.android.libraries.maps.model.LatLng;
import com.google.android.libraries.maps.model.LatLngBounds;
import com.google.android.libraries.maps.model.StyleSpan;
import com.jarvish.android.jarvishpremium.db.GPSLog;

import java.util.ArrayList;

interface TrackDetailContract {

    void onFeedGPSLogs(@NonNull ArrayList<GPSLog> gpsLogs);
    void onDrawPolyline(LatLngBounds bounds, ArrayList<LatLng> latLngs, ArrayList<StyleSpan> styleSpans);
    void onWeatherInfo(String temperature, Drawable icon);
    void onZoomCameraToTrackPoint(LatLng latLng, float heading);
    void onZoomLatLng(LatLng latLng);
    void onPlayingStatus(boolean playing);
    void onInvalidGPSLogs();
    void onGPSLogsError(String msg);
    void onExportResult(boolean result);
}
