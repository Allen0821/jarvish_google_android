/**
 * Created by CupLidSheep on 18,August,2020
 */
package com.jarvish.android.jarvishpremium.track;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.google.android.libraries.maps.model.BitmapDescriptorFactory;
import com.google.android.libraries.maps.model.LatLng;
import com.google.android.libraries.maps.model.LatLngBounds;
import com.google.android.libraries.maps.model.MarkerOptions;
import com.google.android.libraries.maps.model.StrokeStyle;
import com.google.android.libraries.maps.model.StyleSpan;
import com.google.gson.Gson;
import com.google.maps.android.SphericalUtil;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.SharedPreferencesUtil;
import com.jarvish.android.jarvishpremium.ble.JCandy;
import com.jarvish.android.jarvishpremium.ble.JProduct;
import com.jarvish.android.jarvishpremium.db.GPSLog;
import com.jarvish.android.jarvishpremium.db.GPSTrack;
import com.jarvish.android.jarvishpremium.db.TrackDatabase;
import com.jarvish.android.jarvishpremium.db.TrackLog;
import com.jarvish.android.jarvishpremium.gmap.IconGenerator;
import com.jarvish.android.jarvishpremium.gpx.ExportGPXFile;
import com.jarvish.android.jarvishpremium.weather.WeatherUtil;
import com.jarvish.android.jarvishpremium.weather.core.Weather;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class TrackDetailPresenter {
    private static final String TAG = "TrackDetailPresenter";
    private final TrackDetailContract mContract;

    private ArrayList<GPSLog> mGPSLogs;
    private ArrayList<LatLng> mLatLngs;
    private int mStepIndex = 0;
    private boolean playing = true;
    private Disposable mPlayDisposable;


    TrackDetailPresenter(@NonNull TrackDetailContract contract) {
        this.mContract = contract;
    }

    void destroy() {
        if (mPlayDisposable != null){
            mPlayDisposable.dispose();
            mPlayDisposable = null;
        }
        if (mGPSLogs != null){
            mGPSLogs.clear();
            mGPSLogs = null;
        }
        if (mLatLngs != null){
            mLatLngs.clear();
            mLatLngs = null;
        }
    }

    void setPlayTrack() {
        this.playing = !this.playing;
        mContract.onPlayingStatus(this.playing);
    }

    void zoomToCurrentPoint() {
        if (mStepIndex >= mLatLngs.size()) {
            mContract.onZoomLatLng(mLatLngs.get(0));
        } else {
            mContract.onZoomLatLng(mLatLngs.get(mStepIndex));
        }
    }

    void getTrackGPSLogs(Context context, long trackId) {
        Log.d(TAG, "get GPS Track id => " + trackId);
        TrackDatabase.getInstance(context).gpsTrackDao().getGPSTrackById(trackId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new MaybeObserver<GPSTrack>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(GPSTrack gpsTrack) {
                        Log.d(TAG, "on feed: " + new Gson().toJson(gpsTrack));
                        mGPSLogs = gpsTrack.getGpsTrack();
                        if (mGPSLogs.size() < 2) {
                            mContract.onInvalidGPSLogs();
                        } else {
                            mContract.onFeedGPSLogs(mGPSLogs);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "throwable: " + e.getMessage());
                        mContract.onGPSLogsError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void getWeatherInformation(@NonNull Context context, String temperature, int icon) {
        if (temperature != null) {
            String t = getWeatherTemperature(temperature);
            Drawable d = WeatherUtil.getWeatherIcon(context, icon);
            mContract.onWeatherInfo(t, d);
        } else {
            mContract.onWeatherInfo(null, null);
        }
    }

    void drawPolyline(ArrayList<GPSLog> gpsLogs) {
        Log.d(TAG, "drawPolyline");
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        mLatLngs = new ArrayList<>();
        ArrayList<StyleSpan> styleSpans = new ArrayList<>();
        int i = 0;
        for (GPSLog log : gpsLogs) {
            LatLng latLng = new LatLng(log.getLatitude(), log.getLongitude());
            builder.include(latLng);
            mLatLngs.add(latLng);
            Log.d(TAG, "drawPolyline: " + i);
            if (i <= gpsLogs.size() - 2) {
                styleSpans.add(
                        new StyleSpan(StrokeStyle.gradientBuilder(
                                TrackHelper.getSpeedColor(gpsLogs.get(i).getSpeed()),
                                TrackHelper.getSpeedColor(gpsLogs.get(i + 1).getSpeed())).build()));
            }
            i++;
        }
        LatLngBounds bounds = builder.build();
        mContract.onDrawPolyline(bounds, mLatLngs, styleSpans);
    }


    void playTrack() {
        if (mPlayDisposable != null){
            mPlayDisposable.dispose();
            mPlayDisposable = null;
        }
        mStepIndex = 0;
        mPlayDisposable = Observable
                .interval(2000, 300, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Long aLong) -> {
                    if (playing) {
                        if (mStepIndex < mLatLngs.size()) {
                            int lastStep;
                            if (mStepIndex == 0) {
                                lastStep = mLatLngs.size() - 1;
                            } else {
                                lastStep = mStepIndex - 1;
                            }
                            double heading = SphericalUtil.computeHeading(
                                    mLatLngs.get(lastStep), mLatLngs.get(mStepIndex));

                            mContract.onZoomCameraToTrackPoint(mLatLngs.get(mStepIndex), (float) heading);

                            mStepIndex++;
                        } else {
                            mStepIndex = 0;
                        }
                    }
                });
    }


    void exportTrack(Context context, Uri uri, @NonNull TrackLog trackLog, @NonNull ArrayList<GPSLog> gpsLogs) {
        try {
            boolean result = ExportGPXFile.createGPXFile(context, uri, trackLog, gpsLogs);
            if (result) {
                mContract.onExportResult(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mContract.onExportResult(false);
        }
    }


    MarkerOptions createGPSPointIcon(@NonNull Context context, @NonNull LatLng latLng, String helmet) {
        String photo;
        if (helmet == null || helmet.isEmpty()) {
            photo = "x01";
        } else {
            photo = helmet;
        }
        Drawable drawable = ResourcesCompat.getDrawable(
                context.getResources(),
                context.getResources()
                        .getIdentifier(photo, "drawable",
                                context.getPackageName()),
                null);
        IconGenerator iconFactory = new IconGenerator(context, drawable);
        Bitmap icon = iconFactory.makeIcon();
        return new MarkerOptions().
                icon(BitmapDescriptorFactory.fromBitmap(icon)).
                position(latLng).
                anchor(0.5f, 1);
    }

    MarkerOptions createMarker(Context context, @NonNull LatLng latLng, String title, @DrawableRes int resourceId) {
        return new MarkerOptions()
                .position(latLng)
                .icon(TrackHelper.bitmapDescriptorFromVector(context, resourceId))
                .anchor(0.5f,0.5f)
                .title(title);
    }

    private String getWeatherTemperature(@NonNull String weaTemp) {
        String s = weaTemp.substring(0, weaTemp.length() - 2);
        int t = Integer.parseInt(s);
        if (weaTemp.contains("°C")) {
            if (Weather.getInstance().getTemperatureUnits().equals("°C")) {
                return weaTemp;
            } else {
                int temp = t * 9 / 5 + 32;
                return temp + "°F";
            }
        } else {
            if (Weather.getInstance().getTemperatureUnits().equals("°F")) {
                return weaTemp;
            } else {
                int temp = (t - 32) * 5 / 9;
                return temp + "°C";
            }
        }
    }
}
