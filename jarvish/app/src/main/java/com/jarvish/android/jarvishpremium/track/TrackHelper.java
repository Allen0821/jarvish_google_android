/**
 * Created by CupLidSheep on 11,November,2020
 */
package com.jarvish.android.jarvishpremium.track;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.google.android.libraries.maps.model.BitmapDescriptor;
import com.google.android.libraries.maps.model.BitmapDescriptorFactory;
import com.google.android.libraries.maps.model.LatLng;
import com.google.android.libraries.maps.model.MarkerOptions;
import com.jarvish.android.jarvishpremium.Util;


public class TrackHelper {

    public static int getSpeedColor(Double speed) {
        double zza = speed * 60 * 60 / 1000;
        Log.d("TAG", "drawPolyline color: " + zza);
        if (zza < 20.0) {
            return Color.rgb(204, 255, 255);
        } else if (zza >= 20.0 && zza < 40.0) {
            return Color.rgb(0, 255, 255);
        } else if (zza >= 40.0 && zza < 60.0) {
            return Color.rgb(0, 255, 102);
        } else if (zza >= 60.0 && zza < 80.0) {
            return Color.rgb(204, 255, 102);
        } else if (zza >= 80.0 && zza < 100.0) {
            return Color.rgb(255, 204, 102);
        } else {
            return Color.rgb(255, 102, 153);
        }
    }


    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int resourceId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, resourceId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        Bitmap icon = Bitmap.createScaledBitmap(bitmap, Util.dpToPx(context, 24), Util.dpToPx(context, 24), true);
        return BitmapDescriptorFactory.fromBitmap(icon);
    }

}
