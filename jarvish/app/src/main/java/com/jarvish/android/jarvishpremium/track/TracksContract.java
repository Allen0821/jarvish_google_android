/*
 * Created by CupLidSheep on 12,October,2020
 */
package com.jarvish.android.jarvishpremium.track;

import com.jarvish.android.jarvishpremium.db.TrackLog;

import java.util.ArrayList;

interface TracksContract {
    void onFeedTracks(ArrayList<TrackLog> trackLogs);
    void onFetchTrackError(String error);
    void onImportGPXError(String error);
    void onDeleteTrackError(String error);
}
