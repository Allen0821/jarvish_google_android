/*
 * Created by CupLidSheep on 12,October,2020
 */
package com.jarvish.android.jarvishpremium.track;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.adapter.TrackSummaryAdapter;
import com.jarvish.android.jarvishpremium.databinding.FragmentTracksBinding;
import com.jarvish.android.jarvishpremium.db.TrackLog;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TracksFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TracksFragment extends Fragment implements TracksContract, TrackSummaryAdapter.OnTrackSummaryListener {
    private static final String TAG = "TracksFragment";

    public static final int IMPORT_TRACK_CODE = 1224;

    private FragmentTracksBinding mBinding;

   /* @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.swipeTrack)
    SwipeRefreshLayout swipeTrack;
    @BindView(R.id.rvTrackLog)
    RecyclerView rvTrackLog;
    @BindView(R.id.ivLoading)
    ImageView ivLoading;*/

    private TracksPresenter mTracksPresenter;

    private ArrayList<TrackLog> mTrackLogs;
    private TrackSummaryAdapter mTrackSummaryAdapter;

    private Animation animation;

    public TracksFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment TracksFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TracksFragment newInstance() {
        return new TracksFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_tracks, container, false);
        View view = mBinding.getRoot();

        initAdapter();
        setSwipeRefresh();

        IntentFilter filter = new IntentFilter();
        filter.addAction("UNITS.CHANGED");
        requireActivity().registerReceiver(mReceiver, filter);

        mTracksPresenter = new TracksPresenter(this);
        fetchTracks();

        mBinding.ivImport.setOnClickListener(v -> onClickImport());


        return view;
    }


    private void initAdapter() {
        mTrackLogs = new ArrayList<>();
        mBinding.rvTrackLog.setHasFixedSize(true);
        mBinding.rvTrackLog.setLayoutManager(new LinearLayoutManager(requireContext()));
        mTrackSummaryAdapter = new TrackSummaryAdapter(mTrackLogs, this);
        mBinding.rvTrackLog.setAdapter(mTrackSummaryAdapter);
    }


    private void setSwipeRefresh() {
        mBinding.swipeTrack.setDistanceToTriggerSync(128);
        mBinding.swipeTrack.setOnRefreshListener(() -> {
            fetchTracks();
            mBinding.swipeTrack.setRefreshing(false);
        });
    }


    /**
     * Units changed event receiver.
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "receiver #####");
            if (intent.getAction() != null && intent.getAction().equals("UNITS.CHANGED")) {
                Log.d(TAG, "UNITS.CHANGED #####");
                fetchTracks();
            }
        }
    };


    private void startLoadingAnimation() {
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(requireContext(), R.anim.moto_loading_1);
        } else {
            animation.reset();
        }
        mBinding.ivLoading.startAnimation(animation);
        mBinding.ivLoading.setVisibility(View.VISIBLE);
        requireActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void endLoadingAnimation() {
        if (animation != null) {
            mBinding.ivLoading.clearAnimation();
        }
        mBinding.ivLoading.setVisibility(View.GONE);
        requireActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMPORT_TRACK_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            if (data != null) {
                Uri uri = data.getData();
                if (uri != null) {
                    Log.d(TAG, "import file url: " + uri.toString());
                    startLoadingAnimation();
                    mTracksPresenter.fetchTrackFromGpxFile(requireContext(), uri);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        requireActivity().unregisterReceiver(mReceiver);
        mTracksPresenter.destroy();
        super.onDestroy();
    }

    protected void onClickImport() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, IMPORT_TRACK_CODE);
    }

    private void fetchTracks() {
        startLoadingAnimation();
        mTracksPresenter.fetchAllTracksFromDatabase(requireContext());
    }


    /**
     * TrackSummaryAdapter interface, allow TracksFragment
     * Communicate with TrackSummaryAdapter.
     */
    @Override
    public void onSelectedTrackLog(TrackLog trackLog) {
        Intent intent = new Intent();
        intent.putExtra(getString(R.string.key_track_log_index), 0);
        String track = new Gson().toJson(trackLog);
        intent.putExtra(getString(R.string.key_track_log), track);
        intent.setClass(requireActivity(), TrackDetailActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDeleteTrackLog(TrackLog trackLog) {
        new MaterialAlertDialogBuilder(requireContext(), R.style.AppAlertDialog)
                .setTitle(R.string.track_delete_title)
                .setMessage(R.string.track_delete_content)
                .setPositiveButton(R.string.action_confirm, (dialog, which) -> {
                    startLoadingAnimation();
                    mTracksPresenter.deleteTrack(requireContext(), trackLog);
                })
                .setNegativeButton(R.string.action_cancel, null)
                .show();
    }


    /**
     * TracksContract interface, allow TracksFragment
     * Communicate with TracksPresenter.
    */
    @Override
    public void onFeedTracks(ArrayList<TrackLog> trackLogs) {
        if (mTrackLogs != null) {
            mTrackLogs.clear();
            mTrackLogs.addAll(trackLogs);
            mTrackSummaryAdapter.notifyDataSetChanged();
        }
        endLoadingAnimation();
    }

    @Override
    public void onFetchTrackError(String error) {
        endLoadingAnimation();
    }

    @Override
    public void onImportGPXError(String error) {
        endLoadingAnimation();
    }

    @Override
    public void onDeleteTrackError(String error) {
        endLoadingAnimation();
    }
}