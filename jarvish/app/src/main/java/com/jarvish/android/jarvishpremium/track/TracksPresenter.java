/*
 * Created by CupLidSheep on 12,October,2020
 */
package com.jarvish.android.jarvishpremium.track;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.jarvish.android.jarvishpremium.db.TrackDatabase;
import com.jarvish.android.jarvishpremium.db.TrackLog;
import com.jarvish.android.jarvishpremium.gpx.ImportGPXFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class TracksPresenter {
    private static final String TAG = "TracksPresenter";
    private TracksContract mContract;

    private ArrayList<TrackLog> mTrackLogs;

    TracksPresenter(@NonNull TracksContract contract) {
        this.mContract = contract;
        mTrackLogs = new ArrayList<>();
    }


    void destroy() {
       if (mTrackLogs != null) {
           mTrackLogs.clear();
           mTrackLogs = null;
       }
       mContract = null;
    }


    void fetchAllTracksFromDatabase(Context context) {
        Log.d(TAG, "fetch track logs");
        Disposable disposable = TrackDatabase.getInstance(context).trackLogDao().getAllTrackings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .take(1)
                .subscribe(trackLogs -> {
                    mTrackLogs.clear();
                    for (TrackLog trackLog : trackLogs) {
                        Log.d(TAG, "fetch track id =>" + trackLog.getTrackId());
                        mTrackLogs.add(trackLog);
                    }
                    Collections.sort(mTrackLogs, (TrackLog o1, TrackLog o2) -> {
                        Date d1 = new Date(o1.getTimestamp());
                        Date d2 = new Date(o2.getTimestamp());
                        if (d1.after(d2)) {
                            return -1;
                        }
                        return 1;
                    });
                    mContract.onFeedTracks(mTrackLogs);
                }, throwable -> {
                    Log.e(TAG, "fetch track onError: " + throwable.getMessage());
                    mContract.onFetchTrackError(throwable.getMessage());
                });
    }


    void fetchTrackFromGpxFile(@NonNull Context context, @NonNull Uri uri) {
        ImportGPXFile.ImportFile(context, uri, new ImportGPXFile.ImportFileListener() {
            @Override
            public void completed() {
                fetchAllTracksFromDatabase(context);
            }

            @Override
            public void error(String msg) {
                mContract.onImportGPXError(msg);
            }
        });
    }


    void deleteTrack(Context context, TrackLog trackLog) {
        Completable.fromAction(() -> TrackDatabase.getInstance(context).trackLogDao().deleteTrack(trackLog))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "delete complete");
                        mTrackLogs.remove(trackLog);
                        mContract.onFeedTracks(mTrackLogs);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "delete track error: " + e.getMessage());
                        mContract.onDeleteTrackError(e.getMessage());
                    }
                });
    }
}
