/**
 * Created by CupLidSheep on 30,November,2020
 */
package com.jarvish.android.jarvishpremium.tracking;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.util.Log;

import com.jarvish.android.jarvishpremium.SharedPreferencesUtil;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.db.GPSLog;
import com.jarvish.android.jarvishpremium.db.GPSTrack;
import com.jarvish.android.jarvishpremium.db.TrackDatabase;
import com.jarvish.android.jarvishpremium.db.TrackLog;

import java.util.ArrayList;
import java.util.Date;

import io.reactivex.MaybeObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.ACTIVITY_SERVICE;

public class TrackManager {
    private static final String TAG = "TrackManager";
    private static volatile TrackManager mTrackManager;

    private Intent mIntent;

    private final ArrayList<GPSLog> mGpsList;

    private TrackManagerListener mListener;
    private Boolean isPause = false;

    private boolean isRunning = false;

    private TrackManager() {
        if (mTrackManager != null) {
            throw new RuntimeException("Use getInstance() method to get TrackingManager instance.");
        }
        mIntent = null;
        mGpsList = new ArrayList<>();
    }


    public static TrackManager getInstance() {
        if (mTrackManager == null) {
            synchronized (TrackManager.class) {
                if (mTrackManager == null) {
                    mTrackManager = new TrackManager();
                }
            }
        }
        return mTrackManager;
    }


    private boolean isServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.example.MyNeatoIntentService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    private void registerReceiver(Context context) {
        IntentFilter filter = new IntentFilter();
        filter.addAction("track");
        context.registerReceiver(mReceiver, filter);
    }


    private void unregisterReceiver(Context context) {
        if (isRunning) {
            context.unregisterReceiver(mReceiver);
        }
    }


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra("track_location");
            if (isPause) {
                return;
            }

            if (location == null) {
                return;
            }
            Log.d(TAG, "location: " + location.getLatitude() + ", " +
                    location.getLongitude());
            if (filterLocation(location)) {
                mGpsList.add(buildGpsLog(location));
                calculateInfo(location);
                mLastLocation = location;
                mLastSpeed = location.getSpeed();
                if (mListener != null) {
                    mListener.onLocationChanged(location);
                }
            }
        }
    };


    public void start(Context context, TrackManagerListener listener) {
        Context ctx = context.getApplicationContext();
        if (isServiceRunning(ctx)) {
            Log.d(TAG, "tracking is running");
            return;
        }
        Log.d(TAG, "start tracking");
        mGpsList.clear();
        mListener = listener;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mIntent = new Intent();
            mIntent.setClass(ctx, TrackService.class);
            ctx.startForegroundService(mIntent);
        } else {
            mIntent = new Intent();
            mIntent.setClass(ctx, TrackService.class);
            ctx.startService(mIntent);
        }
        registerReceiver(ctx);
        isRunning = true;
    }


    public void stop(Context context) {
        Context ctx = context.getApplicationContext();
        if (mIntent != null) {
            ctx.stopService(mIntent);
        }
        unregisterReceiver(ctx);
        storeGPSTrack(ctx);
        mListener = null;
        isRunning = false;
    }


    public void pause() {
        isPause = true;
    }


    public void resume() {
        isPause = false;
    }


    private double mLastSpeed = 0;
    private Location mLastLocation;
    private double mTotalDistance;
    private double mTotalSpeed;
    private double mAverageSpeed;
    private double mMaxSpeed;
    private long mDuration;
    private long mLastStep;

    private boolean filterLocation(Location location) {
        if (mGpsList.size() == 0) {
            return true;
        }
        if (location.getSpeed() < 0 || (mLastSpeed == 0 && location.getSpeed() == 0)) {
            return false;
        }
        return mLastLocation != null && location.distanceTo(mLastLocation) > 5;
    }


    private GPSLog buildGpsLog(Location location) {
        GPSLog gpsLog = new GPSLog();
        gpsLog.setLatitude(location.getLatitude());
        gpsLog.setLongitude(location.getLongitude());
        gpsLog.setAltitude(location.getAltitude());
        gpsLog.setSpeed(location.getSpeed());
        if (mLastStep != 0) {
            mDuration = mDuration + (location.getTime() - mLastStep);
        }
        mLastStep = location.getTime();
        gpsLog.setTimestamp(location.getTime());
        return  gpsLog;
    }


    private void calculateInfo(Location location) {
        if (mLastLocation != null) {
            mTotalDistance = mTotalDistance + location.distanceTo(mLastLocation);
        }
        mTotalSpeed = mTotalSpeed + location.getSpeed();
        if (location.getSpeed() > mMaxSpeed) {
            mMaxSpeed = location.getSpeed();
        }
    }


    private void storeGPSTrack(Context context) {
        Log.d(TAG, "store GPS track size: " + mGpsList.size());
        if (mGpsList.size() >= 2) {
            Log.d(TAG, "store GPS track");
            GPSTrack gpsTrack = new GPSTrack();
            gpsTrack.setGpsTrack(mGpsList);
            gpsTrack.setGpsTrackId(new Date().getTime());
            TrackDatabase.getInstance(context).gpsTrackDao().insertGPSTrack(gpsTrack)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new MaybeObserver<Long>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "store GPS track onSubscribe");
                        }

                        @Override
                        public void onSuccess(Long aLong) {
                            Log.d(TAG, "store GPS track: " + aLong);
                            storeTrackLog(context, aLong);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "onError: " + e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "store GPS track onComplete");
                        }
                    });
        } else {
            if (mListener != null) {
                mListener.onInvalidTrack();
            }
        }
    }

    private void storeTrackLog(Context context, long id) {
        Log.d(TAG, "store track log");
        Log.d(TAG, "mAverageSpeed " + mAverageSpeed);
        Log.d(TAG, "mMaxSpeed " + mMaxSpeed);
        Log.d(TAG, "mTotalDistance: " + mTotalDistance);
        TrackLog trackLog = new TrackLog();
        mMaxSpeed = Util.roundingDoubleToDouble(mMaxSpeed, 2);
        mTotalDistance = Util.roundingDoubleToDouble(mTotalDistance, 2);
        trackLog.setDistance(mTotalDistance);
        trackLog.setMaxSpeed(mMaxSpeed);
        trackLog.setTitle(Util.getDefaultTrackLogTitle(context));
        long d1 = (mGpsList.get(mGpsList.size() - 1).getTimestamp() - mGpsList.get(0).getTimestamp()) / 1000;
        long d2 = mDuration / 1000;
        trackLog.setDuration(Math.min(d1, d2));
        mAverageSpeed = mTotalSpeed / mGpsList.size();
        mAverageSpeed = Util.roundingDoubleToDouble(mAverageSpeed, 2);
        trackLog.setAverageSpeed(mAverageSpeed);
        trackLog.setGpsTrackId(id);
        trackLog.setTimestamp(new Date().getTime());

        SharedPreferences sharedPref = context.getSharedPreferences(
                "track_extra", Context.MODE_PRIVATE);
        trackLog.setWeaTemp(sharedPref.getString("weather_temperature", ""));
        trackLog.setWeaIcon(sharedPref.getInt("weather_icon", -10));
        trackLog.setWeaDesc(sharedPref.getString("weather_description", ""));

        trackLog.setHelmet(SharedPreferencesUtil.getYourHelmetPhoto(context));

        TrackDatabase.getInstance(context).trackLogDao().insertTrack(trackLog)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new MaybeObserver<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(Long aLong) {
                        Log.d(TAG, "onSuccess!!!");
                        Intent intent = new Intent();
                        intent.setAction("UNITS.CHANGED");
                        context.sendBroadcast(intent);
                        mGpsList.clear();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "store track log onError: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "store track log onComplete");
                    }
                });
    }


    Boolean verifyTrack() {
        return mGpsList != null && mGpsList.size() >= 2;
    }

    public interface TrackManagerListener {

        void onLocationChanged(Location location);
        void onInvalidTrack();
    }
}