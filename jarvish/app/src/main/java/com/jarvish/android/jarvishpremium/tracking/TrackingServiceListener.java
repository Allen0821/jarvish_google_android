package com.jarvish.android.jarvishpremium.tracking;

import android.location.Location;

public interface TrackingServiceListener {

    void onLocationChanged(Location location);
}
