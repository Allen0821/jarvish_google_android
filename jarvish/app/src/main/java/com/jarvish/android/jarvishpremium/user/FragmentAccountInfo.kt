package com.jarvish.android.jarvishpremium.user

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.adapter.UserInfoAdapter
import com.jarvish.android.jarvishpremium.databinding.FragmentAccountInfoBinding
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModel

class FragmentAccountInfo: Fragment() {

    private lateinit var binding: FragmentAccountInfoBinding
    private val viewModel: AccountViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_account_info, container, false)
        binding.fragment = this
        binding.viewModel = viewModel

        binding.rvInfo.adapter = adapter
        viewModel.account.observe(viewLifecycleOwner) {
            Log.d("[Account]", "user account changed $it")
            adapter.submitList(viewModel.getUserInfoList(it))
        }
        viewModel.getUserAccount()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
    }


    fun onCloseAccountInfo() {
        requireActivity().finish()
    }

    private val linkElderLaunch = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == AppCompatActivity.RESULT_OK) {
            viewModel.getUserAccount()
        }
    }

    val adapter = UserInfoAdapter {
        when (it.titleResource) {
            R.string.user_email_address -> {
                if (it.type == 1) {
                    Navigation.findNavController(binding.root).navigate(R.id.action_email_update)
                }
            }
            R.string.user_email_verification -> {
                if (it.type == 1) {
                    Navigation.findNavController(binding.root).navigate(R.id.action_AccountInfo_to_EmailVerification)
                }
            }
            R.string.user_elder -> {
                if (it.type == 1) {
                    val intent = Intent(requireActivity(), LinkElderActivity::class.java)
                    intent.putExtra(getString(R.string.key_uid), viewModel.user.uid)
                    linkElderLaunch.launch(intent)
                }
            }
            else -> Log.d("[Account]", "click: $it")
        }
    }

}