package com.jarvish.android.jarvishpremium.user

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.ViewUtil
import com.jarvish.android.jarvishpremium.databinding.FragmentElderLinkBinding
import com.jarvish.android.jarvishpremium.user.model.LinkElderErrorCode
import com.jarvish.android.jarvishpremium.user.viewModel.LinkElderViewModel

class FragmentElderLink: Fragment() {

    private lateinit var binding: FragmentElderLinkBinding
    private val viewModel: LinkElderViewModel by activityViewModels()

    private var canBack = true

    private lateinit var callbackManager: CallbackManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_elder_link, container, false)
        binding.fragment = this
        binding.viewModel = viewModel

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (canBack) {
                onCloseLinkElder(false)
            }
        }

        binding.etEmail.doOnTextChanged { _, _, _, _ ->
            binding.tilEmail.error = null
        }

        binding.etPassword.doOnTextChanged { _, _, _, _ ->
            binding.tilPassword.error = null
        }

        viewModel.linkFromEmailResult.observe(viewLifecycleOwner) { event ->
            ViewUtil.enableInteraction(requireActivity())
            canBack = true
            event.getContentIfNotHandled()?.let {
                when (it) {
                    LinkElderErrorCode.NONE ->
                        alertView()
                    LinkElderErrorCode.INVALID_EMAIL_ADDRESS ->
                        binding.tilEmail.error = getString(it.nameResource)
                    LinkElderErrorCode.INVALID_PASSWORD ->
                        binding.tilPassword.error = getString(it.nameResource)
                    else -> ViewUtil.alertView(requireContext(), it.nameResource)
                }
            }
        }

        viewModel.linkFromFacebookResult.observe(viewLifecycleOwner) { event ->
            ViewUtil.enableInteraction(requireActivity())
            canBack = true
            event.getContentIfNotHandled()?.let {
                when (it) {
                    LinkElderErrorCode.NONE ->
                        alertView()
                    else -> ViewUtil.alertView(requireContext(), it.nameResource)
                }
            }
        }

        return  binding.root
    }

    private fun alertView() {
        AlertDialog.Builder(requireContext(), R.style.AppAlertDialog)
                .setTitle(R.string.user_elder_link_Complected_title)
                .setMessage(R.string.user_elder_link_Complected_content)
                .setNegativeButton(R.string.action_confirm) { _, _ ->
                    onCloseLinkElder(true)
                }
                .show()
    }

    fun onCloseLinkElder(result: Boolean) {
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        if (result) {
            requireActivity().setResult(RESULT_OK)
        }
        requireActivity().finish()
    }

    fun onForgotPassword() {
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        Navigation.findNavController(binding.root).navigate(R.id.action_ElderLink_to_ElderPwdForgot)
    }

    fun onEmailLogin() {
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        ViewUtil.disableInteraction(requireActivity())
        canBack = false
        viewModel.linkFromEmail()
    }

    fun onFacebookLogin() {
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        callbackManager = CallbackManager.Factory.create()
        val loginManager = LoginManager.getInstance()
        loginManager.logOut()
        loginManager
                .setLoginBehavior(LoginBehavior.WEB_ONLY)
                .registerCallback(callbackManager,
                        object : FacebookCallback<LoginResult> {
                            override fun onSuccess(result: LoginResult?) {
                                if (result != null) {
                                    //ViewUtil.disableInteraction(requireActivity())
                                    //canBack = false
                                    viewModel.linkFromFacebook(result.accessToken)
                                }
                            }

                            override fun onCancel() {
                                TODO("Not yet implemented")
                            }

                            override fun onError(error: FacebookException?) {
                                TODO("Not yet implemented")
                            }

                        })
        loginManager
                .logInWithReadPermissions(this, listOf("email", "public_profile"))
    }


    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 64206) {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

}