package com.jarvish.android.jarvishpremium.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.ViewUtil
import com.jarvish.android.jarvishpremium.databinding.FragmentElderPwdForgotBinding
import com.jarvish.android.jarvishpremium.user.model.LinkElderErrorCode
import com.jarvish.android.jarvishpremium.user.viewModel.LinkElderViewModel

class FragmentElderPwdForgot: Fragment() {

    private lateinit var binding: FragmentElderPwdForgotBinding
    private val viewModel: LinkElderViewModel by activityViewModels()

    private var canBack = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_elder_pwd_forgot, container, false)
        binding.fragment = this
        binding.viewModel = viewModel

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (canBack) {
                onCloseForgot()
            }
        }

        binding.etVCodeEmail.doOnTextChanged { _, _, _, _ ->
            binding.tilVCodeEmail.error = null
        }

        viewModel.requestVerificationCodeResult.observe(viewLifecycleOwner) { event ->
            ViewUtil.enableInteraction(requireActivity())
            canBack = true
            event.getContentIfNotHandled()?.let {
                when (it) {
                    LinkElderErrorCode.NONE ->
                        Navigation.findNavController(binding.root).navigate(R.id.action_ElderPwdForgot_to_ElderPwdReset)
                    LinkElderErrorCode.INVALID_EMAIL_ADDRESS ->
                        binding.tilVCodeEmail.error = getString(it.nameResource)
                    else -> ViewUtil.alertView(requireContext(), it.nameResource)
                }
            }
        }

        return  binding.root
    }

    fun onCloseForgot() {
        viewModel.vCodeEmail = ""
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        Navigation.findNavController(binding.root).navigate(R.id.action_ElderPwdForgot_to_ElderLink)
    }

    fun onRequestVerificationCode() {
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        ViewUtil.disableInteraction(requireActivity())
        canBack = false
        viewModel.requestVerificationCode()
    }
}