package com.jarvish.android.jarvishpremium.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.ViewUtil
import com.jarvish.android.jarvishpremium.databinding.FragmentElderPwdResetBinding
import com.jarvish.android.jarvishpremium.user.model.LinkElderErrorCode
import com.jarvish.android.jarvishpremium.user.viewModel.LinkElderViewModel

class FragmentElderPwdReset: Fragment() {
    private lateinit var binding: FragmentElderPwdResetBinding
    private val viewModel: LinkElderViewModel by activityViewModels()

    private var canBack = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_elder_pwd_reset, container, false)
        binding.fragment = this
        binding.viewModel = viewModel

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (canBack) {
                onCloseReset()
            }
        }

        binding.etVCode.doOnTextChanged { _, _, _, _ ->
            binding.tilVCode.error = null
        }

        binding.etPassword.doOnTextChanged { _, _, _, _ ->
            binding.tilPassword.error = null
        }

        binding.etRePassword.doOnTextChanged { _, _, _, _ ->
            binding.tilRePassword.error = null
        }

        viewModel.requestResetPasswordResult.observe(viewLifecycleOwner) { event ->
            ViewUtil.enableInteraction(requireActivity())
            canBack = true
            event.getContentIfNotHandled()?.let {
                when (it) {
                    LinkElderErrorCode.NONE ->
                        Navigation.findNavController(binding.root).navigate(R.id.action_ElderPwdReset_to_ElderPwdResetDone)
                    LinkElderErrorCode.INVALID_VERIFICATION_CODE ->
                        binding.tilVCode.error = getString(it.nameResource)
                    LinkElderErrorCode.INVALID_PASSWORD ->
                        binding.tilPassword.error = getString(it.nameResource)
                    LinkElderErrorCode.PASSWORD_NOT_MATCH ->
                        binding.tilRePassword.error = getString(it.nameResource)
                    else -> ViewUtil.alertView(requireContext(), it.nameResource)
                }
            }
        }

        return binding.root
    }

    fun onCloseReset() {
        viewModel.vCode = ""
        viewModel.newPassword = ""
        viewModel.reNewPassword = ""
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        Navigation.findNavController(binding.root).navigate(R.id.action_ElderPwdReset_to_ElderPwdForgot)
    }

    fun requestResetPassword() {
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        ViewUtil.disableInteraction(requireActivity())
        canBack = false
        viewModel.requestResetPassword()
    }
}