package com.jarvish.android.jarvishpremium.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.databinding.FragmentElderPwdResetDoneBinding


class FragmentElderPwdResetDone: Fragment() {

    private lateinit var binding: FragmentElderPwdResetDoneBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_elder_pwd_reset_done, container, false)
        binding.fragment = this

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            onCloseResetDone()
        }

        return binding.root
    }

    fun onCloseResetDone() {
        Navigation.findNavController(binding.root).navigate(R.id.action_ElderPwdResetDone_to_ElderLink)
    }
}