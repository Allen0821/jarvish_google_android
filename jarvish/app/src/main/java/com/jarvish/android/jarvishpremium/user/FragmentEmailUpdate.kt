package com.jarvish.android.jarvishpremium.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.ViewUtil
import com.jarvish.android.jarvishpremium.databinding.FragmentAccountEmailUpdateBinding
import com.jarvish.android.jarvishpremium.user.model.UserManagerErrorCode
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModel

class FragmentEmailUpdate: Fragment() {
    private lateinit var binding: FragmentAccountEmailUpdateBinding
    private val viewModel: AccountViewModel by activityViewModels()

    private var canBack = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_account_email_update, container, false)
        binding.fragment = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (canBack) {
                onCloseUpdateEmail()
            }
        }

        viewModel.requestUpdateEmailResult.observe(viewLifecycleOwner) { event ->
            ViewUtil.enableInteraction(requireActivity())
            canBack = true
            event.getContentIfNotHandled()?.let {
                showHint(it.nameResource)
                if (it == UserManagerErrorCode.CHANGE_EMAIL_OK) {
                    onCloseUpdateEmail()
                }
            }
        }

        return binding.root
    }

    fun onCloseUpdateEmail() {
        viewModel.updateEmail.value = ""
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        Navigation.findNavController(binding.root).navigate(R.id.action_close_email_update)
    }

    fun onUpdateEmail() {
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        ViewUtil.disableInteraction(requireActivity())
        canBack = false
        viewModel.updateEmailAddress()
    }

    private fun showHint(@StringRes hint: Int) {
        Snackbar.make(binding.root, hint, Snackbar.LENGTH_SHORT)
                .show()
    }
}