package com.jarvish.android.jarvishpremium.user

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.databinding.FragmentAccountEmailVerificationBinding
import com.jarvish.android.jarvishpremium.user.model.UserManagerErrorCode
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModel

class FragmentEmailVerification: Fragment() {
    private lateinit var binding: FragmentAccountEmailVerificationBinding
    private val viewModel: AccountViewModel by activityViewModels()

    private var canBack = true


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_account_email_verification, container, false)
        binding.fragment = this
        binding.viewModel = viewModel

        viewModel.requestSendVerificationResult.observe(viewLifecycleOwner) { event ->
            canBack = true
            event.getContentIfNotHandled()?.let {
                if (it == UserManagerErrorCode.VERIFY_EMAIL_OK) {
                    Navigation.findNavController(binding.root).navigate(
                            R.id.action_EmailVerification_to_EmailVerificationSent)
                } else {
                    Snackbar.make(binding.root, it.nameResource, Snackbar.LENGTH_SHORT)
                            .show()
                }
            }
        }

        return binding.root
    }

    fun onCloseEmailVerification() {
        Navigation.findNavController(binding.root).navigate(R.id.action_EmailVerification_to_AccountInfo)
    }

    fun onSendEmail() {
        canBack = false
        viewModel.verifyEmail()
    }
}