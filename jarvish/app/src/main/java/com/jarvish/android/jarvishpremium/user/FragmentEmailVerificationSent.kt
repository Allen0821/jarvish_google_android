package com.jarvish.android.jarvishpremium.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.databinding.FragmentAccountEmailVerificationSentBinding
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModel

class FragmentEmailVerificationSent: Fragment() {

    private lateinit var binding: FragmentAccountEmailVerificationSentBinding
    private val viewModel: AccountViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_account_email_verification_sent, container, false)
        binding.fragment = this
        binding.viewModel = viewModel

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            onCloseEmailVerificationSent()
        }

        return binding.root
    }

    fun onCloseEmailVerificationSent() {
        Firebase.auth.currentUser?.reload()
        Navigation.findNavController(binding.root).navigate(
                R.id.action_EmailVerificationSent_to_AccountInfo)

    }
}