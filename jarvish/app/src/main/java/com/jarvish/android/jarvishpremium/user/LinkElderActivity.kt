package com.jarvish.android.jarvishpremium.user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.databinding.ActivityLinkElderBinding
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModel
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModelFactory
import com.jarvish.android.jarvishpremium.user.viewModel.LinkElderViewModel
import com.jarvish.android.jarvishpremium.user.viewModel.LinkElderViewModelFactory

class LinkElderActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLinkElderBinding
    private lateinit var viewModel: LinkElderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_link_elder)
        val uid = intent.getStringExtra(getString(R.string.key_uid))
        viewModel = ViewModelProvider(this, LinkElderViewModelFactory(uid!!))
                .get(LinkElderViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

    }
}