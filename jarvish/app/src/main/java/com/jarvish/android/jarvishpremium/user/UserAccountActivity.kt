package com.jarvish.android.jarvishpremium.user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.databinding.ActivityUserAccountBinding
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModel
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModelFactory

class UserAccountActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUserAccountBinding
    private lateinit var viewModel: AccountViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_account)
        Firebase.auth.currentUser?.reload()
        val user = Firebase.auth.currentUser
        Log.d("[Account]", "account id: $user")
        viewModel = ViewModelProvider(this, AccountViewModelFactory(user!!))
                .get(AccountViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this



    }


}