package com.jarvish.android.jarvishpremium.user



import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.firestore.Profile
import com.jarvish.android.jarvishpremium.firestore.Register
import com.jarvish.android.jarvishpremium.user.model.UserInfo
import java.util.*

@BindingAdapter("setItemTitle")
fun TextView.setItemTitle(item: UserInfo) {
    text = context.getString(item.titleResource)
}

@BindingAdapter("setItemContent")
fun TextView.setItemContent(item: UserInfo) {
    text = when (item.titleResource) {
        R.string.user_elder -> {
            if (item.content.isEmpty()) {
                context.getString(R.string.user_not_elder_content)
            } else {
                item.content
            }
        }
        else -> item.content
    }

    val selectable = when (item.titleResource) {
        R.string.user_id -> true
        else -> false
    }
    Log.d("XXX", "titleResource: ${item.titleResource}, selectable: $selectable")
    setTextIsSelectable(selectable)
}

@BindingAdapter("setItemAction")
fun ImageView.setItemAction(type: Int) {
    visibility = when (type) {
        0 -> View.GONE
        else -> View.VISIBLE
    }
}


@BindingAdapter("setUserAvatar")
fun ImageView.setUserAvatar(avatarUri: Uri?) {
    //val uri: Uri? = Uri.parse(avatarUri)
    Log.d("xxx", "uri: $avatarUri")
    Glide.with(this)
            .load(avatarUri)
            .placeholder(R.drawable.ic_user)
            .error(R.drawable.ic_user)
            .into(this)
}

@BindingAdapter("setUserName")
fun EditText.setUserName(profile: Profile?) {
    profile?.let {
        setText(it.name) }
}

@BindingAdapter("sendVerifyEmailDescription")
fun TextView.sendVerifyEmailDescription(email: String) {
    text = context.getString(R.string.user_email_verification_description, email)
}

@BindingAdapter("sentVerifyEmailDescription")
fun TextView.sentVerifyEmailDescription(email: String) {
    text = context.getString(R.string.user_email_verification_sent_description, email)
}

@BindingAdapter("setSerialNumber")
fun TextView.setSerialNumber(serial: String) {
    text = context.getString(R.string.product_registered_serial_number, serial)
}

@BindingAdapter("setRegisterDate")
fun TextView.setSerialNumber(date: Date?) {
    val stringDate = Register.registeredDate(date)
    text = context.getString(R.string.product_registered_date, stringDate)
}