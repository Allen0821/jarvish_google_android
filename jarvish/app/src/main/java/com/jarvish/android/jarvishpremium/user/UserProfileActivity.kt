package com.jarvish.android.jarvishpremium.user

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.StringRes
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.jarvish.android.jarvishpremium.FileUtil
import com.jarvish.android.jarvishpremium.Permission
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.ViewUtil
import com.jarvish.android.jarvishpremium.databinding.ActivityUserProfileBinding
import com.jarvish.android.jarvishpremium.firestore.Profile
import com.jarvish.android.jarvishpremium.user.model.UserManagerErrorCode
import com.jarvish.android.jarvishpremium.user.viewModel.ProfileViewModel
import com.yalantis.ucrop.UCrop
import java.io.File
import java.util.*

private const val TAG = "Profile"

class UserProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUserProfileBinding
    private lateinit var viewModel: ProfileViewModel

    private var canBack = true
    private lateinit var imagePath: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_profile)
        binding.activity = this

        val profile = intent.getSerializableExtra("EXTRA_PROFILE") as? Profile
        if (profile != null) {
            Log.d(TAG, "profile: " + profile.name)
            viewModel = ProfileViewModel(profile)
            binding.viewModel = viewModel
        }

        binding.lifecycleOwner = this

        viewModel.saveResult.observe(this) { event ->
            ViewUtil.enableInteraction(this)
            canBack = true
            event.getContentIfNotHandled()?.let {
                showHint(it.nameResource)
                if (it == UserManagerErrorCode.UPDATE_PROFILE_OK) {
                    finish()
                }
            }
        }
    }

    override fun onBackPressed() {
        if (canBack) {
            onDiscardAlert()
        }
    }

    fun onCloseUserProfile() {
        onDiscardAlert()
    }

    private fun onDiscardAlert() {
        MaterialAlertDialogBuilder(this, R.style.AppAlertDialog)
                .setTitle(R.string.user_discard_edit_content)
                .setPositiveButton(R.string.user_discard) { _, _ ->
                    finish()
                }
                .setNegativeButton(R.string.action_cancel, null)
                .show()
    }

    private val pickPhoto = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == RESULT_OK) {
            Log.d("[profile]", "${it.data?.data}")
            it.data?.data?.let { uri ->
                onOpenUCrop(uri)
            }
        }
    }

    private val takePhoto = registerForActivityResult(ActivityResultContracts.TakePicture()) {
        if (it) {
            Log.i("[profile]", "Got image at: $imagePath")
            onOpenUCrop(imagePath)
        }
    }

    private fun onOpenUCrop(uri: Uri) {
        val options = UCrop.Options()
        options.setHideBottomControls(true)
        options.setCircleDimmedLayer(true)
        options.setShowCropGrid(false)
        options.setStatusBarColor(getColor(R.color.black))
        options.setToolbarColor(getColor(R.color.black))
        options.setToolbarWidgetColor(getColor(R.color.white))
        options.setToolbarTitle(getString(R.string.user_photo_edit))
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG)
        options.setCompressionQuality(80)
        UCrop.of(uri, Uri.fromFile(File(cacheDir, "${System.currentTimeMillis()}.jpg")))
                .withAspectRatio(1F, 1F)
                .withOptions(options)
                .start(this)
    }


    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            data?.let {
                viewModel.avatarUrl.value = UCrop.getOutput(it)
                viewModel.isAvatarUrlChanged = true
            }
        }
    }

    private val requestCameraPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { resultsMap ->
                resultsMap.forEach {
                    Log.i(TAG, "Permission: ${it.key}, granted: ${it.value}")
                    when (it.key) {
                        Manifest.permission.CAMERA -> {
                            if (!it.value) {
                                showHint(R.string.permission_denied_camera)
                                return@registerForActivityResult
                            }
                        }
                        Manifest.permission.WRITE_EXTERNAL_STORAGE -> {
                            if (!it.value) {
                                showHint(R.string.permission_denied_files_media)
                                return@registerForActivityResult
                            }
                        }
                    }
                }
                onTakePhoto()
    }

    private fun onPickPhoto() {
        val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        pickPhoto.launch(gallery)
    }


    private fun onTakePhoto() {
        if (Permission.hasCameraPermission(this)) {
            val file = FileUtil().createImageFile(this)
            imagePath = FileProvider.getUriForFile(
                    applicationContext,
                    "com.jarvish.android.jarvishpremium.fileprovider",
                    file)
            takePhoto.launch(imagePath)
        } else {
            requestCameraPermissionLauncher.launch(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA))
        }
    }

    fun onEditAvatar() {
        MaterialAlertDialogBuilder(this, R.style.AppAlertDialog)
                .setTitle(R.string.user_photo_edit)
                .setItems(R.array.user_add_photo) { _, witch ->
                    when(witch) {
                        0 -> onPickPhoto()
                        else -> onTakePhoto()
                    }
                }
                .show()
    }

    fun onSaveProfile() {
        ViewUtil.hideKeyboard(this, binding.root)
        ViewUtil.disableInteraction(this)
        canBack = false
        viewModel.onSaveChanged()
    }

    private fun showHint(@StringRes hint: Int) {
        Snackbar.make(binding.root, hint, Snackbar.LENGTH_SHORT)
                .show()
    }
}