package com.jarvish.android.jarvishpremium.user.helmetregistration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.google.zxing.integration.android.IntentIntegrator
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.ScanerActivity
import com.jarvish.android.jarvishpremium.ViewUtil
import com.jarvish.android.jarvishpremium.databinding.FragmentHelmetRegisterNewBinding
import com.jarvish.android.jarvishpremium.firestore.RegisterCode

private const val TAG = "Register"

class FragmentHelmetRegisterNew: Fragment() {
    private lateinit var binding: FragmentHelmetRegisterNewBinding
    private val viewModel: HelmetRegistrationViewModel by activityViewModels()

    private var canBack = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_helmet_register_new, container, false)
        binding.fragment = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = this


        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (canBack) {
                onCloseRegisterNew()
            }
        }

        viewModel.requestSerialNumberResult.observe(viewLifecycleOwner) { event ->
            ViewUtil.enableInteraction(requireActivity())
            canBack = true
            event.getContentIfNotHandled()?.let {
                when (it) {
                    RegisterCode.NONE -> ViewUtil.snackbarView(binding.root, requireContext(), it.nameResource)
                    else -> ViewUtil.alertView(requireContext(), it.nameResource)
                }
            }
        }

        return binding.root
    }

    fun onCloseRegisterNew() {
        Navigation.findNavController(binding.root)
                .navigate(R.id.action_fragmentHelmetRegisterNew_to_fragmentHelmetRegistration)
    }

    fun onScan() {
        val integrator = IntentIntegrator(requireActivity())
        integrator.captureActivity = ScanerActivity::class.java
        integrator.setOrientationLocked(false)
        integrator.setBeepEnabled(false)
        integrator.initiateScan()
    }

    fun onRegister() {
        ViewUtil.hideKeyboard(requireContext(), binding.root)
        ViewUtil.disableInteraction(requireActivity())
        canBack = false
        viewModel.onRegister()
    }
}