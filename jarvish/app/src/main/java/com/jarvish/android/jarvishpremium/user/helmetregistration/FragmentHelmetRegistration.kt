package com.jarvish.android.jarvishpremium.user.helmetregistration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.databinding.FragmentHelmetRegistrationBinding
import com.jarvish.android.jarvishpremium.user.viewModel.LinkElderViewModel

class FragmentHelmetRegistration: Fragment() {

    private lateinit var binding: FragmentHelmetRegistrationBinding
    private val viewModel: HelmetRegistrationViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_helmet_registration, container, false)
        binding.fragment = this
        binding.viewModel = viewModel
        return binding.root
    }

    fun onCloseHelmetRegistration() {
        requireActivity().finish()
    }

    fun onRegisterNewHelmet() {
        Navigation.findNavController(binding.root)
                .navigate(R.id.action_fragmentHelmetRegistration_to_fragmentHelmetRegisterNew)
    }

    fun onViewRegisteredHelmet() {
        Navigation.findNavController(binding.root)
                .navigate(R.id.action_fragmentHelmetRegistration_to_fragmentHelmetViewRegistered)
    }
}