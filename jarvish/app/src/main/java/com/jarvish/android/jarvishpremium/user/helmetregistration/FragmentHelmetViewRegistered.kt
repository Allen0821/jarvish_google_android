package com.jarvish.android.jarvishpremium.user.helmetregistration

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.adapter.RegisterAdapter
import com.jarvish.android.jarvishpremium.databinding.FragmentHelmetViewRegisteredBinding

class FragmentHelmetViewRegistered: Fragment() {
    private lateinit var binding: FragmentHelmetViewRegisteredBinding
    private val viewModel: HelmetRegistrationViewModel by activityViewModels()

    private lateinit var adapter: RegisterAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_helmet_view_registered, container, false)
        binding.fragment = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.queryRegisters()

        adapter = RegisterAdapter()
        binding.rvRegisters.adapter = adapter
        viewModel.registereds.observe(viewLifecycleOwner) {
            Log.d("StoreRegister", "get: " + it.size)
            adapter.submitList(it)
        }

        return binding.root
    }

    fun onCloseViewRegistered() {
        Navigation.findNavController(binding.root)
                .navigate(R.id.action_fragmentHelmetViewRegistered_to_fragmentHelmetRegistration)
    }
}