package com.jarvish.android.jarvishpremium.user.helmetregistration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.zxing.integration.android.IntentIntegrator
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.databinding.ActivityHelmetRegistrationBinding
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModel
import com.jarvish.android.jarvishpremium.user.viewModel.AccountViewModelFactory

private const val TAG = "Register"

class HelmetRegistrationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHelmetRegistrationBinding
    private lateinit var viewModel: HelmetRegistrationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_helmet_registration)
        val user = Firebase.auth.currentUser
        viewModel = ViewModelProvider(this, HelmetRegistrationViewModelFactory(user!!.uid))
                .get(HelmetRegistrationViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Log.e(TAG, "no result")
            } else {
                Log.d(TAG, "result: " + result.contents)
                viewModel.serialNumber.value = result.contents
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}