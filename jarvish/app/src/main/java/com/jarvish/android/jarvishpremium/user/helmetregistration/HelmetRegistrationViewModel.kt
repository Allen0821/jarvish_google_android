package com.jarvish.android.jarvishpremium.user.helmetregistration

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jarvish.android.jarvishpremium.firestore.Account
import com.jarvish.android.jarvishpremium.firestore.Register
import com.jarvish.android.jarvishpremium.firestore.RegisterCode
import com.jarvish.android.jarvishpremium.firestore.StoreRegister
import com.jarvish.android.jarvishpremium.login.Event

class HelmetRegistrationViewModel(val uid: String): ViewModel() {
    val isProgress = MutableLiveData<Boolean>().apply { value = false }
    var serialNumber = MutableLiveData<String>()

    private val _requestSerialNumberResult = MutableLiveData<Event<RegisterCode>>()
    val requestSerialNumberResult: LiveData<Event<RegisterCode>>
        get() = _requestSerialNumberResult

    private val storeRegister = StoreRegister()

    val registereds: MutableLiveData<List<Register>> = MutableLiveData()

    fun onRegister() {
        if (Register.isValidSerial(serialNumber.value)) {
            checkSerialNumberRegistered()
        } else {
            _requestSerialNumberResult.value = Event(RegisterCode.INVALID_SERIAL)
        }
    }

    private fun checkSerialNumberRegistered() {
        storeRegister.getDocument(serialNumber.value!!) {
            if (it != null) {
                _requestSerialNumberResult.value = Event(RegisterCode.SERIAL_BEEN_REGISTERED)
            } else {
                registerSerialNumber()
            }
        }
    }

    private fun registerSerialNumber() {
        isProgress.value = true
        val register = Register(
                serial = serialNumber.value!!,
                uid = uid,
                name = "",
                category = 1,
                expired = false)
        storeRegister.createDocumentWithID(serialNumber.value!!, register) {
            isProgress.value = false
            if (it) {
                _requestSerialNumberResult.value = Event(RegisterCode.NONE)
            } else {
                _requestSerialNumberResult.value = Event(RegisterCode.REGISTERED_FAILURE)
            }
        }
    }

    fun queryRegisters() {
        storeRegister.queryRegistered(uid) { registers ->
            registereds.value = registers
        }
    }
}