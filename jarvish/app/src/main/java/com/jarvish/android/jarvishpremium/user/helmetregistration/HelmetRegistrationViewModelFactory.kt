package com.jarvish.android.jarvishpremium.user.helmetregistration

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class HelmetRegistrationViewModelFactory(private val uid: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HelmetRegistrationViewModel::class.java)) {
            return HelmetRegistrationViewModel(uid) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}