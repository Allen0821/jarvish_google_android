package com.jarvish.android.jarvishpremium.user.model

import android.util.Patterns
import com.facebook.AccessToken
import com.jarvish.android.jarvishpremium.model.JarvishLogin
import com.jarvish.android.jarvishpremium.model.JarvishResetPassword
import com.jarvish.android.jarvishpremium.model.JarvishSession
import com.jarvish.android.jarvishpremium.network.JarvishApi

class LinkElder {

    fun linkFromEmailPassword(email: String, password: String, completed: (memberId: String?) -> Unit) {
        val jarvishLogin = JarvishLogin()
        jarvishLogin.email = email
        jarvishLogin.password = password
        JarvishApi.getInstance().jarvishLogin(jarvishLogin, object: JarvishApi.JarvishApiHandler.loginHandler {
            override fun onSuccess(session: JarvishSession?) {
                if (session != null) {
                    completed(session.memberId)
                } else {
                    completed(null)
                }
            }

            override fun onError(error: Int) {
                completed(null)
            }

            override fun onFailure(msg: String?) {
                completed(null)
            }
        })
    }

    fun linkFromFacebookLogin(token: AccessToken, completed: (memberId: String?) -> Unit) {
        JarvishApi.getInstance().jarvishFacebookLogin(token.token, object : JarvishApi.JarvishApiHandler.loginHandler {
            override fun onSuccess(session: JarvishSession?) {
                if (session != null) {
                    completed(session.memberId)
                } else {
                    completed(null)
                }
            }

            override fun onError(error: Int) {
                completed(null)
            }

            override fun onFailure(msg: String?) {
                completed(null)
            }

        })
    }

    fun requestVerificationCode(email: String, completed: (result: LinkElderErrorCode) -> Unit) {
        JarvishApi.getInstance().jarvishRequestResetPassword(email,
                object: JarvishApi.JarvishApiHandler.resetPasswordHandler {
            override fun onSuccess() {
                completed(LinkElderErrorCode.NONE)
            }

            override fun onError(error: Int) {
                completed(LinkElderErrorCode.SEND_VERIFICATION_CODE_FAILURE)
            }

            override fun onFailure(msg: String?) {
                completed(LinkElderErrorCode.SEND_VERIFICATION_CODE_FAILURE)
            }
        })
    }

    fun requestResetPassword(
            verifyCode: String,
            email: String,
            password: String,
            completed: (result: LinkElderErrorCode) -> Unit) {

        val resetPassword = JarvishResetPassword()
        resetPassword.verifyCode = verifyCode
        resetPassword.email = email
        resetPassword.password = password
        JarvishApi.getInstance().jarvishResetPassword(resetPassword, object: JarvishApi.JarvishApiHandler.resetPasswordHandler {
            override fun onSuccess() {
                completed(LinkElderErrorCode.NONE)
            }

            override fun onError(error: Int) {
                completed(LinkElderErrorCode.RESET_PASSWORD_FAILURE)
            }

            override fun onFailure(msg: String?) {
                completed(LinkElderErrorCode.RESET_PASSWORD_FAILURE)
            }

        })
    }

    companion object {
        fun isEmailAddressValid(email: String): Boolean
            = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    }
}