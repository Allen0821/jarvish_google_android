package com.jarvish.android.jarvishpremium.user.model

import androidx.annotation.StringRes
import com.jarvish.android.jarvishpremium.R

enum class LinkElderErrorCode(@StringRes val nameResource: Int) {
    NONE(R.string.error_none),
    INVALID_EMAIL_ADDRESS(R.string.error_invalid_email_address),
    INVALID_PASSWORD(R.string.code_elder_invalid_password),
    PASSWORD_NOT_MATCH(R.string.code_elder_password_not_match),
    SEND_VERIFICATION_CODE_FAILURE(R.string.code_elder_send_verification_code_failure),
    INVALID_VERIFICATION_CODE(R.string.code_elder_invalid_verification_code),
    RESET_PASSWORD_FAILURE(R.string.code_elder_reset_password_failed),
    LINK_FAILURE(R.string.code_elder_link_elder_failure),
    SIGN_IN_FAILURE(R.string.code_elder_link_elder_sign_in_failure);
}