package com.jarvish.android.jarvishpremium.user.model

import androidx.annotation.StringRes

data class UserInfo(@StringRes val titleResource: Int, val content: String, val type: Int = 0) {

}
