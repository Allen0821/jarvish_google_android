package com.jarvish.android.jarvishpremium.user.model

import android.net.Uri
import android.util.Log
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase

private const val TAG = "UserManager"

class UserManager(private val user: FirebaseUser) {

    fun updateUserProfile (name: String?, photo: Uri?, completed: (result: Boolean) -> Unit) {
        val profileUpdates = userProfileChangeRequest {
            displayName = name
            photoUri = photo
        }

        user.updateProfile(profileUpdates)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "User profile updated.")
                        completed(true)
                    } else {
                        Log.e(TAG, "update user profile error: ${task.exception}")
                        completed(false)
                    }
                }
    }

    fun changeEmailAddress(email: String, completed: (errorCode: UserManagerErrorCode) -> Unit) {
        Log.d(TAG, "changeEmailAddress: $email")
        user.updateEmail(email)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "User email address updated.")
                        completed(UserManagerErrorCode.CHANGE_EMAIL_OK)
                    } else {
                        Log.e(TAG, "change email address error: ${task.exception}")
                        when (task.exception) {
                            is FirebaseAuthRecentLoginRequiredException ->
                                completed(UserManagerErrorCode.NEED_REAUTHENTICATION)
                            is FirebaseAuthUserCollisionException ->
                                completed(UserManagerErrorCode.EMAIL_ALREADY_IN_USE)
                            else ->
                                completed(UserManagerErrorCode.CHANGE_EMAIL_FAILURE)
                        }

                    }
                }
    }


    fun verifyEmailAddress(completed: (errorCode: UserManagerErrorCode) -> Unit) {
        Firebase.auth.useAppLanguage()
        user.sendEmailVerification()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "Email sent.")
                        completed(UserManagerErrorCode.VERIFY_EMAIL_OK)
                    } else {
                        completed(UserManagerErrorCode.VERIFY_EMAIL_FAILURE)
                    }
                }
    }
}