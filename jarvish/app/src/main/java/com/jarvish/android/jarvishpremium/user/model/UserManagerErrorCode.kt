package com.jarvish.android.jarvishpremium.user.model

import androidx.annotation.StringRes
import com.jarvish.android.jarvishpremium.R

enum class UserManagerErrorCode(@StringRes val nameResource: Int) {
    CHANGE_EMAIL_OK(R.string.code_change_email_ok),
    VERIFY_EMAIL_OK(R.string.error_verify_email_ok),
    VERIFY_EMAIL_FAILURE(R.string.error_verify_email_failure),
    UPDATE_PROFILE_OK(R.string.error_user_profile_update_ok),
    UPDATE_PROFILE_FAILURE(R.string.error_user_profile_update_failure),
    EMAIL_ALREADY_IN_USE(R.string.error_change_email_user_in_use),
    NEED_REAUTHENTICATION(R.string.error_change_email_reauth),
    CHANGE_EMAIL_FAILURE(R.string.error_change_email_failure);
}