package com.jarvish.android.jarvishpremium.user.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseUser
import com.jarvish.android.jarvishpremium.MainContext
import com.jarvish.android.jarvishpremium.R
import com.jarvish.android.jarvishpremium.firestore.Account
import com.jarvish.android.jarvishpremium.firestore.StoreAccount
import com.jarvish.android.jarvishpremium.login.Event
import com.jarvish.android.jarvishpremium.user.model.UserInfo
import com.jarvish.android.jarvishpremium.user.model.UserManager
import com.jarvish.android.jarvishpremium.user.model.UserManagerErrorCode

private const val TAG = "Account"

class AccountViewModel(val user: FirebaseUser): ViewModel() {

    val userManager = UserManager(user)
    val storeAccount = StoreAccount()

    val account = MutableLiveData<Account>()

    private val _requestUpdateEmailResult = MutableLiveData<Event<UserManagerErrorCode>>()
    val requestUpdateEmailResult: LiveData<Event<UserManagerErrorCode>>
        get() = _requestUpdateEmailResult

    private val _requestSendVerificationResult = MutableLiveData<Event<UserManagerErrorCode>>()
    val requestSendVerificationResult: LiveData<Event<UserManagerErrorCode>>
        get() = _requestSendVerificationResult

    val isProgress = MutableLiveData<Boolean>().apply { value = false }

    var updateEmail = MutableLiveData<String>().apply { value = "" }

    fun getUserAccount() {
        isProgress.value = true
        storeAccount.getDocument(user.uid) {
            it?.let {
                Log.d(TAG, "user account: $it")
                isProgress.value = false
                account.value = it
                if (user.isEmailVerified && !account.value?.emailVerified!!) {
                    updateAccountEmailVerification()
                }
            }
        }
    }

    fun getUserInfoList(account: Account): ArrayList<UserInfo> {
        val infos = ArrayList<UserInfo>()
        infos.add(UserInfo(R.string.user_id, account.uid, 0))

        /*var emailType = 0
        for (profile in user.providerData) {
            val providerId = profile.providerId
            Log.d("Auth", "User is signed in providerId: $providerId")
            if (providerId == "facebook.com") {
                emailType = 1
            }
        }*/

        val email = when {
            account.email.isNullOrEmpty() -> ""
            else -> account.email
        }
        infos.add(UserInfo(R.string.user_email_address, email!!, 1))


        if (user.isEmailVerified) {
            infos.add(UserInfo(
                    R.string.user_email_verification,
                    MainContext.INSTANCE.context.getString(R.string.user_email_verification_true),
                    0))
        } else {
            infos.add(UserInfo(
                    R.string.user_email_verification,
                    MainContext.INSTANCE.context.getString(R.string.user_email_verification_false),
                    1))
        }



        var elderId = ""
        var elderType = 1
        if (account.elder) {
            elderId = account.elderId.toString()
            elderType = 0
        } else {
            elderId = ""
            elderType = 1
        }
        infos.add(UserInfo(R.string.user_elder, elderId, elderType))

        var created = ""
        account.created?.let { created = Account.createdFormat(it) }
        infos.add(UserInfo(R.string.user_created_date, created, 0))

        Log.d(TAG, "user info list: $infos")
        return infos
    }


    fun updateEmailAddress() {
        updateEmail.value?.let { email ->
            isProgress.value = true
            userManager.changeEmailAddress(email) { errorCode ->
                isProgress.value = false
                if (errorCode == UserManagerErrorCode.CHANGE_EMAIL_OK) {
                    changeEmail(email) {
                        if (it) {
                            account.value?.email = email
                            _requestUpdateEmailResult.value = Event(UserManagerErrorCode.CHANGE_EMAIL_OK)
                        } else {
                            _requestUpdateEmailResult.value = Event(UserManagerErrorCode.CHANGE_EMAIL_FAILURE)
                        }
                    }
                } else {
                    _requestUpdateEmailResult.value = Event(errorCode)
                }
            }
        }
    }

    private fun changeEmail(email: String, completed: (result: Boolean) -> Unit) {
        val hashMap: HashMap<String, Any?> = HashMap()
        hashMap["email"] = email
        hashMap["emailVerified"] = false
        storeAccount.updateDocument(user.uid, hashMap) {
            completed(it)
        }
    }

    fun verifyEmail() {
        isProgress.value = true
        userManager.verifyEmailAddress {
            isProgress.value = false
            _requestSendVerificationResult.value = Event(it)
        }
    }

    private fun updateAccountEmailVerification() {
        val hashMap: HashMap<String, Any?> = HashMap()
        hashMap["emailVerified"] = true
        storeAccount.updateDocument(user.uid, hashMap) {
            Log.d(TAG, "emailValid updated.")
        }
    }
}