package com.jarvish.android.jarvishpremium.user.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseUser

class AccountViewModelFactory(private val user: FirebaseUser) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AccountViewModel::class.java)) {
            return AccountViewModel(user) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}