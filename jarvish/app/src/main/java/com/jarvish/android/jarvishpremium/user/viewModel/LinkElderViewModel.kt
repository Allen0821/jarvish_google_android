package com.jarvish.android.jarvishpremium.user.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.facebook.AccessToken
import com.jarvish.android.jarvishpremium.firestore.StoreAccount
import com.jarvish.android.jarvishpremium.login.Event
import com.jarvish.android.jarvishpremium.user.model.LinkElder
import com.jarvish.android.jarvishpremium.user.model.LinkElderErrorCode

class LinkElderViewModel(private val uid: String): ViewModel() {

    var isProgress = MutableLiveData<Boolean>()

    private val _linkFromEmailResult = MutableLiveData<Event<LinkElderErrorCode>>()
    val linkFromEmailResult: LiveData<Event<LinkElderErrorCode>>
        get() = _linkFromEmailResult

    private val _linkFromFacebookResult = MutableLiveData<Event<LinkElderErrorCode>>()
    val linkFromFacebookResult: LiveData<Event<LinkElderErrorCode>>
        get() = _linkFromFacebookResult

    private val _requestVerificationCodeResult = MutableLiveData<Event<LinkElderErrorCode>>()
    val requestVerificationCodeResult: LiveData<Event<LinkElderErrorCode>>
        get() = _requestVerificationCodeResult

    private val _requestResetPasswordResult = MutableLiveData<Event<LinkElderErrorCode>>()
    val requestResetPasswordResult: LiveData<Event<LinkElderErrorCode>>
        get() = _requestResetPasswordResult

    var email = ""

    var password = ""

    var vCodeEmail: String = ""

    var vCode: String = ""

    var newPassword: String = ""

    var reNewPassword: String = ""

    var elderID = ""

    fun linkFromEmail() {

        if (!LinkElder.isEmailAddressValid(email)) {
            _linkFromEmailResult.value = Event(LinkElderErrorCode.INVALID_EMAIL_ADDRESS)
            return
        }

        if (password.isEmpty()) {
            _linkFromEmailResult.value = Event(LinkElderErrorCode.INVALID_PASSWORD)
            return
        }

        isProgress.value = true
        val linkElder = LinkElder()
        linkElder.linkFromEmailPassword(email, password) { id ->
            if (id != null) {
                updateElder(id) {
                    isProgress.value = false
                    if (it) {
                        elderID = id
                        _linkFromEmailResult.value = Event(LinkElderErrorCode.NONE)
                    } else {
                        _linkFromEmailResult.value = Event(LinkElderErrorCode.LINK_FAILURE)
                    }
                }
            } else {
                isProgress.value = false
                _linkFromEmailResult.value = Event(LinkElderErrorCode.SIGN_IN_FAILURE)
            }
        }
    }

    fun linkFromFacebook(token: AccessToken) {
        isProgress.value = true
        val linkElder = LinkElder()
        linkElder.linkFromFacebookLogin(token) { id ->
            if (id != null) {
                updateElder(id) {
                    isProgress.value = false
                    if (it) {
                        elderID = id
                        _linkFromEmailResult.value = Event(LinkElderErrorCode.NONE)
                    } else {
                        _linkFromEmailResult.value = Event(LinkElderErrorCode.LINK_FAILURE)
                    }
                }
            } else {
                isProgress.value = false
                _linkFromEmailResult.value = Event(LinkElderErrorCode.SIGN_IN_FAILURE)
            }
        }
    }

    private fun updateElder(elderID: String, completed: (result: Boolean) -> Unit) {
        val storeAccount = StoreAccount()
        val hashMap: HashMap<String, Any?> = HashMap()
        hashMap["elder"] = true
        hashMap["elderId"] = elderID
        storeAccount.updateDocument(uid, hashMap) {
            completed(it)
        }
    }

    fun requestVerificationCode() {
        if (!LinkElder.isEmailAddressValid(vCodeEmail)) {
            _requestVerificationCodeResult.value = Event(LinkElderErrorCode.INVALID_EMAIL_ADDRESS)
            return
        }

        isProgress.value = true
        val linkElder = LinkElder()
        linkElder.requestVerificationCode(vCodeEmail) {
            isProgress.value = false
            _requestVerificationCodeResult.value = Event(it)
        }
    }

    fun requestResetPassword() {
        if (vCode.isEmpty()) {
            _requestResetPasswordResult.value = Event(LinkElderErrorCode.INVALID_VERIFICATION_CODE)
            return
        }

        if (newPassword.isEmpty()) {
            _requestResetPasswordResult.value = Event(LinkElderErrorCode.INVALID_PASSWORD)
            return
        }

        if (newPassword != reNewPassword) {
            _requestResetPasswordResult.value = Event(LinkElderErrorCode.PASSWORD_NOT_MATCH)
            return
        }

        isProgress.value = true
        val linkElder = LinkElder()
        linkElder.requestResetPassword(vCode, vCodeEmail, newPassword) {
            isProgress.value = false
            _requestResetPasswordResult.value = Event(it)
        }
    }
}