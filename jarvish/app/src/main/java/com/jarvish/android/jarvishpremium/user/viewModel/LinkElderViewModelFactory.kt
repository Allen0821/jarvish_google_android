package com.jarvish.android.jarvishpremium.user.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class LinkElderViewModelFactory(private val uid: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LinkElderViewModel::class.java)) {
            return LinkElderViewModel(uid) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}