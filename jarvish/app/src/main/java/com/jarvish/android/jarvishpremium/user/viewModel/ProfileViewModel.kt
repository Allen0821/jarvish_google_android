package com.jarvish.android.jarvishpremium.user.viewModel

import android.net.Uri
import android.util.Log
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import com.jarvish.android.jarvishpremium.BR
import com.jarvish.android.jarvishpremium.FireStorage
import com.jarvish.android.jarvishpremium.firestore.Profile
import com.jarvish.android.jarvishpremium.firestore.StoreProfile
import com.jarvish.android.jarvishpremium.login.Event
import com.jarvish.android.jarvishpremium.user.model.LinkElderErrorCode
import com.jarvish.android.jarvishpremium.user.model.UserManager
import com.jarvish.android.jarvishpremium.user.model.UserManagerErrorCode
import retrofit2.http.Url

class ProfileViewModel(val profile: Profile): ViewModel() {

    private val _saveResult = MutableLiveData<Event<UserManagerErrorCode>>()
    val saveResult: LiveData<Event<UserManagerErrorCode>>
        get() = _saveResult

    var name: String = ""
    var isProgress = MutableLiveData<Boolean>()
    var avatarUrl = MutableLiveData<Uri>()
    var isAvatarUrlChanged: Boolean = false

    //var profile =  Profile()

    init {
        if(profile.name != null) {
            name = profile.name!!
        }

        avatarUrl.value = Uri.parse(profile.avatarUrl)
    }

    /*fun getProfile() {
        isProgress.value = true
        val storeProfile = StoreProfile()
        storeProfile.getDocument(user.uid) { result ->
            result?.let { data ->
                isProgress.value = false
                profile = data
                Log.d("[Profile]", "profile: $profile")
            }
        }
    }*/


    fun onSaveChanged() {
        isProgress.value = true
        profile.name = name
        if (isAvatarUrlChanged && avatarUrl.value != null) {
            val fireStorage = FireStorage()
            fireStorage.uploadUserAvatar(profile.uid, avatarUrl.value!!) { downloadUri ->
                if (downloadUri != null) {
                    profile.avatarUrl = downloadUri.toString()
                    updateUser()
                } else {
                    isProgress.value = false
                    _saveResult.value = Event(UserManagerErrorCode.UPDATE_PROFILE_FAILURE)
                }
            }
        } else {
            updateUser()
        }
    }

    private fun updateUser() {
        val userManager = UserManager(Firebase.auth.currentUser!!)
        userManager.updateUserProfile(profile.name, Uri.parse(profile.avatarUrl)) { result ->
            if (result) {
                updateProfile() {
                    isProgress.value = false
                    if (it) {
                        _saveResult.value = Event(UserManagerErrorCode.UPDATE_PROFILE_OK)
                    } else {
                        _saveResult.value = Event(UserManagerErrorCode.UPDATE_PROFILE_FAILURE)
                    }
                }
            } else {
                isProgress.value = false
                _saveResult.value = Event(UserManagerErrorCode.UPDATE_PROFILE_FAILURE)
            }
        }
    }

    private fun updateProfile(completed: (result: Boolean) -> Unit) {
        Log.d("[Profile]", "name: $name")
        val storeProfile = StoreProfile()
        val hashMap: HashMap<String, Any?> = HashMap()
        hashMap["name"] = profile.name
        hashMap["avatarUrl"] = profile.avatarUrl
        storeProfile.updateDocument(profile.uid, hashMap) {
            completed(it)
        }
    }

}