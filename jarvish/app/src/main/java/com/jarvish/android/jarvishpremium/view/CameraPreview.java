package com.jarvish.android.jarvishpremium.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.icatch.wificam.customer.ICatchWificamPreview;
import com.icatch.wificam.customer.ICatchWificamSession;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.icatch.wificam.customer.type.ICatchMJPGStreamParam;
import com.icatch.wificam.customer.type.ICatchPreviewMode;
import com.jarvish.android.jarvishpremium.icatch.ICatch;


import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class CameraPreview extends View implements Runnable {
    private static final String TAG = "CameraPreview";
    private byte[] CameraPixelBuf;
    private ByteBuffer PreViewBmpBuf;
    private int frameWidth = 1000;
    private int frameHeight = 1000;
    private Bitmap videoFrameBitmap;
    private Rect drawFrameRect;
    private ICatchMJPGStreamParam param;
    private ICatchPreviewMode previewMode;
    private ICatchWificamSession mSession;
    private ICatchWificamPreview icatchMedia;

    private ExecutorService executorpool;
    private Future<Object> future;



    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(TAG, "init Camera Preview" + getWidth() + " " + getHeight());
        if (isInEditMode()) { return; }

        if (ICatch.getInstance().prepareSession()) {
            mSession = ICatch.getInstance().getSession();
        } else {
            return;
        }

        try {
            icatchMedia = mSession.getPreviewClient();//session.getMediaClient();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void destroy() {

    }


    public void setScreen(int width, int height) {
        Log.d(TAG, "setScreen Camera Preview" + width + " " + height);
        frameWidth = width;
        frameHeight = height;
        param = new ICatchMJPGStreamParam(frameWidth, frameHeight);
        CameraPixelBuf = new byte[frameWidth * frameHeight * 4];
        PreViewBmpBuf = ByteBuffer.wrap(CameraPixelBuf);
        videoFrameBitmap = Bitmap.createBitmap(frameWidth, frameHeight,	Bitmap.Config.ARGB_8888);
        drawFrameRect = new Rect(0, 0, frameWidth, frameHeight);
        setWillNotDraw(false);
        previewMode = ICatchPreviewMode.ICH_VIDEO_PREVIEW_MODE;
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d(TAG, "onMeasure Camera Preview");
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d(TAG, "onDraw Camera Preview: " + getWidth() + " " + getHeight());
        canvas.drawBitmap(videoFrameBitmap, null, drawFrameRect, null);
    }



    public void start(){
        /* start stream */
        boolean disableAudio = true;
        Log.d(TAG, "Start media stream ");
        try {
            icatchMedia.start(param, previewMode, disableAudio);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //
        if (executorpool == null ) {
            executorpool = Executors.newSingleThreadExecutor();
            future = executorpool.submit(this, null);
        } else if (executorpool.isShutdown()) {
            try {
                future = executorpool.invokeAny(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void Stop() {
        //executor.shutdown();
        Log.d(TAG, "stop media stream ");
        shutdownAndAwaitTermination(executorpool);
        try {
            icatchMedia.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(60, TimeUnit.SECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }



    @Override
    public void run() {
        ICatchFrameBuffer buffer = new ICatchFrameBuffer();
        buffer.setBuffer(CameraPixelBuf);
        /* Data thread */
        while (true) {
            /* Receive data, !!!image format: rgb565!!! */
            //Log.d(TAG, "get next video frame - start ");
            try {
                if (!icatchMedia.getNextVideoFrame(buffer )) {
                    Log.d(TAG, "get next video frame - false");
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            PreViewBmpBuf.rewind();
            videoFrameBitmap.copyPixelsFromBuffer(PreViewBmpBuf);
            postInvalidate();
            Log.d(TAG, "get next video frame ok!");
        }
    }
}
