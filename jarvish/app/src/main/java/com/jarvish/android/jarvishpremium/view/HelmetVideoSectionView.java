package com.jarvish.android.jarvishpremium.view;


import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.VideoHeaderViewBinding;
import com.jarvish.android.jarvishpremium.databinding.VideoItemViewBinding;

import com.jarvish.android.jarvishpremium.helmet.video.HelmetFile;

import java.util.ArrayList;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

public class HelmetVideoSectionView extends StatelessSection {
    //private final Context mContext;
    private final String mTitle;
    private final ArrayList<HelmetFile> mICatchFiles;
    private final OnHelmetVideoSectionViewListener mListener;
    //private final ICatchWificamPlayback mPlayback;


    public HelmetVideoSectionView(String title, ArrayList<HelmetFile> files,
                                  HelmetVideoSectionView.OnHelmetVideoSectionViewListener listener) {
        super(SectionParameters.builder()
                .itemResourceId(R.layout.video_item_view)
                .headerResourceId(R.layout.video_header_view)
                .build());
        //this.mContext = context;
        //this.mPlayback = playback;
        this.mTitle = title;
        this.mICatchFiles = files;
        this.mListener = listener;
    }


    @Override
    public int getContentItemsTotal() {
        return mICatchFiles.size();
    }


    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        VideoItemViewBinding binding = VideoItemViewBinding.bind(view);
        return new ItemViewHolder(binding);
    }


    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.bind(mICatchFiles.get(position), position, mListener);
    }


    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        VideoHeaderViewBinding binding = VideoHeaderViewBinding.bind(view);
        return new HeaderViewHolder(binding);
    }


    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        final HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
        headerViewHolder.bind(mTitle);
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {
        private final VideoItemViewBinding mBinding;


        ItemViewHolder(VideoItemViewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(HelmetFile file, int position, HelmetVideoSectionView.OnHelmetVideoSectionViewListener listener) {

            if (file.getBitmap() != null) {
                mBinding.ivItem.setImageBitmap(file.getBitmap());
                mBinding.ivPlay.setVisibility(View.VISIBLE);
            }

            mBinding.ivPlay.setOnClickListener(v -> listener.onClickItem(mTitle, position, file));
        }

    }


    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final VideoHeaderViewBinding mBinding;

        HeaderViewHolder(VideoHeaderViewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(String title) {
            mBinding.tvHeader.setText(title);
        }
    }




    public interface OnHelmetVideoSectionViewListener {
        void onClickItem(String tag, int position, HelmetFile file);
    }
}
