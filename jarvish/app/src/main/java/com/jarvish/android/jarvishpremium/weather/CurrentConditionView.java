package com.jarvish.android.jarvishpremium.weather;

import android.content.Context;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.WeatherCurrentViewBinding;
import com.jarvish.android.jarvishpremium.weather.core.Weather;
import com.jarvish.android.jarvishpremium.weather.model.Current;


public class CurrentConditionView {

    private final Context mContext;

    CurrentConditionView(Context context) {
        mContext = context;
    }

    void setCurrentConditionView(WeatherCurrentViewBinding binding, Current current) {
        binding.ivIcon.setImageDrawable(WeatherUtil.getWeatherIcon(mContext, current.getWxIcon()));
        String temperature = current.getTemp() + Weather.getInstance().getTemperatureUnits();
        binding.tvTemperature.setText(temperature);
        String feelLike = mContext.getString(R.string.weather_feel_like) + " " + current.getFeelsLike() + "°";
        binding.tvFeelLike.setText(feelLike);
        binding.tvWxPhrase.setText(current.getWxPhrase());
    }

}
