package com.jarvish.android.jarvishpremium.weather;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.databinding.ActivityWeatherBinding;
import com.jarvish.android.jarvishpremium.weather.adapter.HourlyAdapter;
import com.jarvish.android.jarvishpremium.weather.adapter.MoreInfoAdapter;
import com.jarvish.android.jarvishpremium.weather.adapter.WeeklyAdapter;
import com.jarvish.android.jarvishpremium.weather.core.Weather;
import com.jarvish.android.jarvishpremium.weather.model.Hourly;
import com.jarvish.android.jarvishpremium.weather.model.MoreInfo;
import com.jarvish.android.jarvishpremium.weather.model.Current;
import com.jarvish.android.jarvishpremium.weather.model.WPlace;
import com.jarvish.android.jarvishpremium.weather.model.Weekly;

import java.util.ArrayList;


public class WeatherActivity extends AppCompatActivity {
    private static final String TAG = "WeatherActivity";

    private ActivityWeatherBinding mBinding;

    private WeatherPresenter mWeatherPresenter;

    private WeatherActivityViewModel viewModel;

    private CurrentConditionView mCurrentConditionView;
    private Current mCurrent;

    private WeatherSearch mWeatherSearch;

    private ArrayList<Hourly> mHourlies;
    private HourlyAdapter mHourlyAdapter;

    private ArrayList<Weekly> mWeeklies;
    private WeeklyAdapter mWeeklyAdapter;

    private ArrayList<MoreInfo> mMoreInfos;
    private MoreInfoAdapter mMoreInfoAdapter;


    private boolean isHere = true;
    private WPlace mCurrentWPlace;
    private WPlace mWPlace;

    public int height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_weather);
        height = getHeight();
        mBinding.setActivity(this);
        viewModel = new WeatherActivityViewModel();
        mBinding.setViewModel(viewModel);
        mBinding.setLifecycleOwner(this);

        mWeatherPresenter = new WeatherPresenter(WeatherActivity.this, mWeatherContract);

        String observation = getIntent().getStringExtra(getString(R.string.key_current_condition));
        Log.d(TAG, "observation: " + observation);
        mCurrent = new Gson().fromJson(observation, Current.class);
        mBinding.topBar.getBackground().setAlpha(0);

        initView();

        mBinding.ivBack.setColorFilter(ContextCompat.getColor(this, R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN);
        mBinding.ivSearch.setColorFilter(ContextCompat.getColor(this, R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN);
        mBinding.nsContent.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            Log.d(TAG, "nsContent: " + " " + scrollX + " " + scrollY + " " + oldScrollX + " " + oldScrollY);
            double x = 255.0 * scrollY / 100;
            if (x > 255) {
                x = 255;
            }
            if (x == 255) {
                mBinding.tvPlace.setTextColor(getResources().getColor(R.color.colorAccent, null));
                mBinding.ivBack.setColorFilter(getResources().getColor(R.color.colorAccent, null));
                mBinding.ivSearch.setColorFilter(getResources().getColor(R.color.colorAccent, null));
            } else {
                mBinding.tvPlace.setTextColor(getResources().getColor(R.color.white, null));
                mBinding.ivBack.setColorFilter(getResources().getColor(R.color.white, null));
                mBinding.ivSearch.setColorFilter(getResources().getColor(R.color.white, null));
            }
            Log.d(TAG, "Alpha " + (int)x);
            mBinding.topBar.getBackground().setAlpha((int)x);
        });

        mBinding.tvTime.setVisibility(View.INVISIBLE);
        mBinding.rvInfo.setVisibility(View.INVISIBLE);
        mBinding.rvHourly.setVisibility(View.INVISIBLE);
        mBinding.rvWeekly.setVisibility(View.INVISIBLE);

        mBinding.ivSearch.setOnClickListener(v -> onClickSearch());
        mBinding.ivBack.setOnClickListener(v -> onClickBack());
    }

    public int getHeight() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int pixels = height - Util.dpToPx(this, 278);
        Log.d(TAG, "height pixels " + pixels);
        return pixels;
    }

    @BindingAdapter("android:layout_height")
    public static void setWeatherCurrentHeight(View view, int height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        //layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
        //view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height));
    }


    private void initView() {
        mCurrentConditionView = new CurrentConditionView(WeatherActivity.this);
        setCurrentConditionView(mCurrent);


        //initiate detail of current condition view
        mMoreInfos = new ArrayList<>();
        mMoreInfoAdapter = new MoreInfoAdapter(mMoreInfos);
        mBinding.rvInfo.setLayoutManager(new GridLayoutManager(
                WeatherActivity.this,
                5));
        mBinding.rvInfo.setHasFixedSize(true);
        mBinding.rvInfo.setAdapter(mMoreInfoAdapter);
        setMoreInfoView(mCurrent);


        //initiate hourly condition view
        mHourlies = new ArrayList<>();
        mHourlyAdapter = new HourlyAdapter(mHourlies);
        mBinding.rvHourly.setLayoutManager(new LinearLayoutManager(
                WeatherActivity.this,
                LinearLayoutManager.HORIZONTAL,
                false));
        mBinding.rvHourly.setHasFixedSize(true);
        mBinding.rvHourly.setAdapter(mHourlyAdapter);

        //initiate weekly condition view
        mWeeklies = new ArrayList<>();
        mWeeklyAdapter = new WeeklyAdapter(mWeeklies);
        mBinding.rvWeekly.setLayoutManager(new LinearLayoutManager(
                WeatherActivity.this,
                LinearLayoutManager.VERTICAL,
                false));
        mBinding.rvWeekly.setHasFixedSize(true);
        mBinding.rvWeekly.setAdapter(mWeeklyAdapter);

        //initiate search place view
        mWeatherSearch = new WeatherSearch(WeatherActivity.this, mWSViewInterface);
        mWeatherSearch.initSearchPlace(mBinding.vSearch);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isHere) {
            mWeatherPresenter.getWeatherConditions();
        } else {
            mWeatherPresenter.getWeatherConditions(mWPlace.getLat(), mWPlace.getLnt());
        }
        viewModel.setFormatTime();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    private void setCurrentConditionView(Current current) {
        mCurrentConditionView.setCurrentConditionView(mBinding.vCurrent, current);
        mBinding.ivBg.setImageDrawable(WeatherUtil.getWeatherBackgroundDrawable(this,
                current.getDayInd(), current.getWxIcon()));
    }


    private void setMoreInfoView(Current current) {
        if (mMoreInfos == null) {
            mMoreInfos = new ArrayList<>();
        } else {
            mMoreInfos.clear();
        }
        mMoreInfos.add(new MoreInfo(
                getString(R.string.weather_rh_index),
                current.getRh() + "%"));
        mMoreInfos.add(new MoreInfo(
                getString(R.string.weather_vis_index),
                current.getVis() + Weather.getInstance().getDistanceUnits()));
        mMoreInfos.add(new MoreInfo(
                getString(R.string.weather_uv_index),
                mCurrent.getUvDesc()));
        mMoreInfos.add(new MoreInfo(
                getString(R.string.weather_wind_speed),
                current.getWspd() + Weather.getInstance().getSpeedUnits()));
        mMoreInfos.add(new MoreInfo(
                getString(R.string.weather_wind_direction),
                WeatherUtil.getWindDirectionDes(getApplicationContext(), current.getWdirCardinal())));
        mMoreInfoAdapter.notifyDataSetChanged();
    }

    protected void onClickBack() {
        finish();
    }

    protected void onClickSearch() {
        mWeatherSearch.showView(mCurrentWPlace);
        viewModel.isSearchVisible.setValue(true);
    }


    private final WeatherContract mWeatherContract = new WeatherContract() {
        @Override
        public void onCurrentCondition(Current current) {
            setCurrentConditionView(current);
            setMoreInfoView(current);
        }

        @Override
        public void onHourlyCondition(ArrayList<Hourly> hourlies) {
            mHourlies.clear();
            mHourlies.addAll(hourlies);
            mHourlyAdapter.notifyDataSetChanged();
            mBinding.tvTime.setVisibility(View.VISIBLE);
            mBinding.rvInfo.setVisibility(View.VISIBLE);
            mBinding.rvHourly.setVisibility(View.VISIBLE);
        }

        @Override
        public void onWeeklyCondition(ArrayList<Weekly> weeklies) {
            mWeeklies.clear();
            mWeeklies.addAll(weeklies);
            mWeeklyAdapter.notifyDataSetChanged();
            mBinding.rvWeekly.setVisibility(View.VISIBLE);
        }

        @Override
        public void onWeatherFormatTime(String time) {
            mBinding.tvTime.setText(time);
        }

        @Override
        public void onCurrentWPlace(WPlace wPlace) {
            mCurrentWPlace = wPlace;
            if (mCurrentWPlace != null) {
                mBinding.tvPlace.setText(mCurrentWPlace.getName());
            }
        }
    };


    private final WeatherSearch.WSViewInterface mWSViewInterface = new WeatherSearch.WSViewInterface() {
        @Override
        public void onWSBack() {
            viewModel.isSearchVisible.setValue(false);
        }

        @Override
        public void onWPlaceCondition(WPlace place) {
            viewModel.isSearchVisible.setValue(false);
            mWPlace = place;
            mBinding.tvPlace.setText(mWPlace.getName());
            if (place.getType() == 0) {
                isHere = true;
                mWeatherPresenter.getWeatherConditions();
            } else {
                isHere = false;
                mWeatherPresenter.getWeatherConditions(place.getLat(), place.getLnt());
            }
            viewModel.setFormatTime();
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}
