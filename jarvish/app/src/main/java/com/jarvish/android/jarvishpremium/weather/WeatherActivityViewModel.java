/**
 * Created by CupLidSheep on 21,April,2021
 */
package com.jarvish.android.jarvishpremium.weather;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class WeatherActivityViewModel extends ViewModel {
    private final static String TAG = "WeatherViewModel";
    public MutableLiveData<Boolean> isSearchVisible = new MutableLiveData<>();
    public MutableLiveData<String> today = new MutableLiveData<>();

    public void setFormatTime() {
        String format = "EE, MMMM d";
        Locale locale = Locale.getDefault();
        if (locale.getLanguage().equals("zh")) {
            if (locale.getCountry().equals("TW")) {
                format =  "EE, M月d日";
            }
        }

        try{
            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());
            Log.d(TAG, "setFormatTime1");
            today.setValue(formatter.format(new Date()));
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "setFormatTime2");
            today.setValue("");
        }
    }


}
