package com.jarvish.android.jarvishpremium.weather;

import com.jarvish.android.jarvishpremium.weather.model.Current;
import com.jarvish.android.jarvishpremium.weather.model.Hourly;
import com.jarvish.android.jarvishpremium.weather.model.WPlace;
import com.jarvish.android.jarvishpremium.weather.model.Weekly;

import java.util.ArrayList;

interface WeatherContract {
    void onCurrentCondition(Current current);
    void onHourlyCondition(ArrayList<Hourly> hourlies);
    void onWeeklyCondition(ArrayList<Weekly> weeklies);
    void onCurrentWPlace(WPlace wPlace);
    void onWeatherFormatTime(String time);
}
