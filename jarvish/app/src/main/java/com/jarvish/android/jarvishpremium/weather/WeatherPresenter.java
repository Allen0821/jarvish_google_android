/*
 * Created by CupLidSheep on 08,September,2020
 */
package com.jarvish.android.jarvishpremium.weather;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;


import com.jarvish.android.jarvishpremium.JLocation;
import com.jarvish.android.jarvishpremium.weather.core.Weather;
import com.jarvish.android.jarvishpremium.weather.model.Current;
import com.jarvish.android.jarvishpremium.weather.model.Hourly;
import com.jarvish.android.jarvishpremium.weather.model.WPlace;
import com.jarvish.android.jarvishpremium.weather.model.Weekly;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class WeatherPresenter {
    private static final String TAG = "WeatherPresenter";
    private final Context mContext;
    private final WeatherContract mWeatherContract;

    public WeatherPresenter(Context context, WeatherContract weatherContract) {
        this.mContext = context;
        this.mWeatherContract = weatherContract;
    }


    public void getWeatherConditions() {
        JLocation.getLastLocation(mContext, new JLocation.Call() {
            @Override
            public void onCompleted(Location location) {
                if (location != null) {
                    getCurrentWPlace(location.getLatitude(), location.getLongitude());
                    getCurrentCondition(location.getLatitude(), location.getLongitude());
                    getHourlyConditions(location.getLatitude(), location.getLongitude());
                    getWeeklyConditions(location.getLatitude(), location.getLongitude());
                }
            }

            @Override
            public void onError() {

            }
        });
    }


    public void getWeatherConditions(double lat, double lnt) {
        getCurrentCondition(lat, lnt);
        getHourlyConditions(lat, lnt);
        getWeeklyConditions(lat, lnt);
    }


    private void getCurrentCondition(double lat, double lnt) {
        Weather.getInstance().getCurrentConditions(lat, lnt, new Weather.OnCurrentConditions() {
            @Override
            public void onObservation(Current current) {
                if (mWeatherContract != null) {
                    mWeatherContract.onCurrentCondition(current);
                }
            }

            @Override
            public void onFailure(String error) {
                Log.e(TAG, "current observation error: " + error);
            }
        });
    }


    private void getHourlyConditions(double lat, double lnt) {
        Weather.getInstance().getHourlyConditions(lat, lnt,
                new Weather.OnHourlyConditions() {
                    @Override
                    public void onObservation(ArrayList<Hourly> hourlies) {
                        if (mWeatherContract != null) {
                            mWeatherContract.onHourlyCondition(hourlies);
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        Log.e(TAG, "hourly observation error: " + error);
                    }
                });
    }


    private void getWeeklyConditions(double lat, double lnt) {
        Weather.getInstance().getWeeklyConditions(lat, lnt,
                new Weather.OnWeeklyConditions() {
                    @Override
                    public void onObservation(ArrayList<Weekly> weeklies) {
                        if (mWeatherContract != null) {
                            mWeatherContract.onWeeklyCondition(weeklies);
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        Log.e(TAG, "weekly observation error: " + error);
                    }
                });
    }


    public void getCurrentWPlace(double lat, double lnt) {
        String placeName = "";
        String country = "";
        WPlace wPlace;
        try {
            Geocoder geoCoder = new Geocoder(mContext);
            List<Address> matches = geoCoder.getFromLocation(lat, lnt, 1);
            Address bestMatch = (matches.isEmpty() ? null : matches.get(0));
            if (bestMatch != null) {
                if (bestMatch.getSubLocality() != null) {
                    placeName = bestMatch.getSubLocality();
                } else if (bestMatch.getLocality() != null) {
                    placeName = bestMatch.getLocality();
                } else if (bestMatch.getSubAdminArea() != null) {
                    placeName = bestMatch.getSubAdminArea();
                } else if (bestMatch.getAdminArea() != null) {
                    placeName = bestMatch.getAdminArea();
                }

                if (bestMatch.getCountryName() != null) {
                    country = bestMatch.getCountryName();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        wPlace = new WPlace("current", placeName, "", country, lat, lnt, 0);
        Log.d(TAG, "location name: " + wPlace.getName());
        if (mWeatherContract != null) {
            mWeatherContract.onCurrentWPlace(wPlace);
        }
    }

}
