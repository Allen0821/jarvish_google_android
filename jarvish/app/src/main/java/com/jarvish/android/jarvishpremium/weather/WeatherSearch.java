/**
 * Created by CupLidSheep on 08,September,2020
 */
package com.jarvish.android.jarvishpremium.weather;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AddressComponent;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.Util;
import com.jarvish.android.jarvishpremium.databinding.WeatherLocationSearchBinding;
import com.jarvish.android.jarvishpremium.weather.adapter.WPlaceAdapter;
import com.jarvish.android.jarvishpremium.weather.model.WPlace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

class WeatherSearch implements WPlaceAdapter.WPlaceAdapterListener {
    private static final String TAG = "WeatherSearch";
    private final Activity mActivity;
    private final PlacesClient mPlacesClient;
    private final WSViewInterface mWSViewInterface;

    private WPlaceAdapter mWPlaceAdapter;
    private ArrayList<WPlace> mWPlaces;
    private ArrayList<WPlace> mPrefWPlaces;

    private WeatherLocationSearchBinding mBinding;

    WeatherSearch(Activity activity, WSViewInterface wsViewInterface) {
        this.mActivity = activity;
        this.mWSViewInterface = wsViewInterface;
        Places.initialize(getApplicationContext(), mActivity.getString(R.string.place_api_key));
        mPlacesClient = Places.createClient(mActivity);
    }

    void initSearchPlace(WeatherLocationSearchBinding binding) {
        mBinding = binding;
        mPrefWPlaces = new ArrayList<>();
        mWPlaces = new ArrayList<>();
        mWPlaceAdapter = new WPlaceAdapter(mWPlaces, this);
        mBinding.rvSearch.setLayoutManager(new LinearLayoutManager(mActivity));
        mBinding.rvSearch.setHasFixedSize(true);
        mBinding.rvSearch.setAdapter(mWPlaceAdapter);
        mBinding.rvSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Util.hideSoftKeyboard(mActivity);
                return false;
            }

        });

        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                OnSearchTextChanged(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mBinding.ivBack.setOnClickListener(v -> onClickBack());
        mBinding.ivClear.setOnClickListener(v -> onClickClear() );
    }

    protected void OnSearchTextChanged(CharSequence text) {
        Log.d(TAG, "input: " + text);
        mWPlaces.clear();
        if (text.length() == 0) {
            mWPlaces.addAll(mPrefWPlaces);
            mWPlaceAdapter.notifyDataSetChanged();
        } else {
            mWPlaceAdapter.notifyDataSetChanged();
            AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
            FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                    .setSessionToken(token)
                    .setQuery(text.toString())
                    .build();
            mPlacesClient.findAutocompletePredictions(request).addOnSuccessListener(findAutocompletePredictionsResponse -> {
                for (AutocompletePrediction prediction : findAutocompletePredictionsResponse.getAutocompletePredictions()) {
                    //Log.d(TAG, "Place xxxxx: " + prediction.getPrimaryText(null).toString());
                    fetchPlaceLatLng(prediction.getPlaceId());
                }
            }).addOnFailureListener(Throwable::printStackTrace);
        }
    }

    private void fetchPlaceLatLng(String placeId) {
        final List<Place.Field> placeFields = Arrays.asList(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG,
                Place.Field.ADDRESS,
                Place.Field.ADDRESS_COMPONENTS);
        final FetchPlaceRequest request = FetchPlaceRequest.newInstance(placeId, placeFields);
        mPlacesClient.fetchPlace(request).addOnSuccessListener((response) -> {
            Place place = response.getPlace();
            Log.i(TAG, "Place zzzzz: " + place.getAddressComponents());
            Log.i(TAG, "Place address: " + place.getAddress());

            if (place.getLatLng() != null) {
                mWPlaces.add(getWPlace(place));
                mWPlaceAdapter.notifyDataSetChanged();
            }
        }).addOnFailureListener(Throwable::printStackTrace);
    }



    private WPlace getWPlace(final Place place) {
        WPlace wPlace = new WPlace();
        wPlace.setId(place.getId());
        wPlace.setName(place.getName());
        if (place.getAddressComponents() != null) {
            List<AddressComponent> addressComponents = place.getAddressComponents().asList();
            String area1 = "";
            String area2 = "";
            String area3 = "";
            for (AddressComponent addressComponent : addressComponents) {
                if (addressComponent.getTypes().get(0).equals("administrative_area_level_1")) {
                    area1 = addressComponent.getName();
                }
                if (addressComponent.getTypes().get(0).equals("administrative_area_level_2")) {
                    area2 = addressComponent.getName();
                }
                if (addressComponent.getTypes().get(0).equals("administrative_area_level_3")) {
                    area3 = addressComponent.getName();
                }
                if (addressComponent.getTypes().get(0).equals("country")) {
                    wPlace.setCountry(addressComponent.getName());
                }
            }
            if (!area1.isEmpty()) {
                wPlace.setArea(area1);
            } else if (!area2.isEmpty()) {
                wPlace.setArea(area2);
            } else if (!area3.isEmpty()) {
                wPlace.setArea(area3);
            }
        }

        if (place.getLatLng() != null) {
            wPlace.setLat(place.getLatLng().latitude);
            wPlace.setLnt(place.getLatLng().longitude);
        }

        wPlace.setType(2);

        return wPlace;
    }



    void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = mActivity.getCurrentFocus();
        if (view == null) {
            view = new View(mActivity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void onClickBack() {
        if (mWSViewInterface != null) {
            hideView();
            mWSViewInterface.onWSBack();
        }
    }


    protected void onClickClear() {
        mBinding.etSearch.getText().clear();
    }


    @Override
    public void onSelectedWPlace(WPlace wPlace) {
        if (mWSViewInterface != null) {
            hideView();
            mWSViewInterface.onWPlaceCondition(wPlace);
            if (wPlace.getType() == 2) {
                addPreferWPlace(wPlace);
            }
        }
    }

    @Override
    public void onDeleteWPlace(WPlace wPlace) {
        mWPlaces.remove(wPlace);
        mWPlaceAdapter.notifyDataSetChanged();
        ArrayList<WPlace> wPlaces = WeatherUtil.getPreferWPlaces(mActivity);
        if (wPlaces != null) {
            for (WPlace place : wPlaces) {
                Log.d(TAG, "xxxx " + new Gson().toJson(place) + " cc " + place.getId() + " cc " + wPlace.getId());
                if (place.getId().equals(wPlace.getId())) {
                    Log.d(TAG, "xxxx ttt");
                    wPlaces.remove(place);
                    break;
                }
            }
        }
        WeatherUtil.setPreferWPlaces(mActivity, wPlaces);
    }

    public void showView(WPlace wPlace) {
        mPrefWPlaces.clear();
        mPrefWPlaces.add(0, wPlace);
        ArrayList<WPlace> wPlaces = WeatherUtil.getPreferWPlaces(mActivity);
        if (wPlaces != null) {
            mPrefWPlaces.addAll(wPlaces);
        }
        mWPlaces.clear();
        mWPlaces.addAll(mPrefWPlaces);
        mWPlaceAdapter.notifyDataSetChanged();
    }

    private void hideView() {
        hideKeyboard();
        mBinding.etSearch.getText().clear();
        mWPlaces.clear();
        mPrefWPlaces.clear();
        mWPlaceAdapter.notifyDataSetChanged();
    }



    private void addPreferWPlace(WPlace wPlace) {
        wPlace.setType(1);
        ArrayList<WPlace> wPlaces = WeatherUtil.getPreferWPlaces(mActivity);
        if (wPlaces == null) {
            wPlaces = new ArrayList<>();
            wPlaces.add(wPlace);
        } else {
            wPlaces.add(wPlace);
        }
        WeatherUtil.setPreferWPlaces(mActivity, wPlaces);
    }

    interface WSViewInterface {
        void onWSBack();
        void onWPlaceCondition(WPlace place);
    }
}
