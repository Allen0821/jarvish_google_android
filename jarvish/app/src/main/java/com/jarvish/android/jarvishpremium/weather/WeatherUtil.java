package com.jarvish.android.jarvishpremium.weather;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;

import androidx.core.content.res.ResourcesCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.weather.model.WPlace;

import java.util.ArrayList;

public class WeatherUtil {

    public static Drawable getWeatherIcon(Context context, int code) {
        return ResourcesCompat.getDrawable(context.getResources(),
                context.getResources()
                .getIdentifier("ic_w" + code, "drawable", context.getPackageName()),
                null);
    }


    public static Drawable getWeatherBackgroundDrawable(Context context, String dayIndex, int code) {
        String name;
        if (dayIndex.equals("N")) {
            name = "night_";
        } else {
            name = "day_";
        }

        if ((code >= 3 && code <= 12) ||
                code == 35 ||
                (code >= 37 && code <= 40) ||
                code == 45 ||
                code == 47) {
            name = name + "rain";
        } else if ((code >= 23 && code <= 30)){
            name = name + "cloud";
        } else if ((code >= 31 && code <= 34) ||
                code == 36 ||
                code == 44){
            name = name + "sun";
        } else {
            name = name + "others";
        }

        return ResourcesCompat.getDrawable(context.getResources(), context.getResources()
                .getIdentifier(name, "drawable", context.getPackageName()), null);
    }


    public static String getWindDirectionDes(Context context, String wdir) {
        if (wdir == null) {
            return context.getString(R.string.weather_wdir_var);
        }
        if (wdir.equals("N")) {
            return context.getString(R.string.weather_wdir_n);
        } else if (wdir.equals("NNE")) {
            return context.getString(R.string.weather_wdir_nne);
        } else if (wdir.equals("NE")) {
            return context.getString(R.string.weather_wdir_ne);
        } else if (wdir.equals("ENE")) {
            return context.getString(R.string.weather_wdir_ene);
        } else if (wdir.equals("E")) {
            return context.getString(R.string.weather_wdir_e);
        } else if (wdir.equals("ESE")) {
            return context.getString(R.string.weather_wdir_ese);
        } else if (wdir.equals("SE")) {
            return context.getString(R.string.weather_wdir_se);
        } else if (wdir.equals("SSE")) {
            return context.getString(R.string.weather_wdir_sse);
        } else if (wdir.equals("S")) {
            return context.getString(R.string.weather_wdir_s);
        } else if (wdir.equals("SSW")) {
            return context.getString(R.string.weather_wdir_ssw);
        } else if (wdir.equals("SW")) {
            return context.getString(R.string.weather_wdir_sw);
        } else if (wdir.equals("WSW")) {
            return context.getString(R.string.weather_wdir_wsw);
        } else if (wdir.equals("W")) {
            return context.getString(R.string.weather_wdir_w);
        } else if (wdir.equals("WNW")) {
            return context.getString(R.string.weather_wdir_wnw);
        } else if (wdir.equals("NW")) {
            return context.getString(R.string.weather_wdir_nw);
        } else if (wdir.equals("NNW")) {
            return context.getString(R.string.weather_wdir_nnw);
        } else if (wdir.equals("CALM")) {
            return context.getString(R.string.weather_wdir_calm);
        } else {
            return context.getString(R.string.weather_wdir_var);
        }
    }


    static void setPreferWPlaces(Context context, ArrayList<WPlace> wPlaces) {
        String wplaces = new Gson().toJson(wPlaces);
        context.getSharedPreferences("weather_prefer_info", Context.MODE_PRIVATE)
                .edit()
                .putString("prefer_wplaces", wplaces)
                .apply();
    }


    static ArrayList<WPlace> getPreferWPlaces(Context context) {
        String wplaces = context.getSharedPreferences("weather_prefer_info", Context.MODE_PRIVATE)
                .getString("prefer_wplaces", "");
        if (wplaces == null || wplaces.isEmpty()) {
            return null;
        } else {
            return new Gson().fromJson(wplaces, new TypeToken<ArrayList<WPlace>>(){}.getType());
        }
    }
}
