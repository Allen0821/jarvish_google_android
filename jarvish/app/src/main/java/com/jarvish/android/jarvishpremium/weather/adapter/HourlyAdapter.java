package com.jarvish.android.jarvishpremium.weather.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.HourlyViewBinding;
import com.jarvish.android.jarvishpremium.weather.WeatherUtil;
import com.jarvish.android.jarvishpremium.weather.model.Hourly;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class HourlyAdapter extends RecyclerView.Adapter<HourlyAdapter.HourlyObservationViewHolder> {
    //private static final String TAG = "HourlyObservationAdapter";

    private final ArrayList<Hourly> mHourlies;

    public HourlyAdapter(ArrayList<Hourly> hourlies) {
        this.mHourlies = hourlies;
    }

    @NonNull
    @Override
    public HourlyObservationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        HourlyViewBinding binding =
                HourlyViewBinding.inflate(layoutInflater, parent, false);
        return new HourlyAdapter.HourlyObservationViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull HourlyObservationViewHolder holder, int position) {
        holder.bind(this.mHourlies.get(position));
    }


    @Override
    public int getItemCount() {
        return this.mHourlies.size();
    }

    static class HourlyObservationViewHolder extends RecyclerView.ViewHolder {
        private final HourlyViewBinding mBinding;

        HourlyObservationViewHolder(HourlyViewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }


        void bind(Hourly hourly) {
            mBinding.tvTime.setText(getTimeFormatString(hourly.getValidTimeLocal()));
            mBinding.ivIcon.setImageDrawable(WeatherUtil.getWeatherIcon(itemView.getContext(), hourly.getIconCode()));
            String precipChance = hourly.getPrecipChance() + "%";
            mBinding.tvPrecipChance.setText(precipChance);
            String temperature = hourly.getTemperature() + "°";
            mBinding.tvTemperature.setText(temperature);
        }


        private String getTimeFormatString(String time) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZ", Locale.getDefault());
            try {
                Date date = dateFormat.parse(time);
                DateFormat out = new SimpleDateFormat("HH:mm", Locale.getDefault());
                return out.format(date);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }
}
