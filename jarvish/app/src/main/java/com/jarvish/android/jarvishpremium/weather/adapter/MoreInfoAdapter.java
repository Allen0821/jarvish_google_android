package com.jarvish.android.jarvishpremium.weather.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.databinding.WeatherMoreInfoViewBinding;
import com.jarvish.android.jarvishpremium.weather.model.MoreInfo;

import java.util.ArrayList;


public class MoreInfoAdapter extends RecyclerView.Adapter<MoreInfoAdapter.MoreInfoViewHolder> {


    private final ArrayList<MoreInfo> mMoreInfos;


    public MoreInfoAdapter(ArrayList<MoreInfo> moreInfos) {
        this.mMoreInfos = moreInfos;
    }


    @NonNull
    @Override
    public MoreInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        WeatherMoreInfoViewBinding binding =
                WeatherMoreInfoViewBinding.inflate(layoutInflater, parent, false);
        return new MoreInfoAdapter.MoreInfoViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull MoreInfoViewHolder holder, int position) {
        holder.bind(this.mMoreInfos.get(position));
    }


    @Override
    public int getItemCount() {
        return this.mMoreInfos.size();
    }


    static class MoreInfoViewHolder extends RecyclerView.ViewHolder {
        private final WeatherMoreInfoViewBinding mBinding;

        MoreInfoViewHolder(WeatherMoreInfoViewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }


        void bind(MoreInfo moreInfo) {
            mBinding.tvTitle.setText(moreInfo.getTitle());
            mBinding.tvValue.setText(moreInfo.getValue());
        }
    }
}
