/*
 * Created by CupLidSheep on 08,September,2020
 */
package com.jarvish.android.jarvishpremium.weather.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jarvish.android.jarvishpremium.R;
import com.jarvish.android.jarvishpremium.databinding.WeatherLocationSearchItemBinding;
import com.jarvish.android.jarvishpremium.weather.model.WPlace;

import java.util.ArrayList;


public class WPlaceAdapter extends RecyclerView.Adapter<WPlaceAdapter.WPlaceViewHolder> {

    private final ArrayList<WPlace> mWPlaces;
    private final WPlaceAdapterListener mWPlaceAdapterListener;

    public WPlaceAdapter(ArrayList<WPlace> wplaces, WPlaceAdapterListener listener) {
        this.mWPlaces = wplaces;
        this.mWPlaceAdapterListener = listener;
    }


    @NonNull
    @Override
    public WPlaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        WeatherLocationSearchItemBinding binding =
                WeatherLocationSearchItemBinding.inflate(layoutInflater, parent, false);
        return new WPlaceAdapter.WPlaceViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull WPlaceViewHolder holder, int position) {
        holder.bind(this.mWPlaces.get(position), mWPlaceAdapterListener);
    }

    @Override
    public int getItemCount() {
        return this.mWPlaces.size();
    }

    static class WPlaceViewHolder extends RecyclerView.ViewHolder {
        private final WeatherLocationSearchItemBinding mBinding;

        public WPlaceViewHolder(@NonNull WeatherLocationSearchItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(final WPlace wPlace, WPlaceAdapterListener listener) {
            mBinding.tvPlace.setText(wPlace.getName());

            if (wPlace.getType() == 0) {
                mBinding.tvLocation.setText(R.string.weather_search_current_place);
            } else {
                String location = "";

                if (wPlace.getArea() != null && wPlace.getCountry() != null) {
                    location = wPlace.getArea() + ", " + wPlace.getCountry();
                } else if (wPlace.getArea() != null && wPlace.getCountry() == null) {
                    location = wPlace.getArea();
                } else if (wPlace.getArea() == null && wPlace.getCountry() != null) {
                    location = wPlace.getCountry();
                }
                mBinding.tvLocation.setText(location);
            }

            if (wPlace.getType() == 0) {
                mBinding.ivLocation.setImageDrawable(ContextCompat.getDrawable(
                        mBinding.ivLocation.getContext(), R.drawable.ic_my_location_black_36dp));
            } else {
                mBinding.ivLocation.setImageDrawable(ContextCompat.getDrawable(
                        mBinding.ivLocation.getContext(), R.drawable.ic_location_48));
            }

            if (wPlace.getType() == 1) {
                mBinding.ivDelete.setVisibility(View.VISIBLE);
            } else {
                mBinding.ivDelete.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(view -> listener.onSelectedWPlace(wPlace));
            mBinding.ivDelete.setOnClickListener(view -> listener.onDeleteWPlace(wPlace));
        }
    }

    public interface WPlaceAdapterListener {
        void onSelectedWPlace(WPlace wPlace);
        void onDeleteWPlace(WPlace wPlace);
    }
}
