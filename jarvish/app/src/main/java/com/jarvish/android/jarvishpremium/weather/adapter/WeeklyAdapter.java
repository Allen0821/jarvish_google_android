package com.jarvish.android.jarvishpremium.weather.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.jarvish.android.jarvishpremium.databinding.WeeklyViewBinding;
import com.jarvish.android.jarvishpremium.weather.WeatherUtil;
import com.jarvish.android.jarvishpremium.weather.model.Weekly;

import java.util.ArrayList;


public class WeeklyAdapter extends RecyclerView.Adapter<WeeklyAdapter.WeeklyViewHolder> {

    private final ArrayList<Weekly> mWeeklies;

    public WeeklyAdapter(ArrayList<Weekly> weeklies) {
        this.mWeeklies = weeklies;
    }


    @NonNull
    @Override
    public WeeklyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        WeeklyViewBinding binding =
                WeeklyViewBinding.inflate(layoutInflater, parent, false);
        return new WeeklyAdapter.WeeklyViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull WeeklyViewHolder holder, int position) {
        holder.bind(this.mWeeklies.get(position));
    }


    @Override
    public int getItemCount() {
        return this.mWeeklies.size();
    }

    static class WeeklyViewHolder extends RecyclerView.ViewHolder {
        private final WeeklyViewBinding mBinding;

        WeeklyViewHolder(WeeklyViewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(Weekly weekly) {
            mBinding.tvDay.setText(weekly.getDayOfWeek());
            mBinding.tvWxPhraseLong.setText(weekly.getWeeklyPart().get(0).getWxPhraseLong());
            String precipChance = weekly.getWeeklyPart().get(0).getPrecipChance() + "%";
            mBinding.tvPrecipChance.setText(precipChance);
            String temperatureMax = weekly.getWeeklyPart().get(0).getTemperature() + "°";
            mBinding.tvTemperatureMax.setText(temperatureMax);
            String temperatureMin = weekly.getWeeklyPart().get(1).getTemperature() + "°";
            mBinding.tvTemperatureMin.setText(temperatureMin);
            mBinding.ivIcon.setImageDrawable(WeatherUtil.getWeatherIcon(
                    itemView.getContext(), weekly.getWeeklyPart().get(0).getIconCode()));
        }
    }
}
