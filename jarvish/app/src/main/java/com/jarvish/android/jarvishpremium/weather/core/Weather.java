package com.jarvish.android.jarvishpremium.weather.core;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.jarvish.android.jarvishpremium.weather.model.Hourly;
import com.jarvish.android.jarvishpremium.weather.model.Current;
import com.jarvish.android.jarvishpremium.weather.model.Weekly;
import com.jarvish.android.jarvishpremium.weather.model.WeeklyPart;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Weather {
    private static final String TAG = "Weather";
    private static volatile Weather mWeather;
    private static final String WEATHER_URL = "https://api.weather.com/";
    private static final String PREF_UNITS_KEY = "weatherzzzaunits";

    private String mWeatherApiKey;

    private final WeatherAPI mWeatherAPI;

    private String units = "m";
    private String language = "en-US";
    private final String format = "json";


    private Weather() {
        if (mWeather != null){
            throw new RuntimeException("Use getInstance() method to get Weather instance.");
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WEATHER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        mWeatherAPI = retrofit.create(WeatherAPI.class);
    }


    public static Weather getInstance() {
        if (mWeather == null) {
            synchronized (Weather.class) {
                if (mWeather == null) {
                    mWeather = new Weather();
                }
            }
        }
        return mWeather;
    }



    public void initialize(@NonNull Context var0, @NonNull String apiKey) {
        units = PreferenceManager
                .getDefaultSharedPreferences(var0.getApplicationContext())
                .getString(PREF_UNITS_KEY, "m");
        setLanguage();
        mWeatherApiKey = apiKey;
    }


    public void setUnits(@NonNull Context var0, int index) {
        if (index == 0) {
            this.units = "m";
        } else {
            this.units = "e";
        }
        Log.d(TAG, "units: " + this.units);
        PreferenceManager.getDefaultSharedPreferences(var0.getApplicationContext())
                .edit()
                .putString(PREF_UNITS_KEY, this.units)
                .apply();
    }



    private void setLanguage() {
        Locale locale = Locale.getDefault();
        if (locale.getLanguage().equals("zh")) {
            if (locale.getCountry().equals("TW")) {
                this.language = "zh-TW";
                return;
            }
        }
        this.language = "en-US";
    }


    public void getCurrentConditions(double latitude, double longitude, OnCurrentConditions onCurrentConditions) {
        Log.d(TAG, "getCurrentConditions units: " + this.units);
        mWeatherAPI.getCurrentConditions(latitude, longitude, units, language, mWeatherApiKey)
                .enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.d(TAG, "getCurrentConditions response: " + response);
                if (response.code() != 200) {
                    onCurrentConditions.onFailure("response code " + response.code());
                    return;
                }

                if (response.body() == null) {
                    onCurrentConditions.onFailure("response body empty");
                    return;
                }

                JsonObject object = response.body().getAsJsonObject("observation");
                if (object != null) {
                    Current current = new Gson().fromJson(object, Current.class);
                    Log.d(TAG, "observation: " + current);
                    onCurrentConditions.onObservation(current);
                    return;
                }

                onCurrentConditions.onFailure("observation empty");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                onCurrentConditions.onFailure(t.getMessage());
            }
        });
    }


    public void getHourlyConditions(double latitude, double longitude, OnHourlyConditions onHourlyConditions) {
        String geocode = latitude + "," + longitude;
        mWeatherAPI.get2DaysConditions(geocode, format, units, language, mWeatherApiKey)
                .enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.d(TAG, "getHourlyConditions response: " + response);
                if (response.code() != 200) {
                    onHourlyConditions.onFailure("response code " + response.code());
                    return;
                }

                if (response.body() == null) {
                    onHourlyConditions.onFailure("response body empty");
                    return;
                }

                ArrayList<Hourly> hourlies = parse2DaysConditions(response.body());
                onHourlyConditions.onObservation(hourlies);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                onHourlyConditions.onFailure(t.getMessage());
            }
        });

    }


    private ArrayList<Hourly> parse2DaysConditions(JsonObject object) {
        ArrayList<Hourly> hourlies = new ArrayList<>();

        JsonArray validTimeLocalJson = object.getAsJsonArray("validTimeLocal");
        ArrayList<String> validTimeLocal = new Gson().fromJson(validTimeLocalJson, new TypeToken<ArrayList<String>>(){}.getType());
        JsonArray precipChanceJson = object.getAsJsonArray("precipChance");
        ArrayList<Integer> precipChance = new Gson().fromJson(precipChanceJson, new TypeToken<ArrayList<Integer>>(){}.getType());
        JsonArray iconCodeJson = object.getAsJsonArray("iconCode");
        ArrayList<Integer> iconCode = new Gson().fromJson(iconCodeJson, new TypeToken<ArrayList<Integer>>(){}.getType());
        JsonArray temperatureJson = object.getAsJsonArray("temperature");
        ArrayList<Integer> temperature = new Gson().fromJson(temperatureJson, new TypeToken<ArrayList<Integer>>(){}.getType());
        JsonArray expirationTimeUtcJson = object.getAsJsonArray("expirationTimeUtc");
        ArrayList<Long> expirationTimeUtc = new Gson().fromJson(expirationTimeUtcJson, new TypeToken<ArrayList<Long>>(){}.getType());


        if (validTimeLocal != null) {
            for (int i = 0; i < 24; i++) {
                Hourly hourly = new Hourly();
                hourly.setValidTimeLocal(validTimeLocal.get(i));
                hourly.setPrecipChance(precipChance.get(i));
                hourly.setIconCode(iconCode.get(i));
                hourly.setTemperature(temperature.get(i));
                hourly.setExpirationTimeUtc(expirationTimeUtc.get(i));
                hourlies.add(hourly);
            }
        }

        return hourlies;
    }


    public void getWeeklyConditions(double latitude, double longitude, OnWeeklyConditions onWeeklyConditions) {
        String geocode = latitude + "," + longitude;
        mWeatherAPI.get7daysConditions(geocode, format, units, language, mWeatherApiKey)
                .enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.d(TAG, "getWeeklyConditions response: " + response);
                if (response.code() != 200) {
                    onWeeklyConditions.onFailure("response code " + response.code());
                    return;
                }

                if (response.body() == null) {
                    onWeeklyConditions.onFailure("response body empty");
                    return;
                }

                onWeeklyConditions.onObservation(parse7daysConditions(response.body()));
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                onWeeklyConditions.onFailure(t.getMessage());
            }
        });
    }


    private ArrayList<Weekly> parse7daysConditions(JsonObject object) {
        ArrayList<Weekly> weeklies = new ArrayList<>();
        Log.d(TAG, "parse7daysConditions: " + object.toString());

        JsonArray dayOfWeekArray = object.getAsJsonArray("dayOfWeek");
        ArrayList<String> dayOfWeek = new Gson().fromJson(dayOfWeekArray,
                new TypeToken<ArrayList<String>>(){}.getType());
        JsonArray expirationTimeUtcArray = object.getAsJsonArray("expirationTimeUtc");
        ArrayList<Long> expirationTimeUtc = new Gson().fromJson(expirationTimeUtcArray,
                new TypeToken<ArrayList<Long>>(){}.getType());
        JsonArray temperatureMaxArray = object.getAsJsonArray("temperatureMax");
        ArrayList<Integer> temperatureMax = new Gson().fromJson(temperatureMaxArray,
                new TypeToken<ArrayList<Integer>>(){}.getType());
        Log.d(TAG, "temperatureMax: " + temperatureMax);
        JsonArray temperatureMinArray = object.getAsJsonArray("temperatureMin");
        ArrayList<Integer> temperatureMin = new Gson().fromJson(temperatureMinArray,
                new TypeToken<ArrayList<Integer>>(){}.getType());
        Log.d(TAG, "temperatureMin: " + temperatureMin);


        JsonArray jsonArray = object.getAsJsonArray("daypart");
        ArrayList<JsonObject> daypartArray = new Gson().fromJson(jsonArray, new TypeToken<ArrayList<JsonObject>>(){}.getType());

        JsonObject daypart = daypartArray.get(0);

        JsonArray daypartNameJson = daypart.getAsJsonArray("daypartName");
        ArrayList<String> daypartName = new Gson().fromJson(daypartNameJson, new TypeToken<ArrayList<String>>(){}.getType());
        Log.d(TAG, "daypartNameJson: " + daypartNameJson);

        JsonArray iconCodeJson = daypart.getAsJsonArray("iconCode");
        ArrayList<Integer> iconCode = new Gson().fromJson(iconCodeJson, new TypeToken<ArrayList<Integer>>(){}.getType());
        Log.d(TAG, "iconCodeJson: " + iconCode);

        JsonArray temperatureJson = daypart.getAsJsonArray("temperature");
        ArrayList<Integer> temperature = new Gson().fromJson(temperatureJson, new TypeToken<ArrayList<Integer>>(){}.getType());

        JsonArray precipChanceJson = daypart.getAsJsonArray("precipChance");
        ArrayList<Integer> precipChance = new Gson().fromJson(precipChanceJson, new TypeToken<ArrayList<Integer>>(){}.getType());

        JsonArray wxPhraseLongJson = daypart.getAsJsonArray("wxPhraseLong");
        ArrayList<String> wxPhraseLong = new Gson().fromJson(wxPhraseLongJson, new TypeToken<ArrayList<String>>(){}.getType());
        Log.d(TAG, "wxPhraseLongJson: " + wxPhraseLongJson);


        if (dayOfWeek != null) {
            for (int i = 1; i < dayOfWeek.size(); i++) {
                Weekly weekly = new Weekly();
                weekly.setDayOfWeek(dayOfWeek.get(i));
                weekly.setExpirationTimeUtc(expirationTimeUtc.get(i));
                weekly.setTemperatureMax(temperatureMax.get(i));
                weekly.setTemperatureMin(temperatureMin.get(i));
                ArrayList<WeeklyPart> weeklyParts = new ArrayList<>();

                WeeklyPart day = new WeeklyPart();
                day.setDaypartName(daypartName.get(i*2));
                day.setIconCode(iconCode.get(i*2));
                day.setPrecipChance(precipChance.get(i*2));
                day.setTemperature(temperature.get(i*2));
                day.setWxPhraseLong(wxPhraseLong.get(i*2));
                weeklyParts.add(day);

                WeeklyPart night = new WeeklyPart();
                night.setDaypartName(daypartName.get(i*2 + 1));
                night.setIconCode(iconCode.get(i*2 + 1));
                night.setPrecipChance(precipChance.get(i*2 + 1));
                night.setTemperature(temperature.get(i*2 + 1));
                night.setWxPhraseLong(wxPhraseLong.get(i*2 + 1));
                weeklyParts.add(night);

                weekly.setWeeklyPart(weeklyParts);

                weeklies.add(weekly);
            }
        }

        return weeklies;
    }



    public String getTemperatureUnits() {
        if (this.units.equals("m")) {
            return "°C";
        } else {
            return "°F";
        }
    }


    public String getDistanceUnits() {
        if (this.units.equals("m")) {
            return "km";
        } else {
            return "mi";
        }
    }


    public String getSpeedUnits() {
        if (this.units.equals("m")) {
            return "km/hr";
        } else {
            return "mi/hr";
        }
    }


    public interface OnCurrentConditions {
        void onObservation(Current current);
        void onFailure(String error);
    }

    public interface OnHourlyConditions {
        void onObservation(ArrayList<Hourly> hourlies);
        void onFailure(String error);
    }

    public interface OnWeeklyConditions {
        void onObservation(ArrayList<Weekly> weeklies);
        void onFailure(String error);
    }
}
