package com.jarvish.android.jarvishpremium.weather.core;

import com.google.gson.JsonObject;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherAPI {

    @GET("v1/geocode/{latitude}/{longitude}/observations.json")
    Call<JsonObject> getCurrentConditions(
            @Path("latitude") double latitude,
            @Path("longitude") double longitude,
            @Query("units") String units,
            @Query("language") String language,
            @Query("apiKey") String apiKey);


    @GET("v3/wx/forecast/hourly/2day")
    Call<JsonObject> get2DaysConditions(
            @Query("geocode") String geocode,
            @Query("format") String format,
            @Query("units") String units,
            @Query("language") String language,
            @Query("apiKey") String apiKey);


    @GET("v3/wx/forecast/daily/7day")
    Call<JsonObject> get7daysConditions(
            @Query("geocode") String geocode,
            @Query("format") String format,
            @Query("units") String units,
            @Query("language") String language,
            @Query("apiKey") String apiKey);
}
