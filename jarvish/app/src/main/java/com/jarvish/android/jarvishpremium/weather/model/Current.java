package com.jarvish.android.jarvishpremium.weather.model;

import com.google.gson.annotations.SerializedName;

public class Current {

    @SerializedName("blunt_phrase")
    private String bluntPhrase;

    @SerializedName("class")
    private String identifier;

    @SerializedName("clds")
    private String clds;

    @SerializedName("day_ind")
    private String dayInd;

    @SerializedName("dewpt")
    private int dewpt;

    @SerializedName("expire_time_gmt")
    private long expireTimeGmt;

    @SerializedName("feels_like")
    private int feelsLike;

    @SerializedName("gust")
    private int gust;

    @SerializedName("heat_index")
    private int heatIndex;

    @SerializedName("icon_extd")
    private int iconExtd;

    @SerializedName("key")
    private String key;

    @SerializedName("max_temp")
    private int maxTemp;

    @SerializedName("min_temp")
    private int minTemp;

    @SerializedName("obs_id")
    private String obsID;

    @SerializedName("obs_name")
    private String obsName;

    @SerializedName("precip_hrly")
    private double precipHrly;

    @SerializedName("precip_total")
    private double precipTotal;

    @SerializedName("pressure")
    private double pressure;

    @SerializedName("pressure_desc")
    private String pressureDesc;

    @SerializedName("pressure_tend")
    private int pressureTend;

    @SerializedName("qualifier")
    private String qualifier;

    @SerializedName("qualifier_svrty")
    private String qualifierSvrty;

    @SerializedName("rh")
    private int rh;

    @SerializedName("snow_hrly")
    private int snowHrly;

    @SerializedName("temp")
    private int temp;

    @SerializedName("terse_phrase")
    private String terse_phrase;

    @SerializedName("uv_desc")
    private String uvDesc;

    @SerializedName("uv_index")
    private int uvIndex;

    @SerializedName("valid_time_gmt")
    private long validTimeGmt;

    @SerializedName("vis")
    private double vis;

    @SerializedName("wc")
    private int wc;

    @SerializedName("wdir")
    private int wdir;

    @SerializedName("wdir_cardinal")
    private String wdirCardinal;

    @SerializedName("wspd")
    private int wspd;

    @SerializedName("wx_icon")
    private int wxIcon;

    @SerializedName("wx_phrase")
    private String wxPhrase;

    @SerializedName("water_temp")
    private int waterTemp;

    @SerializedName("primary_wave_period")
    private int primaryWavePeriod;

    @SerializedName("primary_wave_height")
    private double primaryWaveHeight;

    @SerializedName("primary_swell_period")
    private int primarySwellPeriod;

    @SerializedName("primary_swell_height")
    private double primarySwellHeight;

    @SerializedName("primary_swell_direction")
    private int primarySwellDirection;

    @SerializedName("secondary_swell_period")
    private int secondarySwellPeriod;

    @SerializedName("secondary_swell_height")
    private double secondarySwellHeight;

    @SerializedName("secondary_swell_direction")
    private int secondarySwellDirection;



    public String getBluntPhrase() {
        return bluntPhrase;
    }

    public void setBluntPhrase(String bluntPhrase) {
        this.bluntPhrase = bluntPhrase;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getClds() {
        return clds;
    }

    public void setClds(String clds) {
        this.clds = clds;
    }

    public String getDayInd() {
        return dayInd;
    }

    public void setDayInd(String dayInd) {
        this.dayInd = dayInd;
    }

    public int getDewpt() {
        return dewpt;
    }

    public void setDewpt(int dewpt) {
        this.dewpt = dewpt;
    }

    public long getExpireTimeGmt() {
        return expireTimeGmt;
    }

    public void setExpireTimeGmt(long expireTimeGmt) {
        this.expireTimeGmt = expireTimeGmt;
    }

    public int getFeelsLike() {
        return feelsLike;
    }

    public void setFeelsLike(int feelsLike) {
        this.feelsLike = feelsLike;
    }

    public int getGust() {
        return gust;
    }

    public void setGust(int gust) {
        this.gust = gust;
    }

    public int getHeatIndex() {
        return heatIndex;
    }

    public void setHeatIndex(int heatIndex) {
        this.heatIndex = heatIndex;
    }

    public int getIconExtd() {
        return iconExtd;
    }

    public void setIconExtd(int iconExtd) {
        this.iconExtd = iconExtd;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(int maxTemp) {
        this.maxTemp = maxTemp;
    }

    public int getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(int minTemp) {
        this.minTemp = minTemp;
    }

    public String getObsID() {
        return obsID;
    }

    public void setObsID(String obsID) {
        this.obsID = obsID;
    }

    public String getObsName() {
        return obsName;
    }

    public void setObsName(String obsName) {
        this.obsName = obsName;
    }

    public double getPrecipHrly() {
        return precipHrly;
    }

    public void setPrecipHrly(double precipHrly) {
        this.precipHrly = precipHrly;
    }

    public double getPrecipTotal() {
        return precipTotal;
    }

    public void setPrecipTotal(double precipTotal) {
        this.precipTotal = precipTotal;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public String getPressureDesc() {
        return pressureDesc;
    }

    public void setPressureDesc(String pressureDesc) {
        this.pressureDesc = pressureDesc;
    }

    public int getPressureTend() {
        return pressureTend;
    }

    public void setPressureTend(int pressureTend) {
        this.pressureTend = pressureTend;
    }

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }

    public String getQualifierSvrty() {
        return qualifierSvrty;
    }

    public void setQualifierSvrty(String qualifierSvrty) {
        this.qualifierSvrty = qualifierSvrty;
    }

    public int getRh() {
        return rh;
    }

    public void setRh(int rh) {
        this.rh = rh;
    }

    public int getSnowHrly() {
        return snowHrly;
    }

    public void setSnowHrly(int snowHrly) {
        this.snowHrly = snowHrly;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public String getTerse_phrase() {
        return terse_phrase;
    }

    public void setTerse_phrase(String terse_phrase) {
        this.terse_phrase = terse_phrase;
    }

    public String getUvDesc() {
        return uvDesc;
    }

    public void setUvDesc(String uvDesc) {
        this.uvDesc = uvDesc;
    }

    public int getUvIndex() {
        return uvIndex;
    }

    public void setUvIndex(int uvIndex) {
        this.uvIndex = uvIndex;
    }

    public long getValidTimeGmt() {
        return validTimeGmt;
    }

    public void setValidTimeGmt(long validTimeGmt) {
        this.validTimeGmt = validTimeGmt;
    }

    public double getVis() {
        return vis;
    }

    public void setVis(double vis) {
        this.vis = vis;
    }

    public int getWc() {
        return wc;
    }

    public void setWc(int wc) {
        this.wc = wc;
    }

    public int getWdir() {
        return wdir;
    }

    public void setWdir(int wdir) {
        this.wdir = wdir;
    }

    public String getWdirCardinal() {
        return wdirCardinal;
    }

    public void setWdirCardinal(String wdirCardinal) {
        this.wdirCardinal = wdirCardinal;
    }

    public int getWspd() {
        return wspd;
    }

    public void setWspd(int wspd) {
        this.wspd = wspd;
    }

    public int getWxIcon() {
        return wxIcon;
    }

    public void setWxIcon(int wxIcon) {
        this.wxIcon = wxIcon;
    }

    public String getWxPhrase() {
        return wxPhrase;
    }

    public void setWxPhrase(String wxPhrase) {
        this.wxPhrase = wxPhrase;
    }

    public int getWaterTemp() {
        return waterTemp;
    }

    public void setWaterTemp(int waterTemp) {
        this.waterTemp = waterTemp;
    }

    public int getPrimaryWavePeriod() {
        return primaryWavePeriod;
    }

    public void setPrimaryWavePeriod(int primaryWavePeriod) {
        this.primaryWavePeriod = primaryWavePeriod;
    }

    public double getPrimaryWaveHeight() {
        return primaryWaveHeight;
    }

    public void setPrimaryWaveHeight(double primaryWaveHeight) {
        this.primaryWaveHeight = primaryWaveHeight;
    }

    public int getPrimarySwellPeriod() {
        return primarySwellPeriod;
    }

    public void setPrimarySwellPeriod(int primarySwellPeriod) {
        this.primarySwellPeriod = primarySwellPeriod;
    }

    public double getPrimarySwellHeight() {
        return primarySwellHeight;
    }

    public void setPrimarySwellHeight(double primarySwellHeight) {
        this.primarySwellHeight = primarySwellHeight;
    }

    public int getPrimarySwellDirection() {
        return primarySwellDirection;
    }

    public void setPrimarySwellDirection(int primarySwellDirection) {
        this.primarySwellDirection = primarySwellDirection;
    }

    public int getSecondarySwellPeriod() {
        return secondarySwellPeriod;
    }

    public void setSecondarySwellPeriod(int secondarySwellPeriod) {
        this.secondarySwellPeriod = secondarySwellPeriod;
    }

    public double getSecondarySwellHeight() {
        return secondarySwellHeight;
    }

    public void setSecondarySwellHeight(double secondarySwellHeight) {
        this.secondarySwellHeight = secondarySwellHeight;
    }

    public int getSecondarySwellDirection() {
        return secondarySwellDirection;
    }

    public void setSecondarySwellDirection(int secondarySwellDirection) {
        this.secondarySwellDirection = secondarySwellDirection;
    }
}
