package com.jarvish.android.jarvishpremium.weather.model;

public class Hourly {
    private int iconCode;
    private int precipChance;
    private int temperature;
    private String validTimeLocal;
    private long expirationTimeUtc;

    public int getIconCode() {
        return iconCode;
    }

    public void setIconCode(int iconCode) {
        this.iconCode = iconCode;
    }

    public int getPrecipChance() {
        return precipChance;
    }

    public void setPrecipChance(int precipChance) {
        this.precipChance = precipChance;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getValidTimeLocal() {
        return validTimeLocal;
    }

    public void setValidTimeLocal(String validTimeLocal) {
        this.validTimeLocal = validTimeLocal;
    }

    public long getExpirationTimeUtc() {
        return expirationTimeUtc;
    }

    public void setExpirationTimeUtc(long expirationTimeUtc) {
        this.expirationTimeUtc = expirationTimeUtc;
    }
}
