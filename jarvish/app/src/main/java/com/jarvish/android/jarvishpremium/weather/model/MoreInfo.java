package com.jarvish.android.jarvishpremium.weather.model;

import android.graphics.drawable.Drawable;

public class MoreInfo {
    private String title;
    private String value;
    private Drawable drawable;


    public MoreInfo(String title, String value) {
        this.title = title;
        this.value = value;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
