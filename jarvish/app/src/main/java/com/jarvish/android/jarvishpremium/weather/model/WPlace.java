/**
 * Created by CupLidSheep on 08,September,2020
 */
package com.jarvish.android.jarvishpremium.weather.model;

public class WPlace {
    private String id;
    private String name;
    private String area;
    private String country;
    private double lat;
    private double lnt;
    private int type;

    public WPlace() {

    }

    public WPlace(String id, String name, String area, String country, double lat, double lnt, int type) {
        this.id = id;
        this.name = name;
        this.area = area;
        this.country = country;
        this.lat = lat;
        this.lnt = lnt;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLnt() {
        return lnt;
    }

    public void setLnt(double lnt) {
        this.lnt = lnt;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
