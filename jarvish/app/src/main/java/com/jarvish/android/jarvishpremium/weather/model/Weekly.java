package com.jarvish.android.jarvishpremium.weather.model;

import java.util.ArrayList;

public class Weekly {
    private String dayOfWeek;
    private long expirationTimeUtc;
    private int temperatureMax;
    private int temperatureMin;
    private ArrayList<WeeklyPart> weeklyPart;

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public long getExpirationTimeUtc() {
        return expirationTimeUtc;
    }

    public void setExpirationTimeUtc(long expirationTimeUtc) {
        this.expirationTimeUtc = expirationTimeUtc;
    }

    public int getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(int temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public int getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(int temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public ArrayList<WeeklyPart> getWeeklyPart() {
        return weeklyPart;
    }

    public void setWeeklyPart(ArrayList<WeeklyPart> weeklyPart) {
        this.weeklyPart = weeklyPart;
    }
}
