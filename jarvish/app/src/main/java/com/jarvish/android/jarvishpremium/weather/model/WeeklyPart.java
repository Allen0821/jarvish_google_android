package com.jarvish.android.jarvishpremium.weather.model;

public class WeeklyPart {
    private String daypartName;
    private String wxPhraseLong;
    private int iconCode;
    private int temperature;
    private int precipChance;


    public String getDaypartName() {
        return daypartName;
    }

    public void setDaypartName(String daypartName) {
        this.daypartName = daypartName;
    }

    public String getWxPhraseLong() {
        return wxPhraseLong;
    }

    public void setWxPhraseLong(String wxPhraseLong) {
        this.wxPhraseLong = wxPhraseLong;
    }

    public int getIconCode() {
        return iconCode;
    }

    public void setIconCode(int iconCode) {
        this.iconCode = iconCode;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getPrecipChance() {
        return precipChance;
    }

    public void setPrecipChance(int precipChance) {
        this.precipChance = precipChance;
    }
}
