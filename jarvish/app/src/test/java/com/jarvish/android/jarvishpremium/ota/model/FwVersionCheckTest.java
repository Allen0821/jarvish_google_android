package com.jarvish.android.jarvishpremium.ota.model;


import org.junit.Assert;
import org.junit.Test;


public class FwVersionCheckTest {

    @Test
    public void isValidVersionTest() {
        Assert.assertFalse(FwVersionCheck.isValidVersionName(""));
        Assert.assertFalse(FwVersionCheck.isValidVersionName("111.11.86.0.24"));
        Assert.assertFalse(FwVersionCheck.isValidVersionName("111.11.86"));
        Assert.assertFalse(FwVersionCheck.isValidVersionName("fw111.11.86.0"));
        Assert.assertFalse(FwVersionCheck.isValidVersionName("111.mcu11.86.0"));
        Assert.assertTrue(FwVersionCheck.isValidVersionName("111.11.86.0"));
    }

    @Test
    public void isNewReleaseForJarvishXTest() {
        Assert.assertTrue(FwVersionCheck
                .isNewReleaseForJarvishX("", 500));
        Assert.assertTrue(FwVersionCheck
                .isNewReleaseForJarvishX("400.100.86.1.24", 500));
        Assert.assertTrue(FwVersionCheck
                .isNewReleaseForJarvishX("j400.100.86.1.24", 500));

        Assert.assertFalse(FwVersionCheck
                .isNewReleaseForJarvishX("500.100.86.1", 500));

        Assert.assertTrue(FwVersionCheck
                .isNewReleaseForJarvishX("400.100.86.1", 500));
    }

    @Test
    public void isNewReleaseForFEVoRXTest() {
        Assert.assertTrue(FwVersionCheck
                .isNewReleaseForFEVoRX("", 600));
        Assert.assertTrue(FwVersionCheck
                .isNewReleaseForFEVoRX("500.11.86.1.24", 600));
        Assert.assertTrue(FwVersionCheck
                .isNewReleaseForFEVoRX("j500.11.86.1", 600));


        Assert.assertFalse(FwVersionCheck
                .isNewReleaseForFEVoRX("600.100.86.1", 600));
        Assert.assertFalse(FwVersionCheck
                .isNewReleaseForFEVoRX("503.100.86.1", 600));


        Assert.assertTrue(FwVersionCheck
                .isNewReleaseForFEVoRX("550.100.86.1", 600));
    }
}